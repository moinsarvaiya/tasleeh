// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/8.10.1/firebase-app.js');
importScripts(
  'https://www.gstatic.com/firebasejs/8.10.1/firebase-messaging.js'
);

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
const firebaseConfig = {
  apiKey: 'AIzaSyB1uOUM0S1Nf6NmZRSBcM7MuDvlVXRor74',
  authDomain: 'izhal-fef3a.firebaseapp.com',
  projectId: 'izhal-fef3a',
  storageBucket: 'izhal-fef3a.appspot.com',
  messagingSenderId: '50160841087',
  appId: '1:50160841087:web:242a8e431c97f300aeea4a',
  measurementId: 'G-5P0QRHQCFS',
};

firebase.initializeApp(firebaseConfig);

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();