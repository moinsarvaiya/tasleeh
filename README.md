
# izhal

A brief description of what this project does and who it's for

## Flutter and Dart Versions

Versions of Flutter and Dart.

- Flutter: ^3.16.0
- Dart: ^3.2.0

## Getting Started

To run this Flutter project, follow these steps:

    1. Clone the repository to your local machine.
    2. Install Flutter and Dart SDK on your system.
    3. Run `flutter pub get` in the project root directory to fetch dependencies.
    4. Connect a physical device or start an emulator.
    5. Run `flutter run` to launch the application.

## Folder structure

    ├─── web
    ├─── lib
          ├─── api
          ├─── commons
          ├─── core
                   └─── app_preference
                   └─── auth
                   └─── constant 
                   └─── extensions 
                   └─── notification
                   └─── permission
                   └─── routes 
          ├─── custom_classes
          ├─── l10n
          ├─── models
          ├─── pages
          ├─── utils
          ├─── firebase_options.dart
          └─── main.dart
    └─── pubspec.yaml

- This project is a Flutter application that follows a specific folder structure and includes various components, utilities, and utilizes the GetX state management library following the MVC pattern. It serves as a starting point for developing Flutter applications.
- **api**: This folder contains files related to API integration and communication. It typically includes classes or files responsible for making HTTP requests, handling responses, and managing API endpoints.

    - **api_interface.dart**: This file defines an abstract class called `ApiCallBacks`. It includes methods such as `onSuccess`, `onError`, `onConnectionError`, and `onLoading`. These methods serve as callbacks for different stages of API requests and responses, allowing for handling success, error, connection issues, and loading states.

    - **garage_status.dart**: This file responsible for handle redirection after login. In this file user redirect to `dashboard` or `on-boarding` screen based on garage status.

    - **api_presenter.dart**: The `ApiPresenter` class is defined in this file. It takes an `ApiCallBacks` object as a constructor parameter. This class acts as a presenter that communicates with the API repository. It manages the API calls and handles responses, forwarding them to the appropriate callback methods defined in the `ApiCallBacks` interface.

    - **http_exception.dart**: This file includes an `HttpException` class, which represents an exception that can occur during HTTP requests. It can be used to handle and provide meaningful error messages when an error occurs during API communication.

    - **api_end_points.dart**: The `api_endpoints.dart` file contains constants or variables that represent different API endpoints or URLs. It provides a centralized location to manage and access the various endpoints used in the application.

    - **rest_client.dart**: The `RestClient` class is defined in this file. It takes an `ApiPresenter` object as a constructor parameter. This class encapsulates the logic for making API requests using HTTP methods such as GET, POST, PUT, DELETE, etc. It manages the request types, constructs the API URLs using the base URL defined in `web_url`, and communicates with the API presenter to handle the responses.

    - **web_fields_key.dart**: This file defines the body parameters used in API requests. It typically includes variables or constants representing the data to be sent in the request body.

      These files collectively provide a structured approach to handle API integration and communication within the project. They help manage API endpoints, handle request/response logic, and provide callbacks for different stages of API communication.

- **core**: The core folder is a structured directory for organizing essential and shared functionality.

    - **app_preferences**: The app_preferences folder manages application-specific preferences and settings. It often includes classes or files responsible for storing and retrieving user preferences, settings, or configurations.

    - **auth**: This package serves as the central hub for handling social authentications, including services like `Google` and `Facebook`. Additionally, it provides a convenient `sign-out` handler for managing user sessions.

    - **constant**: The constants folder holds constant values used throughout the application. It is common to define things like API URLs, default configurations, error messages, or any other values that remain constant across different parts of the app.

    - **extensions**: The provided directory defines a Dart extension called ContextExtension on the BuildContext class in Flutter. This extension simplifies and enhances the usage of the BuildContext by providing convenient access to various properties and functionalities commonly used in Flutter applications.

    - **notification**: The notification folder handles push notifications and related functionality. It typically includes classes or files responsible for registering devices for push notifications, handling incoming notifications, and managing notification-related logic.

    - **permission**: We have implemented two classes to manage dashboard permissions for different types of users `1.Manager` `2.Employee`. When a `Manager` logs in, they are granted access to the Branch Manager and My Account sections in addition to the standard features. On the other hand, when an `Employee` logs in, they do not have access to these two sections.

    - **routes**: The routes folder manages the routing and navigation within the application. It typically includes a file or class that defines the different routes or screens in the app and handles the navigation between them.

- **custom_classes**: In this package, we have defined custom classes such as `CustomDropdownField`, `CustomExpansionPanel`, and `CustomSVGImage`. These classes are tailored to match the design specifications provided in our Figma design files. We utilize these custom classes to create a consistent and cohesive user interface across our screens. Each class is designed to enhance the visual appeal and functionality of our UI elements, ensuring a seamless user experience.

- **l10n**: We've implemented an l10n package to support multi-language functionality in our app, accommodating both `English` and `Arabic` languages. This package enables seamless localization, allowing users to switch between languages effortlessly, enhancing accessibility and user experience.

- **models**: The models package contains all the essential data models that serve as the backbone for our UI. These models encapsulate the necessary attributes and behaviors of various entities within our application, ensuring consistency and integrity of data representation throughout the user interface.

- **pages**:  Inside the `pages` package, we organize our screen designs into separate classes for both `web` and `mobile` views. Each screen class is coupled with its respective controller class, which encapsulates the business logic associated with that particular screen. This separation of concerns ensures clear code organization and facilitates maintenance and scalability of our application.

- **utils**: The utils folder houses utility classes and functions that provide common functionality across the application. It can include various subfolders or files dedicated to specific utility categories or functionalities.

- **firebase_options**: This class, `DefaultFirebaseOptions`, provides a platform-specific set of default Firebase options based on the target platform in a Flutter application. It determines the current platform (Android or iOS) and returns the corresponding FirebaseOptions configuration.

- **main.dart**: The main.dart file serves as the entry point of the application. It typically includes the main function that runs the Flutter application and sets up the initial configuration, such as the root widget or the MaterialApp.





## State Management with GetX and MVC Pattern

This project utilizes the GetX state management library, which provides a lightweight and powerful solution for managing application state in Flutter. The MVC (Model-View-Controller) pattern is followed to separate concerns and ensure a clean and organized code structure.

- **Model**: The model folder defines data models used in the application. These models represent different data structures or entities used in the app and are responsible for serialization, deserialization, and type safety.

- **Controller**: The controller folder contains controllers or state management classes following the MVC pattern. Controllers handle the business logic and manage the state of the application. They communicate with models, fetch data from APIs, and update the state of the application, which triggers UI updates.

- **View**: The view folder represents the UI components of the application. Screens and widgets reside in the screens and widgets folders, respectively. Views are responsible for rendering the UI and displaying data to the user. They interact with controllers to retrieve data and update the UI accordingly.


## Additional Information

For more information about Flutter, GetX state management library, and MVC pattern, refer to the following resources:

- [Flutter Documentation](https://flutter.dev/docs)
- [GetX Documentation](https://github.com/jonataslaw/getx)
- [Flutter State Management Guide](https://docs.flutter.dev/data-and-backend/state-mgmt/options)
- [MVC Pattern in Flutter](https://www.educba.com/what-is-mvc-design-pattern/)

## Built With

Dart and Flutter
