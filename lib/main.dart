import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:url_strategy/url_strategy.dart';

import 'Utils/language_provider.dart';
import 'common/error_screen.dart';
import 'core/app_preference/app_preferences.dart';
import 'core/app_preference/storage_keys.dart';
import 'core/auth/facebook_auth_handler.dart';
import 'core/auth/google_auth_handler.dart';
import 'core/constant/ui_constants.dart';
import 'core/notification/firebase_notification_service.dart';
import 'core/routes/route_paths.dart';
import 'core/routes/routes.dart';
import 'firebase_options.dart';
import 'l10n/languages.dart';
import 'l10n/supported_languages.dart';

void main() async {
  setPathUrlStrategy();
  await AppPreferences.init();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  FirebaseNotificationService.firebaseMessagingInitialization();
  FacebookAuthHandler.init();
  GoogleAuthHandler.init();

  runApp(
    ChangeNotifierProvider(
      create: (context) => LanguageProvider(),
      child: const App(),
    ),
  );
}

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Al-baida Garage',
      debugShowCheckedModeBanner: false,
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      locale: Get.deviceLocale,
      translations: Languages(),
      supportedLocales: SupportedLanguages.supportedLanguages,
      fallbackLocale: const Locale('en', 'US'),
      onInit: () {
        if (AppPreferences.sharedPrefRead(StorageKeys.lang) == null) {
          AppPreferences.sharedPrefWrite(StorageKeys.lang, 'en');
        } else {
          if (AppPreferences.sharedPrefRead(StorageKeys.lang).toString() ==
              englishLocaleCode) {
            Get.updateLocale(const Locale('en', 'EN'));
          } else if (AppPreferences.sharedPrefRead(StorageKeys.lang)
                  .toString() ==
              arabicLocaleCode) {
            Get.updateLocale(
              const Locale('ar', 'AR'),
            );
          }
        }
      },
      getPages: Routes.pages,
      initialRoute: RoutePaths.SIGNUP,
      unknownRoute: GetPage(
        name: RoutePaths.PAGE_NOT_FOUND,
        page: () => const ErrorScreen(),
      ),
    );
  }
}
