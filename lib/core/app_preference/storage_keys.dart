/// Key of the storage that value store in local
abstract final class StorageKeys {
  StorageKeys._();

  static const String accessToken = 'token';
  static const String userId = 'userId';
  static const String lang = 'lang';
  static const String garageStatus = 'status';
  static const String userType = 'userType';
  static const String garageId = 'garageId';
  static const String addBranchesFrom = 'addBranchesFrom';
  static const String isAddBranchesFromAuth = 'isAddBranchesFromAuth';
}

abstract final class EnvironmentKeys {
  EnvironmentKeys._();

  static const String googleMapKey = 'GOOGLE_MAPS_API_KEY';
  static const String facebookAppId = 'FACEBOOK_APP_ID';
  static const String googleAuthId = 'GOOGLE_AUTH_ID';
}
