import 'package:shared_preferences/shared_preferences.dart';

class AppPreferences {
  static late SharedPreferences _prefsInstance;

  static Future<void> init() async {
    _prefsInstance = await SharedPreferences.getInstance();
  }

  static dynamic sharedPrefRead(String key) {
    return _prefsInstance.get(key);
  }

  static Future<void> sharedPrefWrite(String key, dynamic value) async {
    if (value is bool) {
      await _prefsInstance.setBool(key, value);
    } else if (value is int) {
      await _prefsInstance.setInt(key, value);
    } else if (value is double) {
      await _prefsInstance.setDouble(key, value);
    } else if (value is String) {
      await _prefsInstance.setString(key, value);
    }
  }

  static Future<void> sharedPrefRemove(String key) async {
    await _prefsInstance.remove(key);
  }

  static Future<void> sharedPrefEraseAllData() async {
    await _prefsInstance.clear();
  }
}
