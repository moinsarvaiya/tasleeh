import 'package:google_sign_in/google_sign_in.dart';

import '../../Utils/utilities.dart';
import '../app_preference/storage_keys.dart';

class GoogleAuthHandler {
  static late String _clientId;
  static late GoogleSignIn _googleSignIn;

  static Future<void> init() async {
    _clientId = await getFromEnv(EnvironmentKeys.googleAuthId);
    _googleSignIn = GoogleSignIn(clientId: _clientId);
  }

  static Future<Map<String, dynamic>?> login() async {
    try {
      GoogleSignInAccount? account = await _googleSignIn.signIn();
      if (account == null) {
        return null;
      }
      GoogleSignInAuthentication auth = await account.authentication;
      String? accessToken = auth.accessToken;
      return {
        'account': account,
        'accessToken': accessToken,
      };
    } catch (error) {
      return null;
    }
  }

  static Future<void> signOut() => _googleSignIn.disconnect();
}
