import 'package:al_baida_garage_fe/Utils/utilities.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

import '../../api/api_end_point.dart';
import '../../api/api_interface.dart';
import '../../api/api_presenter.dart';
import '../../api/garage_status.dart';
import '../constant/base_extension.dart';
import '../constant/ui_constants.dart';
import 'facebook_auth_handler.dart';
import 'google_auth_handler.dart';

class SocialAuthHandler implements ApiCallBacks {
  Future googleLogin() async {
    Map<String, dynamic>? loginResult = await GoogleAuthHandler.login();
    if (loginResult != null) {
      String? accessToken = loginResult['accessToken'];
      dynamic account = loginResult['account'];
      final email = account.email;
      final firstName = account.displayName.split(' ')[0];
      final lastName = account.displayName.split(' ')[1];
      final loginPlatform = socialLoginPlatform[SocialLoginType.google];

      String fcmToken = await getFCMToken();

      ApiPresenter(this).socialLogin(
        email,
        firstName,
        lastName,
        accessToken!,
        loginPlatform!,
        fcmToken,
      );
    }
  }

  void facebookLogin() async {
    final LoginResult result = await FacebookAuth.instance.login(
      permissions: [
        'email',
        'public_profile',
      ],
    );

    if (result.status == LoginStatus.success) {
      Map<String, dynamic> userData = await FacebookAuth.instance
          .getUserData(fields: "first_name,last_name,email");

      final AccessToken accessToken = result.accessToken!;
      final email = userData['email'];
      final firstName = userData['first_name'];
      final lastName = userData['last_name'];
      final loginPlatform = socialLoginPlatform[SocialLoginType.google];

      String fcmToken = await getFCMToken();

      ApiPresenter(this).socialLogin(
        email,
        firstName,
        lastName,
        accessToken.token,
        loginPlatform!,
        fcmToken,
      );
    } else {
      result.message.toString().showErrorSnackBar();
    }
  }

  void signOut() {
    GoogleAuthHandler.signOut();
    FacebookAuthHandler.signOut();
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: 'Error');
  }

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {}

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) async {
    switch (apiEndPoint) {
      case ApiEndPoints.socialLogin:
        GarageStatus().getGarageStatus(
          object['data']['accessToken'],
          object['data']['userId'],
          userType: object['data']['userType'],
          employeeType: object['data']['employeeType'],
          garageId: object['data']['garageId'],
        );
        break;
    }
  }
}
