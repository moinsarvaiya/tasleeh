import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

import '../../Utils/utilities.dart';
import '../app_preference/storage_keys.dart';

class FacebookAuthHandler {
  static void init() async {
    String appId = await getFromEnv(EnvironmentKeys.facebookAppId);

    await FacebookAuth.i.webAndDesktopInitialize(
      appId: appId,
      cookie: true,
      xfbml: true,
      version: "v15.0",
    );
  }

  static void signOut() async {
    await FacebookAuth.instance.logOut();
  }
}
