import 'package:get/get.dart';

import '../../api/api_end_point.dart';
import '../../api/api_interface.dart';
import '../../api/api_presenter.dart';
import '../app_preference/app_preferences.dart';
import '../app_preference/storage_keys.dart';
import '../routes/route_paths.dart';
import 'social_login_handler.dart';

class CommonSignOut implements ApiCallBacks {
  void signOut() {
    ApiPresenter(this).signOut();
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {}

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {}

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.logout:
        String lang =
            AppPreferences.sharedPrefRead(StorageKeys.lang).toString();
        AppPreferences.sharedPrefEraseAllData();
        AppPreferences.sharedPrefWrite(StorageKeys.lang, lang);
        Get.offAllNamed(RoutePaths.LOGIN);
        SocialAuthHandler().signOut();
        break;
    }
  }
}
