
import 'package:flutter/cupertino.dart';

extension ResponsiveContext on BuildContext {
  double get screenWidth => MediaQuery.of(this).size.width;

  double get screenHeight => MediaQuery.of(this).size.height;

  bool get isMobile => MediaQuery.of(this).size.width < 600; // You can adjust the breakpoint for mobile devices

  bool get isTabletView =>
      MediaQuery.of(this).size.width >= 600 &&
      MediaQuery.of(this).size.width < 1024; // You can adjust the breakpoint for tablets

  bool get isDesktop => MediaQuery.of(this).size.width >= 768; // You can adjust the breakpoint for desktops
}

