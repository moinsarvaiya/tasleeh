class Validation {
  static isNotEmpty(String? field) {
    return field != null && field.isNotEmpty;
  }

  static bool isValidEmail(String? email) {
    if (email == null || email.isEmpty) {
      return false;
    }

    RegExp emailRegex = RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');
    return emailRegex.hasMatch(email);
  }

  static bool isNumberWithinRange(String? number) {
    return number != null && number.length >= 3 && number.length <= 8;
  }

  static bool doFieldsMatch(String field1, String field2) {
    return field1 == field2;
  }
}
