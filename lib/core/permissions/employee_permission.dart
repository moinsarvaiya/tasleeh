class EmployeePermission {
  EmployeePermission._privateConstructor();

  static final EmployeePermission _instance = EmployeePermission._privateConstructor();

  static EmployeePermission get instance => _instance;

  bool isBranchManagementVisible = false;
  bool isOrderListVisible = true;
  bool isPaymentVisible = true;
  bool isHelpVisible = true;
  bool isMyAccountVisible = false;
}
