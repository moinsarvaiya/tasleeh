class ManagerPermission {
  ManagerPermission._privateConstructor();

  static final ManagerPermission _instance = ManagerPermission._privateConstructor();

  static ManagerPermission get instance => _instance;

  bool isBranchManagementVisible = true;
  bool isOrderListVisible = true;
  bool isPaymentVisible = true;
  bool isHelpVisible = true;
  bool isMyAccountVisible = true;
}
