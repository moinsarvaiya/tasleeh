import 'package:al_baida_garage_fe/Pages/Dashboard/multi_branch_handler/multi_branch_handler_controller.dart';
import 'package:get/get.dart';

import '../../Pages/Dashboard/MyAccount/account_details/accounts_details_controller.dart';
import '../../Pages/Dashboard/MyAccount/basic_details_dashboard/basic_details_dashboard_controller.dart';
import '../../Pages/Dashboard/MyAccount/marketing/marketing_dashboard_controller.dart';
import '../../Pages/Dashboard/MyAccount/my_account_dashboard_controller.dart';
import '../../Pages/Dashboard/MyAccount/services/services_dashboard_controller.dart';
import '../../Pages/Dashboard/Payment/payment_controller.dart';
import '../../Pages/Dashboard/custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import '../../Pages/Dashboard/garage_notification_manager/garage_notification_manager_controller.dart';
import '../../Pages/Dashboard/order_list/requested_order/order_controller.dart';
import '../../Pages/Dashboard/order_list/return_order_detail/return_order_detail_controller.dart';
import '../../Pages/Dashboard/order_list/return_orders/return_order_controller.dart';
import '../../Pages/account_detail/account_details_controller.dart';
import '../../Pages/authentication/create_reset_password/create_reset_password_controller.dart';
import '../../Pages/authentication/forget_password/forget_password_controller.dart';
import '../../Pages/authentication/login/login_controllers.dart';
import '../../Pages/authentication/otp_verification/otp_verification_controller.dart';
import '../../Pages/authentication/sign_up/sign_up_controller.dart';
import '../../Pages/basic_details/basic_details_controller.dart';
import '../../Pages/custom_stepper/custom_stepper_controller.dart';
import '../../Pages/custom_stepper/widgets/branch_handler/branch_handler_controller.dart';
import '../../Pages/marketing/marketing_controller.dart';
import '../../Pages/onboarding_summary/onboarding_summary_controller.dart';
import '../../Pages/services/service_controller.dart';
import '../../Pages/staff/add_staff_controller.dart';

class SignupBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SignupController>(() => SignupController());
  }
}

class OTPVerificationBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OTPVerificationController>(() => OTPVerificationController());
  }
}

class CreateResetPasswordBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CreateResetPasswordController>(() => CreateResetPasswordController());
  }
}

class LoginBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LoginController>(() => LoginController());
  }
}

class CustomStepperBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CustomStepperController>(() => CustomStepperController());
    Get.lazyPut<BasicDetailsController>(() => BasicDetailsController());
    Get.lazyPut<MarketingController>(() => MarketingController());
    Get.lazyPut<ServiceController>(() => ServiceController());
    Get.lazyPut<BranchHandlerController>(() => BranchHandlerController());
    Get.lazyPut<AddStaffController>(() => AddStaffController());
    Get.lazyPut<OnboardingSummaryController>(() => OnboardingSummaryController());
    Get.lazyPut<AccountDetailsController>(() => AccountDetailsController());
  }
}

class ForgetPasswordBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ForgetPasswordController>(() => ForgetPasswordController());
  }
}

class DashboardStepperBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MyAccountDashboardController>(
        () => MyAccountDashboardController());
    Get.lazyPut<CustomDashboardStepperController>(
        () => CustomDashboardStepperController());
    Get.lazyPut<GarageNotificationsManagerController>(
        () => GarageNotificationsManagerController());

    Get.lazyPut<ReturnOrderController>(() => ReturnOrderController());
    Get.lazyPut<ReturnOrderDetailController>(() => ReturnOrderDetailController());
    Get.lazyPut<PaymentController>(() => PaymentController());

    Get.lazyPut<OrderController>(() => OrderController());

    Get.lazyPut<BasicDetailsDashboardController>(() => BasicDetailsDashboardController());

    Get.lazyPut<AddStaffController>(() => AddStaffController());
    Get.lazyPut<AccountDetailsDashboardController>(() => AccountDetailsDashboardController());
    Get.lazyPut<MarketingDashboardController>(() => MarketingDashboardController());
    Get.lazyPut<ServicesDashboardController>(() => ServicesDashboardController());
    Get.lazyPut<BranchHandlerController>(() => BranchHandlerController());
    Get.lazyPut<BasicDetailsController>(() => BasicDetailsController());
    Get.lazyPut<AccountDetailsController>(() => AccountDetailsController());
    Get.lazyPut<MultiBranchHandlerController>(() => MultiBranchHandlerController());
  }
}
