// ignore_for_file: constant_identifier_names

class RoutePaths {
  static const String SIGNUP = "/signup";
  static const String OTP_VERIFICATION = "/otpVerification";
  static const String CREATE_RESET_PASSWORD = "/createResetPassword";
  static const String CUSTOM_STEPPER = "/register";
  static const String FORGET_PASSWORD = "/forgetPassword";
  static const String LOGIN = "/login";
  static const String PAYMENT = '/payment';
  static const String RETURN_ORDERS = '/return-orders';
  static const String DASHBOARD_STEPPER = '/dashboard';
  static const String SUPPORT = '/support';
  static const String PAGE_NOT_FOUND = '/page-not-found';
}
