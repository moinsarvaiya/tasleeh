import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../Pages/Dashboard/custom_dashboard_drawer/custom_dashboard_stepper_screen.dart';
import '../../Pages/authentication/create_reset_password/create_reset_password_screen.dart';
import '../../Pages/authentication/forget_password/forget_password_screen.dart';
import '../../Pages/authentication/login/login_screen.dart';
import '../../Pages/authentication/otp_verification/otp_verification_screen.dart';
import '../../Pages/authentication/sign_up/sign_up_screen.dart';
import '../../Pages/custom_stepper/custom_stepper_screen.dart';
import '../app_preference/app_preferences.dart';
import '../app_preference/storage_keys.dart';
import '../constant/app_color.dart';
import 'bindings.dart';
import 'route_paths.dart';

/// All app navigation and routing  with arguments
class Routes {
  static List<GetPage>? get pages {
    List<GetPage> list = [];

    list.add(
      _buildRoute(
        name: RoutePaths.SIGNUP,
        widget: const SignupScreen(),
        bindings: SignupBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.OTP_VERIFICATION,
        widget: const OTPVerificationScreen(),
        bindings: OTPVerificationBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.CREATE_RESET_PASSWORD,
        widget: const CreateResetPasswordScreen(),
        bindings: CreateResetPasswordBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.FORGET_PASSWORD,
        widget: const ForgetPasswordScreen(),
        bindings: ForgetPasswordBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.LOGIN,
        widget: const LoginScreen(),
        bindings: LoginBindings(),
      ),
    );

    list.add(
      _buildRoute(
        name: RoutePaths.CUSTOM_STEPPER,
        widget: const CustomStepperScreen(),
        bindings: CustomStepperBindings(),
        isProtected: true,
      ),
    );

    list.add(_buildRoute(
      name: RoutePaths.DASHBOARD_STEPPER,
      widget: const CustomDashboardStepperScreen(),
      bindings: DashboardStepperBindings(),
      isProtected: true,
    ));

    return list;
  }

  static GetPage _buildRoute({
    required String name,
    required Widget widget,
    Bindings? bindings,
    bool isProtected = false,
  }) {
    return GetPage(
      name: name,
      page: () => isProtected ? _guardedRoute(widget) : widget,
      binding: bindings,
    );
  }

  static Widget _guardedRoute(Widget widget) {
    if (isAuthenticated()) {
      return widget;
    } else {
      Get.offAndToNamed(RoutePaths.SIGNUP);
      return const Center(
        child: CircularProgressIndicator(
          color: AppColors.primaryTextColor,
          backgroundColor: AppColors.colorWhite,
        ),
      );
    }
  }

  static bool isAuthenticated() {
    if (AppPreferences.sharedPrefRead(StorageKeys.accessToken) != null) {
      return true;
    }
    return false;
  }
}
