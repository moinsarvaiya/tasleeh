import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' show Colors, DropdownMenuItem, EdgeInsets, EdgeInsetsDirectional, ScaffoldState, SizedBox;
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../l10n/app_string_key.dart';
import 'app_color.dart';

const emptyWidget = SizedBox();
const emptyWidgetWide = SizedBox(width: double.infinity);
const defaultLatitude = 25.2854;
const defaultLongitude = 51.5310;
// Margins

const horizontalMargin4 = SizedBox(width: 4.0);
const horizontalMargin8 = SizedBox(width: 8.0);
const horizontalMargin12 = SizedBox(width: 12.0);
const horizontalMargin16 = SizedBox(width: 16.0);
const horizontalMargin24 = SizedBox(width: 24.0);
const horizontalMargin32 = SizedBox(width: 32.0);
const horizontalMargin48 = SizedBox(width: 48.0);

const verticalMargin4 = SizedBox(height: 4.0);
const verticalMargin8 = SizedBox(height: 8.0);
const verticalMargin12 = SizedBox(height: 12.0);
const verticalMargin16 = SizedBox(height: 16.0);
const verticalMargin24 = SizedBox(height: 24.0);
const verticalMargin32 = SizedBox(height: 32.0);
const verticalMargin48 = SizedBox(height: 48.0);

// Paddings

const emptyPadding = EdgeInsets.zero;

const horizontalPadding4 = EdgeInsets.symmetric(horizontal: 4.0);
const horizontalPadding8 = EdgeInsets.symmetric(horizontal: 8.0);
const horizontalPadding12 = EdgeInsets.symmetric(horizontal: 12.0);
const horizontalPadding16 = EdgeInsets.symmetric(horizontal: 16.0);
const horizontalPadding24 = EdgeInsets.symmetric(horizontal: 24.0);
const horizontalPadding32 = EdgeInsets.symmetric(horizontal: 32.0);
const horizontalPadding48 = EdgeInsets.symmetric(horizontal: 48.0);

const verticalPadding2 = EdgeInsets.symmetric(vertical: 2.0);
const verticalPadding4 = EdgeInsets.symmetric(vertical: 4.0);
const verticalPadding8 = EdgeInsets.symmetric(vertical: 8.0);
const verticalPadding12 = EdgeInsets.symmetric(vertical: 12.0);
const verticalPadding16 = EdgeInsets.symmetric(vertical: 16.0);
const verticalPadding24 = EdgeInsets.symmetric(vertical: 24.0);
const verticalPadding32 = EdgeInsets.symmetric(vertical: 32.0);
const verticalPadding48 = EdgeInsets.symmetric(vertical: 48.0);

const allPadding4 = EdgeInsets.all(4.0);
const allPadding8 = EdgeInsets.all(8.0);
const allPadding12 = EdgeInsets.all(12.0);
const allPadding16 = EdgeInsets.all(16.0);
const allPadding24 = EdgeInsets.all(24.0);
const allPadding32 = EdgeInsets.all(32.0);
const allPadding48 = EdgeInsets.all(48.0);

const topPadding1 = EdgeInsets.only(top: 1.0);
const topPadding2 = EdgeInsets.only(top: 2.0);
const topPadding4 = EdgeInsets.only(top: 4.0);
const topPadding8 = EdgeInsets.only(top: 8.0);
const topPadding12 = EdgeInsets.only(top: 12.0);
const topPadding16 = EdgeInsets.only(top: 16.0);
const topPadding24 = EdgeInsets.only(top: 24.0);
const topPadding32 = EdgeInsets.only(top: 32.0);
const topPadding48 = EdgeInsets.only(top: 48.0);

const bottomPadding1 = EdgeInsets.only(bottom: 1.0);
const bottomPadding2 = EdgeInsets.only(bottom: 2.0);
const bottomPadding4 = EdgeInsets.only(bottom: 4.0);
const bottomPadding8 = EdgeInsets.only(bottom: 8.0);
const bottomPadding12 = EdgeInsets.only(bottom: 12.0);
const bottomPadding16 = EdgeInsets.only(bottom: 16.0);
const bottomPadding24 = EdgeInsets.only(bottom: 24.0);
const bottomPadding32 = EdgeInsets.only(bottom: 32.0);
const bottomPadding48 = EdgeInsets.only(bottom: 48.0);

const leftPadding4 = EdgeInsets.only(left: 4.0);
const leftPadding8 = EdgeInsets.only(left: 8.0);
const leftPadding12 = EdgeInsets.only(left: 12.0);
const leftPadding16 = EdgeInsets.only(left: 16.0);
const leftPadding24 = EdgeInsets.only(left: 24.0);
const leftPadding32 = EdgeInsets.only(left: 32.0);
const leftPadding48 = EdgeInsets.only(left: 48.0);

const rightPadding4 = EdgeInsets.only(right: 4.0);
const rightPadding8 = EdgeInsets.only(right: 8.0);
const rightPadding12 = EdgeInsets.only(right: 12.0);
const rightPadding16 = EdgeInsets.only(right: 16.0);
const rightPadding24 = EdgeInsets.only(right: 24.0);
const rightPadding32 = EdgeInsets.only(right: 32.0);
const rightPadding48 = EdgeInsets.only(right: 48.0);

const startPadding4 = EdgeInsetsDirectional.only(start: 4.0);
const startPadding8 = EdgeInsetsDirectional.only(start: 8.0);
const startPadding12 = EdgeInsetsDirectional.only(start: 12.0);
const startPadding16 = EdgeInsetsDirectional.only(start: 16.0);
const startPadding24 = EdgeInsetsDirectional.only(start: 24.0);
const startPadding32 = EdgeInsetsDirectional.only(start: 32.0);
const startPadding48 = EdgeInsetsDirectional.only(start: 48.0);

const endPadding4 = EdgeInsetsDirectional.only(end: 4.0);
const endPadding8 = EdgeInsetsDirectional.only(end: 8.0);
const endPadding12 = EdgeInsetsDirectional.only(end: 12.0);
const endPadding16 = EdgeInsetsDirectional.only(end: 16.0);
const endPadding24 = EdgeInsetsDirectional.only(end: 24.0);
const endPadding32 = EdgeInsetsDirectional.only(end: 32.0);
const endPadding48 = EdgeInsetsDirectional.only(end: 48.0);

const double borderRadius = 8.0;

const int toastVisibleTime = 5;

bool notificationsEnabled = false;

Enum userComesFrom = UserComesFrom.signUp;

enum UserComesFrom { signUp, login, forgotPassword }

const maxImageSize = 1024 * 1024;
const maxImageUploadSize = 1;

enum BranchFrom { get, patch, delete }

Enum branchFrom = BranchFrom.get;

enum TestimonialFrom { post, patch, delete, get }

Enum testimonialFrom = TestimonialFrom.post;

enum SocialMediaLinksFrom { post, get }

Enum socialMediaLinksFrom = SocialMediaLinksFrom.post;

enum StaffFrom { post, patch, delete, get }

Enum staffFrom = StaffFrom.post;

enum GarageImageFrom { post, patch, delete, get }

Enum garageImageFrom = GarageImageFrom.post;

enum LevelFrom { post, patch, delete, get }

Enum levelFrom = GarageImageFrom.post;

enum StepperPosition {
  basicDetails,
  accountDetails,
  marketing,
  services,
  addStaff,
  summary,
}

Enum stepperPosition = StepperPosition.basicDetails;

Map<String, String> orderListType = {
  'ACTIVE_ORDER': 'active',
};

enum WorkStatusType {
  newWork,
  accepted,
  rejected,
  cancelled,
  pickupScheduled,
  vehicleInTransit,
  vehicleReached,
  serviceStarted,
  serviceCompleted,
  paymentPending,
  outForDelivery,
  delivered,
  warrantyClaimed,
}

Map<WorkStatusType, String> workStatusType = {
  WorkStatusType.newWork: 'new',
  WorkStatusType.accepted: 'accepted',
  WorkStatusType.rejected: 'rejected',
  WorkStatusType.cancelled: 'cancelled',
  WorkStatusType.pickupScheduled: 'pickup-scheduled',
  WorkStatusType.vehicleInTransit: 'vehicle-in-transit',
  WorkStatusType.vehicleReached: 'vehicle-reached',
  WorkStatusType.serviceStarted: 'service-started',
  WorkStatusType.serviceCompleted: 'service-completed',
  WorkStatusType.paymentPending: 'payment-pending',
  WorkStatusType.outForDelivery: 'out-for-delivery',
  WorkStatusType.delivered: 'delivered',
  WorkStatusType.warrantyClaimed: 'warranty-claimed',
};

Map<WorkStatusType, Color> workStatusColor = {
  WorkStatusType.accepted: Colors.teal,
  WorkStatusType.serviceCompleted: AppColors.successTextGreenColor,
  WorkStatusType.vehicleInTransit: Colors.blue,
  WorkStatusType.vehicleReached: Colors.grey,
  WorkStatusType.serviceStarted: Colors.orange,
};

enum GarageStatusType {
  incomplete,
  reviewed,
  newStatus,
  accepted,
  rejected,
  blacklisted,
}

Map<GarageStatusType, String> garageStatusType = {
  GarageStatusType.incomplete: 'incomplete',
  GarageStatusType.reviewed: 'reviewed',
  GarageStatusType.newStatus: 'new',
  GarageStatusType.accepted: 'accepted',
  GarageStatusType.rejected: 'rejected',
  GarageStatusType.blacklisted: 'blacklisted',
};

Map<GarageStatusType, Color> garageStatusColor = {
  GarageStatusType.incomplete: Colors.orange,
  GarageStatusType.reviewed: Colors.blue,
  GarageStatusType.newStatus: Colors.green,
  GarageStatusType.accepted: Colors.teal,
  GarageStatusType.rejected: Colors.red,
  GarageStatusType.blacklisted: Colors.grey,
};

enum OrderStatus { requested, active, completed, returned }

Enum orderType = OrderStatus.requested;

const customDebounceTime = 1;

final staffTypes = [AppStringKey.driver.tr, AppStringKey.branch_staff.tr];

final GlobalKey<ScaffoldState> mobileViewDrawerKey = GlobalKey<ScaffoldState>();
final GlobalKey<ScaffoldState> dashboardPaymentScreenDrawerKey = GlobalKey<ScaffoldState>();
final GlobalKey<ScaffoldState> dashboardOrderListDrawerKey = GlobalKey<ScaffoldState>();
final GlobalKey<ScaffoldState> dashboardCompletedOrderListDrawerKey = GlobalKey<ScaffoldState>();
final GlobalKey<ScaffoldState> updateOrderDrawerKey = GlobalKey<ScaffoldState>();
final GlobalKey<ScaffoldState> addLevelInService = GlobalKey<ScaffoldState>();
final GlobalKey<ScaffoldState> addLevelInServiceForOnboarding = GlobalKey<ScaffoldState>();

const List<DropdownMenuItem> languageSelectorItems = [
  DropdownMenuItem(
    value: 'en',
    child: Text('English'),
  ),
  DropdownMenuItem(
    value: 'ar',
    child: Text('العربية'),
  ),
];

const String englishLocaleCode = 'en';
const String arabicLocaleCode = 'ar';

bool isUpdatingMyAccounts = false;

const Map<SocialMediaTypes, String> socialMediaTypes = {
  SocialMediaTypes.facebook: 'facebook',
  SocialMediaTypes.twitter: 'twitter',
  SocialMediaTypes.instagram: 'instagram',
  SocialMediaTypes.website: 'website',
  SocialMediaTypes.tiktok: 'tiktok',
};

enum SocialMediaTypes { facebook, twitter, instagram, website, tiktok }

// enum AddBranchesFrom { addBranch, editBranch}

const addBranch = 'add_branch';
const editBranch = 'edit_branch';

int pageLimit = 10;

enum GarageProfileAction {
  get,
  update,
}

Enum garageProfileAction = GarageProfileAction.get;

enum SocialLoginType { google, facebook }

Map<SocialLoginType, String> socialLoginPlatform = {SocialLoginType.google: 'Google', SocialLoginType.facebook: 'Facebook'};

enum UserType { manager, employee }

Map<UserType, String> userTypePlatform = {UserType.manager: 'manager', UserType.employee: 'employee'};

const LatLng initialMapLocation = LatLng(23.0225, 72.5714); // Default location of Qatar City

enum ServiceRedirectionFrom {
  onBoarding,
  myAccount,
  summary
}

Enum serviceRedirectionFrom = ServiceRedirectionFrom.onBoarding;
