import 'dart:math';

import 'package:flutter/cupertino.dart';

class ScaleSize {
  static double textScaleFactor(BuildContext context, {double maxTextScaleFactor = 2}) {
    final width = MediaQuery.of(context).size.width;
    double val = (width / 1400) * maxTextScaleFactor;
    return max(1, min(val, maxTextScaleFactor));
  }

  static double textScaleFactorMobile(BuildContext context, {double maxTextScaleFactor = 1}) {
    final width = MediaQuery.of(context).size.width;
    double val = (width / 375) * maxTextScaleFactor;
    return max(1, min(val, maxTextScaleFactor));
  }
}