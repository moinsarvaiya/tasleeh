// All static images path
class AppImages {
  static const String imagePrefix = 'assets/images/';

  static const String appTopIcon = "${imagePrefix}svg/app_top_icon.svg";
  static const String signOut = "${imagePrefix}svg/sign_out.svg";
  static const String googleLogo = "${imagePrefix}svg/googleLogo.svg";
  static const String facebookLogo = "${imagePrefix}svg/facebookLogo.svg";
  static const String qatarFlag = "${imagePrefix}svg/qatarFlag.svg";
  static const String icDone = "${imagePrefix}svg/ic_done.svg";
  static const String icBasicDetail =
      "${imagePrefix}svg/stepper_basic_detail.svg";
  static const String icAccountDetail =
      "${imagePrefix}svg/stepper_account_detail.svg";
  static const String icMarketing = "${imagePrefix}svg/stepper_marketing.svg";
  static const String icServices = "${imagePrefix}svg/stepper_service.svg";
  static const String icStaff = "${imagePrefix}svg/stepper_staff.svg";
  static const String checkCircleIcon = "${imagePrefix}svg/checkCircle.svg";
  static const String marketingDescIcon =
      "${imagePrefix}svg/marketing_desc_icon.svg";
  static const String dragDropTopImage =
      "${imagePrefix}svg/drag_drop_top_image.svg";
  static const String forwardArrow = "${imagePrefix}svg/ic_forward_arrow.svg";
  static const String clock = "${imagePrefix}svg/ic_clock.svg";
  static const String emailIcon = "${imagePrefix}svg/email_icon.svg";
  static const String leftPaneIcon = "${imagePrefix}svg/left_pane.svg";
  static const String sampleProfileImage = "${imagePrefix}png/sample.png";
  static const String editIcon = "${imagePrefix}svg/edit.svg";
  static const String profileImage = "${imagePrefix}png/profileImage.png";
  static const String salesIcon = "${imagePrefix}svg/saleIcon.svg";
  static const String pendingIcon = "${imagePrefix}svg/totalPending.svg";
  static const String cartIcon = "${imagePrefix}svg/cart.svg";
  static const String downloadIcon = "${imagePrefix}svg/download.svg";
  static const String filter = "${imagePrefix}svg/tune.svg";
  static const String shoppingBag = "${imagePrefix}svg/shopping_bag.svg";
  static const String vehicle = "${imagePrefix}svg/car_repair.svg";
  static const String service = "${imagePrefix}svg/warehouse.svg";
  static const String instagramIcon = "${imagePrefix}svg/instagram.svg";
  static const String twitterIcon = "${imagePrefix}svg/twitter.svg";
  static const String tikTokIcon = "${imagePrefix}svg/tiktok.svg";
  static const String ordersReceived = "${imagePrefix}svg/orders_received.svg";
  static const String ordersPending = "${imagePrefix}svg/orders_pending.svg";
  static const String ordersCompleted =
      "${imagePrefix}svg/orders_completed.svg";
  static const String dashboardStepperOrderList =
      "${imagePrefix}svg/ic_dashboard_order_list.svg";
  static const String dashboardStepperSelectedOrderList =
      "${imagePrefix}svg/ic_dashboard_selected_order_list.svg";
  static const String dashboardStepperPayment =
      "${imagePrefix}svg/ic_dashboard_payment.svg";
  static const String dashboardStepperSelectedPayment =
      "${imagePrefix}svg/ic_dashboard_selected_payment.svg";
  static const String dashboardStepperHelp =
      "${imagePrefix}svg/ic_dashboard_help.svg";
  static const String dashboardStepperSelectedHelp =
      "${imagePrefix}svg/ic_dashboard_selected_help.svg";
  static const String dashboardStepperMyAccount =
      "${imagePrefix}svg/ic_dashboard_my_account.svg";
  static const String dashboardStepperSelectedMyAccount =
      "${imagePrefix}svg/ic_dashboard_selected_my_account.svg";
  static const String dashboardStepperProfile =
      "${imagePrefix}svg/ic_dashboard_profile.svg";
  static const String dashboardStepperNotification =
      "${imagePrefix}svg/ic_dashboard_notification.svg";
  static const String callIcon = "${imagePrefix}svg/call.svg";
  static const String mailIcon = "${imagePrefix}svg/email.svg";
  static const String supportIcon = "${imagePrefix}svg/support.svg";
  static const String check = "${imagePrefix}svg/check.svg";
  static const String cross = "${imagePrefix}svg/cross.svg";
  static const String menu = "${imagePrefix}svg/ic_dashboard_menu.svg";
  static const String serviceTitleIcon = "${imagePrefix}svg/ic_service.svg";
  static const String vehicleTitleIcon =
      "${imagePrefix}svg/ic_vehicle_model.svg";
  static const String remarks = "${imagePrefix}svg/remarks.svg";
  static const String serviceCompleted =
      "${imagePrefix}svg/ic_service_completed.svg";
  static const String arrowHeadDown = "${imagePrefix}svg/arrow_head_down.svg";
  static const String multiLanguageIcon =
      "${imagePrefix}svg/multi_language.svg";
  static const String websiteIcon = "${imagePrefix}svg/website_icon.svg";
  static const String warning = "${imagePrefix}svg/warning.svg";
  static const String googleMapPin = "${imagePrefix}svg/google_map_pin.svg";
  static const String failed = "${imagePrefix}svg/failed.svg";
  static const String reject = "${imagePrefix}svg/rejectIcon.svg";

  static const String deleteIcon = "${imagePrefix}svg/deleteIcon.svg";
  static const String noInternet = "${imagePrefix}png/no_internet.png";
}
