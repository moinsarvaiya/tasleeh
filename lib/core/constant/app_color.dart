import 'package:flutter/material.dart';

class AppColors {
  static const Color colorWhite = Color.fromRGBO(255, 255, 255, 1);
  static const Color colorBlack = Color.fromRGBO(0, 0, 0, 1.0);
  static const Color colorGrey = Color.fromRGBO(182, 182, 182, 1.0);
  static const Color colorBackground = Color.fromRGBO(243, 243, 243, 1);
  static const Color borderColor = Color.fromRGBO(211, 211, 211, 1.0);
  static const Color colorDragImageBack = Color.fromRGBO(250, 250, 250, 1);
  static const Color colorHoverClickRed = Color.fromRGBO(255, 108, 108, 1.0);

  // text colors
  static const Color primaryTextColor = Color.fromRGBO(38, 38, 38, 1);
  static const Color secondaryTextColor = Color.fromRGBO(140, 140, 140, 1);
  static const Color tertiaryTextColor = Color.fromRGBO(184, 184, 184, 1);
  static const Color errorTextColor = Color.fromRGBO(255, 77, 79, 1);
  static const Color completedStepTextColor = Color.fromRGBO(38, 38, 38, 1);
  static const Color inCompletedStepTextColor = Color.fromRGBO(89, 89, 89, 1);
  static const Color inputFieldHintTextColor = Color.fromRGBO(0, 0, 0, 0.25);
  static const Color inputFieldTextColor = Color.fromRGBO(0, 0, 0, 0.45);
  static const Color inputFieldHeading = Color.fromRGBO(0, 0, 0, 0.85);
  static const Color highlightedBlueText = Color.fromRGBO(72, 115, 252, 1);
  static const Color cardTextColor = Color.fromRGBO(77, 77, 77, 1);
  static const Color successTextGreenColor = Color.fromRGBO(82, 196, 26, 1);

  // background colors
  static const Color primaryBackgroundColor = Color.fromRGBO(243, 243, 243, 1);
  static const Color secondaryBackgroundColor =
      Color.fromRGBO(250, 250, 250, 1);
  static const Color tertiaryBackgroundColor = Color.fromRGBO(237, 237, 237, 1);
  static const Color dividerColor = Color.fromRGBO(240, 240, 240, 1);
  static const Color strokeColor = Color.fromRGBO(217, 217, 217, 1);
  static const Color popUpBackgroundColor = Color.fromRGBO(71, 71, 71, 0.2);
  static const Color lightBlueBackgroundColor =
      Color.fromRGBO(152, 177, 255, 1);

  // CTA color
  static const Color primaryCtaColor = Color.fromRGBO(17, 17, 17, 1);
  static const Color secondaryCtaColor = Color.fromRGBO(243, 245, 247, 1);
  static const Color inActiveCtaColor = Color.fromRGBO(245, 245, 245, 1);
  static const Color primaryCtaTextColor = Color.fromRGBO(255, 255, 255, 0.85);
  static const Color secondaryCtaTextColor = Color.fromRGBO(38, 38, 38, 1);
  static const Color inActiveCtaTextColor = Color.fromRGBO(184, 184, 184, 1);

  // icon colors
  static const Color iconColor = Color.fromRGBO(38, 38, 38, 1);
  static const Color inputFieldIconColor = Color.fromRGBO(184, 184, 184, 1);
  static const Color imageTextColor = Color.fromRGBO(24, 144, 255, 1);
  static const Color secondaryIconColor = Color.fromRGBO(163, 165, 180, 1);

  // border colors
  static const Color calenderBorderColor = Color.fromRGBO(0, 0, 0, 0.15);
  static const Color lightBlueBorderColor = Color.fromRGBO(234, 239, 255, 1);
  static const Color greenBackgroundColor = Color.fromRGBO(221, 243, 234, 1);
  static const Color lightGreenBackgroundColor =
      Color.fromRGBO(246, 255, 237, 1);
  static const Color redBackgroundColor = Color.fromRGBO(255, 82, 85, 1);
  static const Color lightRedBackgroundColor = Color.fromRGBO(255, 241, 240, 1);
  static const Color incrementColor = Color.fromRGBO(77, 148, 120, 1);
  static const Color yellowColor = Color.fromRGBO(255, 197, 61, 1);
}
