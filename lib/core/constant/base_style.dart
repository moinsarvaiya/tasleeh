import 'package:flutter/material.dart';

import 'base_fonts.dart';

class BaseStyle {
  static TextStyle textStyleRobotoRegular(double size, Color color) {
    return TextStyle(
      fontSize: size,
      color: color,
      fontFamily: BaseFonts.roboto,
      fontWeight: FontWeight.normal,
    );
  }

  static TextStyle textStyleRobotoBold(double size, Color color) {
    return TextStyle(
      fontSize: size,
      color: color,
      fontFamily: BaseFonts.roboto,
      fontWeight: FontWeight.bold,
      height: 1.2,
    );
  }

  static TextStyle textStyleRobotoMedium(double size, Color color) {
    return TextStyle(
      fontSize: size,
      color: color,
      fontFamily: BaseFonts.roboto,
      fontWeight: FontWeight.w400,
      overflow: TextOverflow.ellipsis,
    );
  }

  static TextStyle textStyleRobotoSemiBold(double size, Color color) {
    return TextStyle(
      fontSize: size,
      color: color,
      fontWeight: FontWeight.w500,
      fontFamily: BaseFonts.roboto,
    );
  }

  static TextStyle underLineTextStyle(double size, Color color,
      {FontWeight fontWeight = FontWeight.bold}) {
    return TextStyle(
      decoration: TextDecoration.underline,
      color: color,
      fontSize: size,
      fontFamily: BaseFonts.roboto,
      fontWeight: fontWeight,
    );
  }

  static TextStyle textStyleRobotoMediumBold(double size, Color color) {
    return TextStyle(
      fontSize: size,
      color: color,
      fontFamily: BaseFonts.roboto,
      fontWeight: FontWeight.w400,
    );
  }

  static TextStyle textStyleRobotoExtraBold(double size, Color color) {
    return TextStyle(
      fontSize: size,
      color: color,
      fontWeight: FontWeight.w700,
      fontFamily: BaseFonts.roboto,
    );
  }
}
