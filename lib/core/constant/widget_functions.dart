import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

BoxBorder LRTB_Border(
  double leftBorder,
  double rightBorder,
  double topBorder,
  double bottomBorder,
  Color borderColor,
) {
  return Border(
    right: BorderSide(
      color: borderColor,
      width: rightBorder,
    ),
    left: BorderSide(
      color: borderColor,
      width: leftBorder,
    ),
    top: BorderSide(
      color: borderColor,
      width: topBorder,
    ),
    bottom: BorderSide(
      color: borderColor,
      width: bottomBorder,
    ),
  );
}
