import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

extension DoubleExtension on double {
  /// All Circular BorderRadius
  BorderRadius get toAllBorderRadius {
    return BorderRadius.all(Radius.circular(this));
  }

  /// All EdgeInsets
  EdgeInsets get toAll {
    return EdgeInsets.all(this);
  }

  /// Vertical EdgeInsets
  EdgeInsets get toVertical {
    return EdgeInsets.symmetric(vertical: this);
  }

  /// Horizontal EdgeInsets
  EdgeInsets get toHorizontal {
    return EdgeInsets.symmetric(horizontal: this);
  }

  /// Horizontal EdgeInsets
  Radius get toRadius {
    return Radius.circular(this);
  }
}

extension StringExtension on String {
  /// SnackBar using GetX
  Future<void> get showSnackBar async {
    Get.showSnackbar(
      GetSnackBar(
        borderRadius: 10.0,
        margin: const EdgeInsets.all(10),
        message: this,
        isDismissible: true,
        snackStyle: SnackStyle.FLOATING,
        snackPosition: SnackPosition.BOTTOM,
        duration: const Duration(milliseconds: 2000),
      ),
    );
  }

  /// SnackBar using GetX
  Future<void> showErrorSnackBar({String? title = ""}) async {
    Get.showSnackbar(
      GetSnackBar(
        borderRadius: 10.0,
        title: title!.isEmpty ? 'Error' : title,
        margin: const EdgeInsets.all(10),
        message: this,
        isDismissible: true,
        snackStyle: SnackStyle.FLOATING,
        snackPosition: SnackPosition.BOTTOM,
        duration: const Duration(milliseconds: 2000),
        backgroundColor: Colors.red,
      ),
    );
  }

  /// SnackBar using GetX
  Future<void> showSuccessSnackBar({String? title = ""}) async {
    Get.showSnackbar(
      GetSnackBar(
        borderRadius: 10.0,
        title: title!.isEmpty ? 'Success' : title,
        margin: const EdgeInsets.all(10),
        message: this,
        isDismissible: true,
        snackStyle: SnackStyle.FLOATING,
        snackPosition: SnackPosition.BOTTOM,
        duration: const Duration(milliseconds: 1000),
        backgroundColor: Colors.green,
      ),
    );
  }

  /// AssetImageProvider
  AssetImage get toAsset {
    return AssetImage(this);
  }

  /// AssetImageProvider
  NetworkImage get toNetwork {
    return NetworkImage(this);
  }
}

extension TextSpanExtension on TextSpan {
  /// onClick of TextSpan
  TextSpan onClick(VoidCallback onClick) {
    TapGestureRecognizer recognizer = TapGestureRecognizer();
    recognizer.onTap = onClick;
    return TextSpan(
      text: text,
      style: style,
      recognizer: recognizer,
    );
  }
}

double getPrice(String price) {
  double priceInt = double.parse(price.replaceAll('£', ''));
  return priceInt;
}

extension ColorExtension on Color {
  InputBorder get toUnderlineBorder {
    return UnderlineInputBorder(
      borderSide: BorderSide(color: this, width: 1.25),
    );
  }
}

String getCurrentData() {
  var now = DateTime.now();
  var formatter = DateFormat('dd MMM, yyyy');
  return formatter.format(now);
}

extension ObjectExtension on Object? {
  /// Log in debug mode
  void get toLog {
    if (kDebugMode) {}
  }

  void get toHttpLog {
    if (kDebugMode) {}
  }

  void get toErrorLog {
    if (kDebugMode) {}
  }
}
