import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';

import '../../Pages/Dashboard/garage_notification_manager/garage_notification_manager_controller.dart';
import '../../firebase_options.dart';
import '../constant/ui_constants.dart';

class FirebaseNotificationService {
  static Future<void> firebaseMessagingInitialization() async {
    FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
    FirebaseMessaging messaging = FirebaseMessaging.instance;

    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    ///App in foreground
    FirebaseMessaging.onMessage.listen((RemoteMessage? message) {
      if (message != null) {
        Get.find<GarageNotificationsManagerController>()
            .newNotification(message);
      }
    });

    notificationsEnabled =
        settings.authorizationStatus == AuthorizationStatus.authorized
            ? true
            : false;
  }
}

/// This function called when app is in the background or terminated.
@pragma('vm:entry-point')
Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
}
