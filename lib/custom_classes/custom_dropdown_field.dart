import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

import '../core/constant/app_color.dart';
import '../core/constant/base_style.dart';

class CustomDropDownField extends StatelessWidget {
  final String hintTitle;
  final String labelText;
  final List<String> items;
  final String validationText;
  final bool? labelVisibility;
  final Function(String? val) onChanged;
  final Color? backgroundColor;
  final bool? isReadOnly;
  final String? selectedValue;
  final bool? isRequireField;
  final double? width;
  final double? height;
  final EdgeInsets? buttonPadding;
  final TextStyle? textStyle;

  const CustomDropDownField({
    Key? key,
    required this.hintTitle,
    required this.items,
    required this.validationText,
    required this.onChanged,
    required this.labelText,
    this.labelVisibility = true,
    this.backgroundColor = Colors.white,
    this.isReadOnly = false,
    this.selectedValue,
    this.isRequireField = true,
    this.width,
    this.height,
    this.buttonPadding = const EdgeInsets.only(
      left: 15,
    ),
    this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Visibility(
            visible: labelVisibility!,
            child: Text(
              labelText,
              style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                    color: AppColors.inputFieldHeading,
                  ),
            ),
          ),
          const SizedBox(
            height: 6,
          ),
          Theme(
            data: Theme.of(context).copyWith(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              hoverColor: Colors.transparent,
            ),
            child: DropdownButtonFormField2(
              isDense: false,
              decoration: InputDecoration(
                constraints: height != null ? BoxConstraints(maxHeight: height!) : null,
                focusColor: Colors.transparent,
                disabledBorder: InputBorder.none,
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(3),
                  borderSide: const BorderSide(
                    width: 0,
                    color: AppColors.strokeColor,
                  ),
                ),
                errorBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(3),
                    topRight: Radius.circular(3),
                    topLeft: Radius.circular(3),
                    bottomLeft: Radius.circular(3),
                  ),
                  borderSide: BorderSide(
                    width: 0,
                    color: AppColors.tertiaryTextColor,
                  ),
                ),
                focusedBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(3),
                    topRight: Radius.circular(3),
                    topLeft: Radius.circular(3),
                  ),
                  borderSide: BorderSide(
                    width: 0,
                    color: AppColors.tertiaryTextColor,
                  ),
                ),
                focusedErrorBorder: const OutlineInputBorder(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(3),
                    topRight: Radius.circular(3),
                    topLeft: Radius.circular(3),
                  ),
                  borderSide: BorderSide(
                    width: 0,
                    color: AppColors.tertiaryTextColor,
                  ),
                ),
                contentPadding: const EdgeInsets.only(
                  right: 20,
                ),
                isDense: false,
                filled: true,
                fillColor: backgroundColor,
                border: InputBorder.none,
              ),
              hint: selectedValue != null && selectedValue!.isNotEmpty
                  ? Text(
                      selectedValue!,
                      style: textStyle ??
                          BaseStyle.textStyleRobotoRegular(
                            14,
                            AppColors.primaryTextColor,
                          ),
                    )
                  : Text(
                      hintTitle,
                      style: textStyle ??
                          BaseStyle.textStyleRobotoRegular(
                            14,
                            AppColors.colorGrey,
                          ),
                    ),
              items: items
                  .map(
                    (item) => DropdownMenuItem<String>(
                      value: item,
                      child: Text(
                        item,
                        style: BaseStyle.textStyleRobotoRegular(14, AppColors.colorBlack),
                      ),
                    ),
                  )
                  .toList(),
              buttonStyleData: ButtonStyleData(
                padding: buttonPadding,
              ),
              onChanged: isReadOnly! ? null : onChanged,
              iconStyleData: const IconStyleData(
                iconEnabledColor: AppColors.colorWhite,
                icon: Icon(
                  Icons.keyboard_arrow_down,
                  size: 20,
                  color: AppColors.colorGrey,
                ),
              ),
              dropdownStyleData: DropdownStyleData(
                isOverButton: false,
                useSafeArea: false,
                padding: EdgeInsets.zero,
                elevation: 2,
                maxHeight: MediaQuery.sizeOf(context).height / 4.5,
                width: width,
                decoration: BoxDecoration(
                  color: AppColors.colorWhite,
                  border: Border.all(
                    color: AppColors.tertiaryTextColor,
                  ),
                  borderRadius: const BorderRadius.only(
                    bottomRight: Radius.circular(3),
                    bottomLeft: Radius.circular(3),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
