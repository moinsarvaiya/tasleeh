import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';

import '../core/constant/scale_size.dart';

class CustomText extends StatelessWidget {
  final String text;
  final TextStyle textStyle;
  final TextAlign textAlign;
  final int maxLines;
  final TextDirection textDirection;
  final TextOverflow textOverflow;
  final bool? isResponsive;

  const CustomText({
    super.key,
    required this.text,
    required this.textStyle,
    this.textAlign = TextAlign.center,
    this.maxLines = 1,
    this.textDirection = TextDirection.ltr,
    this.textOverflow = TextOverflow.ellipsis,
    this.isResponsive,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: textStyle,
      textAlign: textAlign,
      textScaler: isResponsive ?? true
          ? TextScaler.linear(context.isDesktop ? ScaleSize.textScaleFactor(context) : ScaleSize.textScaleFactorMobile(context))
          : null,
      maxLines: maxLines,
      textDirection: textDirection,
      overflow: textOverflow,
    );
  }
}
