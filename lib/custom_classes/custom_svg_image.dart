import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/svg.dart';

class CustomSvgImage extends StatelessWidget {
  final String image;
  final double height;


  const CustomSvgImage({
    super.key,
    required this.image, required this.height,
  });

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        double logoWidth = constraints.maxWidth * 0.3; // Adjust the width as a fraction of available width
        double logoHeight = constraints.maxHeight * 0.10; // Adjust the height as a fraction of available height

        // If you want to limit the maximum and minimum size of the image, you can use constraints or MediaQuery
        // For instance:
        // double logoWidth = constraints.maxWidth * 0.3.clamp(100, 300); // Clamps between 100 and 300
        // double logoHeight = constraints.maxHeight * 0.1.clamp(30, 100); // Clamps between 30 and 100

        return SvgPicture.asset(
          image, // Replace with your SVG file path
          width: logoWidth,
          height: logoHeight,
        );
      },
    );
  }
}
