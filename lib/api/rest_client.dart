import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

import '../common/common_modal_widget.dart';
import '../common/custom_loader.dart';
import '../core/app_preference/app_preferences.dart';
import '../core/app_preference/storage_keys.dart';
import '../core/constant/app_images.dart';
import '../l10n/app_string_key.dart';
import 'api_end_point.dart';
import 'api_interface.dart';
import 'http_exception.dart';

enum RequestType { post, put, get, delete, patch }

class RestClient {
  final ApiCallBacks _apiCallBacks;

  late http.Response response;
  late Duration timeOut;
  late Map<String, String> headers;
  late String url;
  late RequestType requestType;
  late Map requestParam;
  late String apiEndPoint;
  int apiCallCount = 0;
  int totalApiCall = 3;

  RestClient(this._apiCallBacks);

  /* Call api using http request
  * @param context
  * @param requestType - GET,POST,DELETE...
  * @param requestParams - Pass api request parameters (Body)
  * @param apiEndPoint - In which form data is return from api (Object or Array)
  * @param responseListener - Callback to handle api response
  * */

  Future<void> apiCall({
    Map requestParam = const {},
    String apiEndPoint = "",
    RequestType requestType = RequestType.get,
    bool isLoading = true,
    String queryParams = '',
    bool needAuthorization = true,
    File? profileImage,
    String? imageName,
    int apiCallCountValue = 0,
    List<File>? imageList,
  }) async {
    this.requestType = requestType;
    this.requestParam = requestParam;
    this.apiEndPoint = apiEndPoint;
    apiCallCount = apiCallCountValue;
    bool isConnected = await isConnectedToNetwork();
    if (!isConnected) {
      showDialog(
        context: Get.context!,
        builder: (BuildContext context) {
          return CommonModalWidget(
            modalIcon: AppImages.noInternet,
            title: AppStringKey.no_internet_connection.tr,
            description: AppStringKey.no_internet_connection_desc.tr,
            firstButtonContent: AppStringKey.retry.tr,
            firstButtonOnTap: () {
              Get.back();
              RestClient(_apiCallBacks).apiCall(
                requestParam: requestParam,
                apiEndPoint: apiEndPoint,
                requestType: requestType,
                isLoading: isLoading,
                queryParams: queryParams,
                needAuthorization: needAuthorization,
                profileImage: profileImage,
                imageName: imageName,
                imageList: imageList,
              );
            },
          );
        },
      );
    } else {
      _apiCallBacks.onLoading(isLoading, apiEndPoint);

      //time out of the API
      timeOut = const Duration(seconds: 30);
      headers = {
        HttpHeaders.contentTypeHeader: 'application/json',
        'Authorization': needAuthorization ? 'Bearer ${AppPreferences.sharedPrefRead(StorageKeys.accessToken)}' : '',
      };

      url = ApiEndPoints.baseUrl + apiEndPoint;
      if (queryParams.isNotEmpty) {
        url += queryParams;
      }
      try {
        callApiMethod();
      } catch (e) {
        if (kDebugMode) {}
        if (e.runtimeType == TimeoutException) {
          if (apiCallCount < totalApiCall) {
            apiCallCount++;
            callApiMethod();
          } else {
            LoadingDialog.closeFullScreenDialog();
            HttpExceptionHandler.onException(e, _apiCallBacks, apiEndPoint);
          }
        } else {
          LoadingDialog.closeFullScreenDialog();
          HttpExceptionHandler.onException(e, _apiCallBacks, apiEndPoint);
        }
        // }
        // finally {
        _apiCallBacks.onLoading(false, apiEndPoint);
      }
    }
  }

  void callApiMethod() async {
    switch (requestType) {
      case RequestType.post:
        response = await post(
          Uri.parse(url),
          headers: headers,
          body: json.encode(requestParam),
        ).timeout(timeOut);
        break;

      case RequestType.put:
        response = await put(
          Uri.parse(url),
          headers: headers,
          body: json.encode(requestParam),
        ).timeout(timeOut);
        break;

      case RequestType.get:
        response = await get(
          Uri.parse(url),
          headers: headers,
        ).timeout(timeOut);
        break;

      case RequestType.delete:
        response = await delete(
          Uri.parse(url),
          headers: headers,
          body: json.encode(requestParam),
        ).timeout(timeOut);
        break;
      case RequestType.patch:
        response = await patch(
          Uri.parse(url),
          headers: headers,
          body: json.encode(requestParam),
        ).timeout(timeOut);
        break;
    }
    handleResponse(response, apiEndPoint, response.body);
  }

  void handleResponse(response, String apiEndPoint, String jsonResponse) {
    LoadingDialog.closeFullScreenDialog();
    final responseBody = json.decode(jsonResponse);
    if (responseBody['code'] == 401) {
      // Get.offAllNamed(RoutePaths.LOGIN, arguments: {'isDoctor': isDoctor});
      throw (responseBody['detail']);
    }
    if (responseBody['code'] != 200 && responseBody['code'] != 201) {
      _apiCallBacks.onError(responseBody['error'], response.statusCode, apiEndPoint);
      // throw ('something went wrong');
    } else {
      if (responseBody['success'] && (responseBody['code'] == 200 || responseBody['code'] == 201)) {
        _apiCallBacks.onSuccess(responseBody, apiEndPoint);
      } else {
        _apiCallBacks.onError('something went wrong', response.statusCode, apiEndPoint);
      }
    }
  }

  Future<bool> isConnectedToNetwork() async {
    final connectivityResult = await (Connectivity().checkConnectivity());

    return connectivityResult != ConnectivityResult.none;
  }
}
