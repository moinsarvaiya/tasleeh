abstract class ApiCallBacks {
  void onSuccess(dynamic object, String apiEndPoint);

  void onError(String errorMsg, dynamic responseCode, String apiEndPoint);

  void onConnectionError(String error, String apiEndPoint);

  void onLoading(bool isLoading, String apiEndPoint);
}
