class ApiEndPoints {
  //BASE URL
  static const String baseUrl = '';

  //API END POINTS
  static const String otpSend = '/api/v1/auth/otp/send';
  static const String otpVerify = '/api/v1/auth/otp/verify';
  static const String createPassword = '/api/v1/auth/password';
  static const String forgetPassword = '/api/v1/auth/forgot-password';
  static const String signIn = '/api/v1/auth/sign-in';
  static const String socialLogin = '/api/v1/auth/social-login';

  static const String imageUpload = '/api/v1/photo/uploads';
  static const String getBranchId = '/api/v1/garage/garages';
  static const String getBranchData = '/api/v1/garage/garages/';
  static const String deleteBranch = '/api/v1/garage/garages/';
  static const String submitFieldData = '/api/v1/garage/garages/';
  static const String renameBranch = '/api/v1/garage/rename-branch/';
  static const String accountDetails = '/api/v1/garage/banks';
  static const String submitAccountDetails = '/api/v1/garage/banks/';
  static const String getAccountDetails = '/api/v1/garage/';
  static const String garageImage = '/api/v1/garage/garage-images/';
  static const String getGarageImage = '/api/v1/garage/';
  static const String garageTestimonial = '/api/v1/garage/testimonials/';
  static const String socialMediaLinks = '/api/v1/garage/social-links/';

  // service flow end-points
  static const String getMultiSelectionBandSearchData = '/api/v1/service/makes';
  static const String getCategories = '/api/v1/service/categories';
  static const String getSubCategories = '/api/v1/service/sub-categories/';
  static const String submitLevelData = '/api/v1/service/levels';
  static const String deleteLevel = '/api/v1/service/levels/';
  static const String getLevelData = '/api/v1/service/levels/';
  static const String submitServiceData = '/api/v1/service/services';
  static const String getSelectedCarData = '/api/v1/service/services/models/';
  static const String getServiceData = '/api/v1/service/services/';

  static const String getCityData = '/api/v1/garage/cities';
  static const String logout = '/api/v1/auth/logout';
  static const String staffDetails = '/api/v1/garage/employees/';
  static const String getStaffDetails = '/api/v1/garage/'; 

  static const String getUpdateOrderCategory = '/api/v1/order/category/';
  static const String getUpdateOrderSubCategory = '/api/v1/order/sub-category/';
  static const String getUpdateOrderSubCategoryLevel = '/api/v1/order/service-level/';
  static const String updateOrderData = '/api/v1/order/orders-detail/';
  static const String subServiceData = '/api/v1/service/services/subservice/';
  static const String editLevelData = '/api/v1/service/services/levels/';
  static const String garageStatus= '/api/v1/garage/garage-status/';
  static const String getMultiBranch= '/api/v1/garage/multi-branch';
  static const String getBranchReviews= '/api/v1/garage/garage/reviews/';
  static const String getPaymentStatus= '/api/v1/order/payment/status/';
  static const String updateLanguage = '/api/v1/user/user/profile/update';
}
