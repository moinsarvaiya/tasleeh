class WebFieldKey {
  //FOR Auth

  static const String strPostMethod = 'POST';
  static const String strContentType = 'Content-Type';
  static const String strMultipartFormData = 'multipart/form-data';
  static const String strAuthorization = 'Authorization';
  static const String countryCode = "countryCode";
  static const String phone = 'phone';
  static const String otp = 'otp';
  static const String userType = 'userType';
  static const String firebaseToken = 'firebaseToken';
  static const String password = 'password';
  static const String confirmPassword = 'confirmPassword';
  static const String calledFrom = 'calledFrom';
  static const String email = 'email';
  static const String firstName = 'firstName';
  static const String lastName = 'lastName';
  static const String socialLoginToken = 'socialLoginToken';
  static const String platform = 'platform';
  static const String strFile = 'file';
  static const String strFolder = 'folder';
  static const String garage = 'garage';
  static const String title = 'title';
  static const String id = 'id';
  static const String branchName = 'branchName';
  static const String provider = 'provider';
  static const String orderDate = 'orderDate';
  static const String pickupDate = 'pickupDate';
  static const String serviceDetail = 'serviceDetail';
  static const String minPrice = 'minPrice';
  static const String maxPrice = 'maxPrice';
  static const String workStatus = 'workStatus';
  static const String page = 'page';
  static const String limit = 'limit';
  static const String discountedPrice = 'discountedPrice';
  static const String price = 'price';
  static const String description = 'description';
  static const String orderId = 'orderId';
  static const String lang = 'lang';
}
