import 'package:al_baida_garage_fe/common/submission_failed_modal.dart';
import 'package:al_baida_garage_fe/models/comments_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../common/common_modal_widget.dart';
import '../core/app_preference/app_preferences.dart';
import '../core/app_preference/storage_keys.dart';
import '../core/constant/app_images.dart';
import '../core/constant/ui_constants.dart';
import '../core/routes/route_paths.dart';
import '../l10n/app_string_key.dart';
import 'api_end_point.dart';
import 'api_interface.dart';
import 'api_presenter.dart';

class GarageStatus implements ApiCallBacks {
  String _token = '';
  String _userId = '';
  String _userType = '';
  String _employeeType = '';
  String _garageId = '';

  void getGarageStatus(
    String accessToken,
    String userId, {
    String userType = '',
    String employeeType = '',
    String garageId = '',
  }) {
    _token = accessToken;
    _userId = userId;
    _userType = userType;
    _employeeType = employeeType;
    _garageId = garageId;
    ApiPresenter(this).getGarageStatus(userId);
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    // TODO: implement onConnectionError
  }

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {
    // TODO: implement onError
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    // TODO: implement onLoading
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.garageStatus:
        if (_garageId != '') {
          if (garageStatusType[GarageStatusType.incomplete] == object['data']['status']) {
            AppPreferences.sharedPrefWrite(StorageKeys.accessToken, _token);
            AppPreferences.sharedPrefWrite(StorageKeys.userId, _userId);
            AppPreferences.sharedPrefWrite(StorageKeys.userType, _userType);
            AppPreferences.sharedPrefWrite(StorageKeys.garageStatus, object['data']['status']);
            AppPreferences.sharedPrefWrite(StorageKeys.garageId, _garageId);
            AppPreferences.sharedPrefWrite(StorageKeys.addBranchesFrom, editBranch);
            AppPreferences.sharedPrefWrite(StorageKeys.isAddBranchesFromAuth, true);
            Get.toNamed(RoutePaths.CUSTOM_STEPPER);
          } else if (garageStatusType[GarageStatusType.accepted] == object['data']['status']) {
            AppPreferences.sharedPrefWrite(StorageKeys.accessToken, _token);
            AppPreferences.sharedPrefWrite(StorageKeys.userId, _userId);
            AppPreferences.sharedPrefWrite(StorageKeys.userType, _userType);
            AppPreferences.sharedPrefWrite(StorageKeys.garageId, _garageId);
            AppPreferences.sharedPrefWrite(StorageKeys.garageStatus, object['data']['status']);
            Get.toNamed(RoutePaths.DASHBOARD_STEPPER);
          } else if (garageStatusType[GarageStatusType.newStatus] == object['data']['status']) {
            showDialog(
                context: Get.context!,
                builder: (BuildContext context) {
                  return CommonModalWidget(
                    modalIcon: AppImages.warning,
                    title: AppStringKey.application_review.tr,
                    description: AppStringKey.application_review_description.tr,
                    firstButtonContent: AppStringKey.explore_izhal.tr,
                    firstButtonOnTap: () {
                      Navigator.pop(context);
                    },
                  );
                });
          } else if (garageStatusType[GarageStatusType.reviewed] == object['data']['status']) {
            AppPreferences.sharedPrefWrite(StorageKeys.accessToken, _token);
            AppPreferences.sharedPrefWrite(StorageKeys.userId, _userId);
            AppPreferences.sharedPrefWrite(StorageKeys.userType, _userType);
            AppPreferences.sharedPrefWrite(StorageKeys.garageId, _garageId);
            AppPreferences.sharedPrefWrite(StorageKeys.garageStatus, object['data']['status']);
            List<CommentModel> reviews = [];
            reviews.addAll((object['data']['reviews'] as List).map((e) => CommentModel.fromJson(e)));
            showDialog(
                context: Get.context!,
                builder: (BuildContext context) {
                  return SubmissionFailedModal(reviews: reviews);
                });
          } else if (garageStatusType[GarageStatusType.rejected] == object['data']['status']) {
            showDialog(
                context: Get.context!,
                builder: (BuildContext context) {
                  return CommonModalWidget(
                    modalIcon: AppImages.failed,
                    title: AppStringKey.rejected_application.tr,
                    description: AppStringKey.rejected_application_description.tr,
                    firstButtonContent: AppStringKey.explore_izhal.tr,
                    firstButtonOnTap: () {
                      Navigator.pop(context);
                    },
                  );
                });
          } else if (garageStatusType[GarageStatusType.blacklisted] == object['data']['status']) {
            AppPreferences.sharedPrefWrite(StorageKeys.accessToken, _token);
            AppPreferences.sharedPrefWrite(StorageKeys.userId, _userId);
            AppPreferences.sharedPrefWrite(StorageKeys.userType, _userType);
            AppPreferences.sharedPrefWrite(StorageKeys.garageId, _garageId);
            AppPreferences.sharedPrefWrite(StorageKeys.garageStatus, object['data']['status']);
            showDialog(
                context: Get.context!,
                builder: (BuildContext context) {
                  return CommonModalWidget(
                    modalIcon: AppImages.failed,
                    title: AppStringKey.garage_blacklist.tr,
                    description: AppStringKey.garage_blacklist_description.tr,
                    firstButtonContent: AppStringKey.explore_izhal.tr,
                    firstButtonOnTap: () {
                      Navigator.pop(context);
                    },
                  );
                });
          } else {
            AppPreferences.sharedPrefWrite(StorageKeys.accessToken, _token);
            AppPreferences.sharedPrefWrite(StorageKeys.userId, _userId);
            AppPreferences.sharedPrefWrite(StorageKeys.userType, _userType);
            AppPreferences.sharedPrefWrite(StorageKeys.garageId, _garageId);
            AppPreferences.sharedPrefWrite(StorageKeys.addBranchesFrom, addBranch);
            AppPreferences.sharedPrefWrite(StorageKeys.isAddBranchesFromAuth, true);
            Get.toNamed(RoutePaths.CUSTOM_STEPPER);
          }
        } else {
          AppPreferences.sharedPrefWrite(StorageKeys.accessToken, _token);
          AppPreferences.sharedPrefWrite(StorageKeys.userId, _userId);
          AppPreferences.sharedPrefWrite(StorageKeys.userType, _userType);
          AppPreferences.sharedPrefWrite(StorageKeys.garageId, _garageId);
          AppPreferences.sharedPrefWrite(StorageKeys.addBranchesFrom, addBranch);
          AppPreferences.sharedPrefWrite(StorageKeys.isAddBranchesFromAuth, true);
          Get.toNamed(RoutePaths.CUSTOM_STEPPER);
        }
        break;
    }
  }
}
