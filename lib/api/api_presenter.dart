import '../Utils/app_constants.dart';
import '../core/app_preference/app_preferences.dart';
import '../core/app_preference/storage_keys.dart';
import 'api_end_point.dart';
import 'api_interface.dart';
import 'rest_client.dart';
import 'web_fields_key.dart';

class ApiPresenter {
  final ApiCallBacks _apiCallBacks;

  ApiPresenter(this._apiCallBacks);

  Future<void> signUp(String countryCode, String phone) async {
    Map requestParam = toSignupJson(countryCode, phone);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: ApiEndPoints.otpSend,
      requestType: RequestType.post,
      needAuthorization: false,
    );
  }

  Future<void> forgetPassword(String countryCode, String phone) async {
    Map requestParam = toSignupJson(countryCode, phone);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: ApiEndPoints.forgetPassword,
      requestType: RequestType.post,
      needAuthorization: false,
    );
  }

  Map<String, dynamic> toSignupJson(String countryCode, String phone) => {
        WebFieldKey.countryCode: countryCode,
        WebFieldKey.phone: phone,
        WebFieldKey.userType: AppConstants.userType,
      };

  Future<void> login(
    String countryCode,
    String phone,
    String password,
    String fcmToken,
  ) async {
    Map requestParam = toLoginJson(
      countryCode,
      phone,
      password,
      fcmToken,
    );
    await RestClient(_apiCallBacks).apiCall(
        requestParam: requestParam,
        apiEndPoint: ApiEndPoints.signIn,
        requestType: RequestType.post,
        needAuthorization: false);
  }

  Map<String, dynamic> toLoginJson(
    String countryCode,
    String phone,
    String password,
    String fcmToken,
  ) =>
      {
        WebFieldKey.countryCode: countryCode,
        WebFieldKey.phone: phone,
        WebFieldKey.password: password,
        WebFieldKey.firebaseToken: fcmToken,
      };

  Future<void> resendOTP(String phoneNumber) async {
    Map requestParam = toResendOTPJson(phoneNumber);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: ApiEndPoints.otpSend,
      requestType: RequestType.post,
      needAuthorization: false,
    );
  }

  Map<String, dynamic> toResendOTPJson(String phoneNumber) => {
        WebFieldKey.countryCode: AppConstants.countryCode,
        WebFieldKey.phone: phoneNumber,
        WebFieldKey.userType: AppConstants.userType,
      };

  Future<void> otpVerify(String phoneNumber, String otp) async {
    Map requestParam = toOtpVerifyJson(phoneNumber, otp);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: ApiEndPoints.otpVerify,
      requestType: RequestType.post,
      needAuthorization: false,
    );
  }

  Map<String, dynamic> toOtpVerifyJson(String phoneNumber, String otp) => {
        WebFieldKey.countryCode: AppConstants.countryCode,
        WebFieldKey.phone: phoneNumber,
        WebFieldKey.otp: otp,
        WebFieldKey.userType: AppConstants.userType,
      };

  Future<void> setPassword(String password, String confirmPassword,
      String phoneNumber, String calledFrom) async {
    Map requestParam =
        toSetPasswordJson(password, confirmPassword, phoneNumber, calledFrom);

    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: ApiEndPoints.createPassword,
      requestType: RequestType.patch,
      needAuthorization: false,
    );
  }

  Map<String, dynamic> toSetPasswordJson(String password,
          String confirmPassword, String phoneNumber, String calledFrom) =>
      {
        WebFieldKey.password: password,
        WebFieldKey.confirmPassword: confirmPassword,
        WebFieldKey.phone: phoneNumber,
        WebFieldKey.userType: AppConstants.userType,
        WebFieldKey.calledFrom: calledFrom,
      };

  Future<void> socialLogin(
    String email,
    String firstName,
    String lastName,
    String socialLoginToken,
    String platform,
    String fcmToken,
  ) async {
    Map requestParam = toSocialLoginJson(
      email,
      firstName,
      lastName,
      socialLoginToken,
      platform,
      fcmToken,
    );

    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: ApiEndPoints.socialLogin,
      requestType: RequestType.post,
      needAuthorization: false,
    );
  }

  Map<String, dynamic> toSocialLoginJson(
    String email,
    String firstName,
    String lastName,
    String socialLoginToken,
    String platform,
    String fcmToken,
  ) =>
      {
        WebFieldKey.email: email,
        WebFieldKey.firstName: firstName,
        WebFieldKey.lastName: lastName,
        WebFieldKey.socialLoginToken: socialLoginToken,
        WebFieldKey.platform: platform,
        WebFieldKey.userType: AppConstants.userType,
        WebFieldKey.firebaseToken: fcmToken,
      };

  Future<void> getBranchId(Map requestParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: ApiEndPoints.getBranchId,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Future<void> getBranches() async {
    await RestClient(_apiCallBacks).apiCall(
      apiEndPoint: ApiEndPoints.getBranchId,
    );
  }

  Future<void> setBranchData(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.getBranchData,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> deleteBranch(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.deleteBranch,
      requestType: RequestType.delete,
    );
  }

  Future<void> submitFieldData(Map requestParams, String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.submitFieldData,
      requestType: RequestType.patch,
    );
  }

  Future<void> renameBranch(Map requestParams, String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.renameBranch,
      requestType: RequestType.patch,
    );
  }

  Future<void> accountDetails(Map requestParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: ApiEndPoints.accountDetails,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Future<void> getAccountDetails(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: "$queryParams/banks",
      apiEndPoint: ApiEndPoints.getAccountDetails,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> submitAccountDetails(
      Map requestParams, String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.submitAccountDetails,
      requestType: RequestType.patch,
      needAuthorization: true,
    );
  }

  Future<void> garageImage(Map requestParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: ApiEndPoints.garageImage,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Future<void> updateGarageImage(Map requestParams, String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.garageImage,
      requestType: RequestType.patch,
      needAuthorization: true,
    );
  }

  Future<void> deleteGarageImage(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.garageImage,
      requestType: RequestType.delete,
      needAuthorization: true,
    );
  }

  Future<void> getGarageImage(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: "$queryParams/garage-images",
      apiEndPoint: ApiEndPoints.getGarageImage,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> garageTestimonialData(Map requestParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: ApiEndPoints.garageTestimonial,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Future<void> updateGarageTestimonialData(
      Map requestParams, String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.garageTestimonial,
      requestType: RequestType.patch,
      needAuthorization: true,
    );
  }

  Future<void> deleteGarageTestimonialData(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.garageTestimonial,
      requestType: RequestType.delete,
      needAuthorization: true,
    );
  }

  Future<void> getTestimonialData(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.garageTestimonial,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> submitSocialMediaLinks(Map requestParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: ApiEndPoints.socialMediaLinks,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Future<void> getSocialMediaLinks(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.socialMediaLinks,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getMultiSelectionBandSearchData() async {
    await RestClient(_apiCallBacks).apiCall(
      apiEndPoint: ApiEndPoints.getMultiSelectionBandSearchData,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getCategories() async {
    await RestClient(_apiCallBacks).apiCall(
      apiEndPoint: ApiEndPoints.getCategories,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getSubCategories(String categoryId) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: categoryId,
      apiEndPoint: ApiEndPoints.getSubCategories,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> submitLevelData(String title, String id) async {
    Map requestParam = toLevelPostJson(title, id);
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParam,
      apiEndPoint: ApiEndPoints.submitLevelData,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toLevelPostJson(String title, String id) => {
        WebFieldKey.title: title,
        WebFieldKey.garage: toGarageIdJson(id),
      };

  Map<String, dynamic> toGarageIdJson(String id) => {
        WebFieldKey.id: id,
      };

  Future<void> getCityData() async {
    Map requestParas = toSignOutToJson();
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParas,
      apiEndPoint: ApiEndPoints.getCityData,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> signOut() async {
    Map requestParas = toSignOutToJson();
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParas,
      apiEndPoint: ApiEndPoints.logout,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toSignOutToJson() => {
        WebFieldKey.userType: AppConstants.userType,
      };

  Future<void> staffDetails(Map requestParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: ApiEndPoints.staffDetails,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Future<void> deleteStaffRecord(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.staffDetails,
      requestType: RequestType.delete,
      needAuthorization: true,
    );
  }

  Future<void> patchStaffRecord(Map requestParams, String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.staffDetails,
      requestType: RequestType.patch,
      needAuthorization: true,
    );
  }

  Future<void> getStaffDetails(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: "$queryParams/employees",
      apiEndPoint: ApiEndPoints.getStaffDetails,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> deleteLevel(String levelId) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: levelId,
      apiEndPoint: ApiEndPoints.deleteLevel,
      requestType: RequestType.delete,
      needAuthorization: true,
    );
  }

  Future<void> validateOnboardingForm(String currentPage) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: {
        'nextFrom': currentPage,
        'garage': {
          'id': AppPreferences.sharedPrefRead(StorageKeys.garageId),
        }
      },
      apiEndPoint: ApiEndPoints.validateData,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Future<void> getLevelList(String garageId) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: garageId,
      apiEndPoint: ApiEndPoints.getLevelData,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getPaymentSummary(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.getPaymentSummary,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> submitServiceData(Map requestParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      apiEndPoint: ApiEndPoints.submitServiceData,
      requestType: RequestType.post,
      needAuthorization: true,
    );
  }

  Future<void> getFilterPaymentData(
    String garageId,
    String orderDate,
    String pickUpDate,
    String pageNumber,
    String tableLimit,
    String orderId,
  ) async {
    Map<String, String> parameters = {
      if (orderDate.isNotEmpty) WebFieldKey.orderDate: orderDate,
      if (pickUpDate.isNotEmpty) WebFieldKey.pickupDate: pickUpDate,
      if (pageNumber.isNotEmpty) WebFieldKey.page: pageNumber,
      if (tableLimit.isNotEmpty) WebFieldKey.limit: tableLimit,
      if (orderId.isNotEmpty) WebFieldKey.orderId: orderId,
    };

    String queryParams = parameters.entries
        .map((entry) => '${entry.key}=${entry.value}')
        .join('&');
    await RestClient(_apiCallBacks).apiCall(
      queryParams: '$garageId?$queryParams',
      apiEndPoint: ApiEndPoints.getPaymentData,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getPaymentData(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.getPaymentData,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getReturnOrderData(
    String branchId,
    String pagination,
    String limit,
    String orderId,
  ) async {
    Map<String, String> parameters = {
      if (pagination.isNotEmpty) WebFieldKey.page: pagination,
      if (limit.isNotEmpty) WebFieldKey.limit: limit,
      if (orderId.isNotEmpty) WebFieldKey.orderId: orderId,
    };

    String queryParams = parameters.entries
        .map((entry) => '${entry.key}=${entry.value}')
        .join('&');

    await RestClient(_apiCallBacks).apiCall(
      queryParams: '$branchId?$queryParams',
      apiEndPoint: ApiEndPoints.getReturnOrderData,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getSelectedCars(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.getSelectedCarData,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getServiceData(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.getServiceData,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> deleteService(String serviceId) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: serviceId,
      apiEndPoint: ApiEndPoints.getServiceData,
      requestType: RequestType.delete,
      needAuthorization: true,
    );
  }

  Future<void> getServiceLevelData(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.getLevelData,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getOrdersSummary(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.getOrdersSummary,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getGarageProfile(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.getGarageProfile,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getOrderData(
    String garageId,
    String orderStatus,
    String orderDate,
    String pickUpDate,
    String serviceLevel,
    String minPrice,
    String maxPrice,
    String workStatus,
    String page,
    String limit,
    String orderId,
  ) async {
    Map<String, String> parameters = {
      if (orderDate.isNotEmpty) WebFieldKey.orderDate: orderDate,
      if (pickUpDate.isNotEmpty) WebFieldKey.pickupDate: pickUpDate,
      if (serviceLevel.isNotEmpty) WebFieldKey.serviceDetail: serviceLevel,
      if (minPrice.isNotEmpty) WebFieldKey.minPrice: minPrice,
      if (maxPrice.isNotEmpty) WebFieldKey.maxPrice: maxPrice,
      if (workStatus.isNotEmpty) WebFieldKey.workStatus: workStatus,
      if (page.isNotEmpty) WebFieldKey.page: page,
      if (limit.isNotEmpty) WebFieldKey.limit: limit,
      if (orderId.isNotEmpty) WebFieldKey.orderId: orderId,
    };
    String queryParams = parameters.entries
        .map((entry) => '${entry.key}=${entry.value}')
        .join('&');
    await RestClient(_apiCallBacks).apiCall(
      queryParams: '$garageId/$orderStatus?$queryParams',
      apiEndPoint: ApiEndPoints.getOrderData,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> acceptReturnOrder(Map requestParams, String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.acceptReturnOrder,
      requestType: RequestType.patch,
      needAuthorization: true,
    );
  }

  Future<void> acceptRequestOrder(Map requestParams, String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.acceptRequestOrder,
      requestType: RequestType.patch,
      needAuthorization: true,
    );
  }

  Future<void> updateReturnOrder(Map requestParams, String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestParams,
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.acceptReturnOrder,
      requestType: RequestType.patch,
      needAuthorization: true,
    );
  }

  Future<void> getReturnOrderDetails(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.getReturnOrderDetail,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getActiveOrderDetailsData(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.getActiveOrderDetail,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getActiveOrderDetails(String queryParams) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: queryParams,
      apiEndPoint: ApiEndPoints.getOrders,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getUpdateOrderCategoryData(String garageId) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: garageId,
      apiEndPoint: ApiEndPoints.getUpdateOrderCategory,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getGarageStatus(String userId) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: userId,
      apiEndPoint: ApiEndPoints.garageStatus,
      requestType: RequestType.get,
    );
  }

  Future<void> getUpdateOrderSubCategoryData(String categoryId) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: categoryId,
      apiEndPoint: ApiEndPoints.getUpdateOrderSubCategory,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> getUpdateOrderSubCategoryLevelData(String subCategoryId) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: subCategoryId,
      apiEndPoint: ApiEndPoints.getUpdateOrderSubCategoryLevel,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> updateOrderDetail(String subCategoryId, Map requestBody) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: subCategoryId,
      requestParam: requestBody,
      apiEndPoint: ApiEndPoints.updateOrderData,
      requestType: RequestType.patch,
      needAuthorization: true,
    );
  }

  Future<void> getSubServiceData(String subServiceId) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: subServiceId,
      apiEndPoint: ApiEndPoints.subServiceData,
      requestType: RequestType.get,
      needAuthorization: true,
    );
  }

  Future<void> updateSubServiceData(
    String subServiceId,
    String subCategoryId,
    String imageUrl,
  ) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: subServiceId,
      requestParam: {
        'imageUrl': imageUrl,
        'subCategory': {
          'id': subCategoryId,
        }
      },
      apiEndPoint: ApiEndPoints.subServiceData,
      requestType: RequestType.patch,
      needAuthorization: true,
    );
  }

  Future<void> editLevelData(
    String levelId,
    double price,
    double discountedPrice,
    String description,
  ) async {
    Map requestBody =
        toEditLevelDataToJson(price, discountedPrice, description);
    await RestClient(_apiCallBacks).apiCall(
      queryParams: levelId,
      requestParam: requestBody,
      apiEndPoint: ApiEndPoints.editLevelData,
      requestType: RequestType.patch,
      needAuthorization: true,
    );
  }

  Map<String, dynamic> toEditLevelDataToJson(
    double price,
    double discountedPrice,
    String description,
  ) =>
      {
        WebFieldKey.price: int.parse(price.round().toString()),
        WebFieldKey.discountedPrice:
            int.parse(discountedPrice.round().toString()),
        WebFieldKey.description: description,
      };

  Future<void> getMultiBranches() async {
    await RestClient(_apiCallBacks).apiCall(
      apiEndPoint: ApiEndPoints.getMultiBranch,
    );
  }

  Future<void> getBranchReviews(String branchId) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: branchId,
      apiEndPoint: ApiEndPoints.getBranchReviews,
    );
  }

  Future<void> updateLanguage(String lang) async {
    Map requestBody = {
      WebFieldKey.lang: lang,
    };
    await RestClient(_apiCallBacks).apiCall(
      requestParam: requestBody,
      requestType: RequestType.patch,
      apiEndPoint: ApiEndPoints.updateLanguage,
    );
  }

  Future<void> getPaymentStatus(String orderID) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: orderID,
      apiEndPoint: ApiEndPoints.getPaymentStatus,
      needAuthorization: true,
      requestType: RequestType.get
    );
  }

  Future<void> updatePaymentStatus(String orderID) async {
    await RestClient(_apiCallBacks).apiCall(
      queryParams: orderID,
      requestParam: {
        'status' : true
      },
      apiEndPoint: ApiEndPoints.getPaymentStatus,
      needAuthorization: true,
      requestType: RequestType.patch
    );
  }
}
