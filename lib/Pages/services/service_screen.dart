import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../core/constant/ui_constants.dart';
import '../../core/extensions/common_extension.dart';
import 'service_controller.dart';
import 'service_mobile_view.dart';
import 'service_web_view.dart';

class ServiceScreen extends StatefulWidget {
  final bool isReadOnly;
  final ServiceRedirectionFrom serviceRedirectionFrom;

  const ServiceScreen({
    super.key,
    this.isReadOnly = false,
    required this.serviceRedirectionFrom,
  });

  @override
  State<ServiceScreen> createState() => _ServiceScreenState();
}

class _ServiceScreenState extends State<ServiceScreen> {
  var controller = Get.put(ServiceController());

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    stepperPosition = StepperPosition.services;
    controller.redirectionFrom = widget.serviceRedirectionFrom;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (controller.isServiceCalled) {
        if (controller.redirectionFrom == ServiceRedirectionFrom.myAccount) {
          controller.getCategories();
        } else {
          controller.getServiceData();
        }
        controller.getMultiSelectionBandSearchData();
        controller.getLevelList();
        controller.isServiceCalled = false;
      }
    });
  }

  @override
  void didUpdateWidget(covariant ServiceScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: context.isDesktop
          ? ServiceWebView(
              controller: controller,
              isReadOnly: widget.isReadOnly,
            )
          : ServiceMobileView(
              controller: controller,
              isReadOnly: widget.isReadOnly,
            ),
    );
  }
}
