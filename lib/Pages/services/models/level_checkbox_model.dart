class LevelCheckboxModel {
  bool isCheckboxSelected;
  String checkboxValue;

  LevelCheckboxModel({
    required this.isCheckboxSelected,
    required this.checkboxValue,
  });
}
