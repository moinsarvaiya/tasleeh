// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CategoryResponseModel _$CategoryResponseModelFromJson(Map<String, dynamic> json) => CategoryResponseModel(
      id: json['id'] as String,
      title: json['title'] as String,
      tag: json['tag'] as String,
    );

Map<String, dynamic> _$CategoryResponseModelToJson(CategoryResponseModel instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'tag': instance.tag,
    };
