// Define the model classes based on your API request body

class CategoryRequestBodyModel {
  Category category;
  Garage garage;
  List<SubCategoryDto> subCategoryDto;

  CategoryRequestBodyModel({
    required this.category,
    required this.garage,
    required this.subCategoryDto,
  });

  Map<String, dynamic> toJson() {
    return {
      'category': category.toJson(),
      'garage': garage.toJson(),
      'subCategoryDto': subCategoryDto.map((item) => item.toJson()).toList(),
    };
  }
}

class Category {
  String id;

  Category({required this.id});

  Map<String, dynamic> toJson() {
    return {
      'id': id,
    };
  }
}

class Garage {
  String id;

  Garage({required this.id});

  Map<String, dynamic> toJson() {
    return {
      'id': id,
    };
  }
}

class SubCategoryDto {
  SubCategory? subCategory;
  String? imageUrl;
  List<ModelDto>? modelDto;
  List<LevelDto>? levelDto;

  SubCategoryDto({
    this.subCategory,
    this.imageUrl,
    this.modelDto,
    this.levelDto,
  });

  Map<String, dynamic> toJson() {
    return {
      'subCategory': subCategory!.toJson(),
      'imageUrl': imageUrl,
      'modelDto': modelDto!.map((item) => item.toJson()).toList(),
      'levelDto': levelDto!.map((item) => item.toJson()).toList(),
    };
  }
}

class SubCategory {
  String id;

  SubCategory({required this.id});

  Map<String, dynamic> toJson() {
    return {
      'id': id,
    };
  }
}

class ModelDto {
  Model model;

  ModelDto({required this.model});

  Map<String, dynamic> toJson() {
    return {
      'model': model.toJson(),
    };
  }
}

class Model {
  String id;

  Model({required this.id});

  Map<String, dynamic> toJson() {
    return {
      'id': id,
    };
  }
}

class LevelDto {
  Level? level;
  double? price;
  double? discountedPrice;
  String? description;

  LevelDto({
    this.level,
    this.price,
    this.discountedPrice,
    this.description,
  });

  Map<String, dynamic> toJson() {
    return {
      'level': level!.toJson(),
      'price': price,
      'discountedPrice': discountedPrice,
      'description': description,
    };
  }
}

class Level {
  String id;

  Level({required this.id});

  Map<String, dynamic> toJson() {
    return {
      'id': id,
    };
  }
}
