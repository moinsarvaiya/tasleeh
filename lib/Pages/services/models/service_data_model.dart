import 'dart:typed_data';

import 'package:al_baida_garage_fe/Pages/services/models/sub_category_response_model.dart';
import 'package:flutter/cupertino.dart';

import 'car_brand_model.dart';
import 'category_response_model.dart';

class ServiceModel {
  List<CategoryResponseModel> categories;
  bool isCategoryViewVisible;
  bool isDropdownReadOnly;
  List<SubCategoryModel> subCategoryList;

  ServiceModel({
    required this.categories,
    required this.isCategoryViewVisible,
    required this.isDropdownReadOnly,
    required this.subCategoryList,
  });
}

class SubCategoryModel {
  List<SubCategoryResponseModel> subCategories;
  String searchCarName;
  Uint8List? subCategoryImagePath;
  List<LevelModel> levelList;
  TextEditingController textEditingController;
  List<CarBrandModel>? carBrandList;
  List<String> carBrandSelectedIndex;
  List<String> selectedCarTypeIndex;

  SubCategoryModel({
    required this.subCategories,
    required this.searchCarName,
    this.subCategoryImagePath,
    required this.levelList,
    required this.textEditingController,
    required this.carBrandSelectedIndex,
    required this.selectedCarTypeIndex,
    this.carBrandList,
  });
}

class LevelModel {
  int? index;
  String? levelId;
  String levelName;
  String levelPrice;
  String levelDiscountedPrice;
  String levelDescription;

  LevelModel({
    this.index,
    this.levelId,
    required this.levelName,
    required this.levelPrice,
    required this.levelDiscountedPrice,
    required this.levelDescription,
  });
}
