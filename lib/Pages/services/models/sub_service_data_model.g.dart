// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sub_service_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubServiceDataModel _$SubServiceDataModelFromJson(Map<String, dynamic> json) => SubServiceDataModel(
      category: json['category'] == null ? null : SubService.fromJson(json['category'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SubServiceDataModelToJson(SubServiceDataModel instance) => <String, dynamic>{
      'subCategory': instance.category,
    };

SubService _$SubServiceFromJson(Map<String, dynamic> json) => SubService(
      serviceDetailId: json['serviceDetailId'] as String?,
      serviceDetailImage: json['serviceDetailImage'] as String?,
      subcategoryId: json['subcategoryId'] as String?,
      subcategoryName: json['subcategoryName'] as String?,
      serviceDetailModel: (json['serviceDetailModel'] as List<dynamic>?)?.map((e) => ServiceDetailModel.fromJson(e as Map<String, dynamic>)).toList(),
      serviceDetailLevel: (json['serviceDetailLevel'] as List<dynamic>?)?.map((e) => ServiceDetailLevel.fromJson(e as Map<String, dynamic>)).toList(),
    );

Map<String, dynamic> _$SubServiceToJson(SubService instance) => <String, dynamic>{
      'serviceDetailId': instance.serviceDetailId,
      'serviceDetailImage': instance.serviceDetailImage,
      'subcategoryId': instance.subcategoryId,
      'subcategoryName': instance.subcategoryName,
      'serviceDetailModel': instance.serviceDetailModel,
      'serviceDetailLevel': instance.serviceDetailLevel,
    };

ServiceDetailModel _$ServiceDetailModelFromJson(Map<String, dynamic> json) => ServiceDetailModel(
      modelId: json['modelId'] as String?,
      modelName: json['modelName'] as String?,
    );

Map<String, dynamic> _$ServiceDetailModelToJson(ServiceDetailModel instance) => <String, dynamic>{
      'modelId': instance.modelId,
      'modelName': instance.modelName,
    };

ServiceDetailLevel _$ServiceDetailLevelFromJson(Map<String, dynamic> json) => ServiceDetailLevel(
      levelId: json['levelId'] as String?,
      name: json['name'] as String?,
      description: json['description'] as String?,
      actualPrice: json['actualPrice'] as String?,
      discountedPrice: json['discountedPrice'] as String?,
    );

Map<String, dynamic> _$ServiceDetailLevelToJson(ServiceDetailLevel instance) => <String, dynamic>{
      'levelId': instance.levelId,
      'name': instance.name,
      'description': instance.description,
      'actualPrice': instance.actualPrice,
      'discountedPrice': instance.discountedPrice,
    };
