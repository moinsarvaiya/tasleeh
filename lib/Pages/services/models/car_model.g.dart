// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'car_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SelectedCarModel _$SelectedCarModelFromJson(Map<String, dynamic> json) => SelectedCarModel(
      id: json['id'] as String?,
      title: json['title'] as String?,
      imageUrl: json['imageUrl'] as String?,
      models: (json['models'] as List<dynamic>?)?.map((e) => Models.fromJson(e as Map<String, dynamic>)).toList(),
    )..isBrandNameSelected = json['isBrandNameSelected'] as bool?;

Map<String, dynamic> _$SelectedCarModelToJson(SelectedCarModel instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'imageUrl': instance.imageUrl,
      'isBrandNameSelected': instance.isBrandNameSelected,
      'models': instance.models,
    };

Models _$ModelsFromJson(Map<String, dynamic> json) => Models(
      id: json['id'] as String?,
      title: json['title'] as String?,
      imageUrl: json['imageUrl'] as String?,
    );

Map<String, dynamic> _$ModelsToJson(Models instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'imageUrl': instance.imageUrl,
    };
