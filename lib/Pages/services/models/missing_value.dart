class MissingValue {
  final String category;
  final String message;

  MissingValue(this.category, this.message);
}
