import 'package:json_annotation/json_annotation.dart';

part 'get_service_data_model.g.dart';

@JsonSerializable()
class SubmittedServiceDataModel {
  SubmittedCategory? category;

  SubmittedServiceDataModel({this.category});

  factory SubmittedServiceDataModel.fromJson(Map<String, dynamic> json) => _$SubmittedServiceDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$SubmittedServiceDataModelToJson(this);
}

@JsonSerializable()
class SubmittedCategory {
  String? serviceId;
  String? serviceName;
  List<SubmittedSubCategory>? subCategory;

  SubmittedCategory({this.serviceId, this.serviceName, this.subCategory});

  factory SubmittedCategory.fromJson(Map<String, dynamic> json) => _$SubmittedCategoryFromJson(json);

  Map<String, dynamic> toJson() => _$SubmittedCategoryToJson(this);
}

@JsonSerializable()
class SubmittedSubCategory {
  String? serviceDetailId;
  String? sucategotyImage;
  String? serviceDetailImage;
  String? name;

  SubmittedSubCategory({this.serviceDetailId, this.sucategotyImage, this.serviceDetailImage, this.name});

  factory SubmittedSubCategory.fromJson(Map<String, dynamic> json) => _$SubmittedSubCategoryFromJson(json);

  Map<String, dynamic> toJson() => _$SubmittedSubCategoryToJson(this);
}
