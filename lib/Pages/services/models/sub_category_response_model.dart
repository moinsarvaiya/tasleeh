import 'package:json_annotation/json_annotation.dart';

part 'sub_category_response_model.g.dart';

@JsonSerializable()
class SubCategoryResponseModel {
  String id;
  String title;

  SubCategoryResponseModel({
    required this.id,
    required this.title,
  });

  factory SubCategoryResponseModel.fromJson(Map<String, dynamic> json) => _$SubCategoryResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$SubCategoryResponseModelToJson(this);
}
