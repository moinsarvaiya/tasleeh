import 'package:json_annotation/json_annotation.dart';

part 'response_level_list_model.g.dart';

@JsonSerializable()
class ResponseLevelListModel {
  String id;
  String title;
  bool? isCheckboxSelected = false;

  ResponseLevelListModel({
    required this.id,
    required this.title,
    this.isCheckboxSelected,
  });

  factory ResponseLevelListModel.fromJson(Map<String, dynamic> json) => _$ResponseLevelListModelFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseLevelListModelToJson(this);
}
