// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_level_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ResponseLevelListModel _$ResponseLevelListModelFromJson(Map<String, dynamic> json) => ResponseLevelListModel(
      id: json['id'] as String,
      title: json['title'] as String,
      isCheckboxSelected: json['isCheckboxSelected'] as bool?,
    );

Map<String, dynamic> _$ResponseLevelListModelToJson(ResponseLevelListModel instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'isCheckboxSelected': instance.isCheckboxSelected,
    };
