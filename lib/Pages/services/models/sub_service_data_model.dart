import 'package:json_annotation/json_annotation.dart';

part 'sub_service_data_model.g.dart';

@JsonSerializable()
class SubServiceDataModel {
  SubService? category;

  SubServiceDataModel({
    this.category,
  });

  factory SubServiceDataModel.fromJson(Map<String, dynamic> json) => _$SubServiceDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$SubServiceDataModelToJson(this);
}

@JsonSerializable()
class SubService {
  String? serviceDetailId;
  String? serviceDetailImage;
  String? subcategoryId;
  String? subcategoryName;
  List<ServiceDetailModel>? serviceDetailModel;
  List<ServiceDetailLevel>? serviceDetailLevel;

  SubService({
    this.serviceDetailId,
    this.serviceDetailImage,
    this.subcategoryId,
    this.subcategoryName,
    this.serviceDetailModel,
    this.serviceDetailLevel,
  });

  factory SubService.fromJson(Map<String, dynamic> json) => _$SubServiceFromJson(json);

  Map<String, dynamic> toJson() => _$SubServiceToJson(this);
}

@JsonSerializable()
class ServiceDetailModel {
  String? modelId;
  String? modelName;

  ServiceDetailModel({
    this.modelId,
    this.modelName,
  });

  factory ServiceDetailModel.fromJson(Map<String, dynamic> json) => _$ServiceDetailModelFromJson(json);

  Map<String, dynamic> toJson() => _$ServiceDetailModelToJson(this);
}

@JsonSerializable()
class ServiceDetailLevel {
  String? levelId;
  String? name;
  String? description;
  String? actualPrice;
  String? discountedPrice;

  ServiceDetailLevel({
    this.levelId,
    this.name,
    this.description,
    this.actualPrice,
    this.discountedPrice,
  });

  factory ServiceDetailLevel.fromJson(Map<String, dynamic> json) => _$ServiceDetailLevelFromJson(json);

  Map<String, dynamic> toJson() => _$ServiceDetailLevelToJson(this);
}
