// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'car_brand_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CarBrandModel _$CarBrandModelFromJson(Map<String, dynamic> json) => CarBrandModel(
      id: json['id'] as String,
      title: json['title'] as String,
      isBrandNameSelected: json['isBrandNameSelected'] as bool?,
      models: (json['models'] as List<dynamic>).map((e) => CarTypeModel.fromJson(e as Map<String, dynamic>)).toList(),
    );

Map<String, dynamic> _$CarBrandModelToJson(CarBrandModel instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'isBrandNameSelected': instance.isBrandNameSelected,
      'models': instance.models,
    };

CarTypeModel _$CarTypeModelFromJson(Map<String, dynamic> json) => CarTypeModel(
      id: json['id'] as String,
      title: json['title'] as String,
      isCarTypeNameSelected: json['isCarTypeNameSelected'] as bool?,
    );

Map<String, dynamic> _$CarTypeModelToJson(CarTypeModel instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'isCarTypeNameSelected': instance.isCarTypeNameSelected,
    };

SubBrandCarModel _$SubBrandCarModelFromJson(Map<String, dynamic> json) => SubBrandCarModel(
      id: json['id'] as String,
      title: json['title'] as String,
      isSubBrandCarSelected: json['isSubBrandCarSelected'] as bool?,
    );

Map<String, dynamic> _$SubBrandCarModelToJson(SubBrandCarModel instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'isSubBrandCarSelected': instance.isSubBrandCarSelected,
    };
