// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sub_category_response_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubCategoryResponseModel _$SubCategoryResponseModelFromJson(Map<String, dynamic> json) => SubCategoryResponseModel(
      id: json['id'] as String,
      title: json['title'] as String,
    );

Map<String, dynamic> _$SubCategoryResponseModelToJson(SubCategoryResponseModel instance) => <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
    };
