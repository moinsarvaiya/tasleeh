// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_service_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubmittedServiceDataModel _$SubmittedServiceDataModelFromJson(Map<String, dynamic> json) => SubmittedServiceDataModel(
      category: json['category'] == null ? null : SubmittedCategory.fromJson(json['category'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$SubmittedServiceDataModelToJson(SubmittedServiceDataModel instance) => <String, dynamic>{
      'category': instance.category,
    };

SubmittedCategory _$SubmittedCategoryFromJson(Map<String, dynamic> json) => SubmittedCategory(
      serviceId: json['serviceId'] as String?,
      serviceName: json['serviceName'] as String?,
      subCategory: (json['subCategory'] as List<dynamic>?)?.map((e) => SubmittedSubCategory.fromJson(e as Map<String, dynamic>)).toList(),
    );

Map<String, dynamic> _$SubmittedCategoryToJson(SubmittedCategory instance) => <String, dynamic>{
      'serviceId': instance.serviceId,
      'serviceName': instance.serviceName,
      'subCategory': instance.subCategory,
    };

SubmittedSubCategory _$SubmittedSubCategoryFromJson(Map<String, dynamic> json) => SubmittedSubCategory(
      serviceDetailId: json['serviceDetailId'] as String?,
      sucategotyImage: json['sucategotyImage'] as String?,
      serviceDetailImage: json['serviceDetailImage'] as String?,
      name: json['name'] as String?,
    );

Map<String, dynamic> _$SubmittedSubCategoryToJson(SubmittedSubCategory instance) => <String, dynamic>{
      'serviceDetailId': instance.serviceDetailId,
      'sucategotyImage': instance.sucategotyImage,
      'serviceDetailImage': instance.serviceDetailImage,
      'name': instance.name,
    };
