import 'package:json_annotation/json_annotation.dart';

part 'car_model.g.dart';

@JsonSerializable()
class SelectedCarModel {
  String? id;
  String? title;
  String? imageUrl;
  bool? isBrandNameSelected;
  List<Models>? models;

  SelectedCarModel({this.id, this.title, this.imageUrl, this.models});

  factory SelectedCarModel.fromJson(Map<String, dynamic> json) => _$SelectedCarModelFromJson(json);

  Map<String, dynamic> toJson() => _$SelectedCarModelToJson(this);
}

@JsonSerializable()
class Models {
  String? id;
  String? title;
  String? imageUrl;

  Models({this.id, this.title, this.imageUrl});

  factory Models.fromJson(Map<String, dynamic> json) => _$ModelsFromJson(json);

  Map<String, dynamic> toJson() => _$ModelsToJson(this);
}
