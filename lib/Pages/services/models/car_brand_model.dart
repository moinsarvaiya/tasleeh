import 'package:json_annotation/json_annotation.dart';

part 'car_brand_model.g.dart';

@JsonSerializable()
class CarBrandModel {
  String id;
  String title;
  bool? isBrandNameSelected;
  List<CarTypeModel> models;

  CarBrandModel({
    required this.id,
    required this.title,
    this.isBrandNameSelected,
    required this.models,
  });

  factory CarBrandModel.fromJson(Map<String, dynamic> json) => _$CarBrandModelFromJson(json);

  Map<String, dynamic> toJson() => _$CarBrandModelToJson(this);
}

@JsonSerializable()
class CarTypeModel {
  String id;
  String title;
  bool? isCarTypeNameSelected;

  // List<SubBrandCarModel> subBrandCarModelList;

  CarTypeModel({
    required this.id,
    required this.title,
    this.isCarTypeNameSelected,
    // required this.subBrandCarModelList,
  });

  factory CarTypeModel.fromJson(Map<String, dynamic> json) => _$CarTypeModelFromJson(json);

  Map<String, dynamic> toJson() => _$CarTypeModelToJson(this);
}

@JsonSerializable()
class SubBrandCarModel {
  String id;
  String title;
  bool? isSubBrandCarSelected;

  SubBrandCarModel({
    required this.id,
    required this.title,
    this.isSubBrandCarSelected,
  });

  factory SubBrandCarModel.fromJson(Map<String, dynamic> json) => _$SubBrandCarModelFromJson(json);

  Map<String, dynamic> toJson() => _$SubBrandCarModelToJson(this);
}
