import 'package:al_baida_garage_fe/Utils/app_constants.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../Utils/utilities.dart';
import '../../common/common_function.dart';
import '../../common/hover_icon.dart';
import '../../common/input_text_field.dart';
import '../../common/space_horizontal.dart';
import '../../common/space_vertical.dart';
import '../../core/app_preference/app_preferences.dart';
import '../../core/app_preference/storage_keys.dart';
import '../../core/constant/app_color.dart';
import '../../core/constant/app_images.dart';
import '../../core/constant/base_style.dart';
import '../../core/constant/ui_constants.dart';
import '../../core/extensions/common_extension.dart';
import '../../custom_classes/custom_dropdown_field.dart';
import '../../custom_classes/custom_expansion_panel.dart';
import '../../custom_classes/custom_text.dart';
import '../../l10n/app_string_key.dart';
import '../custom_stepper/widgets/custom_square_corner_button.dart';
import 'models/car_model.dart';
import 'models/category_request_body_model.dart';
import 'models/missing_value.dart';
import 'models/service_data_model.dart';
import 'service_controller.dart';
import 'service_web_view.dart';
import 'widgets/added_service_data_mobile_view.dart';

class ServiceMobileView extends StatelessWidget {
  final ServiceController controller;
  final bool isReadOnly;

  const ServiceMobileView({
    super.key,
    required this.controller,
    required this.isReadOnly,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        isReadOnly
            ? const SizedBox.shrink()
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppStringKey.select_categories.tr,
                    style: BaseStyle.textStyleRobotoSemiBold(
                      20,
                      AppColors.primaryTextColor,
                    ),
                  ),
                  CustomTextButtonWithIcon(
                    buttonText: AppStringKey.add_category.tr,
                    callBack: () {
                      List<MissingValue> missingValues =
                          Get.find<ServiceController>().findMissingValues(Get.find<ServiceController>().categoryResponseModel);
                      if (missingValues.isNotEmpty) {
                        for (var missingValue in missingValues) {
                          Utilities.showToast(missingValue.message);
                          break;
                        }
                      } else {
                        controller.addCategoryWidget();
                      }
                    },
                    icon: Icons.add,
                    textStyle: BaseStyle.textStyleRobotoRegular(
                      14,
                      AppColors.primaryTextColor,
                    ),
                  )
                ],
              ),
        const SpaceVertical(0.05),
        Obx(
          () => Visibility(
            visible: controller.submittedServiceList.isNotEmpty && controller.redirectionFrom != ServiceRedirectionFrom.myAccount,
            child: Column(
              children: [
                ...controller.submittedServiceList.indexed.map(
                  (e) {
                    var submittedServiceDataModel = e.$2;
                    return AddedServiceDataMobileView(
                      title: submittedServiceDataModel.category!.serviceName ?? '',
                      description: AppStringKey.submitted_category_desc.tr,
                      subCategory: submittedServiceDataModel.category!.subCategory ?? [],
                      callBack: () {
                        controller.deleteService(submittedServiceDataModel.category!.serviceId ?? '', e.$1);
                      },
                      subCategoryCallBack: (String subCategoryId) {
                        controller.getSubServiceData(subCategoryId);
                      },
                    );
                  },
                ),
              ],
            ),
          ),
        ),
        const SpaceVertical(0.05),
        Obx(
          () => Column(
            children: [
              ...controller.addCategoryList.indexed.map(
                (item) {
                  return Column(
                    children: [
                      Ink(
                        child: InkWell(
                          child: CategoryWidget(
                            serviceModel: item.$2,
                            index: item.$1,
                            controller: controller,
                          ),
                        ),
                      ),
                      Visibility(
                        visible: controller.addCategoryList[item.$1].isCategoryViewVisible,
                        child: SubCategoryWidget(
                          serviceModel: item.$2,
                          index: item.$1,
                          controller: controller,
                        ),
                      )
                    ],
                  );
                },
              ),
            ],
          ),
        )
      ],
    );
  }
}

class CategoryWidget extends StatelessWidget {
  final ServiceModel serviceModel;
  final int index;
  final ServiceController controller;

  const CategoryWidget({
    super.key,
    required this.serviceModel,
    required this.index,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                CustomDropDownField(
                  selectedValue: controller.categorySelectedValue[index],
                  isReadOnly: controller.addCategoryList[index].isDropdownReadOnly,
                  width: context.width * 0.50,
                  labelVisibility: false,
                  labelText: AppStringKey.select_service.tr,
                  validationText: AppStringKey.select_service.tr,
                  hintTitle: AppStringKey.select_service.tr,
                  onChanged: (categoryName) {
                    controller.categorySelectedValue[index] = categoryName!;
                    controller.categoryResponseModel[index].category =
                        Category(id: controller.addCategoryList[index].categories.firstWhere((element) => element.title == categoryName).id);
                    controller.getSubCategories(
                      controller.addCategoryList[index].categories.firstWhere((element) => element.title == categoryName).id,
                      index,
                    );
                  },
                  items: controller.addCategoryList[index].categories.map((category) => category.title).toList(),
                ),
                const SpaceHorizontal(0.01),
              ],
            ),
            Row(
              children: [
                Material(
                  child: Ink(
                    child: InkWell(
                      onTap: () {
                        if (controller.addCategoryList[index].isCategoryViewVisible) {
                          controller.addCategoryList[index].isCategoryViewVisible = false;
                          controller.addCategoryList[index].isDropdownReadOnly = true;
                          controller.addCategoryList.refresh();
                        } else {
                          for (var element in controller.addCategoryList) {
                            element.isCategoryViewVisible = false;
                            element.isDropdownReadOnly = true;
                          }

                          controller.addCategoryList[index].isCategoryViewVisible = true;
                          controller.addCategoryList[index].isDropdownReadOnly = false;
                          controller.addCategoryList.refresh();
                        }
                      },
                      child: Container(
                        width: 25,
                        height: 25,
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 1,
                            color: AppColors.colorBlack,
                          ),
                        ),
                        child: Center(
                          child: Icon(
                            serviceModel.isCategoryViewVisible ? Icons.remove : Icons.add,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                const SpaceHorizontal(0.03),
                HoverIcon(
                  icon: AppImages.deleteIcon,
                  defaultColor: AppColors.colorBlack,
                  hoverColor: AppColors.errorTextColor,
                  onTap: () {
                    controller.categorySelectedValue.removeAt(index);
                    controller.removeCategory(index);
                  },
                  iconSize: 20,
                ),
              ],
            )
          ],
        ),
        const SpaceVertical(0.005),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            serviceModel.isCategoryViewVisible
                ? CustomTextButtonWithIcon(
                    buttonText: AppStringKey.add_sub_category.tr,
                    callBack: () {
                      if (controller.categorySelectedValue[index] != '') {
                        controller.getSubCategories(
                            controller.addCategoryList[index].categories
                                .firstWhere((element) => element.title == controller.categorySelectedValue)
                                .id,
                            index);
                      } else if (controller.categoryResponseModel[index].category.id == controller.addCategoryList[index].categories.first.id) {
                        controller.getSubCategories(controller.addCategoryList[index].categories.first.id, index);
                      } else {
                        Utilities.showToast(AppStringKey.please_delete_category_first.tr);
                      }
                    },
                    textStyle: BaseStyle.textStyleRobotoRegular(
                      12,
                      AppColors.primaryTextColor,
                    ),
                    icon: Icons.add,
                  )
                : controller.addCategoryList[index].subCategoryList.isNotEmpty
                    ? CustomTextButtonWithIcon(
                        buttonText: AppStringKey.sub_categories.tr,
                        callBack: () {},
                        textStyle: BaseStyle.textStyleRobotoRegular(
                          12,
                          AppColors.primaryTextColor,
                        ),
                        icon: Icons.done,
                      )
                    : const SizedBox.shrink(),
            const SpaceVertical(0.02),
            !controller.isAnyLevelListNotEmpty(serviceModel)
                ? const SizedBox.shrink()
                : CustomTextButtonWithIcon(
                    buttonText: AppStringKey.levels.tr,
                    callBack: () {},
                    textStyle: BaseStyle.textStyleRobotoRegular(
                      12,
                      AppColors.primaryTextColor,
                    ),
                    icon: Icons.done,
                  )
          ],
        ),
        const SpaceVertical(0.005),
      ],
    );
  }
}

class SubCategoryWidget extends StatefulWidget {
  final ServiceModel serviceModel;
  final int index;
  final ServiceController controller;

  const SubCategoryWidget({
    super.key,
    required this.serviceModel,
    required this.index,
    required this.controller,
  });

  @override
  State<SubCategoryWidget> createState() => _SubCategoryWidgetState();
}

class _SubCategoryWidgetState extends State<SubCategoryWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ...widget.controller.addCategoryList[widget.index].subCategoryList.indexed.map(
          (subCategory) {
            int subCategoryIndex = subCategory.$1;
            SubCategoryModel subCategoryModel = subCategory.$2;
            return Column(
              children: [
                Container(
                  padding: const EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: AppColors.colorDragImageBack,
                    border: Border.all(
                      width: 1,
                      color: AppColors.strokeColor,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Obx(
                                () => CustomDropDownField(
                                  selectedValue: widget.controller.subCategorySelectedValue[subCategoryIndex],
                                  isReadOnly: false,
                                  width: context.width * 0.50,
                                  labelVisibility: false,
                                  labelText: AppStringKey.select_service.tr,
                                  validationText: AppStringKey.select_service.tr,
                                  hintTitle: AppStringKey.select_service.tr,
                                  onChanged: (subCategoryValue) {
                                    widget.controller.subCategorySelectedValue[subCategoryIndex] =   subCategoryValue!;
                                    widget.controller.categoryResponseModel[widget.index].subCategoryDto[subCategoryIndex].subCategory = SubCategory(
                                      id: widget.controller.subCategoryList.firstWhere((element) => element.title == subCategoryValue).id,
                                    );
                                  },
                                  items: widget.controller.subCategoryList.map((category) => category.title).toList(),
                                ),
                              ),
                              HoverIcon(
                                icon: AppImages.deleteIcon,
                                defaultColor: AppColors.colorGrey,
                                hoverColor: AppColors.errorTextColor,
                                onTap: () {
                                  widget.controller.subCategorySelectedValue.removeAt(subCategoryIndex);
                                  widget.controller.removeSubCategory(widget.index, subCategoryIndex);
                                },
                              )
                            ],
                          ),
                          const SpaceVertical(0.002),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SearchModelWidget(
                                index: widget.index,
                                subCategoryIndex: subCategoryIndex,
                                textEditingController: subCategoryModel.textEditingController,
                              ),
                              Material(
                                child: Ink(
                                  child: InkWell(
                                    onTap: () {
                                      var subCategoryId =
                                          widget.controller.categoryResponseModel[widget.index].subCategoryDto[subCategoryIndex].subCategory!.id;
                                      if (subCategoryId != '') {
                                        widget.controller.getSelectedCars(
                                            widget.controller.categoryResponseModel[widget.index].subCategoryDto[subCategoryIndex].subCategory!.id);
                                      } else {
                                        Utilities.showToast(AppStringKey.select_sub_category.tr);
                                      }
                                    },
                                    child: CustomText(
                                      text: AppStringKey.view.tr,
                                      textStyle: BaseStyle.underLineTextStyle(
                                        13,
                                        AppColors.primaryTextColor,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SpaceVertical(0.02),
                      Row(
                        children: [
                          Visibility(
                            visible: subCategoryModel.subCategoryImagePath == null,
                            child: Row(
                              children: [
                                Material(
                                  color: Colors.transparent,
                                  child: Ink(
                                    child: InkWell(
                                      customBorder: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      onTap: () {
                                        CommonFunction.pickImages(0).then((value) {
                                          widget.controller.addCategoryList[widget.index].subCategoryList[subCategoryIndex].subCategoryImagePath =
                                              value.entries.first.value;
                                          widget.controller.addCategoryList.refresh();
                                          widget.controller.imageUpload(context, value.entries.first.value, subCategoryIndex);
                                        });
                                      },
                                      child: Container(
                                        width: context.width * 0.22,
                                        height: context.height * 0.05,
                                        decoration: DottedDecoration(
                                          shape: Shape.box,
                                          color: AppColors.strokeColor,
                                          borderRadius: BorderRadius.circular(5),
                                          strokeWidth: 1,
                                        ),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [
                                            const Icon(
                                              Icons.add,
                                              size: 22,
                                              color: AppColors.colorBlack,
                                            ),
                                            const SpaceVertical(0.02),
                                            CustomText(
                                              text: AppStringKey.upload_photo.tr,
                                              textStyle: BaseStyle.textStyleRobotoRegular(
                                                10,
                                                AppColors.primaryTextColor,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Visibility(
                            visible: subCategoryModel.subCategoryImagePath != null,
                            child: Container(
                              width: context.width * 0.22,
                              height: context.height * 0.10,
                              decoration: BoxDecoration(
                                color: AppColors.strokeColor,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Stack(
                                children: [
                                  subCategoryModel.subCategoryImagePath != null
                                      ? Positioned(
                                          top: 0,
                                          bottom: 0,
                                          left: 0,
                                          right: 0,
                                          child: Image.memory(
                                            subCategoryModel.subCategoryImagePath!,
                                            fit: BoxFit.cover,
                                          ),
                                        )
                                      : const SizedBox.shrink(),
                                  Positioned(
                                    right: 5,
                                    top: 5,
                                    child: Container(
                                      width: 17,
                                      height: 17,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: AppColors.colorGrey,
                                      ),
                                      child: HoverIcon(
                                        iconSize: 10,
                                        icon: AppImages.deleteIcon,
                                        defaultColor: AppColors.colorGrey,
                                        hoverColor: AppColors.errorTextColor,
                                        onTap: () {
                                          widget.controller.categoryResponseModel[widget.index].subCategoryDto[subCategoryIndex].imageUrl = '';
                                          widget.controller.addCategoryList[widget.index].subCategoryList[subCategoryIndex].subCategoryImagePath =
                                              null;
                                          widget.controller.addCategoryList.refresh();
                                        },
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                          const SpaceHorizontal(0.03),
                          CustomText(
                            maxLines: 2,
                            textAlign: TextAlign.center,
                            text: AppStringKey.upload_photo_desc.tr,
                            textStyle: BaseStyle.textStyleRobotoRegular(
                              12,
                              AppColors.primaryTextColor,
                            ),
                          )
                        ],
                      ),
                      const SpaceVertical(0.02),
                      Padding(
                        padding: const EdgeInsets.only(left: 5),
                        child: CustomText(
                          maxLines: 2,
                          textAlign: TextAlign.center,
                          text: AppStringKey.added_levels.tr,
                          textStyle: BaseStyle.textStyleRobotoSemiBold(
                            12,
                            AppColors.primaryTextColor,
                          ),
                        ),
                      ),
                      const SpaceVertical(0.01),
                      LevelWidget(
                        categoryIndex: widget.index,
                        subCategoryIndex: subCategoryIndex,
                        controller: widget.controller,
                      ),
                      const SpaceVertical(0.01),
                      Theme(
                        data: context.theme.copyWith(useMaterial3: false),
                        child: IntrinsicWidth(
                          child: Material(
                            color: Colors.transparent,
                            child: Ink(
                              child: InkWell(
                                customBorder: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                onTap: () {
                                  var controller = Get.find<ServiceController>();
                                  controller.clearLevelData();
                                  widget.controller.levelIsFromAdd = true;
                                  widget.controller.categoryIndexAddLevel = widget.index;
                                  widget.controller.subCategoryIndexAddLevel = subCategoryIndex;
                                  addLevelBottomSheet(context);
                                },
                                child: Container(
                                  padding: const EdgeInsets.only(top: 5, bottom: 5, right: 10),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Icon(
                                        Icons.add,
                                        size: 17,
                                        color: AppColors.colorBlack,
                                      ),
                                      const SpaceHorizontal(0.002),
                                      Text(
                                        AppStringKey.add_levels.tr,
                                        style: BaseStyle.textStyleRobotoSemiBold(
                                          12,
                                          AppColors.primaryTextColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SpaceVertical(0.01),
              ],
            );
          },
        ),
      ],
    );
  }
}

class LevelWidget extends StatelessWidget {
  final int categoryIndex;
  final int subCategoryIndex;
  final ServiceController controller;

  const LevelWidget({
    super.key,
    required this.categoryIndex,
    required this.subCategoryIndex,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ...controller.addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].levelList.indexed.map(
          (level) {
            int levelIndex = level.$1;
            LevelModel levelModel = level.$2;
            return Container(
              padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 10),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            width: 20,
                            height: 20,
                            decoration: BoxDecoration(
                              border: Border.all(
                                width: 1,
                                color: AppColors.colorBlack,
                              ),
                              borderRadius: BorderRadius.circular(1),
                            ),
                            child: Center(
                              child: CustomText(
                                textAlign: TextAlign.center,
                                text: levelModel.index.toString(),
                                textStyle: BaseStyle.textStyleRobotoRegular(
                                  8,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                            ),
                          ),
                          const SpaceHorizontal(0.04),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              CustomText(
                                textAlign: TextAlign.center,
                                text: levelModel.levelName,
                                textStyle: BaseStyle.textStyleRobotoBold(
                                  12,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              const SpaceVertical(0.01),
                              CustomText(
                                textAlign: TextAlign.center,
                                text: '${levelModel.levelDiscountedPrice} ${AppConstants.currencyString}',
                                textStyle: BaseStyle.textStyleRobotoRegular(
                                  12,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                          color: AppColors.dividerColor,
                          borderRadius: BorderRadius.circular(5),
                          border: Border.all(
                            color: AppColors.dividerColor,
                            width: 1,
                          ),
                        ),
                        child: PopupMenuButton<String>(
                          padding: EdgeInsets.zero,
                          splashRadius: 1,
                          icon: const Icon(
                            Icons.more_vert,
                            size: 10,
                            color: Colors.black, // Set your desired icon color
                          ),
                          surfaceTintColor: AppColors.colorWhite,
                          onSelected: (value) {
                            if (value == 'delete') {
                              controller.categoryIndex = categoryIndex;
                              controller.subCategoryIndex = subCategoryIndex;
                              controller.levelIndex = levelIndex;
                              controller.deleteLevel(levelModel.levelId!, levelIndex);
                            } else if (value == 'edit') {
                              controller.levelIsFromAdd = false;
                              controller.categoryIndexAddLevel = categoryIndex;
                              controller.subCategoryIndexAddLevel = subCategoryIndex;
                              controller.levelForEditSelectedIndex = levelIndex;
                              controller.manualLevelTitle = levelModel.levelName;

                              controller.addLevelPriceController.text = levelModel.levelPrice;
                              controller.addLevelDiscountedPriceController.text = levelModel.levelDiscountedPrice;
                              controller.addLevelDescriptionController.text = levelModel.levelDescription;
                              for (var element in controller.levelCheckboxList) {
                                element.isCheckboxSelected = false;
                              }
                              controller
                                  .levelCheckboxList[controller.levelCheckboxList
                                      .indexWhere((item) => item.title.toLowerCase() == levelModel.levelName.toLowerCase())]
                                  .isCheckboxSelected = true;
                              Scaffold.of(context).openEndDrawer();
                            }
                          },
                          itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
                            PopupMenuItem<String>(
                              value: 'edit',
                              height: 32,
                              padding: const EdgeInsets.symmetric(
                                vertical: 5,
                                horizontal: 12,
                              ),
                              child: Row(
                                children: [
                                  SvgPicture.asset(
                                    AppImages.leftPaneIcon,
                                    height: 16,
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    AppStringKey.edit.tr,
                                    style: BaseStyle.textStyleRobotoRegular(
                                      14,
                                      AppColors.primaryTextColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            PopupMenuItem<String>(
                              value: 'delete',
                              height: 32,
                              padding: const EdgeInsets.symmetric(
                                vertical: 5,
                                horizontal: 12,
                              ),
                              child: Row(
                                children: [
                                  const Icon(
                                    Icons.delete_outline_rounded,
                                    size: 16,
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    AppStringKey.delete.tr,
                                    style: BaseStyle.textStyleRobotoRegular(
                                      14,
                                      AppColors.primaryTextColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  const SpaceVertical(0.01),
                  const Divider(
                    height: 1,
                    color: AppColors.dividerColor,
                  ),
                  const SpaceVertical(0.01),
                ],
              ),
            );
          },
        )
      ],
    );
  }
}

class CustomTextButtonWithIcon extends StatelessWidget {
  final String buttonText;
  final IconData icon;
  final Function() callBack;
  final TextStyle textStyle;

  const CustomTextButtonWithIcon({
    super.key,
    required this.buttonText,
    required this.callBack,
    required this.icon,
    required this.textStyle,
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: callBack,
      style: TextButton.styleFrom(
        textStyle: textStyle,
        shape: const BeveledRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(2),
          ),
        ),
        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 15),
        foregroundColor: AppColors.primaryTextColor,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            icon,
            size: 17,
            color: AppColors.colorBlack,
          ),
          const SpaceHorizontal(0.002),
          Text(
            buttonText,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }
}

void selectedCarMobileDialog(BuildContext context, RxList<SelectedCarModel> selectedCarList) {
  showModalBottomSheet(
    constraints: BoxConstraints(maxHeight: context.screenHeight * 0.85),
    isScrollControlled: true,
    context: context,
    builder: (builder) {
      return GestureDetector(
        onTap: () {
          Get.back();
        },
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
            child: GestureDetector(
              onTap: () {},
              child: Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 8,
                  vertical: 8,
                ),
                width: double.maxFinite,
                height: context.screenHeight,
                color: Colors.white,
                child: Stack(
                  children: [
                    Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 10,
                        vertical: 10,
                      ),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppStringKey.selected_car.tr,
                              style: BaseStyle.textStyleRobotoSemiBold(
                                24,
                                AppColors.primaryTextColor,
                              ),
                            ),
                            const SpaceVertical(0.001),
                            Text(
                              AppStringKey.selected_car_desc.tr,
                              style: BaseStyle.textStyleRobotoRegular(
                                14,
                                AppColors.secondaryTextColor,
                              ),
                              maxLines: 2,
                            ),
                            const SpaceVertical(0.03),
                            Obx(
                              () => Column(
                                children: [
                                  ...selectedCarList.indexed.map(
                                    (e) {
                                      int index = e.$1;
                                      SelectedCarModel carModel = e.$2;
                                      return Container(
                                        padding: const EdgeInsets.all(10),
                                        margin: const EdgeInsets.only(top: 10),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: AppColors.borderColor,
                                            width: 0.5,
                                          ),
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                        child: CustomExpansionPanel(
                                          body: SelectedCarsDialogBody(
                                            controller: Get.find<ServiceController>(),
                                            carsModel: carModel,
                                            index: index,
                                          ),
                                          header: SelectedCarsDialogHeader(
                                            controller: Get.find<ServiceController>(),
                                            carsModel: carModel,
                                            index: index,
                                          ),
                                          onTap: () {
                                            Get.find<ServiceController>().selectedCarList.forEach((element) {
                                              element.isBrandNameSelected = false;
                                            });
                                            Get.find<ServiceController>().selectedCarList[index].isBrandNameSelected = true;
                                            Get.find<ServiceController>().selectedCarList.refresh();
                                          },
                                          isOpenDefault: carModel.isBrandNameSelected ?? true,
                                          key: UniqueKey(),
                                        ),
                                      );
                                    },
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      right: 00,
                      top: 10,
                      child: Material(
                        color: Colors.transparent,
                        child: Ink(
                          child: InkWell(
                            onTap: () {
                              Get.back();
                            },
                            child: const Icon(
                              Icons.close,
                              color: AppColors.colorBlack,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ), //Put your screen design here!
        ),
      );
    },
  );
}

void addLevelBottomSheet(BuildContext context) {
  showModalBottomSheet(
    backgroundColor: Colors.white,
    constraints: BoxConstraints(maxHeight: context.screenHeight * 0.85),
    isScrollControlled: true,
    context: context,
    builder: (builder) {
      return GestureDetector(
        onTap: () {
          Get.back();
        },
        child: ClipRect(
          clipBehavior: Clip.antiAlias,
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(0),
                topRight: Radius.circular(0),
              ),
            ),
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        child: SizedBox(
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppStringKey.add_levels.tr,
                                style: BaseStyle.textStyleRobotoBold(
                                  20,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              const SpaceVertical(0.01),
                              Text(
                                AppStringKey.add_levels_desc.tr,
                                style: BaseStyle.textStyleRobotoRegular(
                                  12,
                                  AppColors.primaryTextColor,
                                ),
                                maxLines: 2,
                              ),
                              const SpaceVertical(0.02),
                              Container(
                                width: double.infinity,
                                height: 2,
                                color: AppColors.dividerColor,
                              ),
                              const SpaceVertical(0.02),
                              Text(
                                AppStringKey.select_your_level.tr,
                                style: BaseStyle.textStyleRobotoSemiBold(
                                  18,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              const SpaceVertical(0.02),
                              Obx(
                                () => ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: Get.find<ServiceController>().levelCheckboxList.length,
                                  itemBuilder: (context, index) {
                                    return Column(
                                      children: [
                                        Material(
                                          color: Colors.transparent,
                                          child: Ink(
                                            child: InkWell(
                                              onTap: () {
                                                var controller = Get.find<ServiceController>();
                                                if (controller.levelCheckboxList[index].isCheckboxSelected!) {
                                                  controller.levelCheckboxList[index].isCheckboxSelected = false;
                                                } else {
                                                  controller.levelCheckboxList[index].isCheckboxSelected = true;
                                                }
                                                controller.addLevelDescriptionController.clear();
                                                controller.addLevelPriceController.clear();
                                                controller.addLevelDiscountedPriceController.clear();
                                                controller.levelCheckboxIndex = index;
                                                // for (var element in controller.levelCheckboxList) {
                                                //   element.isCheckboxSelected = false;
                                                // }
                                                // controller.levelCheckboxList[index].isCheckboxSelected = true;
                                                controller.levelCheckboxList.refresh();
                                              },
                                              child: Container(
                                                padding: const EdgeInsets.all(5),
                                                child: Row(
                                                  children: [
                                                    Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(5),
                                                        color: Get.find<ServiceController>().levelCheckboxList[index].isCheckboxSelected ?? false
                                                            ? AppColors.colorBlack
                                                            : Colors.transparent,
                                                        border: Border.all(
                                                          color: AppColors.colorBlack,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      width: 20,
                                                      height: 20,
                                                      child: Get.find<ServiceController>().levelCheckboxList[index].isCheckboxSelected ?? false
                                                          ? Icon(
                                                              Icons.done,
                                                              size: 15,
                                                              color:
                                                                  Get.find<ServiceController>().levelCheckboxList[index].isCheckboxSelected ?? false
                                                                      ? AppColors.colorWhite
                                                                      : AppColors.colorBlack,
                                                            )
                                                          : const SizedBox.shrink(),
                                                    ),
                                                    const SpaceHorizontal(0.005),
                                                    Text(
                                                      Get.find<ServiceController>().levelCheckboxList[index].title,
                                                      style: BaseStyle.textStyleRobotoMedium(
                                                        15,
                                                        AppColors.colorBlack,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Get.find<ServiceController>().levelCheckboxList[index].isCheckboxSelected ?? false
                                            ? Container(
                                                padding: const EdgeInsets.all(5),
                                                child: Column(
                                                  children: [
                                                    InputTextField(
                                                      textInputType: TextInputType.phone,
                                                      keyBoardAction: TextInputAction.next,
                                                      inputFormatter: [
                                                        FilteringTextInputFormatter.digitsOnly,
                                                      ],
                                                      label: '',
                                                      hintText: AppStringKey.enter_amount.tr,
                                                      isPhoneNo: false,
                                                      isLabelVisible: false,
                                                      isRequireField: false,
                                                      textEditingController: Get.find<ServiceController>().addLevelPriceController,
                                                      onChangeWithValidationStatus: (val, isValid) {
                                                        Get.find<ServiceController>().levelPrice.value = val ?? '';
                                                      },
                                                    ),
                                                    InputTextField(
                                                      textInputType: TextInputType.phone,
                                                      keyBoardAction: TextInputAction.next,
                                                      inputFormatter: [
                                                        FilteringTextInputFormatter.digitsOnly,
                                                      ],
                                                      label: '',
                                                      hintText: AppStringKey.enter_discount_amount.tr,
                                                      isPhoneNo: false,
                                                      isLabelVisible: false,
                                                      isRequireField: false,
                                                      textEditingController: Get.find<ServiceController>().addLevelDiscountedPriceController,
                                                      onChangeWithValidationStatus: (val, isValid) {
                                                        // updateAddButton();
                                                        Get.find<ServiceController>().levelDiscountedPrice.value = val ?? '';
                                                      },
                                                    ),
                                                    InputTextField(
                                                      label: '',
                                                      hintText: AppStringKey.enter_level_desc.tr,
                                                      isPhoneNo: false,
                                                      isLabelVisible: false,
                                                      isRequireField: false,
                                                      textEditingController: Get.find<ServiceController>().addLevelDescriptionController,
                                                      maxLines: 3,
                                                      onChangeWithValidationStatus: (val, isValid) {
                                                        Get.find<ServiceController>().levelDescription.value = val ?? '';
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              )
                                            : const SizedBox.shrink(),
                                        const SpaceVertical(0.02),
                                      ],
                                    );
                                  },
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.all(5),
                                child: InputTextField(
                                  label: '',
                                  hintText: AppStringKey.enter_manually.tr,
                                  isPhoneNo: false,
                                  isLabelVisible: false,
                                  isRequireField: false,
                                  textEditingController: Get.find<ServiceController>().addManuallyLevelController,
                                  icon: Material(
                                    color: Colors.transparent,
                                    child: Ink(
                                      child: InkWell(
                                        onTap: () {
                                          var controller = Get.find<ServiceController>();
                                          if (controller.addManuallyLevelController.text != '') {
                                            var controller = Get.find<ServiceController>();
                                            bool isValueExist =
                                                controller.levelCheckboxList.any((item) => item.title == controller.addManuallyLevelController.text);

                                            if (!isValueExist) {
                                              controller.submitLevelData(
                                                  controller.addManuallyLevelController.text, AppPreferences.sharedPrefRead(StorageKeys.garageId));
                                            } else {
                                              Utilities.showToast(AppStringKey.level_already_added.tr);
                                            }
                                          } else {
                                            Utilities.showToast(AppStringKey.enter_level_name.tr);
                                          }
                                        },
                                        child: SizedBox(
                                          width: 50,
                                          child: Row(
                                            children: [
                                              Container(
                                                height: 50,
                                                width: 2,
                                                color: AppColors.dividerColor,
                                              ),
                                              const SizedBox(
                                                width: 12,
                                              ),
                                              const Icon(Icons.arrow_forward),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              const SpaceVertical(0.05),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          height: 30,
                          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                          decoration: BoxDecoration(
                            color: AppColors.lightBlueBorderColor,
                            borderRadius: BorderRadius.circular(2),
                            border: Border.all(
                              color: AppColors.lightBlueBackgroundColor,
                              width: 1,
                            ),
                          ),
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                AppImages.marketingDescIcon,
                                width: 18,
                                height: 18,
                              ),
                              const SpaceHorizontal(0.005),
                              CustomText(
                                text: AppStringKey.added_dialog_bottom_desc.tr,
                                textStyle: BaseStyle.textStyleRobotoRegular(10, AppColors.colorBlack),
                                maxLines: 2,
                              ),
                            ],
                          ),
                        ),
                        const SpaceVertical(0.02),
                        Container(
                          width: double.infinity,
                          height: 2,
                          color: AppColors.dividerColor,
                        ),
                        const SpaceVertical(0.02),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Obx(
                              () => CustomSquareCornerButton(
                                buttonText: AppStringKey.add.tr,
                                buttonTextColor: Get.find<ServiceController>().levelPrice.value == '' ||
                                        Get.find<ServiceController>().levelDiscountedPrice.value == '' ||
                                        Get.find<ServiceController>().levelDescription.value == ''
                                    ? AppColors.strokeColor
                                    : AppColors.colorWhite,
                                backgroundColor: Get.find<ServiceController>().levelPrice.value == '' ||
                                        Get.find<ServiceController>().levelDiscountedPrice.value == '' ||
                                        Get.find<ServiceController>().levelDescription.value == ''
                                    ? AppColors.inActiveCtaColor
                                    : AppColors.colorBlack,
                                height: 0.06,
                                width: 0.6,
                                textSize: 12,
                                callBack: () {
                                  var controller = Get.find<ServiceController>();

                                  bool isLevelAvailableInMainList = controller.addCategoryList[controller.categoryIndexAddLevel]
                                      .subCategoryList[controller.subCategoryIndexAddLevel].levelList
                                      .any((element) =>
                                          element.levelName.toLowerCase() ==
                                          controller.levelCheckboxList.firstWhere((item) => item.isCheckboxSelected == true).title.toLowerCase());
                                  /*  if (isLevelAvailableInMainList) {
                                Utilities.showToast("Level Already Added...");
                              } else {*/
                                  if (double.parse(controller.addLevelDiscountedPriceController.text) <
                                      double.parse(controller.addLevelPriceController.text)) {
                                    controller.addLevel(
                                        controller.categoryIndexAddLevel,
                                        controller.subCategoryIndexAddLevel,
                                        controller.levelCheckboxList.firstWhere((item) => item.isCheckboxSelected == true).title,
                                        controller.addLevelPriceController.text,
                                        controller.addLevelDiscountedPriceController.text,
                                        controller.addLevelDescriptionController.text,
                                        controller.levelCheckboxList.firstWhere((item) => item.isCheckboxSelected == true).id);

                                    if (controller.levelIsFromAdd) {
                                      controller.categoryResponseModel[controller.categoryIndex].subCategoryDto[controller.subCategoryIndexAddLevel]
                                          .levelDto!
                                          .add(
                                        LevelDto(
                                          level: Level(id: controller.levelCheckboxList.firstWhere((item) => item.isCheckboxSelected!).id),
                                          description: controller.addLevelDescriptionController.value.text,
                                          price: double.parse(controller.addLevelPriceController.text),
                                          discountedPrice: double.parse(controller.addLevelDiscountedPriceController.text),
                                        ),
                                      );
                                    } else {
                                      controller.categoryResponseModel[controller.categoryIndex].subCategoryDto[controller.subCategoryIndexAddLevel]
                                          .levelDto![controller.levelForEditSelectedIndex] = LevelDto(
                                        level: Level(id: controller.levelCheckboxList.firstWhere((item) => item.isCheckboxSelected == true).id),
                                        description: controller.addLevelDescriptionController.value.text,
                                        price: double.parse(controller.addLevelPriceController.text),
                                        discountedPrice: double.parse(controller.addLevelDiscountedPriceController.text),
                                      );
                                    }

                                    controller.addManuallyLevelController.clear();
                                    controller.addLevelPriceController.clear();
                                    controller.addLevelDiscountedPriceController.clear();
                                    controller.addLevelDescriptionController.clear();

                                    for (var element in controller.levelCheckboxList) {
                                      element.isCheckboxSelected = false;
                                    }
                                    controller.levelCheckboxList.refresh();

                                    Get.back();
                                  } else {
                                    Utilities.showToast(AppStringKey.enter_valid_discount_price.tr);
                                  }
                                  // }
                                },
                              ),
                            ),
                            const SpaceVertical(0.015),
                            Material(
                              child: Ink(
                                child: InkWell(
                                  onTap: () {
                                    var controller = Get.find<ServiceController>();
                                    controller.clearLevelData();
                                  },
                                  child: Text(
                                    AppStringKey.reset.tr,
                                    style: BaseStyle.textStyleRobotoMedium(
                                      12,
                                      AppColors.colorBlack,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Positioned(
                  right: 00,
                  top: 00,
                  child: Material(
                    color: Colors.transparent,
                    child: Ink(
                      child: InkWell(
                        onTap: () {
                          var controller = Get.find<ServiceController>();
                          controller.clearLevelData();
                          Get.back();
                        },
                        child: const Icon(
                          Icons.close,
                          color: AppColors.colorBlack,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );
    },
  );
}

class SelectedCarsDialogHeader extends StatelessWidget {
  final ServiceController controller;
  final SelectedCarModel carsModel;
  final int index;

  const SelectedCarsDialogHeader({
    super.key,
    required this.controller,
    required this.carsModel,
    required this.index,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(carsModel.isBrandNameSelected ?? true ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down),
        Text(
          carsModel.title ?? '',
          style: BaseStyle.textStyleRobotoSemiBold(
            15,
            AppColors.primaryTextColor,
          ),
        ),
      ],
    );
  }
}

class SelectedCarsDialogBody extends StatelessWidget {
  final ServiceController controller;
  final SelectedCarModel carsModel;
  final int index;

  const SelectedCarsDialogBody({
    super.key,
    required this.controller,
    required this.carsModel,
    required this.index,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.only(top: 10, left: 7),
      decoration: BoxDecoration(
        color: AppColors.colorDragImageBack,
        border: Border.all(
          color: AppColors.strokeColor,
          width: 0.5,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ...Get.find<ServiceController>().selectedCarList[index].models!.indexed.map(
                (e) => Column(
                  children: [
                    const SpaceVertical(0.01),
                    Text(e.$2.title ?? ''),
                    const SpaceVertical(0.01),
                  ],
                ),
              ),
        ],
      ),
    );
  }
}
