import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';
import 'package:image_network/image_network.dart';

import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/base_style.dart';
import '../../../common/hover_icon.dart';
import '../../../core/constant/app_images.dart';
import '../models/get_service_data_model.dart';

class AddedServiceDataWidget extends StatelessWidget {
  final String title;
  final String description;
  final List<SubmittedSubCategory> subCategory;
  final Function() callBack;
  final Function(String subCategoryId) subCategoryCallBack;

  const AddedServiceDataWidget({
    required this.title,
    required this.description,
    Key? key,
    required this.subCategory,
    required this.callBack,
    required this.subCategoryCallBack,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 5),
      padding: EdgeInsets.symmetric(horizontal: context.screenWidth * 0.02, vertical: context.screenHeight * 0.02),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: AppColors.borderColor,
        ),
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title,
                  style: BaseStyle.textStyleRobotoSemiBold(
                    18,
                    AppColors.primaryTextColor,
                  ),
                ),
                Text(
                  description,
                  style: BaseStyle.textStyleRobotoMediumBold(
                    12,
                    AppColors.inCompletedStepTextColor,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 3,
            child: GridView.count(
              crossAxisCount: 2,
              shrinkWrap: true,
              childAspectRatio: 5,
              crossAxisSpacing: 5,
              mainAxisSpacing: 5,
              children: List.generate(
                subCategory.length,
                (index) {
                  return Row(
                    children: [
                      Builder(builder: (context) {
                        imageCache.clear();
                        return ImageNetwork(
                          key: ValueKey(subCategory[index].serviceDetailImage),
                          image: subCategory[index].serviceDetailImage ?? '',
                          width: 32,
                          height: 32,
                          fitWeb: BoxFitWeb.cover,
                        );
                      }),
                      const SizedBox(
                        width: 12,
                      ),
                      Text(
                        subCategory[index].name ?? '',
                        style: BaseStyle.textStyleRobotoMediumBold(
                          10,
                          AppColors.inCompletedStepTextColor,
                        ),
                        maxLines: 3,
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                      Material(
                        child: Ink(
                          child: InkWell(
                            onTap: () {
                              subCategoryCallBack(subCategory[index].serviceDetailId ?? '');
                            },
                            child: const Icon(
                              Icons.edit,
                              size: 12,
                            ),
                          ),
                        ),
                      )
                    ],
                  );
                },
              ),
            ),
          ),
          HoverIcon(
            icon: AppImages.deleteIcon,
            defaultColor: AppColors.secondaryTextColor,
            hoverColor: AppColors.errorTextColor,
            onTap: callBack,
          )
        ],
      ),
    );
  }
}
