import 'package:al_baida_garage_fe/Pages/services/models/category_request_body_model.dart';
import 'package:al_baida_garage_fe/Pages/services/service_controller.dart';
import 'package:al_baida_garage_fe/Utils/app_constants.dart';
import 'package:al_baida_garage_fe/Utils/utilities.dart';
import 'package:al_baida_garage_fe/common/space_vertical.dart';
import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../common/input_text_field.dart';
import '../../common/space_horizontal.dart';
import '../../core/app_preference/app_preferences.dart';
import '../../core/app_preference/storage_keys.dart';
import '../../core/constant/app_color.dart';
import '../../core/constant/app_images.dart';
import '../../core/constant/base_style.dart';
import '../../custom_classes/custom_text.dart';
import '../../l10n/app_string_key.dart';
import '../custom_stepper/widgets/custom_square_corner_button.dart';

class AddLevelDrawerWidget extends StatefulWidget {
  const AddLevelDrawerWidget({super.key});

  @override
  State<AddLevelDrawerWidget> createState() => _AddLevelDrawerWidgetState();
}

class _AddLevelDrawerWidgetState extends State<AddLevelDrawerWidget> {
  void updateAddButton() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      surfaceTintColor: AppColors.colorWhite,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(00)),
      ),
      backgroundColor: AppColors.colorWhite,
      width: context.screenWidth * 0.3,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: SizedBox(
                      width: double.infinity,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppStringKey.add_levels.tr,
                            style: BaseStyle.textStyleRobotoBold(
                              20,
                              AppColors.primaryTextColor,
                            ),
                          ),
                          const SpaceVertical(0.01),
                          Text(
                            AppStringKey.add_levels_desc.tr,
                            style: BaseStyle.textStyleRobotoRegular(
                              12,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                          ),
                          const SpaceVertical(0.02),
                          Container(
                            width: double.infinity,
                            height: 2,
                            color: AppColors.dividerColor,
                          ),
                          const SpaceVertical(0.02),
                          Text(
                            AppStringKey.select_your_level.tr,
                            style: BaseStyle.textStyleRobotoSemiBold(
                              18,
                              AppColors.primaryTextColor,
                            ),
                          ),
                          const SpaceVertical(0.02),
                          Obx(
                            () => ListView.builder(
                              shrinkWrap: true,
                              itemCount: Get.find<ServiceController>().levelCheckboxList.length,
                              itemBuilder: (context, index) {
                                return Column(
                                  children: [
                                    Material(
                                      color: Colors.transparent,
                                      child: Ink(
                                        child: InkWell(
                                          onTap: () {
                                            var controller = Get.find<ServiceController>();
                                            if(controller.levelCheckboxList[index].isCheckboxSelected ?? false){
                                              controller.levelCheckboxList[index].isCheckboxSelected = false;
                                            }else{
                                              controller.levelCheckboxList[index].isCheckboxSelected = true;
                                            }
                                            controller.addLevelDescriptionController.clear();
                                            controller.addLevelPriceController.clear();
                                            controller.addLevelDiscountedPriceController.clear();
                                            controller.levelCheckboxIndex = index;
                                            // for (var element in controller.levelCheckboxList) {
                                            //   element.isCheckboxSelected = false;
                                            // }
                                            // controller.levelCheckboxList[index].isCheckboxSelected = true;
                                            controller.levelCheckboxList.refresh();
                                          },
                                          child: Container(
                                            padding: const EdgeInsets.all(5),
                                            child: Row(
                                              children: [
                                                Container(
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.circular(5),
                                                    color: Get.find<ServiceController>().levelCheckboxList[index].isCheckboxSelected ?? false
                                                        ? AppColors.colorBlack
                                                        : Colors.transparent,
                                                    border: Border.all(
                                                      color: AppColors.colorBlack,
                                                      width: 1,
                                                    ),
                                                  ),
                                                  width: 20,
                                                  height: 20,
                                                  child: Get.find<ServiceController>().levelCheckboxList[index].isCheckboxSelected ?? false
                                                      ? Icon(
                                                          Icons.done,
                                                          size: 15,
                                                          color: Get.find<ServiceController>().levelCheckboxList[index].isCheckboxSelected ?? false
                                                              ? AppColors.colorWhite
                                                              : AppColors.colorBlack,
                                                        )
                                                      : const SizedBox.shrink(),
                                                ),
                                                const SpaceHorizontal(0.005),
                                                Text(
                                                  Get.find<ServiceController>().levelCheckboxList[index].title,
                                                  style: BaseStyle.textStyleRobotoMedium(
                                                    15,
                                                    AppColors.colorBlack,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Get.find<ServiceController>().levelCheckboxList[index].isCheckboxSelected ?? false
                                        ? Container(
                                            padding: const EdgeInsets.all(5),
                                            child: Column(
                                              children: [
                                                InputTextField(
                                                  textInputType: TextInputType.phone,
                                                  keyBoardAction: TextInputAction.next,
                                                  inputFormatter: [
                                                    FilteringTextInputFormatter.digitsOnly,
                                                  ],
                                                  label: '',
                                                  hintText: AppStringKey.enter_amount.tr,
                                                  isPhoneNo: false,
                                                  isLabelVisible: false,
                                                  isRequireField: false,
                                                  textEditingController: Get.find<ServiceController>().addLevelPriceController,
                                                  onChangeWithValidationStatus: (val, isValid) {
                                                    updateAddButton();
                                                  },
                                                ),
                                                InputTextField(
                                                  textInputType: TextInputType.phone,
                                                  keyBoardAction: TextInputAction.next,
                                                  inputFormatter: [
                                                    FilteringTextInputFormatter.digitsOnly,
                                                  ],
                                                  label: '',
                                                  hintText: "${AppStringKey.enter_discount_amount.tr} ${AppConstants.currencyString}",
                                                  isPhoneNo: false,
                                                  isLabelVisible: false,
                                                  isRequireField: false,
                                                  textEditingController: Get.find<ServiceController>().addLevelDiscountedPriceController,
                                                  onChangeWithValidationStatus: (val, isValid) {
                                                    updateAddButton();
                                                  },
                                                ),
                                                InputTextField(
                                                  label: '',
                                                  hintText: AppStringKey.enter_level_desc.tr,
                                                  isPhoneNo: false,
                                                  isLabelVisible: false,
                                                  isRequireField: false,
                                                  textEditingController: Get.find<ServiceController>().addLevelDescriptionController,
                                                  maxLines: 3,
                                                  onChangeWithValidationStatus: (val, isValid) {
                                                    updateAddButton();
                                                  },
                                                ),
                                              ],
                                            ),
                                          )
                                        : const SizedBox.shrink(),
                                    const SpaceVertical(0.02),
                                  ],
                                );
                              },
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.all(5),
                            child: InputTextField(
                              label: '',
                              hintText: AppStringKey.enter_manually.tr,
                              isPhoneNo: false,
                              isLabelVisible: false,
                              isRequireField: false,
                              textEditingController: Get.find<ServiceController>().addManuallyLevelController,
                              icon: Material(
                                color: Colors.transparent,
                                child: Ink(
                                  child: InkWell(
                                    onTap: () {
                                      var controller = Get.find<ServiceController>();
                                      if (controller.addManuallyLevelController.text != '') {
                                        var controller = Get.find<ServiceController>();
                                        bool isValueExist =
                                            controller.levelCheckboxList.any((item) => item.title == controller.addManuallyLevelController.text);

                                        if (!isValueExist) {
                                          controller.submitLevelData(
                                              controller.addManuallyLevelController.text, AppPreferences.sharedPrefRead(StorageKeys.garageId));
                                        } else {
                                          Utilities.showToast('${AppStringKey.level_already_added.tr}...');
                                        }
                                      } else {
                                        Utilities.showToast(AppStringKey.enter_level_name.tr);
                                      }
                                    },
                                    child: SizedBox(
                                      width: 50,
                                      child: Row(
                                        children: [
                                          Container(
                                            height: 50,
                                            width: 2,
                                            color: AppColors.dividerColor,
                                          ),
                                          const SizedBox(
                                            width: 12,
                                          ),
                                          const Icon(Icons.arrow_forward),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const SpaceVertical(0.05),
                        ],
                      ),
                    ),
                  ),
                ),
                Column(
                  children: [
                    Container(
                      height: 30,
                      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      decoration: BoxDecoration(
                        color: AppColors.lightBlueBorderColor,
                        borderRadius: BorderRadius.circular(2),
                        border: Border.all(
                          color: AppColors.lightBlueBackgroundColor,
                          width: 1,
                        ),
                      ),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            AppImages.marketingDescIcon,
                            width: 18,
                            height: 18,
                          ),
                          const SpaceHorizontal(0.005),
                          CustomText(
                            text: AppStringKey.added_dialog_bottom_desc.tr,
                            textStyle: BaseStyle.textStyleRobotoRegular(5, AppColors.colorBlack),
                            maxLines: 2,
                          ),
                        ],
                      ),
                    ),
                    const SpaceVertical(0.02),
                    Container(
                      width: double.infinity,
                      height: 2,
                      color: AppColors.dividerColor,
                    ),
                    const SpaceVertical(0.02),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomSquareCornerButton(
                          buttonText: AppStringKey.reset.tr,
                          buttonTextColor: AppColors.colorBlack,
                          backgroundColor: AppColors.colorBackground,
                          height: 0.025,
                          width: 0.10,
                          textSize: 7,
                          callBack: Get.find<ServiceController>().addLevelDescriptionController.value.text != '' ||
                                  Get.find<ServiceController>().addLevelDiscountedPriceController.value.text != '' ||
                                  Get.find<ServiceController>().addLevelPriceController.value.text != ''
                              ? () {
                                  var controller = Get.find<ServiceController>();
                                  controller.clearLevelData();
                                }
                              : null,
                        ),
                        const SpaceHorizontal(0.01),
                        CustomSquareCornerButton(
                          buttonText: AppStringKey.add.tr,
                          buttonTextColor: Get.find<ServiceController>().addLevelDescriptionController.value.text == '' ||
                                  Get.find<ServiceController>().addLevelDiscountedPriceController.value.text == '' ||
                                  Get.find<ServiceController>().addLevelPriceController.value.text == ''
                              ? AppColors.strokeColor
                              : AppColors.colorWhite,
                          backgroundColor: Get.find<ServiceController>().addLevelDescriptionController.value.text == '' ||
                                  Get.find<ServiceController>().addLevelDiscountedPriceController.value.text == '' ||
                                  Get.find<ServiceController>().addLevelPriceController.value.text == ''
                              ? AppColors.inActiveCtaColor
                              : AppColors.colorBlack,
                          height: 0.025,
                          width: 0.10,
                          textSize: 7,
                          callBack: () {
                            var controller = Get.find<ServiceController>();

                            bool isLevelAvailableInMainList = controller
                                .addCategoryList[controller.categoryIndexAddLevel].subCategoryList[controller.subCategoryIndexAddLevel].levelList
                                .any((element) =>
                                    element.levelName.toLowerCase() ==
                                    controller.levelCheckboxList.firstWhere((item) => item.isCheckboxSelected == true).title.toLowerCase());
                            /*  if (isLevelAvailableInMainList) {
                              Utilities.showToast("Level Already Added...");
                            } else {*/
                            if (double.parse(controller.addLevelDiscountedPriceController.text) <
                                double.parse(controller.addLevelPriceController.text)) {
                              controller.addLevel(
                                  controller.categoryIndexAddLevel,
                                  controller.subCategoryIndexAddLevel,
                                  controller.levelCheckboxList.firstWhere((item) => item.isCheckboxSelected == true).title,
                                  controller.addLevelPriceController.text,
                                  controller.addLevelDiscountedPriceController.text,
                                  controller.addLevelDescriptionController.text,
                                  controller.levelCheckboxList.firstWhere((item) => item.isCheckboxSelected == true).id);

                              if (controller.levelIsFromAdd) {
                                controller
                                    .categoryResponseModel[controller.categoryIndex].subCategoryDto[controller.subCategoryIndexAddLevel].levelDto!
                                    .add(
                                  LevelDto(
                                    level: Level(id: controller.levelCheckboxList.firstWhere((item) => item.isCheckboxSelected!).id),
                                    description: controller.addLevelDescriptionController.value.text,
                                    price: double.parse(controller.addLevelPriceController.text),
                                    discountedPrice: double.parse(controller.addLevelDiscountedPriceController.text),
                                  ),
                                );
                              } else {
                                controller.categoryResponseModel[controller.categoryIndex].subCategoryDto[controller.subCategoryIndexAddLevel]
                                    .levelDto![controller.levelForEditSelectedIndex] = LevelDto(
                                  level: Level(id: controller.levelCheckboxList.firstWhere((item) => item.isCheckboxSelected == true).id),
                                  description: controller.addLevelDescriptionController.value.text,
                                  price: double.parse(controller.addLevelPriceController.text),
                                  discountedPrice: double.parse(controller.addLevelDiscountedPriceController.text),
                                );
                              }

                              controller.addManuallyLevelController.clear();
                              controller.addLevelPriceController.clear();
                              controller.addLevelDiscountedPriceController.clear();
                              controller.addLevelDescriptionController.clear();

                              for (var element in controller.levelCheckboxList) {
                                element.isCheckboxSelected = false;
                              }
                              controller.levelCheckboxList.refresh();

                              Get.back();
                            } else {
                              Utilities.showToast(AppStringKey.enter_valid_discount_price.tr);
                            }
                            // }
                          },
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
            Positioned(
              right: Directionality.of(context) == TextDirection.ltr ? 00 : null,
              left: Directionality.of(context) == TextDirection.rtl ? 00 : null,
              top: 00,
              child: Material(
                color: Colors.transparent,
                child: Ink(
                  child: InkWell(
                    onTap: () {
                      var controller = Get.find<ServiceController>();
                      controller.clearLevelData();
                      Get.back();
                    },
                    child: const Icon(
                      Icons.close,
                      color: AppColors.colorBlack,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
