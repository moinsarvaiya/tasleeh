import 'dart:convert';
import 'dart:typed_data';

import 'package:al_baida_garage_fe/Pages/Dashboard/MyAccount/services/services_dashboard_controller.dart';
import 'package:al_baida_garage_fe/Pages/services/models/car_model.dart';
import 'package:al_baida_garage_fe/Pages/services/service_mobile_view.dart';
import 'package:al_baida_garage_fe/Pages/services/service_web_view.dart';
import 'package:al_baida_garage_fe/Utils/utilities.dart';
import 'package:al_baida_garage_fe/api/api_end_point.dart';
import 'package:al_baida_garage_fe/api/api_interface.dart';
import 'package:al_baida_garage_fe/api/api_presenter.dart';
import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import '../../api/web_fields_key.dart';
import '../../common/common_function.dart';
import '../../common/custom_loader.dart';
import '../../core/app_preference/app_preferences.dart';
import '../../core/app_preference/storage_keys.dart';
import '../../core/constant/ui_constants.dart';
import '../../l10n/app_string_key.dart';
import '../custom_stepper/custom_stepper_controller.dart';
import 'models/car_brand_model.dart';
import 'models/category_request_body_model.dart';
import 'models/category_response_model.dart';
import 'models/get_service_data_model.dart';
import 'models/missing_value.dart';
import 'models/response_level_list_model.dart';
import 'models/service_data_model.dart';
import 'models/sub_category_response_model.dart';
import 'models/sub_service_data_model.dart';

class ServiceController extends GetxController implements ApiCallBacks {
  RxList<CategoryResponseModel> categoryList = RxList();
  RxList<SubCategoryResponseModel> subCategoryList = RxList();
  RxList<ResponseLevelListModel> levelCheckboxList = RxList();

  TextEditingController addManuallyLevelController = TextEditingController();
  TextEditingController addLevelPriceController = TextEditingController();
  TextEditingController addLevelDiscountedPriceController = TextEditingController();
  TextEditingController addLevelDescriptionController = TextEditingController();
  final OverlayPortalController tooltipController = OverlayPortalController();

  List<String> categorySelectedValue = [];
  List<String> subCategorySelectedValue = [];

  RxList<ServiceModel> addCategoryList = RxList();
  List<CategoryRequestBodyModel> categoryResponseModel = [];

  int categoryIndexAddLevel = 0;
  int subCategoryIndexAddLevel = 0;
  int levelCheckboxIndex = 0;
  RxList<SelectedCarModel> selectedCarList = RxList();
  RxList<CarBrandModel> carBrandModel = RxList();
  RxList<CarBrandModel> filteredCarBrands = RxList();
  RxList<CarBrandModel> selectedCarBrands = RxList();
  RxList<ResponseLevelListModel> levelList = RxList();
  String categoryId = '';
  int categoryIndex = 0;
  int subCategoryIndex = 0;
  int levelIndex = 0;

  String manualLevelTitle = '';
  bool levelIsFromAdd = true;
  int levelForEditSelectedIndex = 0;

  RxString levelPrice = ''.obs;
  RxString levelDiscountedPrice = ''.obs;
  RxString levelDescription = ''.obs;
  bool isServiceCalled = true;
  RxList<SubmittedServiceDataModel> submittedServiceList = RxList();
  bool isFromServiceGet = true;
  int submittedServiceIndex = 0;

  RxList<SubServiceDataModel> subServiceDataModel = RxList();
  TextEditingController editLevelPriceController = TextEditingController();
  TextEditingController editLevelDiscountedPriceController = TextEditingController();
  TextEditingController editLevelDescriptionController = TextEditingController();
  RxBool isLevelSelected = false.obs;
  bool subServiceUpdateFromGet = true;
  int selectedLevelIndex = -1;

  ServiceRedirectionFrom redirectionFrom = ServiceRedirectionFrom.onBoarding;

  void updateDataBasedOnInput(String inputText) {
    filteredCarBrands.value = carBrandModel.where((brand) => brand.title.toLowerCase().contains(inputText.toLowerCase())).toList();
    filteredCarBrands.refresh();
  }

  void getServiceData() {
    isFromServiceGet = true;
    ApiPresenter(this).getServiceData(AppPreferences.sharedPrefRead(StorageKeys.garageId));
  }

  void deleteService(String serviceId, int index) {
    submittedServiceIndex = index;
    isFromServiceGet = false;
    ApiPresenter(this).deleteService(serviceId);
  }

  void getMultiSelectionBandSearchData() {
    ApiPresenter(this).getMultiSelectionBandSearchData();
  }

  void getCategories() {
    ApiPresenter(this).getCategories();
  }

  void getSelectedCars(String subCategoryId) {
    ApiPresenter(this).getSelectedCars(subCategoryId);
  }

  void getSubCategories(String id, int index) {
    categoryIndex = index;
    categoryId = id;
    ApiPresenter(this).getSubCategories(id);
  }

  void getLevelList() {
    print("Called getLevelList");
    levelFrom = LevelFrom.get;
    ApiPresenter(this).getLevelList(AppPreferences.sharedPrefRead(StorageKeys.garageId));
  }

  void submitLevelData(String levelTitle, String garageId) {
    levelFrom = LevelFrom.post;
    ApiPresenter(this).submitLevelData(levelTitle, garageId);
  }

  void deleteLevel(String levelId, int index) {
    levelFrom = LevelFrom.delete;
    levelIndex = index;
    ApiPresenter(this).deleteLevel(levelId);
  }

  void submitServiceData() {
    ApiPresenter(this).submitServiceData({
      'createServiceDto': categoryResponseModel.map((item) => item.toJson()).toList(),
    });
  }

  void editLevelData(
    String levelId,
    double price,
    double discountedPrice,
    String description,
  ) {
    ApiPresenter(this).editLevelData(levelId, price, discountedPrice, description);
  }

  void getSubServiceData(String subServiceId) {
    subServiceUpdateFromGet = true;
    ApiPresenter(this).getSubServiceData(subServiceId);
  }

  void updateSubServiceData(
    String subServiceId,
    String subCategoryId,
    String imageUrl,
  ) {
    subServiceUpdateFromGet = false;
    ApiPresenter(this).updateSubServiceData(
      subServiceId,
      subCategoryId,
      imageUrl,
    );
  }

  void addCategoryWidget() {
    categorySelectedValue.add('');
    final category = ServiceModel(
      categories: categoryList,
      isCategoryViewVisible: true,
      isDropdownReadOnly: false,
      subCategoryList: [],
    );

    categoryResponseModel.add(
      CategoryRequestBodyModel(
        category: Category(id: ''),
        garage: Garage(
          id: AppPreferences.sharedPrefRead(StorageKeys.garageId),
        ),
        subCategoryDto: [],
      ),
    );
    for (var element in addCategoryList) {
      element.isCategoryViewVisible = false;
      element.isDropdownReadOnly = true;
    }

    addCategoryList.add(category);
  }

  void addSubCategory(int categoryIndex) {
    subCategorySelectedValue.add('');
    final subCategory = SubCategoryModel(
      searchCarName: '',
      levelList: [],
      subCategories: subCategoryList,
      textEditingController: TextEditingController(),
      carBrandList: carBrandModel,
      carBrandSelectedIndex: [],
      selectedCarTypeIndex: [],
    );
    addCategoryList[categoryIndex].subCategoryList.add(subCategory);

    categoryResponseModel[categoryIndex].subCategoryDto.add(
          SubCategoryDto(
            levelDto: [],
            modelDto: [],
            subCategory: SubCategory(id: ''),
          ),
        );

    addCategoryList.refresh();
  }

  void addLevel(
      int categoryIndex, int subCategoryIndex, String levelName, String levelPrice, String levelDiscountedPrice, String levelDescription, String id) {
    if (levelIsFromAdd) {
      final level = LevelModel(
        levelId: id,
        index: addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].levelList.length + 1,
        levelName: levelName,
        levelPrice: levelPrice,
        levelDiscountedPrice: levelDiscountedPrice,
        levelDescription: levelDescription,
      );
      addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].levelList.add(level);
    } else {
      if (levelForEditSelectedIndex >= 0 &&
          levelForEditSelectedIndex < addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].levelList.length) {
        final level = LevelModel(
          levelId: id,
          index: levelForEditSelectedIndex + 1,
          levelName: levelName,
          levelPrice: levelPrice,
          levelDiscountedPrice: levelDiscountedPrice,
          levelDescription: levelDescription,
        );
        addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].levelList[levelForEditSelectedIndex] = level;
      }
    }
    addCategoryList.refresh();
  }

  bool isAnyLevelListNotEmpty(ServiceModel serviceModel) {
    for (final subCategory in serviceModel.subCategoryList) {
      if (subCategory.levelList.isNotEmpty) {
        return true;
      }
    }
    return false;
  }

  void removeCategory(int categoryIndex) {
    if (categoryIndex >= 0 && categoryIndex < categoryResponseModel.length) {
      categoryResponseModel.removeAt(categoryIndex);
      addCategoryList.removeAt(categoryIndex);
      addCategoryList.refresh();
    }
  }

  void removeLevels(int categoryIndex, int subCategoryIndex, int levelIndex) {
    categoryResponseModel[categoryIndex].subCategoryDto[subCategoryIndex].levelDto!.removeAt(levelIndex);
    addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].levelList.removeAt(levelIndex);
    addCategoryList.refresh();
  }

  void removeSubCategory(int categoryIndex, int subCategoryIndex) {
    categoryResponseModel[categoryIndex].subCategoryDto.removeAt(subCategoryIndex);
    addCategoryList[categoryIndex].subCategoryList.removeAt(subCategoryIndex);
    addCategoryList.refresh();
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {}

  @override
  void onError(String errorMsg, responseCode, String apiEndPoint) {}

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(dynamic object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.getMultiSelectionBandSearchData:
        updateCarBrandModel(object);
        break;
      case ApiEndPoints.getCategories:
        updateCategoryList(object);
        break;
      case ApiEndPoints.getSubCategories:
        updateSubCategoryList(object);
        break;
      case ApiEndPoints.submitServiceData:
        updateAddCategoryList();
        break;
      case ApiEndPoints.deleteLevel:
        handleDeleteLevel(object);
        break;
      case ApiEndPoints.submitLevelData:
        updateLevelCheckboxList(object);
        break;
      case ApiEndPoints.getSelectedCarData:
        updateSelectedCarModel(object);
        break;
      case ApiEndPoints.getServiceData:
        updateSubmittedServiceList(object);
        break;
      case ApiEndPoints.subServiceData:
        if (subServiceUpdateFromGet) {
          manageSubServiceData(object);
        } else {
          Future.delayed(const Duration(seconds: 5));
          getServiceData();
        }
        break;
      case ApiEndPoints.editLevelData:
        manageEditLevelData(object);
        break;
      default:
        break;
    }
  }

  void manageEditLevelData(dynamic object) {
    isLevelSelected.value = false;
    Get.back();
  }

  void manageSubServiceData(dynamic object) {
    subServiceDataModel.clear();
    subServiceDataModel.addAll((object['data'] as List).map((e) => SubServiceDataModel.fromJson(e)).toList());
    subServiceDataModel.refresh();
    editSubServiceData(Get.context!, subServiceDataModel);
  }

  void updateSubmittedServiceList(dynamic object) {
    if (isFromServiceGet) {
      submittedServiceList.clear();
      submittedServiceList.addAll((object['data'] as List).map((e) => SubmittedServiceDataModel.fromJson(e)));
      submittedServiceList.refresh();
      getCategories();
    } else {
      submittedServiceList.removeAt(submittedServiceIndex);
      submittedServiceList.refresh();
    }
  }

  void updateSelectedCarModel(dynamic object) {
    selectedCarList.clear();
    selectedCarList.addAll((object['data'] as List).map((e) => SelectedCarModel.fromJson(e)));
    selectedCarList.refresh();
    if (selectedCarList.isNotEmpty) {
      if (Get.context!.isDesktop) {
        selectedCarDialog(Get.context!, selectedCarList);
      } else {
        selectedCarMobileDialog(Get.context!, selectedCarList);
      }
    } else {
      Utilities.showToast(AppStringKey.selected_car_not_available.tr);
    }
  }

  void updateCarBrandModel(dynamic object) {
    carBrandModel.addAll((object['data'] as List).map((e) => CarBrandModel.fromJson(e)));
    carBrandModel.refresh();
  }

  void updateCategoryList(dynamic object) {
    categoryList.clear();
    categoryList.addAll((object['data'] as List).map((e) => CategoryResponseModel.fromJson(e)));
    if (submittedServiceList.isEmpty) {
      List<MissingValue> missingValues = Get.find<ServiceController>().findMissingValues(Get.find<ServiceController>().categoryResponseModel);
      if (missingValues.isEmpty) {
        addCategoryWidget();
      }
    }
  }

  void updateSubCategoryList(dynamic object) {
    subCategoryList.clear();
    subCategoryList.addAll((object['data'] as List).map((e) => SubCategoryResponseModel.fromJson(e)));
    addSubCategory(categoryIndex);
  }

  void updateAddCategoryList() {
    if (redirectionFrom == ServiceRedirectionFrom.onBoarding) {
      addCategoryList.clear();
      addCategoryList.refresh();
      categoryResponseModel.clear();
      isServiceCalled = true;
      Get.find<CustomStepperController>().setActivePage(Get.find<CustomStepperController>().selectedIndex.value + 1);
    } else if (redirectionFrom ==  ServiceRedirectionFrom.myAccount) {
      var controller = Get.find<ServicesDashboardController>();
      controller.clearServiceData();
      controller.getBranches();
      Get.back();
    }
  }

  void handleDeleteLevel(dynamic object) {
    if (levelFrom == LevelFrom.delete) {
      removeLevels(categoryIndex, subCategoryIndex, levelIndex);
      getLevelList();
    } else if (levelFrom == LevelFrom.get) {
      levelCheckboxList.clear();
      levelCheckboxList.addAll((object['data'] as List).map((e) => ResponseLevelListModel.fromJson(e)));
    }
  }

  void updateLevelCheckboxList(dynamic object) {
    levelCheckboxList.clear();
    levelCheckboxList.addAll((object['data'] as List).map((e) => ResponseLevelListModel.fromJson(e)));
    if (levelCheckboxList.isNotEmpty) {
      manualLevelTitle = addManuallyLevelController.text;
      addManuallyLevelController.clear();
    }
  }

  void imageUpload(
    BuildContext context,
    Uint8List imageFile,
    int subCategoryIndex, {
    bool isFromEdit = false,
    int fromEditListIndex = 0,
    Rx<SubServiceDataModel>? subServiceDataModel,
  }) async {
    try {
      var headers = {
        WebFieldKey.strContentType: WebFieldKey.strMultipartFormData,
        WebFieldKey.strAuthorization: 'Bearer ${AppPreferences.sharedPrefRead(StorageKeys.accessToken)}',
      };

      var request = http.MultipartRequest(
        WebFieldKey.strPostMethod,
        Uri.parse('${ApiEndPoints.baseUrl}${ApiEndPoints.imageUpload}'),
      );
      request.headers.addAll(headers);
      var stream = http.ByteStream.fromBytes(imageFile);
      var length = imageFile.lengthInBytes;
      request.files.add(
        http.MultipartFile(WebFieldKey.strFile, stream, length, filename: 'sub_category.jpg'),
      );
      request.fields[WebFieldKey.strFolder] = 'garage';
      http.StreamedResponse response = await request.send();
      final responseBody = await response.stream.bytesToString();
      final parsedJson = jsonDecode(responseBody);
      if (parsedJson['code'] == 200 || parsedJson['code'] == 201) {
        if (isFromEdit) {
          subServiceDataModel!.value.category!.serviceDetailImage = parsedJson['data']['url'];
          subServiceDataModel.refresh();
          imageCache.clear();
          updateSubServiceData(
            subServiceDataModel.value.category!.serviceDetailId ?? '',
            subServiceDataModel.value.category!.subcategoryId ?? '',
            subServiceDataModel.value.category!.serviceDetailImage ?? '',
          );
        } else {
          categoryResponseModel[categoryIndex].subCategoryDto[subCategoryIndex].imageUrl = parsedJson['data']['url'];
        }
      } else {
        CommonFunction.showCustomSnackBar(
          context: context,
          message: parsedJson['error'],
          duration: const Duration(seconds: 3),
          actionLabel: 'Close',
        );
      }
      LoadingDialog.closeFullScreenDialog();
    } catch (e) {
      LoadingDialog.closeFullScreenDialog();
      CommonFunction.showCustomSnackBar(
        context: context,
        message: e.toString(),
        duration: const Duration(seconds: 3),
        actionLabel: 'Close',
      );
    }
  }

  List<MissingValue> findMissingValues(List<CategoryRequestBodyModel> categoryResponseModel) {
    List<MissingValue> missingValues = [];
    for (var category in categoryResponseModel) {
      if (category.category.id == '') {
        missingValues.add(MissingValue('', AppStringKey.select_category.tr));
      }

      if (category.garage.id == '') {
        missingValues.add(MissingValue('', '${AppStringKey.missing_garage_id.tr}: ${category.category.id}'));
      }

      for (var subCategory in category.subCategoryDto) {
        if (subCategory.subCategory?.id == '') {
          missingValues.add(MissingValue('', AppStringKey.select_sub_category.tr));
        }

        if (subCategory.imageUrl == null) {
          missingValues.add(MissingValue('', AppStringKey.select_sub_category_image.tr));
        }

        if (subCategory.modelDto == null || subCategory.modelDto!.isEmpty) {
          missingValues.add(MissingValue('', AppStringKey.select_one_model.tr));
        }

        if (subCategory.levelDto == null || subCategory.levelDto!.isEmpty) {
          missingValues.add(MissingValue('', AppStringKey.select_one_level.tr));
        }
      }
    }
    return missingValues;
  }

  void findSubServiceByName(String paramName) {
    for (int i = 0; i < subServiceDataModel.length; i++) {
      var subServiceData = subServiceDataModel[i];
      if (subServiceData.category != null && subServiceData.category!.serviceDetailLevel != null) {
        for (int j = 0; j < subServiceData.category!.serviceDetailLevel!.length; j++) {
          var serviceDetailLevel = subServiceData.category!.serviceDetailLevel![j];
          if (serviceDetailLevel.name == paramName) {
            editLevelPriceController.text = double.parse(serviceDetailLevel.actualPrice ?? '').round().toString();
            editLevelDiscountedPriceController.text = double.parse(serviceDetailLevel.discountedPrice ?? '').round().toString();
            editLevelDescriptionController.text = serviceDetailLevel.description ?? '';
            isLevelSelected.value = true;
            selectedLevelIndex = j;
            return;
          }
        }
      }
    }
    // No match found, reset values and set index to -1
    editLevelPriceController.text = '';
    editLevelDiscountedPriceController.text = '';
    editLevelDescriptionController.text = '';
    isLevelSelected.value = false;
    selectedLevelIndex = -1;
  }

  void clearLevelData() {
    addManuallyLevelController.clear();
    addLevelPriceController.clear();
    addLevelDiscountedPriceController.clear();
    addLevelDescriptionController.clear();

    for (var element in levelCheckboxList) {
      element.isCheckboxSelected = false;
    }
    levelCheckboxList.refresh();
  }

  void extractSelectedCarBrands() {
    selectedCarBrands.clear();
    for (var category in addCategoryList) {
      for (var subCategory in category.subCategoryList) {
        for (var carBrandIndex in subCategory.carBrandSelectedIndex) {
          var carBrand = filteredCarBrands.firstWhere((brand) => brand.id == carBrandIndex);
          var carTypeModels = <CarTypeModel>[];
          for (var carTypeIndex in subCategory.selectedCarTypeIndex) {
            var carType = carBrand.models.firstWhere((model) => model.id == carTypeIndex);
            carTypeModels.add(carType);
          }
          selectedCarBrands.add(CarBrandModel(id: carBrand.id, title: carBrand.title, models: carTypeModels));
        }
      }
    }
  }
}
