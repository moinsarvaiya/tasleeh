import 'package:al_baida_garage_fe/Pages/services/models/sub_service_data_model.dart';
import 'package:al_baida_garage_fe/core/constant/app_images.dart';
import 'package:dotted_decoration/dotted_decoration.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_network/image_network.dart';

import '../../Utils/app_constants.dart';
import '../../Utils/utilities.dart';
import '../../common/common_function.dart';
import '../../common/custom_click_effect_widget.dart';
import '../../common/hover_icon.dart';
import '../../common/hover_text.dart';
import '../../common/input_text_field.dart';
import '../../common/space_horizontal.dart';
import '../../common/space_vertical.dart';
import '../../core/constant/app_color.dart';
import '../../core/constant/base_style.dart';
import '../../core/constant/ui_constants.dart';
import '../../core/extensions/common_extension.dart';
import '../../custom_classes/custom_dropdown_field.dart';
import '../../custom_classes/custom_expansion_panel.dart';
import '../../custom_classes/custom_text.dart';
import '../../l10n/app_string_key.dart';
import '../custom_stepper/widgets/custom_square_corner_button.dart';
import 'models/car_brand_model.dart';
import 'models/car_model.dart';
import 'models/category_request_body_model.dart';
import 'models/missing_value.dart';
import 'models/service_data_model.dart';
import 'service_controller.dart';
import 'widgets/added_service_data_widget.dart';

class ServiceWebView extends StatelessWidget {
  final ServiceController controller;
  final bool isReadOnly;

  const ServiceWebView({
    super.key,
    required this.controller,
    required this.isReadOnly,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        isReadOnly
            ? const SizedBox.shrink()
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppStringKey.select_categories.tr,
                    style: BaseStyle.textStyleRobotoSemiBold(
                      20,
                      AppColors.primaryTextColor,
                    ),
                  ),
                  Visibility(
                    visible: controller.redirectionFrom != ServiceRedirectionFrom.myAccount,
                    child: CustomTextButtonWithIcon(
                      buttonText: AppStringKey.add_category.tr,
                      callBack: () {
                        List<MissingValue> missingValues =
                            Get.find<ServiceController>().findMissingValues(Get.find<ServiceController>().categoryResponseModel);
                        if (missingValues.isNotEmpty) {
                          for (var missingValue in missingValues) {
                            Utilities.showToast(missingValue.message);
                            break;
                          }
                        } else {
                          controller.addCategoryWidget();
                        }
                      },
                      icon: Icons.add,
                      textStyle: BaseStyle.textStyleRobotoRegular(
                        14,
                        AppColors.primaryTextColor,
                      ),
                    ),
                  )
                ],
              ),
        const SpaceVertical(0.03),
        Obx(
          () => Visibility(
            visible: controller.submittedServiceList.isNotEmpty && controller.redirectionFrom != ServiceRedirectionFrom.myAccount,
            child: Column(
              children: [
                ...controller.submittedServiceList.indexed.map(
                  (e) {
                    var submittedServiceDataModel = e.$2;
                    return AddedServiceDataWidget(
                      title: submittedServiceDataModel.category!.serviceName ?? '',
                      description: AppStringKey.submitted_category_desc.tr,
                      subCategory: submittedServiceDataModel.category!.subCategory ?? [],
                      callBack: () {
                        controller.deleteService(submittedServiceDataModel.category!.serviceId ?? '', e.$1);
                      },
                      subCategoryCallBack: (String subCategoryId) {
                        controller.getSubServiceData(subCategoryId);
                      },
                    );
                  },
                ),
                const SpaceVertical(0.03),
              ],
            ),
          ),
        ),
        Obx(
          () => Column(
            children: [
              ...controller.addCategoryList.indexed.map(
                (item) {
                  return Column(
                    children: [
                      Ink(
                        child: InkWell(
                          child: CategoryWidget(
                            serviceModel: item.$2,
                            index: item.$1,
                            isReadOnly: isReadOnly,
                            controller: controller,
                          ),
                        ),
                      ),
                      Visibility(
                        visible: controller.addCategoryList[item.$1].isCategoryViewVisible,
                        child: SubCategoryWidget(
                          serviceModel: item.$2,
                          index: item.$1,
                          controller: controller,
                        ),
                      )
                    ],
                  );
                },
              ),
            ],
          ),
        )
      ],
    );
  }
}

class CategoryWidget extends StatelessWidget {
  final ServiceModel serviceModel;
  final int index;
  final bool isReadOnly;
  final ServiceController controller;

  const CategoryWidget({
    super.key,
    required this.serviceModel,
    required this.index,
    required this.isReadOnly,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        context.isTabletView
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  isReadOnly
                      ? const SizedBox.shrink()
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomDropDownField(
                              isReadOnly: controller.addCategoryList[index].isDropdownReadOnly,
                              width: context.isTabletView ? context.width * 0.30 : context.width * 0.20,
                              labelVisibility: false,
                              labelText: AppStringKey.select_service.tr,
                              validationText: AppStringKey.select_service.tr,
                              hintTitle: AppStringKey.select_service.tr,
                              selectedValue: controller.categorySelectedValue[index],
                              onChanged: (categoryName) {
                                controller.categorySelectedValue[index] = categoryName!;
                                if (controller.categoryResponseModel[index].category.id.isEmpty ||
                                    controller.categoryResponseModel[index].category.id ==
                                        controller.addCategoryList[index].categories.firstWhere((element) => element.title == categoryName).id) {
                                  controller.categoryResponseModel[index].category = Category(
                                      id: controller.addCategoryList[index].categories.firstWhere((element) => element.title == categoryName).id);
                                  controller.getSubCategories(
                                    controller.addCategoryList[index].categories.firstWhere((element) => element.title == categoryName).id,
                                    index,
                                  );
                                } else {
                                  Utilities.showToast(AppStringKey.please_delete_category_first.tr);
                                }
                              },
                              items: controller.addCategoryList[index].categories.map((category) => category.title).toList(),
                            ),
                            const SpaceVertical(0.01),
                            Row(
                              children: [
                                serviceModel.isCategoryViewVisible
                                    ? CustomTextButtonWithIcon(
                                        buttonText: AppStringKey.add_sub_category.tr,
                                        callBack: () {
                                          if (controller.categorySelectedValue != '') {
                                            controller.getSubCategories(
                                                controller.addCategoryList[index].categories
                                                    .firstWhere((element) => element.title == controller.categorySelectedValue)
                                                    .id,
                                                index);
                                          } else if (controller.categoryResponseModel[index].category.id ==
                                              controller.addCategoryList[index].categories.first.id) {
                                            controller.getSubCategories(controller.addCategoryList[index].categories.first.id, index);
                                          } else {
                                            Utilities.showToast(AppStringKey.please_delete_category_first.tr);
                                          }

                                          /*if (controller.categoryResponseModel[index].category.id ==
                                              controller.addCategoryList[index].categories.first.id) {
                                            controller.getSubCategories(controller.addCategoryList[index].categories.first.id, index);
                                          } else {
                                            Utilities.showToast(AppStringKey.please_delete_category_first.tr);
                                          }*/
                                        },
                                        textStyle: BaseStyle.textStyleRobotoRegular(
                                          14,
                                          AppColors.primaryTextColor,
                                        ),
                                        icon: Icons.add,
                                      )
                                    : controller.addCategoryList[index].subCategoryList.isNotEmpty
                                        ? CustomTextButtonWithIcon(
                                            buttonText: AppStringKey.sub_categories.tr,
                                            callBack: () {},
                                            textStyle: BaseStyle.textStyleRobotoRegular(
                                              14,
                                              AppColors.primaryTextColor,
                                            ),
                                            icon: Icons.done,
                                          )
                                        : const SizedBox.shrink(),
                                const SpaceVertical(0.01),
                                !controller.isAnyLevelListNotEmpty(serviceModel)
                                    ? const SizedBox.shrink()
                                    : CustomTextButtonWithIcon(
                                        buttonText: AppStringKey.levels.tr,
                                        callBack: () {},
                                        textStyle: BaseStyle.textStyleRobotoRegular(
                                          14,
                                          AppColors.primaryTextColor,
                                        ),
                                        icon: Icons.done,
                                      )
                              ],
                            )
                          ],
                        ),
                  Row(
                    children: [
                      Material(
                        child: Ink(
                          child: InkWell(
                            onTap: () {
                              if (controller.addCategoryList[index].isCategoryViewVisible) {
                                controller.addCategoryList[index].isCategoryViewVisible = false;
                                controller.addCategoryList[index].isDropdownReadOnly = true;
                                controller.addCategoryList.refresh();
                              } else {
                                for (var element in controller.addCategoryList) {
                                  element.isCategoryViewVisible = false;
                                  element.isDropdownReadOnly = true;
                                }

                                controller.addCategoryList[index].isCategoryViewVisible = true;
                                controller.addCategoryList[index].isDropdownReadOnly = false;
                                controller.addCategoryList.refresh();
                              }
                            },
                            child: Container(
                              width: 24,
                              height: 24,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 1,
                                  color: AppColors.colorBlack,
                                ),
                              ),
                              child: Center(
                                child: Icon(
                                  serviceModel.isCategoryViewVisible ? Icons.remove : Icons.add,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SpaceHorizontal(0.008),
                      HoverIcon(
                        icon: AppImages.deleteIcon,
                        defaultColor: AppColors.colorBlack,
                        hoverColor: AppColors.errorTextColor,
                        onTap: () {
                          controller.categorySelectedValue.removeAt(index);
                          controller.removeCategory(index);
                        },
                        iconSize: 22,
                      ),
                    ],
                  ),
                ],
              )
            : Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  isReadOnly
                      ? const SizedBox.shrink()
                      : Row(
                          children: [
                            CustomDropDownField(
                              isReadOnly: controller.addCategoryList[index].isDropdownReadOnly,
                              width: context.isTabletView ? context.width * 0.20 : context.width * 0.20,
                              labelVisibility: false,
                              labelText: AppStringKey.select_service.tr,
                              validationText: AppStringKey.select_service.tr,
                              hintTitle: AppStringKey.select_service.tr,
                              selectedValue: controller.categorySelectedValue[index],
                              onChanged: (categoryName) {
                                controller.categorySelectedValue[index] = categoryName!;
                                if (controller.categoryResponseModel[index].category.id.isEmpty ||
                                    controller.categoryResponseModel[index].category.id ==
                                        controller.addCategoryList[index].categories.firstWhere((element) => element.title == categoryName).id) {
                                  controller.categoryResponseModel[index].category = Category(
                                      id: controller.addCategoryList[index].categories.firstWhere((element) => element.title == categoryName).id);
                                  controller.getSubCategories(
                                    controller.addCategoryList[index].categories.firstWhere((element) => element.title == categoryName).id,
                                    index,
                                  );
                                } else {
                                  Utilities.showToast(AppStringKey.please_delete_category_first.tr);
                                }
                              },
                              items: controller.addCategoryList[index].categories.map((category) => category.title).toList(),
                            ),
                            const SpaceHorizontal(0.005),
                            serviceModel.isCategoryViewVisible
                                ? CustomTextButtonWithIcon(
                                    buttonText: AppStringKey.add_sub_category.tr,
                                    callBack: () {
                                      if (controller.categoryResponseModel[index].category.id ==
                                          controller.addCategoryList[index].categories.first.id) {
                                        controller.getSubCategories(controller.addCategoryList[index].categories.first.id, index);
                                      } else {
                                        Utilities.showToast(AppStringKey.please_delete_category_first.tr);
                                      }
                                    },
                                    textStyle: BaseStyle.textStyleRobotoRegular(
                                      14,
                                      AppColors.primaryTextColor,
                                    ),
                                    icon: Icons.add,
                                  )
                                : controller.addCategoryList[index].subCategoryList.isNotEmpty
                                    ? CustomTextButtonWithIcon(
                                        buttonText: AppStringKey.sub_categories.tr,
                                        callBack: () {},
                                        textStyle: BaseStyle.textStyleRobotoRegular(
                                          14,
                                          AppColors.primaryTextColor,
                                        ),
                                        icon: Icons.done,
                                      )
                                    : const SizedBox.shrink(),
                            const SpaceHorizontal(0.01),
                            !controller.isAnyLevelListNotEmpty(serviceModel)
                                ? const SizedBox.shrink()
                                : CustomTextButtonWithIcon(
                                    buttonText: AppStringKey.levels.tr,
                                    callBack: () {},
                                    textStyle: BaseStyle.textStyleRobotoRegular(
                                      14,
                                      AppColors.primaryTextColor,
                                    ),
                                    icon: Icons.done,
                                  )
                          ],
                        ),
                  Row(
                    children: [
                      Material(
                        child: Ink(
                          child: InkWell(
                            onTap: () {
                              if (controller.addCategoryList[index].isCategoryViewVisible) {
                                controller.addCategoryList[index].isCategoryViewVisible = false;
                                controller.addCategoryList[index].isDropdownReadOnly = true;
                                controller.addCategoryList.refresh();
                              } else {
                                for (var element in controller.addCategoryList) {
                                  element.isCategoryViewVisible = false;
                                  element.isDropdownReadOnly = true;
                                }

                                controller.addCategoryList[index].isCategoryViewVisible = true;
                                controller.addCategoryList[index].isDropdownReadOnly = false;
                                controller.addCategoryList.refresh();
                              }
                            },
                            child: Container(
                              width: 24,
                              height: 24,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 1,
                                  color: AppColors.colorBlack,
                                ),
                              ),
                              child: Center(
                                child: Icon(
                                  serviceModel.isCategoryViewVisible ? Icons.remove : Icons.add,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SpaceHorizontal(0.008),
                      HoverIcon(
                        icon: AppImages.deleteIcon,
                        defaultColor: AppColors.colorBlack,
                        hoverColor: AppColors.errorTextColor,
                        onTap: () {
                          controller.categorySelectedValue.removeAt(index);
                          controller.removeCategory(index);
                        },
                        iconSize: 22,
                      ),
                    ],
                  ),
                ],
              ),
        const SpaceVertical(0.02)
      ],
    );
  }
}

class SubCategoryWidget extends StatefulWidget {
  final ServiceModel serviceModel;
  final int index;
  final ServiceController controller;

  const SubCategoryWidget({
    super.key,
    required this.serviceModel,
    required this.index,
    required this.controller,
  });

  @override
  State<SubCategoryWidget> createState() => _SubCategoryWidgetState();
}

class _SubCategoryWidgetState extends State<SubCategoryWidget> {
  final GlobalKey<_SubCategoryWidgetState> yourWidgetKey = GlobalKey<_SubCategoryWidgetState>();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ...widget.controller.addCategoryList[widget.index].subCategoryList.indexed.map(
          (subCategory) {
            int subCategoryIndex = subCategory.$1;
            SubCategoryModel subCategoryModel = subCategory.$2;
            return Column(
              children: [
                Container(
                  padding: const EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: AppColors.colorDragImageBack,
                    border: Border.all(
                      width: 1,
                      color: AppColors.strokeColor,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          context.isTabletView
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Obx(
                                          () => CustomDropDownField(
                                            selectedValue: widget.controller.subCategorySelectedValue[subCategoryIndex],
                                            isReadOnly: false,
                                            width: context.width * 0.33,
                                            labelVisibility: false,
                                            labelText: AppStringKey.select_service.tr,
                                            validationText: AppStringKey.select_service.tr,
                                            hintTitle: AppStringKey.select_service.tr,
                                            onChanged: (subCategoryValue) {
                                              widget.controller.subCategorySelectedValue[subCategoryIndex] = subCategoryValue!;
                                              widget.controller.categoryResponseModel[widget.index].subCategoryDto[subCategoryIndex].subCategory =
                                                  SubCategory(
                                                id: widget.controller.subCategoryList.firstWhere((element) => element.title == subCategoryValue).id,
                                              );
                                            },
                                            items: widget.controller.subCategoryList.map((category) => category.title).toList(),
                                          ),
                                        ),
                                        const SpaceVertical(0.01),
                                        SearchModelWidget(
                                          index: widget.index,
                                          subCategoryIndex: subCategoryIndex,
                                          textEditingController: subCategoryModel.textEditingController,
                                        )
                                      ],
                                    ),
                                    const SpaceVertical(0.01),
                                    Material(
                                      child: Ink(
                                        child: InkWell(
                                          onTap: () {
                                            // widget.controller.extractSelectedCarBrands();
                                            var subCategoryId = widget
                                                .controller.categoryResponseModel[widget.index].subCategoryDto[subCategoryIndex].subCategory!.id;
                                            if (subCategoryId != '') {
                                              widget.controller.getSelectedCars(widget
                                                  .controller.categoryResponseModel[widget.index].subCategoryDto[subCategoryIndex].subCategory!.id);
                                            } else {
                                              Utilities.showToast(AppStringKey.select_sub_category.tr);
                                            }
                                          },
                                          child: CustomText(
                                            text: AppStringKey.view_added_card.tr,
                                            textStyle: BaseStyle.underLineTextStyle(
                                              7,
                                              AppColors.primaryTextColor,
                                              fontWeight: FontWeight.normal,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    const SpaceVertical(0.01),
                                    Visibility(
                                      visible: subCategoryModel.subCategoryImagePath == null,
                                      child: Material(
                                        color: Colors.transparent,
                                        child: Ink(
                                          child: InkWell(
                                            customBorder: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(5),
                                            ),
                                            onTap: () {
                                              CommonFunction.pickImages(0).then((value) {
                                                widget.controller.addCategoryList[widget.index].subCategoryList[subCategoryIndex]
                                                    .subCategoryImagePath = value.entries.first.value;
                                                widget.controller.addCategoryList.refresh();

                                                widget.controller.imageUpload(context, value.entries.first.value, subCategoryIndex);
                                              });
                                            },
                                            child: Container(
                                              width: context.width * 0.20,
                                              height: context.height * 0.2,
                                              decoration: DottedDecoration(
                                                shape: Shape.box,
                                                color: AppColors.strokeColor,
                                                borderRadius: BorderRadius.circular(5),
                                                strokeWidth: 1,
                                              ),
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  const Icon(
                                                    Icons.add,
                                                    size: 22,
                                                    color: AppColors.colorBlack,
                                                  ),
                                                  const SpaceVertical(0.02),
                                                  CustomText(
                                                    text: AppStringKey.upload_photo.tr,
                                                    textStyle: BaseStyle.textStyleRobotoRegular(
                                                      8,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                  ),
                                                  CustomText(
                                                    maxLines: 2,
                                                    textAlign: TextAlign.center,
                                                    text: AppStringKey.upload_photo_desc.tr,
                                                    textStyle: BaseStyle.textStyleRobotoRegular(
                                                      5,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                      visible: subCategoryModel.subCategoryImagePath != null,
                                      child: Container(
                                        width: context.width * 0.20,
                                        height: context.height * 0.2,
                                        decoration: BoxDecoration(
                                          color: AppColors.strokeColor,
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                        child: Stack(
                                          children: [
                                            subCategoryModel.subCategoryImagePath != null
                                                ? Positioned(
                                                    top: 0,
                                                    bottom: 0,
                                                    left: 0,
                                                    right: 0,
                                                    child: Image.memory(
                                                      subCategoryModel.subCategoryImagePath!,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  )
                                                : const SizedBox.shrink(),
                                            Positioned(
                                              right: 5,
                                              top: 5,
                                              child: Container(
                                                width: 17,
                                                height: 17,
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(5),
                                                  color: AppColors.colorGrey,
                                                ),
                                                child: HoverIcon(
                                                  iconSize: 14,
                                                  icon: AppImages.deleteIcon,
                                                  defaultColor: AppColors.colorGrey,
                                                  hoverColor: AppColors.errorTextColor,
                                                  onTap: () {
                                                    widget.controller.categoryResponseModel[widget.index].subCategoryDto[subCategoryIndex].imageUrl =
                                                        '';
                                                    widget.controller.addCategoryList[widget.index].subCategoryList[subCategoryIndex]
                                                        .subCategoryImagePath = null;
                                                    widget.controller.addCategoryList.refresh();
                                                  },
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                )
                              : Row(
                                  children: [
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Obx(
                                          () => CustomDropDownField(
                                            selectedValue: widget.controller.subCategorySelectedValue[subCategoryIndex],
                                            isReadOnly: false,
                                            width: context.width * 0.23,
                                            labelVisibility: false,
                                            labelText: AppStringKey.select_service.tr,
                                            validationText: AppStringKey.select_service.tr,
                                            hintTitle: AppStringKey.select_service.tr,
                                            onChanged: (subCategoryValue) {
                                              widget.controller.subCategorySelectedValue[subCategoryIndex] = subCategoryValue!;
                                              widget.controller.categoryResponseModel[widget.index].subCategoryDto[subCategoryIndex].subCategory =
                                                  SubCategory(
                                                id: widget.controller.subCategoryList.firstWhere((element) => element.title == subCategoryValue).id,
                                              );
                                            },
                                            items: widget.controller.subCategoryList.map((category) => category.title).toList(),
                                          ),
                                        ),
                                        const SpaceVertical(0.01),
                                        SearchModelWidget(
                                          index: widget.index,
                                          subCategoryIndex: subCategoryIndex,
                                          textEditingController: subCategoryModel.textEditingController,
                                        )
                                      ],
                                    ),
                                    const SpaceHorizontal(0.02),
                                    Material(
                                      child: Ink(
                                        child: InkWell(
                                          onTap: () {
                                            // widget.controller.extractSelectedCarBrands();
                                            var subCategoryId = widget
                                                .controller.categoryResponseModel[widget.index].subCategoryDto[subCategoryIndex].subCategory!.id;
                                            if (subCategoryId != '') {
                                              widget.controller.getSelectedCars(widget
                                                  .controller.categoryResponseModel[widget.index].subCategoryDto[subCategoryIndex].subCategory!.id);
                                            } else {
                                              Utilities.showToast(AppStringKey.select_sub_category.tr);
                                            }
                                          },
                                          child: CustomText(
                                            text: AppStringKey.view_added_card.tr,
                                            textStyle: BaseStyle.underLineTextStyle(
                                              7,
                                              AppColors.primaryTextColor,
                                              fontWeight: FontWeight.normal,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    const SpaceHorizontal(0.04),
                                    Visibility(
                                      visible: subCategoryModel.subCategoryImagePath == null,
                                      child: Material(
                                        color: Colors.transparent,
                                        child: Ink(
                                          child: InkWell(
                                            customBorder: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(5),
                                            ),
                                            onTap: () {
                                              CommonFunction.pickImages(0).then((value) {
                                                widget.controller.addCategoryList[widget.index].subCategoryList[subCategoryIndex]
                                                    .subCategoryImagePath = value.entries.first.value;
                                                widget.controller.addCategoryList.refresh();

                                                widget.controller.imageUpload(context, value.entries.first.value, subCategoryIndex);
                                              });
                                            },
                                            child: Container(
                                              width: context.width * 0.09,
                                              height: context.height * 0.2,
                                              decoration: DottedDecoration(
                                                shape: Shape.box,
                                                color: AppColors.strokeColor,
                                                borderRadius: BorderRadius.circular(5),
                                                strokeWidth: 1,
                                              ),
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  const Icon(
                                                    Icons.add,
                                                    size: 22,
                                                    color: AppColors.colorBlack,
                                                  ),
                                                  const SpaceVertical(0.02),
                                                  CustomText(
                                                    text: AppStringKey.upload_photo.tr,
                                                    textStyle: BaseStyle.textStyleRobotoRegular(
                                                      8,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                  ),
                                                  CustomText(
                                                    maxLines: 2,
                                                    textAlign: TextAlign.center,
                                                    text: AppStringKey.upload_photo_desc.tr,
                                                    textStyle: BaseStyle.textStyleRobotoRegular(
                                                      5,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Visibility(
                                      visible: subCategoryModel.subCategoryImagePath != null,
                                      child: Container(
                                        width: context.width * 0.09,
                                        height: context.height * 0.2,
                                        decoration: BoxDecoration(
                                          color: AppColors.strokeColor,
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                        child: Stack(
                                          children: [
                                            subCategoryModel.subCategoryImagePath != null
                                                ? Positioned(
                                                    top: 0,
                                                    bottom: 0,
                                                    left: 0,
                                                    right: 0,
                                                    child: Image.memory(
                                                      subCategoryModel.subCategoryImagePath!,
                                                      fit: BoxFit.cover,
                                                    ),
                                                  )
                                                : const SizedBox.shrink(),
                                            Positioned(
                                              right: 5,
                                              top: 5,
                                              child: Container(
                                                width: 17,
                                                height: 17,
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(5),
                                                  color: AppColors.colorGrey,
                                                ),
                                                child: HoverIcon(
                                                  iconSize: 14,
                                                  icon: AppImages.deleteIcon,
                                                  defaultColor: AppColors.colorGrey,
                                                  hoverColor: AppColors.errorTextColor,
                                                  onTap: () {
                                                    widget.controller.categoryResponseModel[widget.index].subCategoryDto[subCategoryIndex].imageUrl =
                                                        '';
                                                    widget.controller.addCategoryList[widget.index].subCategoryList[subCategoryIndex]
                                                        .subCategoryImagePath = null;
                                                    widget.controller.addCategoryList.refresh();
                                                  },
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                          HoverIcon(
                            icon: AppImages.deleteIcon,
                            defaultColor: AppColors.colorGrey,
                            hoverColor: AppColors.errorTextColor,
                            onTap: () {
                              widget.controller.subCategorySelectedValue.removeAt(subCategoryIndex);
                              widget.controller.removeSubCategory(widget.index, subCategoryIndex);
                            },
                          )
                        ],
                      ),
                      const SpaceVertical(0.03),
                      CustomText(
                        maxLines: 2,
                        textAlign: TextAlign.center,
                        text: AppStringKey.added_levels.tr,
                        textStyle: BaseStyle.textStyleRobotoBold(
                          8,
                          AppColors.primaryTextColor,
                        ),
                      ),
                      const SpaceVertical(0.02),
                      LevelWidget(
                        categoryIndex: widget.index,
                        subCategoryIndex: subCategoryIndex,
                        controller: widget.controller,
                      ),
                      const SpaceVertical(0.02),
                      IntrinsicWidth(
                        child: Material(
                          child: Ink(
                            child: InkWell(
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              onTap: () {
                                var controller = Get.find<ServiceController>();
                                controller.clearLevelData();
                                widget.controller.levelIsFromAdd = true;

                                widget.controller.categoryIndexAddLevel = widget.index;
                                widget.controller.subCategoryIndexAddLevel = subCategoryIndex;
                                if (widget.controller.redirectionFrom == ServiceRedirectionFrom.myAccount) {
                                  addLevelInService.currentState!.openEndDrawer();
                                } else {
                                  addLevelInServiceForOnboarding.currentState!.openEndDrawer();
                                }
                              },
                              child: Container(
                                padding: const EdgeInsets.only(top: 5, bottom: 5, right: 10),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Icon(
                                      Icons.add,
                                      size: 17,
                                      color: AppColors.colorBlack,
                                    ),
                                    const SpaceHorizontal(0.002),
                                    Text(AppStringKey.add_levels.tr),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SpaceVertical(0.01),
              ],
            );
          },
        ),
      ],
    );
  }
}

class SearchModelWidget extends StatefulWidget {
  final int index;
  final int subCategoryIndex;
  final TextEditingController textEditingController;

  const SearchModelWidget({super.key, required this.index, required this.subCategoryIndex, required this.textEditingController});

  @override
  State<SearchModelWidget> createState() => _SearchModelWidgetState();
}

class _SearchModelWidgetState extends State<SearchModelWidget> {
  var controller = Get.find<ServiceController>();
  OverlayPortalController overlayPortalController = OverlayPortalController();
  LayerLink layerLink = LayerLink();
  FocusNode focusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return CompositedTransformTarget(
      link: layerLink,
      child: OverlayPortal(
        controller: overlayPortalController,
        overlayChildBuilder: (BuildContext context) {
          return CompositedTransformFollower(
            link: layerLink,
            targetAnchor: Alignment.bottomLeft,
            child: Align(
              alignment: AlignmentDirectional.topStart,
              child: Obx(
                () => MenuWidget(
                  width: context.isDesktop
                      ? context.isTabletView
                          ? context.width * 0.33
                          : context.width * 0.23
                      : context.width * 0.50,
                  subCategoryIndex: widget.subCategoryIndex,
                  categoryIndex: widget.index,
                  controller: overlayPortalController,
                  carBrandModelList: controller.filteredCarBrands.value,
                ),
              ),
            ),
          );
        },
        child: SizedBox(
          width: context.isDesktop
              ? context.isTabletView
                ? context.width * 0.33
                : context.width * 0.23
              : context.width * 0.50,
          child: InputTextField(
            label: AppStringKey.search_car.tr,
            hintText: AppStringKey.search_car.tr,
            isPhoneNo: false,
            isLabelVisible: false,
            validationRequired: false,
            focusScope: focusNode,
            isRequireField: false,
            onChangeWithValidationStatus: (value, isValid) {
              if (value!.isEmpty) {
                overlayPortalController.hide();
              } else {
                controller.updateDataBasedOnInput(value);
                if (!overlayPortalController.isShowing && controller.filteredCarBrands.isNotEmpty) {
                  overlayPortalController.show();
                }
              }
            },
            textEditingController: widget.textEditingController,
          ),
        ),
      ),
    );
  }
}

class LevelWidget extends StatelessWidget {
  final int categoryIndex;
  final int subCategoryIndex;
  final ServiceController controller;

  const LevelWidget({
    super.key,
    required this.categoryIndex,
    required this.subCategoryIndex,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ...controller.addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].levelList.indexed.map(
          (level) {
            int levelIndex = level.$1;
            LevelModel levelModel = level.$2;
            return Container(
              padding: const EdgeInsets.symmetric(vertical: 3, horizontal: 10),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          CustomText(
                            textAlign: TextAlign.center,
                            text: levelModel.index.toString(),
                            textStyle: BaseStyle.textStyleRobotoRegular(
                              8,
                              AppColors.primaryTextColor,
                            ),
                          ),
                          const SpaceHorizontal(0.04),
                          CustomText(
                            textAlign: TextAlign.center,
                            text: levelModel.levelName,
                            textStyle: BaseStyle.textStyleRobotoRegular(
                              8,
                              AppColors.primaryTextColor,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          CustomText(
                            textAlign: TextAlign.center,
                            text: '${levelModel.levelDiscountedPrice} ${AppConstants.currencyString}',
                            textStyle: BaseStyle.textStyleRobotoRegular(
                              8,
                              AppColors.primaryTextColor,
                            ),
                          ),
                          const SpaceHorizontal(0.04),
                          CustomClickEffectWidget(
                            widget: CustomText(
                              textAlign: TextAlign.center,
                              text: AppStringKey.edit.tr,
                              textStyle: BaseStyle.textStyleRobotoBold(
                                8,
                                AppColors.primaryTextColor,
                              ),
                            ),
                            borderRadius: 0,
                            onTap: () {
                              controller.levelIsFromAdd = false;
                              controller.categoryIndexAddLevel = categoryIndex;
                              controller.subCategoryIndexAddLevel = subCategoryIndex;
                              controller.levelForEditSelectedIndex = levelIndex;
                              controller.manualLevelTitle = levelModel.levelName;

                              controller.addLevelPriceController.text = levelModel.levelPrice;
                              controller.addLevelDiscountedPriceController.text = levelModel.levelDiscountedPrice;
                              controller.addLevelDescriptionController.text = levelModel.levelDescription;
                              for (var element in controller.levelCheckboxList) {
                                element.isCheckboxSelected = false;
                              }
                              controller
                                  .levelCheckboxList[controller.levelCheckboxList
                                      .indexWhere((item) => item.title.toLowerCase() == levelModel.levelName.toLowerCase())]
                                  .isCheckboxSelected = true;
                              addLevelInService.currentState!.openEndDrawer();
                            },
                          ),
                          const SpaceHorizontal(0.02),
                          HoverText(
                            onTap: () {
                              controller.categoryIndex = categoryIndex;
                              controller.subCategoryIndex = subCategoryIndex;
                              controller.levelIndex = levelIndex;
                              controller.deleteLevel(levelModel.levelId!, levelIndex);
                            },
                            defaultColor: AppColors.primaryTextColor,
                            hoverColor: AppColors.colorHoverClickRed,
                            textAlign: TextAlign.center,
                            text: AppStringKey.delete.tr,
                          ),
                        ],
                      )
                    ],
                  ),
                  const SpaceVertical(0.01),
                  const Divider(
                    height: 1,
                    color: AppColors.dividerColor,
                  ),
                  const SpaceVertical(0.01),
                ],
              ),
            );
          },
        )
      ],
    );
  }
}

class CustomTextButtonWithIcon extends StatelessWidget {
  final String buttonText;
  final IconData icon;
  final Function() callBack;
  final TextStyle textStyle;

  const CustomTextButtonWithIcon({
    super.key,
    required this.buttonText,
    required this.callBack,
    required this.icon,
    required this.textStyle,
  });

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: callBack,
      style: TextButton.styleFrom(
        textStyle: textStyle,
        shape: const BeveledRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(2),
          ),
        ),
        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 15),
        foregroundColor: AppColors.primaryTextColor,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            icon,
            size: 17,
            color: AppColors.colorBlack,
          ),
          const SpaceHorizontal(0.002),
          Text(
            buttonText,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
    );
  }
}

void selectedCarDialog(BuildContext context, RxList<SelectedCarModel> selectedCarList) {
  showDialog(
    context: context,
    builder: (BuildContext context) => GestureDetector(
      onTap: () {
        Get.back();
      },
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: GestureDetector(
            onTap: () {},
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: Container(
                margin: EdgeInsets.symmetric(
                  horizontal: context.isDesktop ? context.screenWidth * 0.300 : 50,
                  vertical: context.isDesktop ? 70 : 50,
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: context.isDesktop ? 10 : 30,
                  vertical: context.isDesktop ? 10 : 30,
                ),
                width: double.maxFinite,
                height: context.screenHeight,
                color: Colors.white,
                child: Stack(
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: context.isDesktop ? 20 : 30,
                        vertical: context.isDesktop ? 20 : 30,
                      ),
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppStringKey.selected_car.tr,
                              style: BaseStyle.textStyleRobotoSemiBold(
                                24,
                                AppColors.primaryTextColor,
                              ),
                            ),
                            const SpaceVertical(0.001),
                            Text(
                              AppStringKey.selected_car_desc.tr,
                              style: BaseStyle.textStyleRobotoRegular(
                                14,
                                AppColors.secondaryTextColor,
                              ),
                              maxLines: 2,
                            ),
                            const SpaceVertical(0.03),
                            Obx(
                              () => Column(
                                children: [
                                  ...selectedCarList.indexed.map(
                                    (e) {
                                      int index = e.$1;
                                      SelectedCarModel carModel = e.$2;
                                      return Container(
                                        padding: const EdgeInsets.all(10),
                                        margin: const EdgeInsets.only(top: 10),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            color: AppColors.borderColor,
                                            width: 0.5,
                                          ),
                                          borderRadius: BorderRadius.circular(5),
                                        ),
                                        child: CustomExpansionPanel(
                                          body: SelectedCarsDialogBody(
                                              controller: Get.find<ServiceController>(),
                                              carsModel: carModel,
                                              index: index,
                                              selectedCarList: selectedCarList),
                                          header: SelectedCarsDialogHeader(
                                            controller: Get.find<ServiceController>(),
                                            carsModel: carModel,
                                            index: index,
                                          ),
                                          onTap: () {
                                            Get.find<ServiceController>().selectedCarList.forEach((element) {
                                              element.isBrandNameSelected = false;
                                            });
                                            Get.find<ServiceController>().selectedCarList[index].isBrandNameSelected = true;
                                            Get.find<ServiceController>().selectedCarList.refresh();
                                          },
                                          isOpenDefault: carModel.isBrandNameSelected ?? true,
                                          key: UniqueKey(),
                                        ),
                                      );
                                    },
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      right: 00,
                      top: 10,
                      child: Material(
                        color: Colors.transparent,
                        child: Ink(
                          child: InkWell(
                            onTap: () {
                              Get.back();
                            },
                            child: const Icon(
                              Icons.close,
                              color: AppColors.colorBlack,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ), //Put your screen design here!
      ),
    ),
  );
}

class SelectedCarsDialogHeader extends StatelessWidget {
  final ServiceController controller;
  final SelectedCarModel carsModel;
  final int index;

  const SelectedCarsDialogHeader({
    super.key,
    required this.controller,
    required this.carsModel,
    required this.index,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(carsModel.isBrandNameSelected ?? true ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down),
        Text(
          carsModel.title ?? '',
          style: BaseStyle.textStyleRobotoSemiBold(
            15,
            AppColors.primaryTextColor,
          ),
        ),
      ],
    );
  }
}

class SelectedCarsDialogBody extends StatelessWidget {
  final ServiceController controller;
  final SelectedCarModel carsModel;
  final int index;
  final RxList<SelectedCarModel> selectedCarList;

  const SelectedCarsDialogBody({
    super.key,
    required this.controller,
    required this.carsModel,
    required this.index,
    required this.selectedCarList,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      padding: const EdgeInsets.all(10),
      margin: const EdgeInsets.only(top: 10, left: 7),
      decoration: BoxDecoration(
        color: AppColors.colorDragImageBack,
        border: Border.all(
          color: AppColors.strokeColor,
          width: 0.5,
        ),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ...selectedCarList[index].models!.indexed.map(
                (e) => Column(
                  children: [
                    const SpaceVertical(0.01),
                    Text(e.$2.title ?? ''),
                    const SpaceVertical(0.01),
                  ],
                ),
              ),
        ],
      ),
    );
  }
}

class MenuWidget extends StatelessWidget {
  const MenuWidget({
    super.key,
    this.width,
    required this.subCategoryIndex,
    required this.categoryIndex,
    required this.controller,
    required this.carBrandModelList,
  });

  final double? width;
  final int subCategoryIndex;
  final int categoryIndex;
  final OverlayPortalController controller;
  final List<CarBrandModel> carBrandModelList;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? 200,
      margin: const EdgeInsets.only(top: 6),
      height: context.isDesktop ? context.screenHeight * 0.37 : context.screenWidth * 0.45,
      decoration: ShapeDecoration(
        color: AppColors.colorWhite,
        shape: RoundedRectangleBorder(
          side: const BorderSide(
            width: 0.5,
            color: Colors.black26,
          ),
          borderRadius: BorderRadius.circular(8),
        ),
        shadows: const [
          BoxShadow(
            color: Color(0x11000000),
            blurRadius: 32,
            offset: Offset(0, 20),
            spreadRadius: -8,
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Obx(
            () => Expanded(
              child: SingleChildScrollView(
                child: Container(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    children: [
                      ...carBrandModelList.indexed.map((e) {
                        int brandIndex = e.$1;
                        CarBrandModel carBrandModel = e.$2;
                        ServiceController controller = Get.find<ServiceController>();
                        return Column(
                          children: [
                            Material(
                              color: AppColors.colorWhite,
                              child: Ink(
                                child: InkWell(
                                    onTap: () {
                                      var subCategory = controller.addCategoryList[categoryIndex].subCategoryList[subCategoryIndex];
                                      var selectedCarBrandIndex = carBrandModelList[brandIndex].id;
                                      var selectedCarTypeIndex = carBrandModelList[brandIndex].models.map((e) => e.id).toList();

                                      if (subCategory.carBrandSelectedIndex.contains(selectedCarBrandIndex)) {
                                        subCategory.carBrandSelectedIndex.remove(selectedCarBrandIndex);
                                        for (var element in selectedCarTypeIndex) {
                                          subCategory.selectedCarTypeIndex.remove(element);
                                        }
                                      } else {
                                        subCategory.carBrandSelectedIndex.add(selectedCarBrandIndex);
                                        subCategory.selectedCarTypeIndex.addAll(selectedCarTypeIndex);
                                      }

                                      controller.addCategoryList.refresh();
                                    },
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          carBrandModel.title,
                                          style: BaseStyle.textStyleRobotoSemiBold(
                                            16,
                                            AppColors.secondaryTextColor,
                                          ),
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(5),
                                            color: controller.addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].carBrandSelectedIndex
                                                    .contains(carBrandModelList[brandIndex].id)
                                                ? AppColors.colorBlack
                                                : AppColors.colorWhite,
                                            border: Border.all(
                                              color: AppColors.colorBlack,
                                              width: 1,
                                            ),
                                          ),
                                          width: 18,
                                          height: 18,
                                          child: controller.addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].carBrandSelectedIndex
                                                  .contains(carBrandModelList[brandIndex].id)
                                              ? Icon(
                                                  Icons.done,
                                                  size: 14,
                                                  color: controller
                                                          .addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].carBrandSelectedIndex
                                                          .contains(carBrandModelList[brandIndex].id)
                                                      ? AppColors.colorWhite
                                                      : AppColors.colorBlack,
                                                )
                                              : const SizedBox.shrink(),
                                        ),
                                      ],
                                    )),
                              ),
                            ),
                            Column(
                              children: [
                                ...carBrandModelList[brandIndex].models.indexed.map(
                                  (e) {
                                    int brandTypeIndex = e.$1;
                                    CarTypeModel carTypeModel = e.$2;
                                    return Column(
                                      children: [
                                        Container(
                                          margin: const EdgeInsets.only(left: 8, top: 4, bottom: 4),
                                          child: Material(
                                            color: AppColors.colorWhite,
                                            child: Ink(
                                              child: InkWell(
                                                onTap: () {
                                                  var selectedCarTypeIndex = carBrandModelList[brandIndex].models[brandTypeIndex].id;
                                                  var selectedSubCategory =
                                                      controller.addCategoryList[categoryIndex].subCategoryList[subCategoryIndex];

                                                  var isAllIDAvailable = selectedSubCategory.selectedCarTypeIndex.contains(selectedCarTypeIndex);
                                                  if (isAllIDAvailable) {
                                                    selectedSubCategory.selectedCarTypeIndex.remove(selectedCarTypeIndex);
                                                  } else {
                                                    selectedSubCategory.selectedCarTypeIndex.add(selectedCarTypeIndex);
                                                  }

                                                  isAllIDAvailable = carBrandModelList[brandIndex]
                                                      .models
                                                      .every((element) => selectedSubCategory.selectedCarTypeIndex.contains(element.id));

                                                  if (isAllIDAvailable) {
                                                    selectedSubCategory.carBrandSelectedIndex.add(carBrandModelList[brandIndex].id);
                                                  } else {
                                                    selectedSubCategory.carBrandSelectedIndex.remove(carBrandModelList[brandIndex].id);
                                                  }

                                                  controller.addCategoryList.refresh();
                                                },
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text(
                                                      carTypeModel.title,
                                                      style: BaseStyle.textStyleRobotoSemiBold(
                                                        16,
                                                        AppColors.primaryTextColor,
                                                      ),
                                                    ),
                                                    Container(
                                                      decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(5),
                                                        color: controller
                                                                .addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].selectedCarTypeIndex
                                                                .contains(carBrandModelList[brandIndex].models[brandTypeIndex].id)
                                                            ? AppColors.colorBlack
                                                            : Colors.transparent,
                                                        border: Border.all(
                                                          color: AppColors.colorBlack,
                                                          width: 1,
                                                        ),
                                                      ),
                                                      width: 18,
                                                      height: 18,
                                                      child: controller
                                                              .addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].selectedCarTypeIndex
                                                              .contains(carBrandModelList[brandIndex].models[brandTypeIndex].id)
                                                          ? Icon(
                                                              Icons.done,
                                                              size: 14,
                                                              color: controller.addCategoryList[categoryIndex].subCategoryList[subCategoryIndex]
                                                                      .selectedCarTypeIndex
                                                                      .contains(carBrandModelList[brandIndex].models[brandTypeIndex].id)
                                                                  ? AppColors.colorWhite
                                                                  : AppColors.colorBlack,
                                                            )
                                                          : const SizedBox.shrink(),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  },
                                )
                              ],
                            )
                          ],
                        );
                      })
                    ],
                  ),
                ),
              ),
            ),
          ),
          Column(
            children: [
              Container(
                width: double.infinity,
                height: 2,
                color: AppColors.dividerColor,
              ),
              const SpaceVertical(0.02),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomSquareCornerButton(
                    buttonText: AppStringKey.reset.tr,
                    buttonTextColor: Get.find<ServiceController>()
                                .addCategoryList[categoryIndex]
                                .subCategoryList[subCategoryIndex]
                                .carBrandSelectedIndex
                                .isNotEmpty ||
                            Get.find<ServiceController>()
                                .addCategoryList[categoryIndex]
                                .subCategoryList[subCategoryIndex]
                                .selectedCarTypeIndex
                                .isNotEmpty
                        ? AppColors.colorBlack
                        : AppColors.tertiaryTextColor,
                    backgroundColor: AppColors.colorBackground,
                    height: context.isDesktop ? 0.025 : 0.05,
                    width: context.isDesktop ? 0.10 : 0.20,
                    borderWidth: Get.find<ServiceController>()
                                .addCategoryList[categoryIndex]
                                .subCategoryList[subCategoryIndex]
                                .carBrandSelectedIndex
                                .isNotEmpty ||
                            Get.find<ServiceController>()
                                .addCategoryList[categoryIndex]
                                .subCategoryList[subCategoryIndex]
                                .selectedCarTypeIndex
                                .isNotEmpty
                        ? 2
                        : 0,
                    textSize: context.isDesktop ? 7 : 12,
                    borderColor: Get.find<ServiceController>()
                                .addCategoryList[categoryIndex]
                                .subCategoryList[subCategoryIndex]
                                .carBrandSelectedIndex
                                .isNotEmpty ||
                            Get.find<ServiceController>()
                                .addCategoryList[categoryIndex]
                                .subCategoryList[subCategoryIndex]
                                .selectedCarTypeIndex
                                .isNotEmpty
                        ? AppColors.borderColor
                        : Colors.transparent,
                    callBack: () {
                      Get.find<ServiceController>().addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].selectedCarTypeIndex.clear();
                      Get.find<ServiceController>().addCategoryList[categoryIndex].subCategoryList[subCategoryIndex].carBrandSelectedIndex.clear();
                      Get.find<ServiceController>().addCategoryList.refresh();
                    },
                  ),
                  const SpaceHorizontal(0.01),
                  CustomSquareCornerButton(
                    buttonText: AppStringKey.add.tr,
                    buttonTextColor: Get.find<ServiceController>()
                                .addCategoryList[categoryIndex]
                                .subCategoryList[subCategoryIndex]
                                .carBrandSelectedIndex
                                .isNotEmpty ||
                            Get.find<ServiceController>()
                                .addCategoryList[categoryIndex]
                                .subCategoryList[subCategoryIndex]
                                .selectedCarTypeIndex
                                .isNotEmpty
                        ? AppColors.colorWhite
                        : AppColors.tertiaryTextColor,
                    backgroundColor: Get.find<ServiceController>()
                                .addCategoryList[categoryIndex]
                                .subCategoryList[subCategoryIndex]
                                .carBrandSelectedIndex
                                .isNotEmpty ||
                            Get.find<ServiceController>()
                                .addCategoryList[categoryIndex]
                                .subCategoryList[subCategoryIndex]
                                .selectedCarTypeIndex
                                .isNotEmpty
                        ? AppColors.colorBlack
                        : AppColors.colorBackground,
                    height: context.isDesktop ? 0.025 : 0.05,
                    width: context.isDesktop ? 0.10 : 0.20,
                    textSize: context.isDesktop ? 7 : 12,
                    callBack: () {
                      final serviceController = Get.find<ServiceController>();
                      final categoryResponseModel = serviceController.categoryResponseModel[0];
                      final categoryList = serviceController.addCategoryList[categoryIndex];
                      final subCategoryList = categoryList.subCategoryList[subCategoryIndex];

                      final selectedCarTypeModels = subCategoryList.selectedCarTypeIndex.map((e) => ModelDto(model: Model(id: e))).toList();
                      // final carBrandSelectedModels = subCategoryList.carBrandSelectedIndex.map((e) => ModelDto(model: Model(id: e))).toList();

                      categoryResponseModel.subCategoryDto[subCategoryIndex].modelDto!.addAll(selectedCarTypeModels);
                      controller.hide();
                    },
                  )
                ],
              ),
              const SpaceVertical(0.02),
            ],
          ),
        ],
      ),
    );
  }
}

void editSubServiceData(BuildContext context, RxList<SubServiceDataModel> subServiceDataModel) {
  var controller = Get.find<ServiceController>();
  showDialog(
    context: context,
    builder: (BuildContext context) => IntrinsicHeight(
      child: IntrinsicWidth(
        child: AlertDialog(
          backgroundColor: Colors.transparent,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
          contentPadding: EdgeInsets.zero,
          content: GestureDetector(
            onTap: () {
              controller.isLevelSelected.value = false;
            },
            child: IntrinsicHeight(
              child: Column(
                children: subServiceDataModel.indexed.map(
                  (subCategory) {
                    int subCategoryIndex = subCategory.$1;
                    Rx<SubServiceDataModel> subServiceDataModel = subCategory.$2.obs;
                    return Stack(
                      children: [
                        Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 32, right: context.isMobile || context.isTabletView ? 32 : 48, top: 32, bottom: 32),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: AppColors.colorDragImageBack,
                                border: Border.all(
                                  width: 1,
                                  color: AppColors.strokeColor,
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            subServiceDataModel.value.category!.subcategoryName ?? '',
                                            style: BaseStyle.textStyleRobotoBold(
                                              22,
                                              AppColors.colorBlack,
                                            ),
                                          ),
                                          const SpaceVertical(0.02),
                                          Material(
                                            color: Colors.transparent,
                                            child: Ink(
                                              child: InkWell(
                                                onTap: () {
                                                  var subCategoryId = subServiceDataModel.value.category!.subcategoryId;
                                                  if (subCategoryId != '') {
                                                    controller.getSelectedCars(subCategoryId!);
                                                  } else {
                                                    Utilities.showToast(AppStringKey.select_sub_category.tr);
                                                  }
                                                },
                                                child: CustomText(
                                                  text: AppStringKey.view_added_card.tr,
                                                  textStyle: BaseStyle.underLineTextStyle(
                                                    7,
                                                    AppColors.primaryTextColor,
                                                    fontWeight: FontWeight.normal,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          const SpaceVertical(0.02),
                                          context.isMobile || context.isTabletView
                                              ? Obx(
                                                  () => Visibility(
                                                    visible: subServiceDataModel.value.category!.serviceDetailImage != null,
                                                    child: Container(
                                                      width: context.isMobile || context.isTabletView ? context.width * 0.20 : context.width * 0.09,
                                                      height: context.isMobile || context.isTabletView ? context.height * 0.1 : context.height * 0.2,
                                                      decoration: BoxDecoration(
                                                        color: AppColors.strokeColor,
                                                        borderRadius: BorderRadius.circular(5),
                                                      ),
                                                      child: Stack(
                                                        children: [
                                                          subServiceDataModel.value.category!.serviceDetailImage != null
                                                              ? Positioned(
                                                                  top: 0,
                                                                  bottom: 0,
                                                                  left: 0,
                                                                  right: 0,
                                                                  child: MouseRegion(
                                                                    cursor: SystemMouseCursors.click,
                                                                    child: ImageNetwork(
                                                                      key: ValueKey(subServiceDataModel.value.category!.serviceDetailImage),
                                                                      image: subServiceDataModel.value.category!.serviceDetailImage ?? '',
                                                                      fitWeb: BoxFitWeb.cover,
                                                                      width: context.width * 0.09,
                                                                      height: context.height * 0.2,
                                                                      onTap: () {
                                                                        CommonFunction.pickImages(0).then((value) {
                                                                          controller.imageUpload(context, value.entries.first.value, subCategoryIndex,
                                                                              isFromEdit: true, subServiceDataModel: subServiceDataModel);
                                                                        });
                                                                      },
                                                                    ),
                                                                  ),
                                                                )
                                                              : const SizedBox.shrink(),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              : const SizedBox.shrink(),
                                          context.isMobile || context.isTabletView ? const SpaceVertical(0.02) : const SizedBox.shrink(),
                                          SizedBox(
                                            width: context.isDesktop ? context.screenWidth * 0.3 : context.screenWidth * 0.55,
                                            child: Column(
                                              children: [
                                                CustomDropDownField(
                                                  isReadOnly: false,
                                                  labelVisibility: false,
                                                  textStyle: BaseStyle.textStyleRobotoRegular(
                                                    16,
                                                    AppColors.inputFieldHintTextColor,
                                                  ),
                                                  labelText: AppStringKey.added_levels.tr,
                                                  buttonPadding: EdgeInsets.zero,
                                                  validationText: AppStringKey.added_levels.tr,
                                                  hintTitle: AppStringKey.added_levels.tr,
                                                  onChanged: (levelName) {
                                                    controller.findSubServiceByName(levelName ?? '');
                                                  },
                                                  items: subServiceDataModel.value.category!.serviceDetailLevel!.map((e) => e.name ?? '').toList(),
                                                ),
                                                Obx(
                                                  () => Visibility(
                                                    visible: controller.isLevelSelected.value,
                                                    child: Column(
                                                      children: [
                                                        InputTextField(
                                                          textInputType: TextInputType.phone,
                                                          keyBoardAction: TextInputAction.next,
                                                          inputFormatter: [
                                                            FilteringTextInputFormatter.digitsOnly,
                                                          ],
                                                          label: '',
                                                          hintText: AppStringKey.enter_amount.tr,
                                                          isPhoneNo: false,
                                                          isLabelVisible: false,
                                                          isRequireField: false,
                                                          textEditingController: controller.editLevelPriceController,
                                                          onChangeWithValidationStatus: (val, isValid) {},
                                                        ),
                                                        InputTextField(
                                                          textInputType: TextInputType.phone,
                                                          keyBoardAction: TextInputAction.next,
                                                          inputFormatter: [
                                                            FilteringTextInputFormatter.digitsOnly,
                                                          ],
                                                          label: '',
                                                          hintText: "${AppStringKey.enter_discount_amount.tr} ${AppConstants.currencyString}",
                                                          isPhoneNo: false,
                                                          isLabelVisible: false,
                                                          isRequireField: false,
                                                          textEditingController: controller.editLevelDiscountedPriceController,
                                                          onChangeWithValidationStatus: (val, isValid) {},
                                                        ),
                                                        InputTextField(
                                                          label: '',
                                                          hintText: AppStringKey.enter_level_desc.tr,
                                                          isPhoneNo: false,
                                                          isLabelVisible: false,
                                                          isRequireField: false,
                                                          textEditingController: controller.editLevelDescriptionController,
                                                          maxLines: 3,
                                                          onChangeWithValidationStatus: (val, isValid) {},
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      const SpaceHorizontal(0.02),
                                      context.isMobile || context.isTabletView
                                          ? const SizedBox.shrink()
                                          : Obx(
                                              () => Visibility(
                                                visible: subServiceDataModel.value.category!.serviceDetailImage != null,
                                                child: Container(
                                                  width: context.width * 0.09,
                                                  height: context.height * 0.2,
                                                  decoration: BoxDecoration(
                                                    color: AppColors.strokeColor,
                                                    borderRadius: BorderRadius.circular(5),
                                                  ),
                                                  child: Stack(
                                                    children: [
                                                      subServiceDataModel.value.category!.serviceDetailImage != null
                                                          ? Positioned(
                                                              top: 0,
                                                              bottom: 0,
                                                              left: 0,
                                                              right: 0,
                                                              child: MouseRegion(
                                                                cursor: SystemMouseCursors.click,
                                                                child: ImageNetwork(
                                                                  key: ValueKey(subServiceDataModel.value.category!.serviceDetailImage),
                                                                  image: subServiceDataModel.value.category!.serviceDetailImage ?? '',
                                                                  fitWeb: BoxFitWeb.cover,
                                                                  width: context.width * 0.09,
                                                                  height: context.height * 0.2,
                                                                  onTap: () {
                                                                    CommonFunction.pickImages(0).then((value) {
                                                                      controller.imageUpload(context, value.entries.first.value, subCategoryIndex,
                                                                          isFromEdit: true, subServiceDataModel: subServiceDataModel);
                                                                    });
                                                                  },
                                                                ),
                                                              ),
                                                            )
                                                          : const SizedBox.shrink(),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                    ],
                                  ),
                                  const SpaceVertical(0.03),
                                  Obx(
                                    () => controller.isLevelSelected.value
                                        ? context.isMobile || context.isTabletView
                                            ? Column(
                                                children: [
                                                  Container(
                                                    width: context.screenWidth * 0.55,
                                                    decoration: BoxDecoration(
                                                      border: Border.all(
                                                        width: 1,
                                                        color: AppColors.primaryTextColor,
                                                      ),
                                                    ),
                                                    child: TextButton(
                                                      onPressed: () {
                                                        controller.isLevelSelected.value = false;
                                                        Get.back();
                                                      },
                                                      style: TextButton.styleFrom(
                                                        padding: const EdgeInsets.symmetric(horizontal: 28, vertical: 8),
                                                        textStyle: BaseStyle.textStyleRobotoMedium(14, AppColors.primaryTextColor),
                                                        backgroundColor: AppColors.colorWhite,
                                                        foregroundColor: AppColors.primaryTextColor,
                                                      ),
                                                      child: Text(AppStringKey.cancel.tr),
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    height: 10,
                                                  ),
                                                  Container(
                                                    width: context.screenWidth * 0.55,
                                                    decoration: BoxDecoration(
                                                      border: Border.all(
                                                        width: 1,
                                                        color: AppColors.primaryTextColor,
                                                      ),
                                                    ),
                                                    child: TextButton(
                                                      onPressed: () {
                                                        if (double.parse(controller.editLevelDiscountedPriceController.text) <
                                                            double.parse(controller.editLevelPriceController.text)) {
                                                          controller.editLevelData(
                                                            subServiceDataModel
                                                                    .value.category!.serviceDetailLevel![controller.selectedLevelIndex].levelId ??
                                                                '',
                                                            double.parse(controller.editLevelPriceController.text),
                                                            double.parse(controller.editLevelDiscountedPriceController.text),
                                                            controller.editLevelDescriptionController.text,
                                                          );
                                                        } else {
                                                          Utilities.showToast(AppStringKey.enter_valid_discount_price.tr);
                                                        }
                                                      },
                                                      style: TextButton.styleFrom(
                                                        padding: const EdgeInsets.symmetric(horizontal: 28, vertical: 8),
                                                        textStyle: BaseStyle.textStyleRobotoMedium(14, AppColors.primaryTextColor),
                                                        backgroundColor: AppColors.primaryTextColor,
                                                        foregroundColor: AppColors.colorWhite,
                                                        shape: const BeveledRectangleBorder(),
                                                      ),
                                                      child: Text(AppStringKey.save.tr),
                                                    ),
                                                  ),
                                                ],
                                              )
                                            : Row(
                                                mainAxisAlignment:
                                                    context.isMobile || context.isTabletView ? MainAxisAlignment.center : MainAxisAlignment.end,
                                                children: [
                                                  Container(
                                                    width: 120,
                                                    decoration: BoxDecoration(
                                                      border: Border.all(
                                                        width: 1,
                                                        color: AppColors.primaryTextColor,
                                                      ),
                                                    ),
                                                    child: TextButton(
                                                      onPressed: () {
                                                        controller.isLevelSelected.value = false;
                                                        Get.back();
                                                      },
                                                      style: TextButton.styleFrom(
                                                        padding: const EdgeInsets.symmetric(horizontal: 28, vertical: 8),
                                                        textStyle: BaseStyle.textStyleRobotoMedium(14, AppColors.primaryTextColor),
                                                        backgroundColor: AppColors.colorWhite,
                                                        foregroundColor: AppColors.primaryTextColor,
                                                      ),
                                                      child: Text(AppStringKey.cancel.tr),
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    width: 18,
                                                  ),
                                                  Container(
                                                    width: 120,
                                                    decoration: BoxDecoration(
                                                      border: Border.all(
                                                        width: 1,
                                                        color: AppColors.primaryTextColor,
                                                      ),
                                                    ),
                                                    child: TextButton(
                                                      onPressed: () {
                                                        if (double.parse(controller.editLevelDiscountedPriceController.text) <
                                                            double.parse(controller.editLevelPriceController.text)) {
                                                          controller.editLevelData(
                                                            subServiceDataModel
                                                                    .value.category!.serviceDetailLevel![controller.selectedLevelIndex].levelId ??
                                                                '',
                                                            double.parse(controller.editLevelPriceController.text),
                                                            double.parse(controller.editLevelDiscountedPriceController.text),
                                                            controller.editLevelDescriptionController.text,
                                                          );
                                                        } else {
                                                          Utilities.showToast(AppStringKey.enter_valid_discount_price.tr);
                                                        }
                                                      },
                                                      style: TextButton.styleFrom(
                                                        padding: const EdgeInsets.symmetric(horizontal: 28, vertical: 8),
                                                        textStyle: BaseStyle.textStyleRobotoMedium(14, AppColors.primaryTextColor),
                                                        backgroundColor: AppColors.primaryTextColor,
                                                        foregroundColor: AppColors.colorWhite,
                                                        shape: const BeveledRectangleBorder(),
                                                      ),
                                                      child: Text(AppStringKey.save.tr),
                                                    ),
                                                  ),
                                                ],
                                              )
                                        : const SizedBox.shrink(),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        Positioned(
                          right: 10,
                          top: 10,
                          child: Material(
                            color: Colors.transparent,
                            child: Ink(
                              child: InkWell(
                                onTap: () {
                                  controller.isLevelSelected.value = false;
                                  Get.back();
                                },
                                child: const Icon(
                                  Icons.close,
                                  size: 20,
                                  color: AppColors.colorBlack,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    );
                  },
                ).toList(),
              ),
            ),
          ),
        ),
      ),
    ),
  ).then((value) {
    controller.isLevelSelected.value = false;
  });
}
