import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../Utils/utilities.dart';
import '../../common/input_text_field.dart';
import '../../common/space_horizontal.dart';
import '../../common/space_vertical.dart';
import '../../core/constant/app_color.dart';
import '../../core/constant/ui_constants.dart';
import '../../core/extensions/common_extension.dart';
import '../../core/extensions/validation_function.dart';
import '../../l10n/app_string_key.dart';
import 'account_details_controller.dart';

class AccountDetailWebView extends StatelessWidget {
  final bool isReadOnly;
  final bool enableSync;
  final AccountDetailsController controller;

  const AccountDetailWebView({
    super.key,
    required this.controller,
    required this.enableSync,
    required this.isReadOnly,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: context.screenHeight * 0.7,
      child: Container(
        color: AppColors.colorWhite,
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  flex: 5,
                  child: InputTextField(
                    label: AppStringKey.account_holder_name.tr,
                    hintText: AppStringKey.enter_account_holder_name.tr,
                    isPhoneNo: false,
                    textInputType: TextInputType.text,
                    textEditingController: controller.accountHolderName,
                    isRequireField: true,
                    focusScope: controller.accountHolderNameFocusNode,
                    isReadOnly: isReadOnly,
                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                    onChangeWithValidationStatus: (value, isValid) {
                      if (enableSync && isValid) {
                        Utilities.customDebounce(customDebounceTime, () {
                          controller.submitAccountDetailsData();
                        });
                      }
                    },
                  ),
                ),
                const SpaceHorizontal(0.05),
                const Expanded(
                  flex: 5,
                  child: Visibility.maintain(
                    visible: false,
                    child: SizedBox.shrink(),
                  ),
                ),
              ],
            ),
            const SpaceVertical(0.05),
            Row(
              children: [
                Expanded(
                  flex: 5,
                  child: InputTextField(
                    label: AppStringKey.account_no.tr,
                    hintText: AppStringKey.enter_account_no.tr,
                    isPhoneNo: false,
                    inputFormatter: [FilteringTextInputFormatter.digitsOnly],
                    textEditingController: controller.accountNumber,
                    isRequireField: true,
                    focusScope: controller.accountNumberFocusNode,
                    isReadOnly: isReadOnly,
                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                    maxLength: 15,
                    onChangeWithValidationStatus: (value, isValid) {
                      if (enableSync && isValid) {
                        Utilities.customDebounce(customDebounceTime, () {
                          controller.submitAccountDetailsData();
                        });
                      }
                    },
                  ),
                ),
                const SpaceHorizontal(0.05),
                Expanded(
                  flex: 5,
                  child: InputTextField(
                    label: AppStringKey.re_enter_account_no.tr,
                    hintText: AppStringKey.re_enter_account_no.tr,
                    isPhoneNo: false,
                    inputFormatter: [FilteringTextInputFormatter.digitsOnly],
                    textEditingController: controller.reEnterAccountNumber,
                    isRequireField: true,
                    focusScope: controller.reEnterAccountNumberFocusNode,
                    isReadOnly: isReadOnly,
                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                    maxLength: 15,
                    onChangeWithValidationStatus: (value, isValid) {
                      if (enableSync && isValid) {
                        Utilities.customDebounce(customDebounceTime, () {
                          controller.submitAccountDetailsData();
                        });
                      }
                    },
                    validationFunction: (val) {
                      return Validation.doFieldsMatch(
                          controller.accountNumber.text,
                          controller.reEnterAccountNumber.text);
                    },
                    validationMessage: (val) {
                      return AppStringKey
                          .validation_account_number_not_match.tr;
                    },
                  ),
                ),
              ],
            ),
            const SpaceVertical(0.05),
            Row(
              children: [
                Expanded(
                  flex: 5,
                  child: InputTextField(
                    label: AppStringKey.bank_name.tr,
                    hintText: AppStringKey.enter_bank_name.tr,
                    isPhoneNo: false,
                    textInputType: TextInputType.text,
                    textEditingController: controller.bankName,
                    isRequireField: true,
                    focusScope: controller.bankNameFocusNode,
                    isReadOnly: isReadOnly,
                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                    onChangeWithValidationStatus: (value, isValid) {
                      if (enableSync && isValid) {
                        Utilities.customDebounce(customDebounceTime, () {
                          controller.submitAccountDetailsData();
                        });
                      }
                    },
                  ),
                ),
                const SpaceHorizontal(0.05),
                Expanded(
                  flex: 5,
                  child: InputTextField(
                    label: AppStringKey.bank_address.tr,
                    hintText: AppStringKey.enter_bank_address.tr,
                    isPhoneNo: false,
                    textEditingController: controller.bankAddress,
                    isRequireField: true,
                    focusScope: controller.bankAddressFocusNode,
                    isReadOnly: isReadOnly,
                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                    onChangeWithValidationStatus: (value, isValid) {
                      if (enableSync && isValid) {
                        Utilities.customDebounce(customDebounceTime, () {
                          controller.submitAccountDetailsData();
                        });
                      }
                    },
                  ),
                ),
              ],
            ),
            const SpaceVertical(0.05),
            Row(
              children: [
                Expanded(
                  flex: 5,
                  child: InputTextField(
                    label: AppStringKey.IBAN_code.tr,
                    hintText: AppStringKey.enter_IBAN_code.tr,
                    isPhoneNo: false,
                    textEditingController: controller.ibanCode,
                    isRequireField: true,
                    focusScope: controller.ibanCodeFocusNode,
                    isReadOnly: isReadOnly,
                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                    onChangeWithValidationStatus: (value, isValid) {
                      if (enableSync && isValid) {
                        Utilities.customDebounce(customDebounceTime, () {
                          controller.submitAccountDetailsData();
                        });
                      }
                    },
                  ),
                ),
                const SpaceHorizontal(0.05),
                Expanded(
                  flex: 5,
                  child: InputTextField(
                    label: AppStringKey.swift_code.tr,
                    hintText: AppStringKey.enter_swift_code.tr,
                    isPhoneNo: false,
                    textEditingController: controller.swiftCode,
                    isRequireField: true,
                    focusScope: controller.swiftCodeCodeFocusNode,
                    isReadOnly: isReadOnly,
                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                    onChangeWithValidationStatus: (value, isValid) {
                      if (enableSync && isValid) {
                        Utilities.customDebounce(customDebounceTime, () {
                          controller.submitAccountDetailsData();
                        });
                      }
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
