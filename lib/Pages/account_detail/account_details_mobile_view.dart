import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../Utils/utilities.dart';
import '../../common/input_text_field.dart';
import '../../core/constant/app_color.dart';
import '../../core/constant/ui_constants.dart';
import '../../core/extensions/validation_function.dart';
import '../../l10n/app_string_key.dart';
import 'account_details_controller.dart';

class AccountDetailMobileView extends StatelessWidget {
  final bool isReadOnly;
  final bool enableSync;
  final AccountDetailsController controller;

  const AccountDetailMobileView({
    super.key,
    required this.controller,
    required this.enableSync,
    required this.isReadOnly,
  });
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Container(
        color: AppColors.colorWhite,
        child: Column(
          children: [
            InputTextField(
              label: AppStringKey.account_holder_name.tr,
              hintText: AppStringKey.enter_account_holder_name.tr,
              isPhoneNo: false,
              isRequireField: false,
              textInputType: TextInputType.text,
              textEditingController: controller.accountHolderName,
              isReadOnly: isReadOnly,
              readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
              onChangeWithValidationStatus: (value, isValid) {
                if (isValid) {
                  Utilities.customDebounce(customDebounceTime, () {
                    controller.submitAccountDetailsData();
                  });
                }
              },
            ),
            const SizedBox(
              height: 24,
            ),
            InputTextField(
              label: AppStringKey.account_no.tr,
              hintText: AppStringKey.enter_account_no.tr,
              isPhoneNo: false,
              isRequireField: false,
              textEditingController: controller.accountNumber,
              inputFormatter: [FilteringTextInputFormatter.digitsOnly],
              isReadOnly: isReadOnly,
              readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
              onChangeWithValidationStatus: (value, isValid) {
                if (isValid) {
                  Utilities.customDebounce(customDebounceTime, () {
                    controller.submitAccountDetailsData();
                  });
                }
              },
            ),
            const SizedBox(
              height: 24,
            ),
            InputTextField(
              label: AppStringKey.re_enter_account_no.tr,
              hintText: AppStringKey.re_enter_account_no.tr,
              isPhoneNo: false,
              isRequireField: false,
              inputFormatter: [FilteringTextInputFormatter.digitsOnly],
              textEditingController: controller.reEnterAccountNumber,
              isReadOnly: isReadOnly,
              readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
              onChangeWithValidationStatus: (value, isValid) {
                if (isValid) {
                  Utilities.customDebounce(customDebounceTime, () {
                    controller.submitAccountDetailsData();
                  });
                }
              },
              validationFunction: (val) {
                return Validation.doFieldsMatch(controller.accountNumber.text,
                    controller.reEnterAccountNumber.text);
              },
              validationMessage: (val) {
                return AppStringKey.validation_account_number_not_match.tr;
              },
            ),
            const SizedBox(
              height: 24,
            ),
            InputTextField(
              label: AppStringKey.bank_name.tr,
              hintText: AppStringKey.enter_bank_name.tr,
              isPhoneNo: false,
              isRequireField: false,
              textInputType: TextInputType.text,
              textEditingController: controller.bankName,
              isReadOnly: isReadOnly,
              readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
              onChangeWithValidationStatus: (value, isValid) {
                if (isValid) {
                  Utilities.customDebounce(customDebounceTime, () {
                    controller.submitAccountDetailsData();
                  });
                }
              },
            ),
            const SizedBox(
              height: 24,
            ),
            InputTextField(
              label: AppStringKey.bank_address.tr,
              hintText: AppStringKey.enter_bank_address.tr,
              isPhoneNo: false,
              textEditingController: controller.bankAddress,
              isRequireField: true,
              focusScope: controller.bankAddressFocusNode,
              isReadOnly: isReadOnly,
              readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
              onChangeWithValidationStatus: (value, isValid) {
                if (isValid) {
                  Utilities.customDebounce(customDebounceTime, () {
                    controller.submitAccountDetailsData();
                  });
                }
              },
            ),
            const SizedBox(
              height: 24,
            ),
            InputTextField(
              label: AppStringKey.IBAN_code.tr,
              hintText: AppStringKey.enter_IBAN_code.tr,
              isPhoneNo: false,
              isRequireField: false,
              textEditingController: controller.ibanCode,
              isReadOnly: isReadOnly,
              readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
              onChangeWithValidationStatus: (value, isValid) {
                if (isValid) {
                  Utilities.customDebounce(customDebounceTime, () {
                    controller.submitAccountDetailsData();
                  });
                }
              },
            ),
            const SizedBox(
              height: 24,
            ),
            InputTextField(
              label: AppStringKey.swift_code.tr,
              hintText: AppStringKey.enter_swift_code.tr,
              isPhoneNo: false,
              textEditingController: controller.swiftCode,
              isRequireField: true,
              focusScope: controller.swiftCodeCodeFocusNode,
              isReadOnly: isReadOnly,
              readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
              onChangeWithValidationStatus: (value, isValid) {
                if (isValid) {
                  Utilities.customDebounce(customDebounceTime, () {
                    controller.submitAccountDetailsData();
                  });
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
