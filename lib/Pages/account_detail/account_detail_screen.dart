import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../core/constant/ui_constants.dart';
import '../../core/extensions/common_extension.dart';
import 'account_details_controller.dart';
import 'account_details_mobile_view.dart';
import 'account_details_web_view.dart';

class AccountDetailScreen extends StatefulWidget {
  final bool isReadOnly;
  final bool enableSync;

  const AccountDetailScreen({
    super.key,
    this.isReadOnly = false,
    this.enableSync = true,
  });

  @override
  State<AccountDetailScreen> createState() => _AccountDetailScreenState();
}

class _AccountDetailScreenState extends State<AccountDetailScreen> {
  final controller = Get.find<AccountDetailsController>();

  @override
  void initState() {
    super.initState();
    stepperPosition = StepperPosition.accountDetails;
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.getAccountDetailData();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          child: context.isDesktop
              ? AccountDetailWebView(
                  controller: controller,
                  isReadOnly: widget.isReadOnly,
                  enableSync: widget.enableSync,
                )
              : AccountDetailMobileView(
                  controller: controller,
                  isReadOnly: widget.isReadOnly,
                  enableSync: widget.enableSync,
                ),
        )
      ],
    );
  }
}
