import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../api/api_end_point.dart';
import '../../api/api_interface.dart';
import '../../api/api_presenter.dart';
import '../../core/app_preference/app_preferences.dart';
import '../../core/app_preference/storage_keys.dart';
import '../../core/constant/app_color.dart';
import '../../core/constant/ui_constants.dart';
import '../../l10n/app_string_key.dart';
import '../../models/account_detail_data_model.dart';
import '../../models/garage_id_model.dart';
import '../../models/success_account_data_model.dart';
import '../Dashboard/MyAccount/account_details/accounts_details_controller.dart';
import '../custom_stepper/custom_stepper_controller.dart';
import '../custom_stepper/widgets/branch_handler/branch_handler_controller.dart';

class AccountDetailsController extends GetxController implements ApiCallBacks {
  TextEditingController accountHolderName = TextEditingController();
  TextEditingController accountNumber = TextEditingController();
  TextEditingController reEnterAccountNumber = TextEditingController();
  TextEditingController bankName = TextEditingController();
  TextEditingController ibanCode = TextEditingController();
  TextEditingController bankAddress = TextEditingController();
  TextEditingController swiftCode = TextEditingController();

  FocusNode accountHolderNameFocusNode = FocusNode();
  FocusNode accountNumberFocusNode = FocusNode();
  FocusNode reEnterAccountNumberFocusNode = FocusNode();
  FocusNode bankNameFocusNode = FocusNode();
  FocusNode ibanCodeFocusNode = FocusNode();
  FocusNode bankAddressFocusNode = FocusNode();
  FocusNode swiftCodeCodeFocusNode = FocusNode();

  SuccessAccountModel successAccountModel = SuccessAccountModel();

  int branchIndex = 0;

  void setBranchIndex(int index) {
    submitAccountDetailsData();
    clearData();
    branchIndex = index;
    Get.find<BranchHandlerController>().setActiveBranchIndex(branchIndex);
    getAccountDetailData();
  }

  void setAccountDetailApi() {
    ApiPresenter(this).accountDetails(accountDetailDataModel().toJson());
  }

  void getAccountDetailData() {
    ApiPresenter(this).getAccountDetails(
      AppPreferences.sharedPrefRead(
        StorageKeys.garageId,
      ),
    );
  }

  void submitAccountDetailsData() {
    ApiPresenter(this).submitAccountDetails(
        accountDetailDataModel().toJson(), successAccountModel.id!);
  }

  void setCurrentData() {
    accountHolderName.text = successAccountModel.accountHolderName!;
    accountNumber.text = successAccountModel.accountNumber!;
    reEnterAccountNumber.text = successAccountModel.accountNumber!;
    bankName.text = successAccountModel.bankName!;
    ibanCode.text = successAccountModel.IBAN!;
    swiftCode.text = successAccountModel.swift!;
    bankAddress.text = successAccountModel.address!;
  }

  void validateData() {
    submitAccountDetailsData();
    ApiPresenter(this).validateOnboardingForm('bank-detail');
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {}

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {}

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.accountDetails:
        successAccountModel = SuccessAccountModel.fromJson(object['data']);
        break;
      case ApiEndPoints.getAccountDetails:
        if (object['data'].length > 0) {
          successAccountModel = SuccessAccountModel.fromJson(object['data'][0]);
          setCurrentData();
        } else {
          setAccountDetailApi();
        }
        break;
      case ApiEndPoints.validateData:
        if (object['data'].length == 0) {
          Fluttertoast.cancel();
          Get.find<CustomStepperController>().setActivePage(
              Get.find<CustomStepperController>().selectedIndex.value + 1);
        } else {
          String incompleteBranches =
              object['data'].map((entry) => entry["branchName"]).join(', ');
          Fluttertoast.showToast(
            msg:
                "${AppStringKey.please_complete_required_fields.tr} $incompleteBranches",
            gravity: ToastGravity.BOTTOM,
            webPosition: 'center',
            webBgColor: '#FF4D4F',
            webShowClose: true,
            fontSize: 16,
            timeInSecForIosWeb: toastVisibleTime,
            textColor: AppColors.colorWhite,
          );
        }
        break;
      case ApiEndPoints.submitAccountDetails:
        if (isUpdatingMyAccounts) {
          Get.find<AccountDetailsDashboardController>().getBankData();
          isUpdatingMyAccounts = false;
        }
        break;
    }
  }

  AccountDetailDataModel accountDetailDataModel() => AccountDetailDataModel(
        garage: Garage(
          id: AppPreferences.sharedPrefRead(StorageKeys.garageId),
        ),
        accountHolderName: accountHolderName.text,
        accountNumber: accountNumber.text,
        confirmAccountNumber: reEnterAccountNumber.text,
        bankName: bankName.text,
        IBAN: ibanCode.text,
        swift: swiftCode.text,
        address: bankAddress.text,
      );

  void clearData() {
    accountHolderName.clear();
    accountNumber.clear();
    reEnterAccountNumber.clear();
    bankName.clear();
    ibanCode.clear();
    swiftCode.clear();
    bankAddress.clear();
  }
}
