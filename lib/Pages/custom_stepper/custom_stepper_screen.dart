import 'package:al_baida_garage_fe/common/common_modal_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../Utils/utilities.dart';
import '../../common/language_selector.dart';
import '../../common/space_horizontal.dart';
import '../../common/space_vertical.dart';
import '../../core/app_preference/app_preferences.dart';
import '../../core/app_preference/storage_keys.dart';
import '../../core/auth/signout.dart';
import '../../core/constant/app_color.dart';
import '../../core/constant/app_images.dart';
import '../../core/constant/base_style.dart';
import '../../core/constant/ui_constants.dart';
import '../../core/constant/widget_functions.dart';
import '../../core/extensions/common_extension.dart';
import '../../core/routes/route_paths.dart';
import '../../custom_classes/custom_text.dart';
import '../../l10n/app_string_key.dart';
import '../Dashboard/multi_branch_handler/multi_branch_handler_controller.dart';
import '../account_detail/account_details_controller.dart';
import '../basic_details/basic_details_controller.dart';
import '../marketing/marketing_controller.dart';
import '../onboarding_summary/onboarding_summary_controller.dart';
import '../services/add_level_drawer_widget.dart';
import '../services/models/missing_value.dart';
import '../services/service_controller.dart';
import '../staff/add_staff_controller.dart';
import 'custom_stepper_controller.dart';
import 'widgets/branch_handler/branch_handler_controller.dart';
import 'widgets/custom_square_corner_button.dart';
import 'widgets/stepper_item_widget.dart';
import 'widgets/stepper_item_widget_mobile.dart';

class CustomStepperScreen extends StatefulWidget {
  const CustomStepperScreen({super.key});

  @override
  State<CustomStepperScreen> createState() => _CustomStepperScreenState();
}

class _CustomStepperScreenState extends State<CustomStepperScreen> {
  final controller = Get.find<CustomStepperController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.updateLanguage();
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (!AppPreferences.sharedPrefRead(StorageKeys.isAddBranchesFromAuth) /* && addBranchesFrom == addBranch*/) {
          Get.find<MultiBranchHandlerController>().getBranches();
        }
        return Future.value(true);
      },
      child: Scaffold(
        key: addLevelInServiceForOnboarding,
        endDrawer: const AddLevelDrawerWidget(),
        backgroundColor: AppColors.primaryBackgroundColor,
        bottomNavigationBar: context.isDesktop
            ? const SizedBox.shrink()
            : Container(
                padding: EdgeInsets.symmetric(
                  horizontal: context.screenWidth * 0.001,
                  vertical: context.screenHeight * 0.001,
                ),
                decoration: BoxDecoration(
                  border: LRTB_Border(0.5, 0.5, 0.5, 0.5, AppColors.borderColor),
                  color: AppColors.colorWhite,
                ),
                height: context.screenHeight * 0.1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Obx(
                      () => Visibility(
                        visible: controller.selectedIndex.value < 5,
                        child: CustomSquareCornerButton(
                          buttonText: AppStringKey.skip.tr,
                          buttonTextColor: AppColors.colorBlack,
                          backgroundColor: AppColors.colorBackground,
                          height: 0.07,
                          width: 0.30,
                          textSize: 13,
                          callBack: () {
                            if (controller.selectedIndex.value < controller.stepperList.length - 1) {
                              controller.selectedIndex++;
                              for (var element in controller.stepperList) {
                                element.isSelected = false;
                              }
                              controller.stepperList[controller.selectedIndex.value].isSelected = true;
                              controller.stepperList.refresh();
                            } else {
                              Get.toNamed(RoutePaths.DASHBOARD_STEPPER);
                            }
                          },
                        ),
                      ),
                    ),
                    Visibility(
                      visible: controller.selectedIndex.value < 5,
                      child: const SpaceHorizontal(0.02),
                    ),
                    CustomSquareCornerButton(
                      buttonText: AppStringKey.previous.tr,
                      buttonTextColor: AppColors.colorBlack,
                      backgroundColor: AppColors.colorBackground,
                      height: 0.07,
                      width: 0.30,
                      textSize: 13,
                      callBack: () {
                        if (controller.selectedIndex > 0) {
                          controller.setActivePage(controller.selectedIndex.value - 1);
                        }
                      },
                    ),
                    const SpaceHorizontal(0.01),
                    CustomSquareCornerButton(
                      buttonText: controller.selectedIndex < controller.stepperList.length - 1 ? AppStringKey.next.tr : AppStringKey.finish_setup.tr,
                      buttonTextColor: AppColors.colorWhite,
                      backgroundColor: AppColors.colorBlack,
                      height: 0.07,
                      width: 0.30,
                      textSize: 13,
                      callBack: () {
                        if (controller.selectedIndex < controller.stepperList.length - 1) {
                          if (stepperPosition == StepperPosition.basicDetails) {
                            Get.find<BasicDetailsController>().validateData();
                          } else if (stepperPosition == StepperPosition.accountDetails) {
                            Get.find<AccountDetailsController>().validateData();
                          } else if (stepperPosition == StepperPosition.marketing) {
                            Get.find<MarketingController>().validateData();
                          } else if (stepperPosition == StepperPosition.services) {
                            List<MissingValue> missingValues =
                                Get.find<ServiceController>().findMissingValues(Get.find<ServiceController>().categoryResponseModel);
                            if (missingValues.isNotEmpty) {
                              for (var missingValue in missingValues) {
                                Utilities.showToast(missingValue.message);
                                break;
                              }
                            } else {
                              if (Get.find<ServiceController>().categoryResponseModel.isNotEmpty) {
                                Get.find<ServiceController>().submitServiceData();
                              } else {
                                Get.find<CustomStepperController>().setActivePage(Get.find<CustomStepperController>().selectedIndex.value + 1);
                              }
                            }
                          } else if (stepperPosition == StepperPosition.addStaff) {
                            controller.setActivePage(controller.selectedIndex.value + 1);
                          } else if (stepperPosition == StepperPosition.addStaff) {
                            Get.find<AddStaffController>().validateData();
                          }
                        } else {
                          Get.find<OnboardingSummaryController>().validateData();
                        }
                      },
                    ),
                  ],
                ),
              ),
        body: Column(
          children: [
            context.isDesktop ? _WebAppView(controller: controller) : _MobileAppView(controller: controller),
          ],
        ),
      ),
    );
  }
}

class _MobileAppView extends StatelessWidget {
  final CustomStepperController controller;

  const _MobileAppView({
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: context.screenWidth * 0.1),
          height: context.screenHeight * 0.08,
          color: AppColors.colorWhite,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SvgPicture.asset(
                AppImages.appTopIcon,
                width: 50,
                color: AppColors.primaryTextColor,
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const GlobalLanguageSelector(),
                  const SizedBox(
                    width: 16,
                  ),
                  Material(
                    child: Ink(
                      child: InkWell(
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return CommonModalWidget(
                                  modalIcon: AppImages.warning,
                                  title: AppStringKey.logout_warning.tr,
                                  description: AppStringKey.logout_warning_description.tr,
                                  firstButtonContent: AppStringKey.cancel.tr,
                                  firstButtonOnTap: () {
                                    Navigator.pop(context);
                                  },
                                  firstButtonBackgroundColor: AppColors.secondaryCtaColor,
                                  firstButtonTextColor: AppColors.inputFieldHeading,
                                  secondButtonContent: AppStringKey.logoutButton.tr,
                                  secondButtonOnTap: () {
                                    CommonSignOut().signOut();
                                  },
                                );
                              });
                        },
                        child: SvgPicture.asset(
                          AppImages.signOut,
                          width: context.screenWidth * 0.05,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Obx(
          () => Align(
            alignment: Alignment.center,
            child: SizedBox(
              height: context.screenHeight * 0.1,
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: controller.stepperList.length - 1,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return StepperItemWidgetMobile(
                    stepperList: controller.stepperList,
                    index: index,
                    onTap: (int selectedIndex) {
                      // controller.selectedIndex = selectedIndex;
                      // for (var element in controller.stepperList) {
                      //   element.isSelected = false;
                      // }
                      // controller.stepperList[index].isSelected = true;
                      // controller.stepperList.refresh();
                      // if (controller.stepperList[selectedIndex].title != 'Basic Details') {
                      //   Get.find<BranchHandlerController>().setActiveBranchIndex(0);
                      // }
                    },
                    isStepCompleted: controller.stepperList[index].isSelected!,
                    selectedIndex: controller.selectedIndex.value,
                  );
                },
              ),
            ),
          ),
        ),
        Obx(
          () => Container(
            height: context.screenHeight * 0.72,
            child: SingleChildScrollView(
              padding: const EdgeInsets.fromLTRB(8, 34, 8, 8),
              child: controller.stepperList[controller.selectedIndex.value].childPage,
            ),
          ),
        ),
      ],
    );
  }
}

class _WebAppView extends StatelessWidget {
  final CustomStepperController controller;

  const _WebAppView({
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          flex: 2,
          child: Container(
            padding: EdgeInsets.symmetric(
              horizontal: context.screenWidth * 0.02,
              vertical: context.screenHeight * 0.05,
            ),
            decoration: BoxDecoration(
              border: LRTB_Border(0, 0.5, 0, 0, AppColors.borderColor),
              color: AppColors.colorWhite,
            ),
            height: context.screenHeight,
            child: Container(
              padding: EdgeInsets.only(
                right: context.screenWidth * 0.015,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: SvgPicture.asset(
                      AppImages.appTopIcon,
                      width: 80,
                      color: AppColors.primaryTextColor,
                    ),
                  ),
                  const SpaceVertical(0.08),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(
                        left: context.screenWidth * 0.005,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Obx(
                            () => ListView.builder(
                              itemCount: controller.stepperList.length - 1,
                              shrinkWrap: true,
                              itemBuilder: (context, index) {
                                return StepperItemWidget(
                                  stepperList: controller.stepperList.value,
                                  index: index,
                                  onTap: (int selectedValue) {
                                    controller.selectedIndex.value = selectedValue;
                                    for (var element in controller.stepperList) {
                                      element.isSelected = false;
                                    }
                                    controller.stepperList[index].isSelected = true;
                                    controller.stepperList.refresh();

                                    if (controller.stepperList[selectedValue].title != AppStringKey.stepper_basic_detail.tr) {
                                      Get.find<BranchHandlerController>().setActiveBranchIndex(0);
                                    }
                                  },
                                );
                              },
                            ),
                          ),
                          Material(
                            child: Ink(
                              child: InkWell(
                                onTap: () {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return CommonModalWidget(
                                          modalIcon: AppImages.warning,
                                          title: AppStringKey.logout_warning.tr,
                                          description: AppStringKey.logout_warning_description.tr,
                                          firstButtonContent: AppStringKey.cancel.tr,
                                          firstButtonOnTap: () {
                                            Navigator.pop(context);
                                          },
                                          firstButtonBackgroundColor: AppColors.secondaryCtaColor,
                                          firstButtonTextColor: AppColors.inputFieldHeading,
                                          secondButtonContent: AppStringKey.logoutButton.tr,
                                          secondButtonOnTap: () {
                                            CommonSignOut().signOut();
                                          },
                                        );
                                      });
                                },
                                child: Row(
                                  children: [
                                    SvgPicture.asset(
                                      AppImages.signOut,
                                      width: context.screenWidth * 0.02,
                                    ),
                                    const SpaceHorizontal(0.005),
                                    CustomText(
                                      text: AppStringKey.sign_out.tr,
                                      textStyle: BaseStyle.textStyleRobotoRegular(
                                        8,
                                        AppColors.colorBlack,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        Obx(
          () => Expanded(
            flex: 8,
            child: Column(
              children: [
                SizedBox(
                  height: context.screenHeight * 0.9,
                  child: SingleChildScrollView(
                    padding: const EdgeInsets.symmetric(
                      vertical: 48,
                      horizontal: 40,
                    ),
                    child: controller.stepperList[controller.selectedIndex.value].childPage,
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: context.screenWidth * 0.02, vertical: context.screenHeight * 0.01),
                  decoration: BoxDecoration(
                    border: LRTB_Border(0.5, 0.5, 0.5, 0.5, AppColors.borderColor),
                    color: AppColors.colorWhite,
                  ),
                  height: context.screenHeight * 0.1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Visibility.maintain(
                        visible: controller.selectedIndex.value < 5,
                        child: Material(
                          child: Ink(
                            child: InkWell(
                              onTap: controller.selectedIndex.value < 5
                                  ? () {
                                      if (controller.selectedIndex < controller.stepperList.length - 1) {
                                        controller.selectedIndex++;
                                        for (var element in controller.stepperList) {
                                          element.isSelected = false;
                                        }
                                        controller.stepperList[controller.selectedIndex.value].isSelected = true;
                                        controller.stepperList.refresh();
                                      } else {
                                        Get.toNamed(RoutePaths.DASHBOARD_STEPPER);
                                      }
                                    }
                                  : null,
                              child: CustomText(
                                text: AppStringKey.skip_for_now.tr,
                                textStyle: BaseStyle.underLineTextStyle(
                                  8,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          CustomSquareCornerButton(
                            buttonText: AppStringKey.previous.tr,
                            buttonTextColor: AppColors.colorBlack,
                            backgroundColor: AppColors.colorBackground,
                            height: 0.03,
                            width: 0.1,
                            textSize: 8,
                            callBack: () {
                              if (controller.selectedIndex > 0) {
                                controller.setActivePage(controller.selectedIndex.value - 1);
                              }
                            },
                          ),
                          const SpaceHorizontal(0.01),
                          CustomSquareCornerButton(
                            buttonText:
                                controller.selectedIndex < controller.stepperList.length - 1 ? AppStringKey.next.tr : AppStringKey.finish_setup.tr,
                            buttonTextColor: AppColors.colorWhite,
                            backgroundColor: AppColors.colorBlack,
                            height: 0.03,
                            width: 0.1,
                            textSize: 8,
                            callBack: () {
                              if (controller.selectedIndex < controller.stepperList.length - 1) {
                                if (stepperPosition == StepperPosition.basicDetails) {
                                  Get.find<BasicDetailsController>().validateData();
                                } else if (stepperPosition == StepperPosition.accountDetails) {
                                  Get.find<AccountDetailsController>().validateData();
                                } else if (stepperPosition == StepperPosition.marketing) {
                                  Get.find<MarketingController>().validateData();
                                } else if (stepperPosition == StepperPosition.services) {
                                  List<MissingValue> missingValues =
                                      Get.find<ServiceController>().findMissingValues(Get.find<ServiceController>().categoryResponseModel);
                                  if (missingValues.isNotEmpty) {
                                    for (var missingValue in missingValues) {
                                      Utilities.showToast(missingValue.message);
                                      break;
                                    }
                                  } else {
                                    if (Get.find<ServiceController>().categoryResponseModel.isNotEmpty) {
                                      Get.find<ServiceController>().submitServiceData();
                                    } else {
                                      Get.find<CustomStepperController>().setActivePage(Get.find<CustomStepperController>().selectedIndex.value + 1);
                                    }
                                  }
                                } else if (stepperPosition == StepperPosition.addStaff) {
                                  Get.find<AddStaffController>().validateData();
                                }
                              } else {
                                Get.find<OnboardingSummaryController>().validateData();
                              }
                            },
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}
