import 'package:get/get.dart';

import '../../api/api_end_point.dart';
import '../../api/api_interface.dart';
import '../../api/api_presenter.dart';
import '../../core/app_preference/app_preferences.dart';
import '../../core/app_preference/storage_keys.dart';
import '../../core/constant/app_images.dart';
import '../../core/constant/ui_constants.dart';
import '../../core/routes/route_paths.dart';
import '../../l10n/app_string_key.dart';
import '../account_detail/account_detail_screen.dart';
import '../basic_details/basic_details_screen.dart';
import '../marketing/marketing_screen.dart';
import '../onboarding_summary/onboarding_summary.dart';
import '../services/service_screen.dart';
import '../staff/add_staff_screen.dart';
import 'models/stepper_model.dart';
import 'widgets/branch_handler/branch_handler_screen.dart';

class CustomStepperController extends GetxController implements ApiCallBacks {
  RxList<StepperModel> stepperList = RxList();
  RxInt selectedIndex = 0.obs;

  @override
  void onInit() {
    super.onInit();
    stepperList.add(
      StepperModel(
        icon: AppImages.icBasicDetail,
        title: AppStringKey.stepper_basic_detail,
        isSelected: true,
        childPage: BranchHandler(
          titleName: AppStringKey.basic_details,
          formPage: const BasicDetailsScreen(),
        ),
      ),
    );
    stepperList.add(
      StepperModel(
        icon: AppImages.icAccountDetail,
        title: AppStringKey.stepper_account_details,
        isSelected: false,
        childPage: BranchHandler(
          titleName: AppStringKey.account_detail,
          formPage: const AccountDetailScreen(),
        ),
      ),
    );
    stepperList.add(
      StepperModel(
        icon: AppImages.icMarketing,
        title: AppStringKey.stepper_marketing,
        isSelected: false,
        childPage: BranchHandler(
          titleName: AppStringKey.marketing,
          formPage: const MarketingScreen(),
        ),
      ),
    );
    stepperList.add(
      StepperModel(
        icon: AppImages.icServices,
        title: AppStringKey.stepper_services,
        isSelected: false,
        childPage: BranchHandler(
          titleName: AppStringKey.add_service,
          formPage: const ServiceScreen(
            serviceRedirectionFrom: ServiceRedirectionFrom.onBoarding,
          ),
        ),
      ),
    );
    stepperList.add(
      StepperModel(
        icon: AppImages.icStaff,
        title: AppStringKey.stepper_staff,
        isSelected: false,
        childPage: BranchHandler(
          titleName: AppStringKey.staff_details,
          formPage: const AddStaffScreen(),
        ),
      ),
    );
    stepperList.add(
      StepperModel(
        isSelected: false,
        childPage: const OnBoardingSummary(),
      ),
    );
  }

  void signOut() {
    ApiPresenter(this).signOut();
  }

  void setActivePage(int index) {
    selectedIndex.value = index;
    for (var element in stepperList) {
      element.isSelected = false;
    }
    stepperList[selectedIndex.value].isSelected = true;
    stepperList.refresh();
  }

  void updateLanguage() {
    ApiPresenter(this).updateLanguage(
      AppPreferences.sharedPrefRead(StorageKeys.lang),
    );
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    // TODO: implement onConnectionError
  }

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {
    // TODO: implement onError
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    // TODO: implement onLoading
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.logout:
        String lang =
            AppPreferences.sharedPrefRead(StorageKeys.lang).toString();
        AppPreferences.sharedPrefEraseAllData();
        AppPreferences.sharedPrefWrite(StorageKeys.lang, lang);
        Get.offAllNamed(RoutePaths.LOGIN);
        break;
    }
  }
}
