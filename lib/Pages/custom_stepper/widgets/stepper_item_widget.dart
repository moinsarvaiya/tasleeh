import 'package:al_baida_garage_fe/Pages/custom_stepper/custom_stepper_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../common/space_horizontal.dart';
import '../../../common/space_vertical.dart';
import '../../../core/constant/app_color.dart';
import '../../../core/constant/app_images.dart';
import '../../../core/constant/base_style.dart';
import '../../../core/extensions/common_extension.dart';
import '../../../custom_classes/custom_text.dart';
import '../models/stepper_model.dart';

class StepperItemWidget extends StatelessWidget {
  final List<StepperModel> stepperList;
  final int index;
  final Function(int selectedValue) onTap;

  const StepperItemWidget({
    super.key,
    required this.stepperList,
    required this.index,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    print('title after change ${stepperList[index].title!.tr}');
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        /* Material(
          child: Ink(
            child: InkWell(
              onTap: () {
                onTap(index);
              },
              child:*/
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Get.find<CustomStepperController>().selectedIndex > index
                    ? SvgPicture.asset(
                        AppImages.icDone,
                        width: context.screenWidth * 0.020,
                        height: context.screenHeight * 0.04,
                      )
                    : SvgPicture.asset(
                        stepperList[index].icon!,
                        width: context.screenWidth * 0.020,
                        height: context.screenHeight * 0.04,
                      ),
                const SpaceHorizontal(0.005),
                CustomText(
                  text: stepperList[index].title!.tr,
                  textStyle: stepperList[index].isSelected!
                      ? BaseStyle.textStyleRobotoBold(
                          9,
                          AppColors.colorBlack,
                        )
                      : BaseStyle.textStyleRobotoRegular(
                          8,
                          AppColors.colorBlack,
                        ),
                ),
              ],
            ),
            stepperList[index].isSelected!
                ? Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: context.screenWidth * 0.002,
                      vertical: context.screenHeight * 0.004,
                    ),
                    decoration: BoxDecoration(
                      color: AppColors.colorBlack,
                      borderRadius: BorderRadius.circular(2),
                    ),
                  )
                : const SizedBox.shrink()
          ],
        ),
        /*  ),
          ),
        ),*/
        Visibility(
          visible: index != stepperList.length - 2,
          child: const SpaceVertical(0.002),
        ),
        Visibility(
          visible: index != stepperList.length - 2,
          child: Container(
            margin: EdgeInsets.only(
              left: 13,
              right: 13,
              bottom: context.screenWidth * 0.0070,
              top: context.screenWidth * 0.0070,
            ),
            width: 2,
            height: context.screenHeight * 0.07,
            color: AppColors.colorBlack,
          ),
        )
      ],
    );
  }
}
