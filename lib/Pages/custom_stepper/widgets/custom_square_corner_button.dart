import 'package:al_baida_garage_fe/core/constant/base_style.dart';
import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:al_baida_garage_fe/custom_classes/custom_text.dart';
import 'package:flutter/material.dart';

import '../../../core/constant/app_color.dart';

class CustomSquareCornerButton extends StatelessWidget {
  final String buttonText;
  final Color buttonTextColor;
  final Color backgroundColor;

  final Color borderColor;
  final bool isBorderVisible;
  final double borderWidth;

  final double height;
  final double width;
  final double textSize;

  final bool? isResponsive;
  final Function()? callBack;

  const CustomSquareCornerButton({
    super.key,
    required this.buttonText,
    required this.buttonTextColor,
    required this.backgroundColor,
    required this.height,
    required this.width,
    required this.textSize,
    this.borderColor = AppColors.borderColor,
    this.isBorderVisible = false,
    this.borderWidth = 1.0,
    this.isResponsive,
    this.callBack,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Ink(
        decoration: BoxDecoration(
          color: backgroundColor,
        ),
        child: InkWell(
          onTap: callBack != null
              ? () {
                  callBack!();
                }
              : null,
          child: Container(
            width: context.screenWidth * width,
            height: context.screenWidth * height,
            decoration: BoxDecoration(
              border: Border.all(
                color: borderColor,
                width: borderWidth,
              ),
              borderRadius: const BorderRadius.all(
                Radius.circular(2),
              ),
            ),
            child: Center(
              child: CustomText(
                text: buttonText,
                textStyle: BaseStyle.textStyleRobotoSemiBold(textSize, buttonTextColor),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
