import 'package:al_baida_garage_fe/Pages/custom_stepper/models/stepper_model.dart';
import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';

class StepperItemWidgetMobile extends StatelessWidget {
  final List<StepperModel> stepperList;
  final int index;
  final Function(int index) onTap;
  final bool isStepCompleted;
  final int selectedIndex;

  const StepperItemWidgetMobile({
    super.key,
    required this.stepperList,
    required this.index,
    required this.onTap,
    required this.isStepCompleted,
    required this.selectedIndex,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GestureDetector(
          onTap: () {
            onTap(index);
          },
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(
                      horizontal: context.screenWidth * 0.01,
                      vertical: context.screenHeight * 0.01,
                    ),
                    decoration: BoxDecoration(
                      color: selectedIndex >= index ? Colors.black : Colors.white,
                      border: Border.all(color: Colors.black, width: 2),
                      shape: BoxShape.circle,
                    ),
                    child: selectedIndex >= index
                        ? const Icon(
                            Icons.done,
                            color: Colors.white,
                            size: 15,
                          )
                        : Container(
                            width: 15,
                            height: 15,
                            padding: const EdgeInsets.all(20),
                            decoration: const BoxDecoration(
                              color: Colors.black,
                              shape: BoxShape.circle,
                            ),
                          ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Visibility(
          visible: index != stepperList.length - 2,
          child: Container(
            margin: EdgeInsets.only(
              bottom: context.screenWidth * 0.0070,
              top: context.screenWidth * 0.0070,
            ),
            width: context.screenHeight * 0.07,
            height: 2,
            color: selectedIndex > index ? Colors.black : Colors.grey,
          ),
        )
      ],
    );
  }
}
