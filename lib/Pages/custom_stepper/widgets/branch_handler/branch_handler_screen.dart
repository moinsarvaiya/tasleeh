import 'package:al_baida_garage_fe/Pages/custom_stepper/custom_stepper_controller.dart';
import 'package:al_baida_garage_fe/common/language_selector.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/extensions/common_extension.dart';
import 'branch_handler_controller.dart';

class BranchHandler extends GetView<BranchHandlerController> {
  Widget formPage;
  final String titleName;
  final bool isReadOnly;
  int? position;

  BranchHandler({
    super.key,
    required this.formPage,
    required this.titleName,
    this.isReadOnly = false,
    this.position,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.colorWhite,
        border: isReadOnly ? const Border() : Border.all(color: AppColors.strokeColor, width: 1),
      ),
      padding: isReadOnly == false
          ? context.isDesktop
              ? const EdgeInsets.fromLTRB(68, 40, 68, 32)
              : const EdgeInsets.fromLTRB(12, 16, 12, 16)
          : EdgeInsets.zero,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                titleName,
                style: BaseStyle.textStyleRobotoSemiBold(
                  24,
                  AppColors.primaryTextColor,
                ),
              ),
              isReadOnly
                  ? IconButton(
                      onPressed: () {
                        Get.find<CustomStepperController>().setActivePage(position!);
                      },
                      icon: const Icon(
                        Icons.mode_edit_outline_outlined,
                        color: AppColors.primaryTextColor,
                        size: 24,
                      ),
                    )
                  : context.isDesktop
                      ? const GlobalLanguageSelector()
                      : const SizedBox.shrink(),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: AppColors.strokeColor,
                  width: 1,
                ),
              ),
            ),
            child: Obx(
              () => Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      controller: controller.scrollController,
                      child: Row(
                        children: [
                          for (int i = 0; i < controller.branches.length; i++)
                            Container(
                              margin: EdgeInsets.only(left: i != 0 ? 32 : 0),
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: i == controller.activeBranchIndex.toInt() ? AppColors.primaryTextColor : Colors.transparent,
                                    width: 2,
                                  ),
                                ),
                              ),
                              child: TextButton(
                                onPressed: null,
                                style: TextButton.styleFrom(
                                  padding: EdgeInsets.zero,
                                  minimumSize: const Size(89, 46),
                                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                  foregroundColor: AppColors.primaryTextColor,
                                  shape: const BeveledRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(2),
                                    ),
                                  ),
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      controller.branches[i].branchName!,
                                      style: i == controller.activeBranchIndex.toInt()
                                          ? BaseStyle.textStyleRobotoSemiBold(
                                              16,
                                              AppColors.primaryTextColor,
                                            )
                                          : BaseStyle.textStyleRobotoRegular(
                                              16,
                                              AppColors.primaryTextColor,
                                            ),
                                    ),
                                    const SizedBox(
                                      width: 6,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                        ],
                      ),
                    ),
                  ),
                  controller.branches.length > 1
                      ? IconButton(
                          splashRadius: 1,
                          padding: EdgeInsets.zero,
                          constraints: const BoxConstraints(),
                          iconSize: 20,
                          icon: const Icon(
                            Icons.chevron_right,
                          ),
                          onPressed: () {
                            double offset = controller.scrollController.offset + 100.0;
                            controller.scrollController.animateTo(
                              offset,
                              duration: const Duration(milliseconds: 300),
                              curve: Curves.easeInOut,
                            );
                          },
                        )
                      : Container(),
                  const SizedBox(
                    width: 15,
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 24,
          ),
          formPage
        ],
      ),
    );
  }
}
