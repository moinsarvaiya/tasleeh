import 'package:al_baida_garage_fe/Pages/Dashboard/multi_branch_handler/multi_branch_handler_controller.dart';
import 'package:al_baida_garage_fe/Pages/basic_details/basic_details_controller.dart';
import 'package:al_baida_garage_fe/core/constant/app_color.dart';
import 'package:al_baida_garage_fe/core/constant/base_style.dart';
import 'package:al_baida_garage_fe/core/constant/ui_constants.dart';
import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:al_baida_garage_fe/l10n/app_string_key.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BranchDeleteDialog extends StatelessWidget {
  const BranchDeleteDialog({
    super.key,
    required this.branchName,
    required this.index,
    required this.dialogFrom,
  });

  final String branchName;
  final String dialogFrom;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: IntrinsicWidth(
        child: Container(
          margin: allPadding16,
          padding: allPadding24,
          constraints: BoxConstraints(
            maxWidth: (context.isDesktop ? 712 : double.infinity),
          ),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(4),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                AppStringKey.delete.tr,
                style: BaseStyle.textStyleRobotoSemiBold(
                  context.isDesktop ? 24 : 18,
                  AppColors.primaryTextColor,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                '${AppStringKey.are_you_sure_to_delete.tr} $branchName?',
                style: BaseStyle.textStyleRobotoRegular(
                  14,
                  AppColors.primaryTextColor,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      fixedSize: const Size(140, 45),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0),
                      ),
                      backgroundColor: AppColors.colorBackground,
                    ),
                    onPressed: () {
                      Get.back();
                    },
                    child: Text(
                      AppStringKey.cancel.tr,
                      style: BaseStyle.textStyleRobotoSemiBold(
                        14,
                        AppColors.colorBlack,
                      ),
                    ),
                  ),
                  horizontalMargin16,
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      fixedSize: const Size(140, 45),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0),
                      ),
                      backgroundColor: AppColors.errorTextColor,
                    ),
                    onPressed: () {
                      if (dialogFrom == 'dashboard') {
                        Get.find<MultiBranchHandlerController>().deleteBranch(index);
                      } else {
                        Get.find<BasicDetailsController>().deleteBranch(index);
                      }
                      Get.back();
                    },
                    child: Text(
                      AppStringKey.delete.tr,
                      style: BaseStyle.textStyleRobotoSemiBold(
                        14,
                        AppColors.colorBackground,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
