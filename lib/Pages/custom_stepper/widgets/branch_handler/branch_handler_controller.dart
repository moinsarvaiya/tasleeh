import 'package:al_baida_garage_fe/Pages/custom_stepper/widgets/branch_handler/branch_delete_dialog.dart';
import 'package:al_baida_garage_fe/Pages/custom_stepper/widgets/branch_handler/branch_rename_dialog.dart';
import 'package:al_baida_garage_fe/models/branch_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../l10n/app_string_key.dart';

class BranchHandlerController extends GetxController {
  RxInt activeBranchIndex = RxInt(0);
  RxList<BranchModel> branches = RxList();
  RxList<Widget> branchForm = RxList();
  final ScrollController scrollController = ScrollController();

  void setActiveBranchIndex(int index) {
    activeBranchIndex.value = index;
  }

  void onSelected(String choice, int index, BuildContext context) {
    if (choice == AppStringKey.rename.tr) {
      showDialog(
        context: context,
        builder: (_) {
          return BranchRenameDialog(branchName: branches[index].branchName ?? '', index: index);
        },
      );
    } else if (choice == AppStringKey.delete.tr && branches.length > 1) {
      showDialog(
        context: context,
        builder: (_) {
          return BranchDeleteDialog(
            branchName: branches[index].branchName ?? '',
            index: index,
            dialogFrom: 'on-boarding',
          );
        },
      );
    }
  }
}
