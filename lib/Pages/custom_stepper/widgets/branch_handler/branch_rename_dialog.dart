import 'package:al_baida_garage_fe/Pages/basic_details/basic_details_controller.dart';
import 'package:al_baida_garage_fe/common/input_text_field.dart';
import 'package:al_baida_garage_fe/core/constant/app_color.dart';
import 'package:al_baida_garage_fe/core/constant/base_style.dart';
import 'package:al_baida_garage_fe/core/constant/ui_constants.dart';
import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:al_baida_garage_fe/l10n/app_string_key.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BranchRenameDialog extends StatelessWidget {
  const BranchRenameDialog(
      {super.key, required this.branchName, required this.index});

  final String branchName;
  final int index;

  @override
  Widget build(BuildContext context) {
    final TextEditingController branchController =
        TextEditingController(text: branchName);
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: IntrinsicWidth(
        child: Container(
          margin: allPadding16,
          padding: allPadding24,
          constraints: BoxConstraints(
            maxWidth: (context.isDesktop ? 712 : double.infinity),
          ),
          decoration: BoxDecoration(
            shape: BoxShape.rectangle,
            color: Colors.white,
            borderRadius: BorderRadius.circular(4),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                AppStringKey.rename.tr,
                style: BaseStyle.textStyleRobotoSemiBold(
                  context.isDesktop ? 24 : 18,
                  AppColors.primaryTextColor,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(
                height: 8,
              ),
              ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 356),
                child: InputTextField(
                  hintText: AppStringKey.branch_text.tr,
                  label: AppStringKey.branch_text.tr,
                  textInputType: TextInputType.text,
                  isRequireField: true,
                  textEditingController: branchController,
                  focusScope: FocusNode(),
                ),
              ),
              const SizedBox(
                height: 48,
              ),
              Row(
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      fixedSize: const Size(140, 45),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(0),
                      ),
                      backgroundColor: AppColors.colorBackground,
                    ),
                    onPressed: () {
                      Get.back();
                    },
                    child: Text(
                      AppStringKey.cancel.tr,
                      style: BaseStyle.textStyleRobotoSemiBold(
                        14,
                        AppColors.colorBlack,
                      ),
                    ),
                  ),
                  horizontalMargin16,
                  ListenableBuilder(
                      listenable: branchController,
                      builder: (context, _) {
                        return ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            fixedSize: const Size(140, 45),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(0),
                            ),
                            backgroundColor: AppColors.colorBlack,
                          ),
                          onPressed: branchController.text.isEmpty
                              ? null
                              : () {
                                  Get.find<BasicDetailsController>()
                                      .renameBranchNameAPI(
                                          branchController.text.trim(), index);
                                  Get.back();
                                },
                          child: Text(
                            AppStringKey.rename.tr,
                            style: BaseStyle.textStyleRobotoSemiBold(
                              14,
                              AppColors.colorBackground,
                            ),
                          ),
                        );
                      }),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
