import 'package:flutter/widgets.dart';

class MyAccountDashboardModel {
  String? title;
  bool? isSelected;
  Widget? childPage;

  MyAccountDashboardModel({
    this.title,
    this.isSelected,
    this.childPage,
  });
}
