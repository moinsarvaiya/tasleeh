import 'package:flutter/widgets.dart';

class StepperModel {
  String? icon;
  String? title;
  bool? isSelected;
  Widget? childPage;

  StepperModel({
    this.icon,
    this.title,
    this.isSelected,
    this.childPage,
  });
}
