import 'package:al_baida_garage_fe/Utils/password_helper.dart';
import 'package:al_baida_garage_fe/Utils/utilities.dart';
import 'package:al_baida_garage_fe/core/extensions/validation_function.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../common/input_text_field.dart';
import '../../common/space_vertical.dart';
import '../../core/constant/app_color.dart';
import '../../core/constant/app_images.dart';
import '../../core/constant/base_style.dart';
import '../../core/constant/ui_constants.dart';
import '../../custom_classes/custom_dropdown_field.dart';
import '../../l10n/app_string_key.dart';
import 'add_staff_controller.dart';

class AddStaffMobileView extends StatelessWidget {
  final AddStaffController controller;
  final bool isReadOnly;

  const AddStaffMobileView({
    super.key,
    required this.controller,
    required this.isReadOnly,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppStringKey.add_staff.tr,
            style: BaseStyle.textStyleRobotoSemiBold(
              14,
              AppColors.primaryTextColor,
            ),
          ),
          Obx(
            () => controller.totalRows.value > 0
                ? Column(
                    children: List.generate(
                      controller.totalRows.value,
                      (index) => Center(
                        child: Container(
                          margin: const EdgeInsets.only(top: 16),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        width: 24,
                                        height: 24,
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(2),
                                          border: Border.all(
                                            color: AppColors.popUpBackgroundColor,
                                          ),
                                        ),
                                        child: Text(
                                          (index + 1).toString(),
                                          style: BaseStyle.textStyleRobotoSemiBold(
                                            14,
                                            AppColors.primaryTextColor,
                                          ),
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 8,
                                      ),
                                      Text(
                                        "${AppStringKey.staff.tr} ${index + 1}",
                                        style: BaseStyle.textStyleRobotoSemiBold(
                                          12,
                                          AppColors.primaryTextColor,
                                        ),
                                      )
                                    ],
                                  ),
                                  isReadOnly
                                      ? const SizedBox.shrink()
                                      : PopupMenuButton<String>(
                                          splashRadius: 5,
                                          offset: const Offset(20, 40),
                                          padding: EdgeInsets.zero,
                                          surfaceTintColor: AppColors.colorWhite,
                                          onSelected: (choice) => {
                                            if (choice == "Edit Details")
                                              {
                                                controller.editRow(index),
                                                showModalBottomSheet(
                                                    context: context,
                                                    isDismissible: false,
                                                    isScrollControlled: true,
                                                    backgroundColor: AppColors.colorWhite,
                                                    shape: RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.circular(12),
                                                    ),
                                                    builder: (context) {
                                                      return Container(
                                                        padding: const EdgeInsets.symmetric(
                                                          vertical: 20,
                                                        ),
                                                        decoration: const BoxDecoration(
                                                          color: AppColors.colorWhite,
                                                        ),
                                                        height: context.height * 0.9,
                                                        child: SingleChildScrollView(
                                                          child: Column(
                                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                            children: [
                                                              Container(
                                                                padding: const EdgeInsets.symmetric(
                                                                  horizontal: 16,
                                                                ),
                                                                width: double.infinity,
                                                                child: Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: [
                                                                    Text(
                                                                      AppStringKey.staff_details.tr,
                                                                      style: BaseStyle.textStyleRobotoSemiBold(
                                                                        18,
                                                                        AppColors.primaryTextColor,
                                                                      ),
                                                                    ),
                                                                    const SizedBox(
                                                                      height: 24,
                                                                    ),
                                                                    InputTextField(
                                                                      hintText: AppStringKey.first_name.tr,
                                                                      label: AppStringKey.enter_first_name.tr,
                                                                      isRequireField: false,
                                                                      textEditingController: controller.firstNameControllers[index],
                                                                    ),
                                                                    const SizedBox(
                                                                      height: 24,
                                                                    ),
                                                                    InputTextField(
                                                                      hintText: AppStringKey.last_name.tr,
                                                                      label: AppStringKey.enter_last_name.tr,
                                                                      isRequireField: false,
                                                                      textEditingController: controller.lastNameControllers[index],
                                                                    ),
                                                                    const SizedBox(
                                                                      height: 24,
                                                                    ),
                                                                    InputTextField(
                                                                      hintText: AppStringKey.email.tr,
                                                                      label: AppStringKey.enter_email.tr,
                                                                      isRequireField: false,
                                                                      textEditingController: controller.emailControllers[index],
                                                                    ),
                                                                    const SizedBox(
                                                                      height: 24,
                                                                    ),
                                                                    InputTextField(
                                                                      hintText: AppStringKey.mobile_number.tr,
                                                                      label: AppStringKey.enter_mobile_number.tr,
                                                                      isRequireField: false,
                                                                      isPhoneNo: true,
                                                                      textEditingController: controller.phoneNumberControllers[index],
                                                                    ),
                                                                    const SizedBox(
                                                                      height: 24,
                                                                    ),
                                                                    Text(
                                                                      AppStringKey.create_password.tr,
                                                                      style: BaseStyle.textStyleRobotoRegular(
                                                                        14,
                                                                        AppColors.primaryTextColor,
                                                                      ),
                                                                    ),
                                                                    const SizedBox(
                                                                      height: 8,
                                                                    ),
                                                                    Obx(
                                                                      () => Container(
                                                                        decoration: BoxDecoration(
                                                                          border: Border.all(
                                                                            color: AppColors.strokeColor,
                                                                          ),
                                                                          borderRadius: BorderRadius.circular(2),
                                                                        ),
                                                                        padding: const EdgeInsets.symmetric(
                                                                          horizontal: 12,
                                                                        ),
                                                                        child: TextFormField(
                                                                          maxLines: 1,
                                                                          controller: controller.passwordControllers[index],
                                                                          obscureText: controller.isObscureText[index],
                                                                          decoration: InputDecoration(
                                                                            border: InputBorder.none,
                                                                            hintText: AppStringKey.enter_password.tr,
                                                                            hintStyle: BaseStyle.textStyleRobotoMedium(
                                                                              14,
                                                                              AppColors.inputFieldHintTextColor,
                                                                            ),
                                                                            suffixIcon: controller.isEditingRow(index)
                                                                                ? IconButton(
                                                                                    splashRadius: 5,
                                                                                    icon: Icon(
                                                                                      controller.isObscureText[index]
                                                                                          ? Icons.visibility_off
                                                                                          : Icons.visibility,
                                                                                      size: 14,
                                                                                      color: AppColors.inputFieldIconColor,
                                                                                    ),
                                                                                    onPressed: () {
                                                                                      controller.isObscureText[index] =
                                                                                          !controller.isObscureText[index];
                                                                                    },
                                                                                  )
                                                                                : const Icon(null),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    const SizedBox(
                                                                      height: 24,
                                                                    ),
                                                                    Text(
                                                                      AppStringKey.enter_staff_type.tr,
                                                                      style: BaseStyle.textStyleRobotoRegular(
                                                                        14,
                                                                        AppColors.primaryTextColor,
                                                                      ),
                                                                    ),
                                                                    const SizedBox(
                                                                      height: 8,
                                                                    ),
                                                                    Obx(
                                                                      () => CustomDropDownField(
                                                                        height: 40,
                                                                        hintTitle: AppStringKey.staff_type.tr,
                                                                        items: staffTypes,
                                                                        validationText: "",
                                                                        labelVisibility: false,
                                                                        onChanged: (val) {
                                                                          if (val == AppStringKey.driver.tr) {
                                                                            controller.employeeType[controller.currentEditingRow.value] = 'driver';
                                                                          } else if (val == AppStringKey.branch_staff.tr) {
                                                                            controller.employeeType[controller.currentEditingRow.value] =
                                                                                'branch-staff';
                                                                          }
                                                                        },
                                                                        buttonPadding: EdgeInsets.zero,
                                                                        labelText: "",
                                                                        selectedValue: controller.currentEditingRow.value != -1
                                                                            ? controller.employeeType[index] == 'driver'
                                                                                ? AppStringKey.driver.tr
                                                                                : AppStringKey.branch_staff.tr
                                                                            : '',
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                              const SizedBox(
                                                                height: 16,
                                                              ),
                                                              Container(
                                                                padding: const EdgeInsets.only(bottom: 10),
                                                                child: Column(
                                                                  children: [
                                                                    TextButton(
                                                                      onPressed: () {
                                                                        controller.firstNameControllers.refresh();
                                                                        controller.emailControllers.refresh();
                                                                        controller.phoneNumberControllers.refresh();
                                                                        if (controller.firstNameControllers[index].text.isEmpty) {
                                                                          Utilities.showToast(AppStringKey.staff_first_name_required.tr);
                                                                        } else if (controller.lastNameControllers[index].text.isEmpty) {
                                                                          Utilities.showToast(AppStringKey.staff_last_name_required.tr);
                                                                        } else if (controller.emailControllers[index].text.isEmpty) {
                                                                          Utilities.showToast(AppStringKey.staff_email_required.tr);
                                                                        } else if (controller.phoneNumberControllers[index].text.isEmpty) {
                                                                          Utilities.showToast(AppStringKey.staff_phone_required.tr);
                                                                        } else if (controller.passwordControllers[index].text.isEmpty) {
                                                                          Utilities.showToast(AppStringKey.staff_password_required.tr);
                                                                        } else if (!Validation.isNumberWithinRange(
                                                                            controller.phoneNumberControllers[index].text)) {
                                                                          Utilities.showToast(AppStringKey.staff_phone_length_required.tr);
                                                                        } else if (!Validation.isValidEmail(
                                                                            controller.emailControllers[index].text)) {
                                                                          Utilities.showToast(AppStringKey.staff_email_not_valid.tr);
                                                                        } else if (!PasswordStrengthChecks.isPasswordValid(
                                                                            controller.passwordControllers[index].text)) {
                                                                          if (!PasswordStrengthChecks.hasMinimumLength(
                                                                              controller.passwordControllers[index].text)) {
                                                                            Utilities.showToast(AppStringKey.password_minimum_length_error.tr);
                                                                          } else if (!PasswordStrengthChecks.hasSpecialCharacter(
                                                                              controller.passwordControllers[index].text)) {
                                                                            Utilities.showToast(AppStringKey.password_special_character_error.tr);
                                                                          } else if (!PasswordStrengthChecks.hasLowercase(
                                                                              controller.passwordControllers[index].text)) {
                                                                            Utilities.showToast(AppStringKey.password_lower_case_error.tr);
                                                                          } else if (!PasswordStrengthChecks.hasUppercase(
                                                                              controller.passwordControllers[index].text)) {
                                                                            Utilities.showToast(AppStringKey.password_upper_case_error.tr);
                                                                          } else if (!PasswordStrengthChecks.hasNumber(
                                                                              controller.passwordControllers[index].text)) {
                                                                            Utilities.showToast(AppStringKey.password_numerical_error.tr);
                                                                          }
                                                                        } else if (controller.employeeType[index].isEmpty) {
                                                                          Utilities.showToast(AppStringKey.staff_staff_type_required.tr);
                                                                        } else {
                                                                          controller.saveEdits(index);
                                                                          Navigator.pop(context);
                                                                        }
                                                                      },
                                                                      style: TextButton.styleFrom(
                                                                        shape: const BeveledRectangleBorder(),
                                                                        minimumSize: const Size(343, 40),
                                                                        padding: const EdgeInsets.symmetric(vertical: 7),
                                                                        backgroundColor: AppColors.primaryCtaColor,
                                                                        foregroundColor: AppColors.primaryCtaTextColor,
                                                                        textStyle: BaseStyle.textStyleRobotoRegular(
                                                                          16,
                                                                          AppColors.primaryTextColor,
                                                                        ),
                                                                      ),
                                                                      child: Text(
                                                                        AppStringKey.save_changes.tr,
                                                                      ),
                                                                    ),
                                                                    const SizedBox(height: 10),
                                                                    TextButton(
                                                                      onPressed: () {
                                                                        Navigator.pop(context);
                                                                      },
                                                                      style: TextButton.styleFrom(
                                                                        shape: const BeveledRectangleBorder(),
                                                                        minimumSize: const Size(343, 40),
                                                                        padding: const EdgeInsets.symmetric(vertical: 7),
                                                                        backgroundColor: AppColors.primaryCtaTextColor,
                                                                        foregroundColor: AppColors.primaryCtaColor,
                                                                        textStyle: BaseStyle.textStyleRobotoRegular(
                                                                          16,
                                                                          AppColors.primaryTextColor,
                                                                        ),
                                                                      ),
                                                                      child: Text(
                                                                        AppStringKey.cancel.tr,
                                                                      ),
                                                                    )
                                                                  ],
                                                                ),
                                                              )
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    })
                                              }
                                            else if (choice == "Delete")
                                              {
                                                controller.deleteRow(index),
                                              }
                                          },
                                          child: Container(
                                            height: 24,
                                            width: 24,
                                            decoration: BoxDecoration(
                                              color: AppColors.dividerColor,
                                              border: Border.all(
                                                color: AppColors.strokeColor,
                                                width: 1,
                                              ),
                                              borderRadius: BorderRadius.circular(2),
                                            ),
                                            child: const Icon(
                                              Icons.more_vert,
                                              size: 15,
                                              weight: 400,
                                            ),
                                          ),
                                          itemBuilder: (BuildContext context) => [
                                            PopupMenuItem<String>(
                                              value: AppStringKey.edit_details.tr,
                                              height: 32,
                                              padding: const EdgeInsets.symmetric(
                                                vertical: 5,
                                                horizontal: 12,
                                              ),
                                              child: Row(
                                                children: [
                                                  const Icon(
                                                    Icons.edit_outlined,
                                                    size: 16,
                                                  ),
                                                  const SizedBox(
                                                    width: 8,
                                                  ),
                                                  Text(
                                                    AppStringKey.edit_details.tr,
                                                    style: BaseStyle.textStyleRobotoRegular(
                                                      14,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            PopupMenuItem<String>(
                                              value: AppStringKey.delete.tr,
                                              height: 32,
                                              padding: const EdgeInsets.symmetric(
                                                vertical: 5,
                                                horizontal: 12,
                                              ),
                                              child: Row(
                                                children: [
                                                  const Icon(
                                                    Icons.delete_outline,
                                                    size: 16,
                                                  ),
                                                  const SizedBox(
                                                    width: 8,
                                                  ),
                                                  Text(
                                                    AppStringKey.delete.tr,
                                                    style: BaseStyle.textStyleRobotoRegular(
                                                      14,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                ],
                              ),
                              const SpaceVertical(0.02),
                              Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: AppColors.strokeColor,
                                    width: 1,
                                  ),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.symmetric(
                                        vertical: 12,
                                        horizontal: 16,
                                      ),
                                      child: Text(
                                        controller.firstNameControllers[index].text,
                                        style: BaseStyle.textStyleRobotoBold(
                                          14,
                                          AppColors.primaryTextColor,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      decoration: const BoxDecoration(
                                        border: Border(
                                          top: BorderSide(
                                            color: AppColors.strokeColor,
                                            width: 1,
                                          ),
                                        ),
                                      ),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            flex: 1,
                                            child: Container(
                                              padding: const EdgeInsets.symmetric(
                                                vertical: 12,
                                                horizontal: 16,
                                              ),
                                              decoration: const BoxDecoration(
                                                border: Border(
                                                  right: BorderSide(
                                                    color: AppColors.strokeColor,
                                                    width: 1,
                                                  ),
                                                ),
                                              ),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    children: [
                                                      SvgPicture.asset(
                                                        AppImages.emailIcon,
                                                        height: 12,
                                                        width: 12,
                                                        color: AppColors.inputFieldIconColor,
                                                      ),
                                                      const SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        AppStringKey.email.tr,
                                                        style: BaseStyle.textStyleRobotoRegular(
                                                          12,
                                                          AppColors.inputFieldIconColor,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  Text(
                                                    controller.emailControllers[index].text,
                                                    style: BaseStyle.textStyleRobotoRegular(
                                                      14,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 1,
                                            child: Container(
                                              padding: const EdgeInsets.symmetric(
                                                vertical: 12,
                                                horizontal: 16,
                                              ),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    children: [
                                                      const Icon(
                                                        Icons.car_repair_outlined,
                                                        size: 14,
                                                        color: AppColors.inputFieldIconColor,
                                                      ),
                                                      const SizedBox(
                                                        width: 5,
                                                      ),
                                                      Text(
                                                        AppStringKey.mobile_number.tr,
                                                        style: BaseStyle.textStyleRobotoRegular(
                                                          12,
                                                          AppColors.inputFieldIconColor,
                                                        ),
                                                      )
                                                    ],
                                                  ),
                                                  Text(
                                                    controller.phoneNumberControllers[index].text,
                                                    style: BaseStyle.textStyleRobotoRegular(
                                                      14,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                : Center(
                    child: Text(
                      AppStringKey.currently_no_data_found.tr,
                      style: BaseStyle.textStyleRobotoSemiBold(
                        16,
                        AppColors.primaryTextColor,
                      ),
                    ),
                  ),
          ),
          isReadOnly
              ? const SizedBox.shrink()
              : Container(
                  margin: const EdgeInsets.only(top: 16),
                  child: TextButton(
                    style: TextButton.styleFrom(
                      minimumSize: Size.zero,
                      padding: EdgeInsets.zero,
                      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      foregroundColor: AppColors.primaryTextColor,
                      textStyle: BaseStyle.textStyleRobotoSemiBold(
                        16,
                        AppColors.primaryTextColor,
                      ),
                    ),
                    onPressed: () {
                      controller.addNewRow();
                      showModalBottomSheet(
                        context: context,
                        isDismissible: false,
                        isScrollControlled: true,
                        backgroundColor: AppColors.colorWhite,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        builder: (context) {
                          return Container(
                            padding: const EdgeInsets.symmetric(
                              vertical: 20,
                            ),
                            decoration: const BoxDecoration(
                              color: AppColors.colorWhite,
                            ),
                            height: context.height * 0.9,
                            child: SingleChildScrollView(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 16,
                                    ),
                                    width: double.infinity,
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          AppStringKey.add_another_staff.tr,
                                          style: BaseStyle.textStyleRobotoSemiBold(
                                            18,
                                            AppColors.primaryTextColor,
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 24,
                                        ),
                                        InputTextField(
                                          hintText: AppStringKey.first_name.tr,
                                          label: AppStringKey.enter_first_name.tr,
                                          isRequireField: false,
                                          textEditingController: controller.firstNameControllers[controller.currentEditingRow.value],
                                        ),
                                        const SizedBox(
                                          height: 24,
                                        ),
                                        InputTextField(
                                          hintText: AppStringKey.last_name.tr,
                                          label: AppStringKey.enter_last_name.tr,
                                          isRequireField: false,
                                          textEditingController: controller.lastNameControllers[controller.currentEditingRow.value],
                                        ),
                                        const SizedBox(
                                          height: 24,
                                        ),
                                        InputTextField(
                                          hintText: AppStringKey.email.tr,
                                          label: AppStringKey.enter_email.tr,
                                          isRequireField: false,
                                          textEditingController: controller.emailControllers[controller.currentEditingRow.value],
                                        ),
                                        const SizedBox(
                                          height: 24,
                                        ),
                                        InputTextField(
                                          hintText: AppStringKey.mobile_number.tr,
                                          label: AppStringKey.enter_mobile_number.tr,
                                          isRequireField: false,
                                          isPhoneNo: true,
                                          textEditingController: controller.phoneNumberControllers[controller.currentEditingRow.value],
                                        ),
                                        const SizedBox(
                                          height: 24,
                                        ),
                                        Text(
                                          AppStringKey.create_password.tr,
                                          style: BaseStyle.textStyleRobotoRegular(
                                            14,
                                            AppColors.primaryTextColor,
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 8,
                                        ),
                                        Obx(
                                          () => Container(
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: AppColors.strokeColor,
                                              ),
                                              borderRadius: BorderRadius.circular(2),
                                            ),
                                            padding: const EdgeInsets.symmetric(
                                              horizontal: 12,
                                            ),
                                            child: TextFormField(
                                              maxLines: 1,
                                              obscureText: controller.isObscureText[controller.currentEditingRow.value],
                                              controller: controller.passwordControllers[controller.currentEditingRow.value],
                                              decoration: InputDecoration(
                                                border: InputBorder.none,
                                                hintText: AppStringKey.enter_password.tr,
                                                hintStyle: BaseStyle.textStyleRobotoMedium(
                                                  14,
                                                  AppColors.inputFieldHintTextColor,
                                                ),
                                                suffixIcon: controller.isEditingRow(controller.currentEditingRow.value)
                                                    ? IconButton(
                                                        splashRadius: 5,
                                                        icon: Icon(
                                                          controller.isObscureText[controller.currentEditingRow.value]
                                                              ? Icons.visibility_off
                                                              : Icons.visibility,
                                                          size: 14,
                                                          color: AppColors.inputFieldIconColor,
                                                        ),
                                                        onPressed: () {
                                                          controller.isObscureText[controller.currentEditingRow.value] =
                                                              !controller.isObscureText[controller.currentEditingRow.value];
                                                        },
                                                      )
                                                    : const Icon(null),
                                              ),
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 24,
                                        ),
                                        Text(
                                          AppStringKey.enter_staff_type.tr,
                                          style: BaseStyle.textStyleRobotoRegular(
                                            14,
                                            AppColors.primaryTextColor,
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 8,
                                        ),
                                        CustomDropDownField(
                                          height: 40,
                                          hintTitle: AppStringKey.staff_type.tr,
                                          items: staffTypes,
                                          validationText: "",
                                          labelVisibility: false,
                                          onChanged: (val) {
                                            if (val == AppStringKey.driver.tr) {
                                              controller.employeeType[controller.currentEditingRow.value] = 'driver';
                                            } else if (val == AppStringKey.branch_staff.tr) {
                                              controller.employeeType[controller.currentEditingRow.value] = 'branch-staff';
                                            }
                                          },
                                          buttonPadding: EdgeInsets.zero,
                                          labelText: "",
                                          selectedValue: controller.currentEditingRow.value != -1
                                              ? controller.employeeType[controller.currentEditingRow.value] == 'driver'
                                                  ? AppStringKey.driver.tr
                                                  : AppStringKey.branch_staff.tr
                                              : '',
                                        ),
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 16,
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(bottom: 10),
                                    child: Column(
                                      children: [
                                        TextButton(
                                          onPressed: () {
                                            controller.firstNameControllers.refresh();
                                            controller.emailControllers.refresh();
                                            controller.phoneNumberControllers.refresh();
                                            if (controller.firstNameControllers[controller.currentEditingRow.value].text.isEmpty) {
                                              Utilities.showToast(AppStringKey.staff_first_name_required.tr);
                                            } else if (controller.lastNameControllers[controller.currentEditingRow.value].text.isEmpty) {
                                              Utilities.showToast(AppStringKey.staff_last_name_required.tr);
                                            } else if (controller.emailControllers[controller.currentEditingRow.value].text.isEmpty) {
                                              Utilities.showToast(AppStringKey.staff_email_required.tr);
                                            } else if (controller.phoneNumberControllers[controller.currentEditingRow.value].text.isEmpty) {
                                              Utilities.showToast(AppStringKey.staff_phone_required.tr);
                                            } else if (controller.passwordControllers[controller.currentEditingRow.value].text.isEmpty) {
                                              Utilities.showToast(AppStringKey.staff_password_required.tr);
                                            } else if (!Validation.isNumberWithinRange(
                                                controller.phoneNumberControllers[controller.currentEditingRow.value].text)) {
                                              Utilities.showToast(AppStringKey.staff_phone_length_required.tr);
                                            } else if (!Validation.isValidEmail(
                                                controller.emailControllers[controller.currentEditingRow.value].text)) {
                                              Utilities.showToast(AppStringKey.staff_email_not_valid.tr);
                                            } else if (!PasswordStrengthChecks.isPasswordValid(
                                                controller.passwordControllers[controller.currentEditingRow.value].text)) {
                                              if (!PasswordStrengthChecks.hasMinimumLength(
                                                  controller.passwordControllers[controller.currentEditingRow.value].text)) {
                                                Utilities.showToast(AppStringKey.password_minimum_length_error.tr);
                                              } else if (!PasswordStrengthChecks.hasSpecialCharacter(
                                                  controller.passwordControllers[controller.currentEditingRow.value].text)) {
                                                Utilities.showToast(AppStringKey.password_special_character_error.tr);
                                              } else if (!PasswordStrengthChecks.hasLowercase(
                                                  controller.passwordControllers[controller.currentEditingRow.value].text)) {
                                                Utilities.showToast(AppStringKey.password_lower_case_error.tr);
                                              } else if (!PasswordStrengthChecks.hasUppercase(
                                                  controller.passwordControllers[controller.currentEditingRow.value].text)) {
                                                Utilities.showToast(AppStringKey.password_upper_case_error.tr);
                                              } else if (!PasswordStrengthChecks.hasNumber(
                                                  controller.passwordControllers[controller.currentEditingRow.value].text)) {
                                                Utilities.showToast(AppStringKey.password_numerical_error.tr);
                                              }
                                            } else if (controller.employeeType[controller.currentEditingRow.value].isEmpty) {
                                              Utilities.showToast(AppStringKey.staff_staff_type_required.tr);
                                            } else {
                                              controller.saveNewRow();
                                              Navigator.pop(context);
                                            }
                                          },
                                          style: TextButton.styleFrom(
                                            shape: const BeveledRectangleBorder(),
                                            minimumSize: const Size(343, 40),
                                            padding: const EdgeInsets.symmetric(vertical: 7),
                                            backgroundColor: AppColors.primaryCtaColor,
                                            foregroundColor: AppColors.primaryCtaTextColor,
                                            textStyle: BaseStyle.textStyleRobotoRegular(
                                              16,
                                              AppColors.primaryTextColor,
                                            ),
                                          ),
                                          child: Text(
                                            AppStringKey.save.tr,
                                          ),
                                        ),
                                        const SizedBox(height: 10),
                                        TextButton(
                                          onPressed: () {
                                            controller.deleteRow(controller.currentEditingRow.value);
                                            Navigator.pop(context);
                                          },
                                          style: TextButton.styleFrom(
                                            shape: const BeveledRectangleBorder(),
                                            minimumSize: const Size(343, 40),
                                            padding: const EdgeInsets.symmetric(vertical: 7),
                                            backgroundColor: AppColors.primaryCtaTextColor,
                                            foregroundColor: AppColors.primaryCtaColor,
                                            textStyle: BaseStyle.textStyleRobotoRegular(
                                              16,
                                              AppColors.primaryTextColor,
                                            ),
                                          ),
                                          child: Text(
                                            AppStringKey.cancel.tr,
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    },
                    child: Text(
                      "+ ${AppStringKey.add_another_staff.tr}",
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
