import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../core/constant/ui_constants.dart';
import '../../core/extensions/common_extension.dart';
import 'add_staff_controller.dart';
import 'add_staff_desktop.dart';
import 'add_staff_mobile.dart';

class AddStaffScreen extends StatefulWidget {
  final bool isReadOnly;

  const AddStaffScreen({
    super.key,
    this.isReadOnly = false,
  });

  @override
  State<AddStaffScreen> createState() => _AddStaffScreenState();
}

class _AddStaffScreenState extends State<AddStaffScreen> {
  final controller = Get.find<AddStaffController>();

  @override
  void initState() {
    super.initState();
    stepperPosition = StepperPosition.addStaff;
    controller.clearData();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.getEmployeeRecord();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          child: context.isDesktop
              ? AddStaffDesktopView(
                  controller: controller,
                  isReadOnly: widget.isReadOnly,
                )
              : AddStaffMobileView(
                  controller: controller,
                  isReadOnly: widget.isReadOnly,
                ),
        )
      ],
    );
  }
}
