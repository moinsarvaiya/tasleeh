import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../Utils/app_constants.dart';
import '../../api/api_end_point.dart';
import '../../api/api_interface.dart';
import '../../api/api_presenter.dart';
import '../../core/app_preference/app_preferences.dart';
import '../../core/app_preference/storage_keys.dart';
import '../../core/constant/app_color.dart';
import '../../core/constant/base_extension.dart';
import '../../core/constant/ui_constants.dart';
import '../../l10n/app_string_key.dart';
import '../../models/garage_id_model.dart';
import '../../models/staff_data_model.dart';
import '../custom_stepper/custom_stepper_controller.dart';
import '../custom_stepper/widgets/branch_handler/branch_handler_controller.dart';

class AddStaffController extends GetxController implements ApiCallBacks {
  final formKey = GlobalKey<FormState>();
  RxList<TextEditingController> firstNameControllers = RxList([]);
  RxList<TextEditingController> lastNameControllers = RxList([]);
  RxList<TextEditingController> emailControllers = RxList([]);
  RxList<TextEditingController> phoneNumberControllers = RxList([]);
  RxList<TextEditingController> passwordControllers = RxList([]);
  RxList<String> employeeType = RxList([]);
  List<String> rowId = [];
  List<bool> isObscureText = [true].obs;
  RxInt currentEditingRow = RxInt(-1);
  RxInt totalRows = 0.obs;

  int branchIndex = 0;

  void setBranchIndex(int index) {
    clearData();
    branchIndex = index;
    Get.find<BranchHandlerController>().setActiveBranchIndex(branchIndex);
    getEmployeeRecord();
  }

  void addNewRow() {
    totalRows.value++;
    currentEditingRow.value = totalRows.value - 1;
    firstNameControllers.add(TextEditingController());
    lastNameControllers.add(TextEditingController());
    emailControllers.add(TextEditingController());
    phoneNumberControllers.add(TextEditingController());
    passwordControllers.add(TextEditingController());
    employeeType.add('driver');
    isObscureText.add(true);
    firstNameControllers.refresh();
  }

  void deleteRow(int index) {
    firstNameControllers.removeAt(index);
    lastNameControllers.removeAt(index);
    emailControllers.removeAt(index);
    phoneNumberControllers.removeAt(index);
    passwordControllers.removeAt(index);
    isObscureText.removeAt(index);
    employeeType.removeAt(index);
    totalRows.value--;

    if (rowId.length > index) {
      deleteEmployeeRecord(rowId[index]);
      rowId.removeAt(index);
    }

    if (isEditingRow(index)) {
      currentEditingRow.value = -1;
    }
  }

  bool isEditingRow(int index) {
    if (currentEditingRow.value == index) {
      return true;
    }
    return false;
  }

  void saveEdits(int index) {
    setEmployeeRecord();
  }

  void saveNewRow() {
    getEmployeeId();
  }

  void editRow(int index) {
    currentEditingRow.value = index;
    passwordControllers[currentEditingRow.value].text = "";
  }

  void getEmployeeId() {
    staffFrom = StaffFrom.post;
    ApiPresenter(this).staffDetails(staffDataModel().toJson());
  }

  void setEmployeeRecord() {
    staffFrom = StaffFrom.patch;
    ApiPresenter(this).patchStaffRecord(staffDataModel().toJson(), rowId[currentEditingRow.value]);
  }

  void deleteEmployeeRecord(String employeeId) {
    staffFrom = StaffFrom.delete;
    ApiPresenter(this).deleteStaffRecord(employeeId);
  }

  void getEmployeeRecord() {
    clearData();
    staffFrom = StaffFrom.get;
    ApiPresenter(this).getStaffDetails(staffDataModel().garage!.id.toString());
  }

  void setCurrentData(dynamic object) {
    for (var item in object['data']) {
      if (item['firstName'] == '' && item['lastName'] == '' && item['email'] == '' && item['password'] == '' && item['phone'] == '') {
        deleteEmployeeRecord(item['id']);
      } else {
        totalRows.value++;
        rowId.add(item['id']);
        firstNameControllers.add(TextEditingController(text: item['firstName']));
        lastNameControllers.add(TextEditingController(text: item['lastName']));
        emailControllers.add(TextEditingController(text: item['email']));
        phoneNumberControllers.add(TextEditingController(text: item['phone']));
        passwordControllers.add(TextEditingController(text: AppStringKey.enter_password.tr));
        employeeType.add(item['employeeType']);
        isObscureText.add(true);
      }
    }
  }

  void validateData() {
    ApiPresenter(this).validateOnboardingForm('staff');
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: 'Error');
  }

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {
    errorMsg.showErrorSnackBar(title: 'Error');
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.staffDetails:
        if (staffFrom == StaffFrom.post) {
          rowId.add(object['data']['id']);
          currentEditingRow.value = -1;
        }
        break;
      case ApiEndPoints.getStaffDetails:
        if (object['data'].length > 0) {
          setCurrentData(object);
        }
        break;
      case ApiEndPoints.validateData:
        if (object['data'].length == 0) {
          Fluttertoast.cancel();
          Get.find<CustomStepperController>().setActivePage(Get.find<CustomStepperController>().selectedIndex.value + 1);
        } else {
          String incompleteBranches = object['data'].map((entry) => entry["branchName"]).join(', ');
          Fluttertoast.showToast(
            msg: "${AppStringKey.please_complete_required_fields.tr} $incompleteBranches",
            gravity: ToastGravity.BOTTOM,
            webPosition: 'center',
            webBgColor: '#FF4D4F',
            webShowClose: true,
            fontSize: 16,
            timeInSecForIosWeb: toastVisibleTime,
            textColor: AppColors.colorWhite,
          );
        }
        break;
    }
  }

  StaffDataModel staffDataModel() => StaffDataModel(
        garage: Garage(
          id: AppPreferences.sharedPrefRead(StorageKeys.garageId),
        ),
        firstName: currentEditingRow.value != -1 ? firstNameControllers[currentEditingRow.value].text : "",
        lastName: currentEditingRow.value != -1 ? lastNameControllers[currentEditingRow.value].text : "",
        phone: currentEditingRow.value != -1 ? phoneNumberControllers[currentEditingRow.value].text : "",
        email: currentEditingRow.value != -1 ? emailControllers[currentEditingRow.value].text : "",
        password: currentEditingRow.value != -1 ? passwordControllers[currentEditingRow.value].text : "",
        employeeType: currentEditingRow.value != -1 ? employeeType[currentEditingRow.value] : "",
        countryCode: AppConstants.countryCode,
        userType: userTypePlatform[UserType.employee],
      );

  void clearData() {
    totalRows.value = 0;
    currentEditingRow.value = -1;
    firstNameControllers = RxList([]);
    lastNameControllers = RxList([]);
    emailControllers = RxList([]);
    phoneNumberControllers = RxList([]);
    passwordControllers = RxList([]);
    employeeType = RxList([]);
    isObscureText = [true].obs;
    rowId = [];
  }
}
