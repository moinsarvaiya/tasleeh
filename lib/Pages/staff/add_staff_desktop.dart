import 'package:al_baida_garage_fe/Utils/password_helper.dart';
import 'package:al_baida_garage_fe/Utils/utilities.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../core/constant/app_color.dart';
import '../../core/constant/base_style.dart';
import '../../core/constant/ui_constants.dart';
import '../../core/extensions/validation_function.dart';
import '../../custom_classes/custom_dropdown_field.dart';
import '../../l10n/app_string_key.dart';
import 'add_staff_controller.dart';

class AddStaffDesktopView extends StatelessWidget {
  final AddStaffController controller;
  final bool isReadOnly;

  const AddStaffDesktopView({
    super.key,
    required this.controller,
    required this.isReadOnly,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Obx(
          () => SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Container(
              constraints: const BoxConstraints(minWidth: 700, maxWidth: 1200),
              width: double.maxFinite,
              child: DataTable(
                columnSpacing: 1,
                headingRowColor: MaterialStateColor.resolveWith(
                  (states) {
                    return AppColors.secondaryBackgroundColor;
                  },
                ),
                headingTextStyle: BaseStyle.textStyleRobotoSemiBold(
                  14,
                  AppColors.primaryTextColor,
                ),
                columns: [
                  DataColumn(
                    label: Text(
                      AppStringKey.serial_number.tr,
                    ),
                  ),
                  DataColumn(
                    label: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 16,
                      ),
                      decoration: const BoxDecoration(
                        border: BorderDirectional(
                          start: BorderSide(width: 1, color: AppColors.borderColor),
                        ),
                      ),
                      child: Text(
                        AppStringKey.first_name.tr,
                      ),
                    ),
                  ),
                  DataColumn(
                    label: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 16,
                      ),
                      decoration: const BoxDecoration(
                        border: BorderDirectional(
                          start: BorderSide(width: 1, color: AppColors.borderColor),
                        ),
                      ),
                      child: Text(
                        AppStringKey.last_name.tr,
                      ),
                    ),
                  ),
                  DataColumn(
                    label: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 16,
                      ),
                      decoration: const BoxDecoration(
                        border: BorderDirectional(
                          start: BorderSide(width: 1, color: AppColors.borderColor),
                        ),
                      ),
                      child: Text(
                        AppStringKey.phone_no.tr,
                      ),
                    ),
                  ),
                  DataColumn(
                    label: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 16,
                      ),
                      decoration: const BoxDecoration(
                        border: BorderDirectional(
                          start: BorderSide(width: 1, color: AppColors.borderColor),
                        ),
                      ),
                      child: Text(
                        AppStringKey.email.tr,
                      ),
                    ),
                  ),
                  DataColumn(
                    label: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 16,
                      ),
                      decoration: const BoxDecoration(
                        border: BorderDirectional(
                          start: BorderSide(width: 1, color: AppColors.borderColor),
                        ),
                      ),
                      child: Text(
                        AppStringKey.create_pass.tr,
                      ),
                    ),
                  ),
                  DataColumn(
                    label: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 16,
                      ),
                      decoration: const BoxDecoration(
                        border: BorderDirectional(
                          start: BorderSide(width: 1, color: AppColors.borderColor),
                        ),
                      ),
                      child: Text(
                        AppStringKey.staff_type.tr,
                      ),
                    ),
                  ),
                  if (!isReadOnly)
                    DataColumn(
                      label: Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 16,
                          vertical: 16,
                        ),
                        decoration: const BoxDecoration(
                          border: BorderDirectional(
                            start: BorderSide(width: 1, color: AppColors.borderColor),
                          ),
                        ),
                        child: Text(
                          AppStringKey.actions.tr,
                        ),
                      ),
                    ),
                ],
                rows: List<DataRow>.generate(
                  controller.totalRows.value,
                  (index) {
                    return DataRow(
                      cells: [
                        DataCell(
                          Container(
                            padding: const EdgeInsets.all(8),
                            child: Text(
                              (index + 1).toString(),
                              style: BaseStyle.textStyleRobotoSemiBold(
                                14,
                                AppColors.primaryTextColor,
                              ),
                            ),
                          ),
                        ),
                        DataCell(
                          Container(
                            margin: const EdgeInsets.all(8),
                            child: Obx(
                              () => Container(
                                padding: const EdgeInsets.only(left: 10.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2),
                                  border: controller.isEditingRow(index)
                                      ? Border.all(
                                          color: AppColors.borderColor,
                                          width: 0,
                                        )
                                      : const BorderDirectional(
                                          top: BorderSide.none,
                                          end: BorderSide.none,
                                          start: BorderSide.none,
                                          bottom: BorderSide.none,
                                        ),
                                ),
                                child: TextFormField(
                                  maxLines: 1,
                                  readOnly: !controller.isEditingRow(index),
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.only(bottom: 16),
                                    border: InputBorder.none,
                                    hintText: AppStringKey.first_name.tr,
                                    hintStyle: BaseStyle.textStyleRobotoMedium(
                                      14,
                                      AppColors.inputFieldHintTextColor,
                                    ),
                                  ),
                                  controller: controller.firstNameControllers[index],
                                ),
                              ),
                            ),
                          ),
                        ),
                        DataCell(
                          Container(
                            margin: const EdgeInsets.all(8),
                            child: Obx(
                              () => Container(
                                padding: const EdgeInsets.only(left: 10.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2),
                                  border: controller.isEditingRow(index)
                                      ? Border.all(
                                          color: AppColors.borderColor,
                                          width: 0,
                                        )
                                      : const BorderDirectional(
                                          top: BorderSide.none,
                                          end: BorderSide.none,
                                          start: BorderSide.none,
                                          bottom: BorderSide.none,
                                        ),
                                ),
                                child: TextFormField(
                                  maxLines: 1,
                                  readOnly: !controller.isEditingRow(index),
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.only(bottom: 16),
                                    border: InputBorder.none,
                                    hintText: AppStringKey.last_name.tr,
                                    hintStyle: BaseStyle.textStyleRobotoMedium(
                                      14,
                                      AppColors.inputFieldHintTextColor,
                                    ),
                                  ),
                                  controller: controller.lastNameControllers[index],
                                ),
                              ),
                            ),
                          ),
                        ),
                        DataCell(
                          Container(
                            margin: const EdgeInsets.all(8),
                            child: Obx(
                              () => Container(
                                padding: const EdgeInsets.only(left: 10.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2),
                                  border: controller.isEditingRow(index)
                                      ? Border.all(
                                          color: AppColors.borderColor,
                                          width: 0,
                                        )
                                      : const BorderDirectional(
                                          top: BorderSide.none,
                                          end: BorderSide.none,
                                          start: BorderSide.none,
                                          bottom: BorderSide.none,
                                        ),
                                ),
                                child: TextFormField(
                                  maxLines: 1,
                                  readOnly: !controller.isEditingRow(index),
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.only(bottom: 16),
                                    border: InputBorder.none,
                                    hintText: AppStringKey.phone_no.tr,
                                    hintStyle: BaseStyle.textStyleRobotoMedium(
                                      14,
                                      AppColors.inputFieldHintTextColor,
                                    ),
                                  ),
                                  controller: controller.phoneNumberControllers[index],
                                  keyboardType: TextInputType.number,
                                ),
                              ),
                            ),
                          ),
                        ),
                        DataCell(
                          Container(
                            margin: const EdgeInsets.all(8),
                            child: Obx(
                              () => Container(
                                padding: const EdgeInsets.only(left: 10.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2),
                                  border: controller.isEditingRow(index)
                                      ? Border.all(
                                          color: AppColors.borderColor,
                                          width: 0,
                                        )
                                      : const BorderDirectional(
                                          top: BorderSide.none,
                                          end: BorderSide.none,
                                          start: BorderSide.none,
                                          bottom: BorderSide.none,
                                        ),
                                ),
                                child: TextFormField(
                                  maxLines: 1,
                                  readOnly: !controller.isEditingRow(index),
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.only(bottom: 16),
                                    border: InputBorder.none,
                                    hintText: AppStringKey.email.tr,
                                    hintStyle: BaseStyle.textStyleRobotoMedium(
                                      14,
                                      AppColors.inputFieldHintTextColor,
                                    ),
                                  ),
                                  controller: controller.emailControllers[index],
                                  keyboardType: TextInputType.emailAddress,
                                ),
                              ),
                            ),
                          ),
                        ),
                        DataCell(
                          Container(
                            margin: const EdgeInsets.all(8),
                            child: Obx(
                              () => Container(
                                padding: const EdgeInsets.only(left: 10.0),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2),
                                  border: controller.isEditingRow(index)
                                      ? Border.all(
                                          color: AppColors.borderColor,
                                          width: 0,
                                        )
                                      : const BorderDirectional(
                                          top: BorderSide.none,
                                          end: BorderSide.none,
                                          start: BorderSide.none,
                                          bottom: BorderSide.none,
                                        ),
                                ),
                                child: TextFormField(
                                  maxLines: 1,
                                  readOnly: !controller.isEditingRow(index),
                                  decoration: InputDecoration(
                                    contentPadding: const EdgeInsets.only(bottom: 16),
                                    border: InputBorder.none,
                                    hintText: AppStringKey.enter_password.tr,
                                    hintStyle: BaseStyle.textStyleRobotoMedium(
                                      14,
                                      AppColors.inputFieldHintTextColor,
                                    ),
                                    suffixIcon: controller.isEditingRow(index)
                                        ? IconButton(
                                            splashRadius: 5,
                                            icon: Icon(
                                              controller.isObscureText[index] ? Icons.visibility_off : Icons.visibility,
                                              size: 14,
                                              color: AppColors.inputFieldIconColor,
                                            ),
                                            onPressed: () {
                                              controller.isObscureText[index] = !controller.isObscureText[index];
                                            },
                                          )
                                        : const Icon(null),
                                  ),
                                  controller: controller.passwordControllers[index],
                                  obscureText: controller.isObscureText[index],
                                ),
                              ),
                            ),
                          ),
                        ),
                        DataCell(
                          Container(
                            child: controller.isEditingRow(index)
                                ? CustomDropDownField(
                                    height: 32,
                                    hintTitle: AppStringKey.staff_type.tr,
                                    items: staffTypes,
                                    validationText: "",
                                    labelVisibility: false,
                                    onChanged: (val) {
                                      if (val == AppStringKey.driver.tr) {
                                        controller.employeeType[controller.currentEditingRow.value] = 'driver';
                                      } else if (val == AppStringKey.branch_staff.tr) {
                                        controller.employeeType[controller.currentEditingRow.value] = 'branch-staff';
                                      }
                                    },
                                    buttonPadding: EdgeInsets.zero,
                                    labelText: "",
                                    selectedValue: controller.currentEditingRow.value != -1
                                        ? controller.employeeType[index] == 'driver'
                                            ? AppStringKey.driver.tr
                                            : AppStringKey.branch_staff.tr
                                        : '',
                                  )
                                : Text(
                                    controller.employeeType[index] == 'driver' ? AppStringKey.driver.tr : AppStringKey.branch_staff.tr,
                                    style: BaseStyle.textStyleRobotoMedium(
                                      14,
                                      AppColors.primaryTextColor,
                                    ),
                                  ),
                          ),
                        ),
                        if (!isReadOnly)
                          DataCell(
                            Container(
                              padding: const EdgeInsets.only(
                                top: 8,
                                bottom: 8,
                                left: 8,
                              ),
                              child: Stack(
                                children: [
                                  Row(
                                    children: [
                                      controller.isEditingRow(index)
                                          ? TextButton(
                                              onPressed: () {
                                                if (controller.firstNameControllers[index].text.isEmpty) {
                                                  Utilities.showToast(AppStringKey.staff_first_name_required.tr);
                                                } else if (controller.phoneNumberControllers[index].text.isEmpty) {
                                                  Utilities.showToast(AppStringKey.staff_phone_required.tr);
                                                } else if (!Validation.isNumberWithinRange(controller.phoneNumberControllers[index].text)) {
                                                  Utilities.showToast(AppStringKey.staff_phone_length_required.tr);
                                                } else if (controller.emailControllers[index].text.isEmpty) {
                                                  Utilities.showToast(AppStringKey.staff_email_required.tr);
                                                } else if (!Validation.isValidEmail(controller.emailControllers[index].text)) {
                                                  Utilities.showToast(AppStringKey.staff_email_not_valid.tr);
                                                } else if (!PasswordStrengthChecks.isPasswordValid(controller.passwordControllers[index].text)) {
                                                  if (!PasswordStrengthChecks.hasMinimumLength(controller.passwordControllers[index].text)) {
                                                    Utilities.showToast(AppStringKey.password_minimum_length_error.tr);
                                                  } else if (!PasswordStrengthChecks.hasSpecialCharacter(
                                                      controller.passwordControllers[index].text)) {
                                                    Utilities.showToast(AppStringKey.password_special_character_error.tr);
                                                  } else if (!PasswordStrengthChecks.hasLowercase(controller.passwordControllers[index].text)) {
                                                    Utilities.showToast(AppStringKey.password_lower_case_error.tr);
                                                  } else if (!PasswordStrengthChecks.hasUppercase(controller.passwordControllers[index].text)) {
                                                    Utilities.showToast(AppStringKey.password_upper_case_error.tr);
                                                  } else if (!PasswordStrengthChecks.hasNumber(controller.passwordControllers[index].text)) {
                                                    Utilities.showToast(AppStringKey.password_numerical_error.tr);
                                                  }
                                                } else if (controller.employeeType[index].isEmpty) {
                                                  Utilities.showToast(AppStringKey.staff_staff_type_required.tr);
                                                } else {
                                                  if (controller.rowId.length >= index + 1) {
                                                    controller.saveEdits(index);
                                                  } else {
                                                    controller.saveNewRow();
                                                  }
                                                }
                                              },
                                              style: TextButton.styleFrom(
                                                minimumSize: Size.zero,
                                                padding: EdgeInsets.zero,
                                                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                                foregroundColor: AppColors.primaryTextColor,
                                                textStyle: BaseStyle.textStyleRobotoSemiBold(
                                                  14,
                                                  AppColors.primaryTextColor,
                                                ),
                                              ),
                                              child: Text(
                                                AppStringKey.save.tr,
                                              ),
                                            )
                                          : TextButton(
                                              onPressed: () {
                                                controller.editRow(index);
                                              },
                                              style: TextButton.styleFrom(
                                                minimumSize: Size.zero,
                                                padding: EdgeInsets.zero,
                                                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                                foregroundColor: AppColors.primaryTextColor,
                                                textStyle: BaseStyle.textStyleRobotoSemiBold(
                                                  14,
                                                  AppColors.primaryTextColor,
                                                ),
                                              ),
                                              child: Text(AppStringKey.edit.tr),
                                            ),
                                      const SizedBox(width: 8),
                                      TextButton(
                                        onPressed: () => {
                                          controller.deleteRow(index),
                                        },
                                        style: TextButton.styleFrom(
                                          minimumSize: Size.zero,
                                          padding: EdgeInsets.zero,
                                          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                          foregroundColor: AppColors.primaryTextColor,
                                          textStyle: BaseStyle.textStyleRobotoSemiBold(
                                            14,
                                            AppColors.primaryTextColor,
                                          ),
                                        ),
                                        child: const Text('Delete'),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                      ],
                    );
                  },
                ),
              ),
            ),
          ),
        ),
        Obx(
          () => controller.totalRows.value == 0
              ? Center(
                  child: Container(
                    margin: const EdgeInsets.all(16),
                    child: Text(
                      AppStringKey.currently_no_data_found.tr,
                      style: BaseStyle.textStyleRobotoSemiBold(
                        16,
                        AppColors.primaryTextColor,
                      ),
                    ),
                  ),
                )
              : const SizedBox.shrink(),
        ),
        isReadOnly
            ? const SizedBox.shrink()
            : Container(
                margin: const EdgeInsets.only(top: 55),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Obx(
                      () => TextButton(
                        style: TextButton.styleFrom(
                          minimumSize: Size.zero,
                          padding: EdgeInsets.zero,
                          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                          foregroundColor: AppColors.primaryTextColor,
                          textStyle: BaseStyle.textStyleRobotoSemiBold(
                            16,
                            AppColors.primaryTextColor,
                          ),
                        ),
                        onPressed: controller.currentEditingRow.value == -1 ? () => controller.addNewRow() : null,
                        child: Text(
                          "+ ${AppStringKey.add_another_staff.tr}",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ],
    );
  }
}
