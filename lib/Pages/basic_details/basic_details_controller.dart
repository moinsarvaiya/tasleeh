import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_google_places_sdk/flutter_google_places_sdk.dart' as place;
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:google_geocoding_api/google_geocoding_api.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';

import '../../Utils/app_constants.dart';
import '../../Utils/utilities.dart';
import '../../api/api_end_point.dart';
import '../../api/api_interface.dart';
import '../../api/api_presenter.dart';
import '../../api/web_fields_key.dart';
import '../../common/common_function.dart';
import '../../common/custom_loader.dart';
import '../../common/input_text_field.dart';
import '../../common/space_horizontal.dart';
import '../../common/space_vertical.dart';
import '../../core/app_preference/app_preferences.dart';
import '../../core/app_preference/storage_keys.dart';
import '../../core/constant/app_color.dart';
import '../../core/constant/app_images.dart';
import '../../core/constant/ui_constants.dart';
import '../../core/extensions/common_extension.dart';
import '../../l10n/app_string_key.dart';
import '../../models/branch_model.dart';
import '../../models/submit_branch_data_model.dart';
import '../../models/success_branch_data_model.dart';
import '../Dashboard/MyAccount/basic_details_dashboard/basic_details_dashboard_controller.dart';
import '../Dashboard/custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import '../custom_stepper/custom_stepper_controller.dart';
import '../custom_stepper/widgets/branch_handler/branch_handler_controller.dart';
import '../custom_stepper/widgets/custom_square_corner_button.dart';
import 'city_model.dart';

class BasicDetailsController extends GetxController implements ApiCallBacks {
  List<CityModel> cityList = RxList();
  ValueNotifier<int> updateNotifier = ValueNotifier<int>(0);
  final TextEditingController garageController = TextEditingController();
  final TextEditingController garageEmailController = TextEditingController();
  final TextEditingController garagePhoneNumber1 = TextEditingController();
  final TextEditingController garagePhoneNumber2 = TextEditingController();
  String dateOfEstablishment = '';
  final TextEditingController dateOfEstablishmentController = TextEditingController();
  final TextEditingController operationalHours = TextEditingController();
  final TextEditingController garageAddress1 = TextEditingController();
  final TextEditingController garageAddress2 = TextEditingController();
  final TextEditingController city = TextEditingController();
  final TextEditingController zipCode = TextEditingController();
  final TextEditingController mapLocation = TextEditingController();
  final TextEditingController firstName = TextEditingController();
  final TextEditingController lastName = TextEditingController();
  final TextEditingController phoneNumber = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController pickupSlotLimit = TextEditingController();
  final TextEditingController buildingNumber = TextEditingController();

  final FocusNode garageFocusNode = FocusNode();
  final FocusNode garageEmailFocusNode = FocusNode();
  final FocusNode garagePhoneNumber1FocusNode = FocusNode();
  final FocusNode garagePhoneNumber2FocusNode = FocusNode();
  final FocusNode operationalHoursFocusNode = FocusNode();
  final FocusNode garageAddress1FocusNode = FocusNode();
  final FocusNode garageAddress2FocusNode = FocusNode();
  final FocusNode cityFocusNode = FocusNode();
  final FocusNode zipCodeFocusNode = FocusNode();
  final FocusNode mapLocationFocusNode = FocusNode();
  final FocusNode firstNameFocusNode = FocusNode();
  final FocusNode lastNameFocusNode = FocusNode();
  final FocusNode phoneNumberFocusNode = FocusNode();
  final FocusNode emailFocusNode = FocusNode();
  final FocusNode pickupSlotFocusNode = FocusNode();
  final FocusNode buildingNumberFocusNode = FocusNode();

  RxList<String> logoUrls = RxList();
  RxList<String> licenseUrls = RxList();
  RxList<String> registrationUrls = RxList();
  RxList<String> computerCardUrls = RxList();

  late GoogleMapController googleMapController;
  Rx<LatLng>? selectedLocation = initialMapLocation.obs;
  BitmapDescriptor? customIcon;
  Set<Marker> markers = {};
  RxString formattedAddress = ''.obs;
  RxBool isTextFieldFocus = true.obs;
  RxList<String> placeData = RxList();
  final TextEditingController textEditingController = TextEditingController();
  String? cityName;
  String? country;
  String? googleMapsApiKey;
  SuccessBranchDataModel successBranchDataModel = SuccessBranchDataModel();
  String openingHour = '00:00';
  String closingHour = '00:00';
  int branchIndex = 0;
  var isApiCalled = false;

  int? renameBranchIndex;
  String? renameBranchName;

  bool isGetBranches = true;

  RxInt hoursCenterTime = 0.obs;
  RxInt minuteCenterTime = 0.obs;

  RxString startTime = AppStringKey.start_operation_time.tr.obs;
  RxString endTime = AppStringKey.end_operation_time.tr.obs;

  @override
  void onInit() {
    super.onInit();
    getLocation();
  }

  @pragma('vm:entry-point')
  void getLocation() async {
    try {
      var position = await GeolocatorPlatform.instance.getCurrentPosition();
      selectedLocation!.value = LatLng(position.latitude, position.longitude);
    } catch (e) {
      selectedLocation!.value = const LatLng(defaultLatitude, defaultLongitude);
    }
  }

  String getCurrentDate() {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('yyyy-MM-dd').format(now);
    return formattedDate;
  }

  void getBranches() {
    isGetBranches = true;
    ApiPresenter(this).getBranches();
  }

  void getBranchId() {
    clearData();
    isGetBranches = false;
    ApiPresenter(this).getBranchId(submitBranchDataModel('Branch ${Get.find<BranchHandlerController>().branches.length + 1}').toJson());
  }

  void renameBranchNameAPI(String branchName, int index) {
    renameBranchIndex = index;
    renameBranchName = branchName;
    ApiPresenter(this).renameBranch(
      {
        WebFieldKey.branchName: branchName,
      },
      AppPreferences.sharedPrefRead(StorageKeys.garageId),
    );
  }

  void getCityData() {
    ApiPresenter(this).getCityData();
  }

  void submitFieldData() {
    branchFrom = BranchFrom.patch;
    ApiPresenter(this).submitFieldData(
      submitBranchDataModel(Get.find<BranchHandlerController>().branches[Get.find<BranchHandlerController>().activeBranchIndex.value].branchName!)
          .toJson(),
      AppPreferences.sharedPrefRead(
        StorageKeys.garageId,
      ),
    );
  }

  void setBranchData(int index) {
    branchFrom = BranchFrom.get;
    branchIndex = index;
    ApiPresenter(this).setBranchData(AppPreferences.sharedPrefRead(StorageKeys.garageId));
  }

  void deleteBranch(int index) {
    branchFrom = BranchFrom.delete;
    branchIndex = index;
    ApiPresenter(this).deleteBranch(AppPreferences.sharedPrefRead(StorageKeys.garageId));
  }

  void validateData() {
    submitFieldData();
    ApiPresenter(this).validateOnboardingForm('basic-detail');
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {}

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {}

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.getBranchId:
        // Get all branches
        if (isGetBranches) {
          final List<dynamic> branches = object['data'];
          if (branches.isEmpty) {
            // Create a new branch as there is currently no branch linked to user.
            clearData();
            isGetBranches = false;
            ApiPresenter(this).getBranchId(submitBranchDataModel("Branch 1").toJson());
          } else {
            var controller = Get.find<BranchHandlerController>();
            for (var branch in branches) {
              if (AppPreferences.sharedPrefRead(StorageKeys.garageId) == branch['id']) {
                controller.branches.add(
                  BranchModel(
                    branchId: branch['id'],
                    branchName: branch['branchName'],
                  ),
                );
                controller.branches.refresh();
                controller.setActiveBranchIndex(0);
                isGetBranches = false;
                setBranchData(0);
              }
            }
          }
        } else {
          successBranchDataModel = SuccessBranchDataModel.fromJson(
            object['data'],
          );
          AppPreferences.sharedPrefWrite(StorageKeys.garageId, successBranchDataModel.id);
          Get.find<BranchHandlerController>()
              .branches
              .add(BranchModel(branchId: successBranchDataModel.id, branchName: 'Branch ${Get.find<BranchHandlerController>().branches.length + 1}'));
          Get.find<BranchHandlerController>().branches.refresh();
          Get.find<BranchHandlerController>().setActiveBranchIndex(Get.find<BranchHandlerController>().branches.length - 1);
        }
        break;

      case ApiEndPoints.getBranchData:
        if (branchFrom == BranchFrom.get) {
          clearData();
          Get.find<BranchHandlerController>().setActiveBranchIndex(branchIndex);
          successBranchDataModel = SuccessBranchDataModel.fromJson(object['data']);
          setResponseData(successBranchDataModel);
        } else if (branchFrom == BranchFrom.delete) {
          Get.find<BranchHandlerController>().branches.removeAt(branchIndex);

          if (branchIndex <= Get.find<BranchHandlerController>().activeBranchIndex.value) {
            Get.find<BranchHandlerController>().setActiveBranchIndex(Get.find<BranchHandlerController>().branches.length - 1);
          }
          Get.find<BranchHandlerController>().branches.refresh();
        } else if (branchFrom == BranchFrom.patch) {
          if (isUpdatingMyAccounts) {
            Get.find<BasicDetailsDashboardController>().clearData();
            Get.find<BasicDetailsDashboardController>().getBasicDetailsData();
            Get.find<CustomDashboardStepperController>().updateGarageProfile();
            isUpdatingMyAccounts = false;
          }
        }
        break;

      case ApiEndPoints.renameBranch:
        if (renameBranchIndex != null) {
          Get.find<BranchHandlerController>().branches[renameBranchIndex!].branchName = renameBranchName;
          Get.find<BranchHandlerController>().branches.refresh();
          renameBranchIndex = null;
          renameBranchName = null;
        }
        break;

      case ApiEndPoints.getCityData:
        cityList.clear();
        cityList.addAll((object['data'] as List).map((e) => CityModel.fromJson(e)).toList());
        break;

      case ApiEndPoints.validateData:
        if (object['data'].length == 0) {
          Fluttertoast.cancel();
          Get.find<CustomStepperController>().setActivePage(Get.find<CustomStepperController>().selectedIndex.value + 1);
        } else {
          String incompleteBranches = object['data'].map((entry) => entry["branchName"]).join(', ');
          Fluttertoast.showToast(
            msg: "${AppStringKey.please_complete_required_fields.tr} $incompleteBranches",
            gravity: ToastGravity.BOTTOM,
            webPosition: 'center',
            webBgColor: '#FF4D4F',
            webShowClose: true,
            fontSize: 16,
            timeInSecForIosWeb: toastVisibleTime,
            textColor: AppColors.colorWhite,
          );
        }
        break;
    }
  }

  void imageUpload(BuildContext context, Map<String, Uint8List> imageList, RxList<String> imageUrls) async {
    try {
      var headers = {
        WebFieldKey.strContentType: WebFieldKey.strMultipartFormData,
        WebFieldKey.strAuthorization: 'Bearer ${AppPreferences.sharedPrefRead(StorageKeys.accessToken)}',
      };

      for (var entry in imageList.entries) {
        var request = http.MultipartRequest(
          WebFieldKey.strPostMethod,
          Uri.parse('${ApiEndPoints.baseUrl}${ApiEndPoints.imageUpload}'),
        );
        request.headers.addAll(headers);
        var stream = http.ByteStream.fromBytes(entry.value);
        var length = entry.value.lengthInBytes;
        request.files.add(
          http.MultipartFile(WebFieldKey.strFile, stream, length, filename: '${entry.key}.jpg'),
        );
        request.fields[WebFieldKey.strFolder] = 'garage';
        http.StreamedResponse response = await request.send();
        final responseBody = await response.stream.bytesToString();
        final parsedJson = jsonDecode(responseBody);
        if (parsedJson['code'] == 200 || parsedJson['code'] == 201) {
          imageUrls.add(parsedJson['data']['url'].toString());
          imageUrls.refresh();
          submitFieldData();
        } else {
          CommonFunction.showCustomSnackBar(
            context: Get.context!,
            message: parsedJson['error'],
            duration: const Duration(seconds: 3),
            actionLabel: 'Close',
          );
        }
      }
      LoadingDialog.closeFullScreenDialog();
    } catch (e) {
      LoadingDialog.closeFullScreenDialog();
      CommonFunction.showCustomSnackBar(
        context: Get.context!,
        message: e.toString(),
        duration: const Duration(seconds: 3),
        actionLabel: 'Close',
      );
    }
  }

  SubmitBranchDataModel submitBranchDataModel(
    String branchName,
  ) =>
      SubmitBranchDataModel(
        garageName: garageController.text,
        branchName: branchName,
        email: garageEmailController.text,
        countryCode1: AppConstants.countryCode,
        phone1: garagePhoneNumber1.text,
        countryCode2: AppConstants.countryCode,
        phone2: garagePhoneNumber2.text,
        lat: selectedLocation!.value.latitude.toString(),
        lng: selectedLocation!.value.longitude.toString(),
        logo: logoUrls.isNotEmpty ? logoUrls[0] : '',
        commercialRegistration: registrationUrls.isNotEmpty ? registrationUrls[0] : '',
        tradeLicence: licenseUrls.isNotEmpty ? licenseUrls[0] : '',
        computerCard: computerCardUrls.isNotEmpty ? computerCardUrls[0] : '',
        openingHour: startTime.value == AppStringKey.start_operation_time.tr ? '00:00' : startTime.value,
        closingHour: endTime.value == AppStringKey.end_operation_time.tr ? '00:00' : endTime.value,
        dateOfEstablishment: dateOfEstablishment,
        addressLine1: garageAddress1.text,
        addressLine2: garageAddress2.text,
        buildingNumber: buildingNumber.text,
        pickUp: pickupSlotLimit.text,
        zip: zipCode.text,
        // mapLocation: formattedAddress.value,
        mapLocation: mapLocation.text,
        contactPersonFirstName: firstName.text,
        contactPersonLastName: lastName.text,
        contactPersonEmail: email.text,
        contactPersonCountryCode: AppConstants.countryCode,
        contactPersonPhone: phoneNumber.text,
        city: DataProvider(id: '1'),
        provider: DataProvider(
          id: AppPreferences.sharedPrefRead(StorageKeys.userId),
        ),
      );

  void setResponseData(SuccessBranchDataModel successBranchDataModel) {
    garageController.text = successBranchDataModel.garageName ?? '';
    garageEmailController.text = successBranchDataModel.email ?? '';
    garagePhoneNumber1.text = successBranchDataModel.phone1 ?? '';
    garagePhoneNumber2.text = successBranchDataModel.phone2 ?? '';
    selectedLocation!.value = LatLng(successBranchDataModel.lat!, successBranchDataModel.lng!);
    if (successBranchDataModel.logo!.isNotEmpty) {
      logoUrls.add(successBranchDataModel.logo!);
    }
    if (successBranchDataModel.commercialRegistration!.isNotEmpty) {
      registrationUrls.add(successBranchDataModel.commercialRegistration!);
    }
    if (successBranchDataModel.tradeLicence!.isNotEmpty) {
      licenseUrls.add(successBranchDataModel.tradeLicence!);
    }
    if (successBranchDataModel.computerCard!.isNotEmpty) {
      computerCardUrls.add(successBranchDataModel.computerCard!);
    }
    openingHour = successBranchDataModel.openingHour!;
    closingHour = successBranchDataModel.closingHour!;
    startTime.value = successBranchDataModel.openingHour!;
    endTime.value = successBranchDataModel.closingHour!;
    city.text = successBranchDataModel.city!.name!;
    pickupSlotLimit.text = successBranchDataModel.pickUp!;
    dateOfEstablishment = successBranchDataModel.dateOfEstablishment!;
    dateOfEstablishmentController.text = successBranchDataModel.dateOfEstablishment!;
    garageAddress1.text = successBranchDataModel.addressLine1!;
    garageAddress2.text = successBranchDataModel.addressLine2!;
    buildingNumber.text = successBranchDataModel.buildingNumber!;
    zipCode.text = successBranchDataModel.zip ?? '';
    mapLocation.text = successBranchDataModel.mapLocation ?? '';
    firstName.text = successBranchDataModel.contactPersonFirstName ?? '';
    lastName.text = successBranchDataModel.contactPersonLastName ?? '';
    email.text = successBranchDataModel.contactPersonEmail ?? '';
    phoneNumber.text = successBranchDataModel.contactPersonPhone ?? '';
  }

  void getApiKey() async {
    googleMapsApiKey = await getFromEnv(EnvironmentKeys.googleMapKey);
  }

  List<String> getFilteredOptions(String filter) {
    return placeData.where((String option) {
      return option.toLowerCase().contains(filter.toLowerCase());
    }).toList();
  }

  void getLocationFromLatLong(LatLng location) async {
    final api = GoogleGeocodingApi(googleMapsApiKey!, isLogged: true);
    GoogleGeocodingResponse reversedSearchResults = await api.reverse(
      '${selectedLocation!.value.latitude}, ${selectedLocation!.value.longitude}',
      language: 'en',
    );
    if (reversedSearchResults.status == 'OK' && reversedSearchResults.results.isNotEmpty) {
      final result = reversedSearchResults.results.first;
      formattedAddress.value = result.formattedAddress;
      textEditingController.text = formattedAddress.value;
      for (var component in result.addressComponents) {
        for (var type in component.types) {
          if (type == 'locality') {
            cityName = component.longName;
          } else if (type == 'country') {
            country = component.longName;
          }
        }
      }
    }
  }

  void getPlaceDataFromTextField(String placeName) async {
    if (placeName != '') {
      final places = place.FlutterGooglePlacesSdk(googleMapsApiKey!);
      final predictions = await places.findAutocompletePredictions(placeName);
      placeData.clear();
      placeData.addAll(predictions.predictions.map((prediction) => prediction.fullText).toList());
    } else {
      isTextFieldFocus.value = true;
      placeData.clear();
    }
  }

  void getLatLongFromPlace(String place) async {
    final api = GoogleGeocodingApi(googleMapsApiKey!, isLogged: true);
    GoogleGeocodingResponse reversedSearchResults = await api.search(
      place,
      language: 'en',
    );
    if (reversedSearchResults.status == 'OK' && reversedSearchResults.results.isNotEmpty) {
      double latitude = reversedSearchResults.results.first.geometry!.location.lat;
      double longitude = reversedSearchResults.results.first.geometry!.location.lng;
      LatLng newLocation = LatLng(latitude, longitude);
      selectedLocation!.value = newLocation;
      // Update markers on the map
      // markers.clear();
      // markers.add(
      //   Marker(
      //     markerId: const MarkerId('selected-location'),
      //     position: newLocation,
      //     icon: BitmapDescriptor.defaultMarker,
      //     draggable: false,
      //   ),
      // );
      googleMapController.animateCamera(CameraUpdate.newLatLngZoom(newLocation, 15));
      googleMapController.moveCamera(CameraUpdate.newLatLngZoom(newLocation, 15));
      placeData.clear();
    }
  }

  Future<void> mapDialog(BuildContext context) async {
    await showDialog(
      context: context,
      builder: (BuildContext context) => GestureDetector(
        onTap: () {
          Get.back();
        },
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
            child: GestureDetector(
              onTap: () {},
              child: Container(
                margin: EdgeInsets.symmetric(
                  horizontal: context.isDesktop ? 200 : 50,
                  vertical: context.isDesktop ? 70 : 50,
                ),
                padding: EdgeInsets.symmetric(
                  horizontal: context.isDesktop ? 50 : 30,
                  vertical: context.isDesktop ? 20 : 30,
                ),
                width: double.maxFinite,
                height: context.screenHeight,
                color: Colors.white,
                child: Column(
                  children: [
                    Obx(
                      () => InputTextField(
                        backgroundColor: AppColors.colorBackground,
                        isLabelVisible: false,
                        maxLines: 1,
                        label: formattedAddress.value.toString(),
                        hintText: AppStringKey.find_location.tr,
                        prefixIcon: const Padding(
                          padding: EdgeInsets.only(left: 8),
                          child: Icon(
                            Icons.location_on_rounded,
                            color: AppColors.colorBlack,
                          ),
                        ),
                        onChangeWithValidationStatus: (value, isValid) {
                          isTextFieldFocus.value = false;
                          getPlaceDataFromTextField(value!);
                        },
                        isRequireField: false,
                        textEditingController: textEditingController,
                      ),
                    ),
                    Stack(
                      children: [
                        Column(
                          children: [
                            const SpaceVertical(0.02),
                            SizedBox(
                              width: double.maxFinite,
                              height: context.isDesktop ? context.screenHeight * 0.55 : context.screenHeight * 0.57,
                              child: Stack(
                                children: [
                                  Obx(
                                    () => GoogleMap(
                                      zoomGesturesEnabled: isTextFieldFocus.value,
                                      /*markers: <Marker>{
                                        Marker(
                                          markerId: const MarkerId('selected-location'),
                                          position: selectedLocation!.value,
                                          icon: BitmapDescriptor.defaultMarker,
                                          draggable: false,
                                        ),
                                      },*/
                                      initialCameraPosition: CameraPosition(
                                        target: selectedLocation!.value,
                                        zoom: 15,
                                      ),
                                      onMapCreated: (GoogleMapController mapController) {
                                        googleMapController = mapController;
                                        Future.delayed(
                                          const Duration(seconds: 1),
                                          () {
                                            googleMapController.animateCamera(
                                              CameraUpdate.newLatLng(
                                                selectedLocation!.value,
                                              ),
                                            );
                                          },
                                        );
                                      },
                                      myLocationEnabled: true,
                                      onCameraMove: (CameraPosition position) {
                                        selectedLocation!.value = position.target;
                                        // markers.removeWhere((Marker marker) => marker.markerId.value == 'selected-location');
                                        getLocationFromLatLong(
                                          selectedLocation!.value,
                                        );
                                      },
                                    ),
                                  ),
                                  Positioned(
                                    child: Center(
                                      child: SvgPicture.asset(
                                        AppImages.googleMapPin,
                                        width: 70,
                                        height: 40,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        Obx(
                          () => Visibility(
                            visible: placeData.isNotEmpty,
                            child: Container(
                              color: AppColors.colorWhite,
                              width: double.maxFinite,
                              height: context.isDesktop ? context.screenHeight * 0.25 : context.screenHeight * 0.27,
                              child: ListView(
                                children: getFilteredOptions(textEditingController.text).map((String option) {
                                  return ListTile(
                                    title: Row(
                                      children: [
                                        const Icon(
                                          Icons.location_on_rounded,
                                          color: AppColors.colorBlack,
                                        ),
                                        const SpaceHorizontal(0.005),
                                        Expanded(
                                          child: Text(
                                            option,
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                      ],
                                    ),
                                    onTap: () {
                                      isTextFieldFocus.value = true;
                                      formattedAddress.value = option;
                                      textEditingController.text = option;
                                      getLatLongFromPlace(option);
                                    },
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SpaceVertical(0.01),
                    context.isDesktop
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomSquareCornerButton(
                                buttonText: AppStringKey.cancel.tr,
                                buttonTextColor: AppColors.colorBlack,
                                backgroundColor: AppColors.colorBackground,
                                height: context.isDesktop ? 0.03 : 0.07,
                                width: context.isDesktop ? 0.1 : 0.30,
                                textSize: context.isDesktop ? 8 : 13,
                                isResponsive: false,
                                callBack: () {
                                  textEditingController.clear();
                                  placeData.clear();
                                  Get.back();
                                },
                              ),
                              const SpaceHorizontal(0.01),
                              CustomSquareCornerButton(
                                buttonText: AppStringKey.confirm_location.tr,
                                buttonTextColor: AppColors.colorWhite,
                                backgroundColor: AppColors.colorBlack,
                                height: context.isDesktop ? 0.03 : 0.07,
                                width: context.isDesktop ? 0.1 : 0.30,
                                textSize: context.isDesktop ? 8 : 13,
                                isResponsive: false,
                                callBack: () {
                                  mapLocation.text = formattedAddress.value;
                                  Get.back();
                                  submitFieldData();
                                },
                              )
                            ],
                          )
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CustomSquareCornerButton(
                                buttonText: AppStringKey.confirm_location.tr,
                                buttonTextColor: AppColors.colorWhite,
                                backgroundColor: AppColors.colorBlack,
                                height: 0.07,
                                width: 0.45,
                                textSize: 13,
                                isResponsive: false,
                                callBack: () {
                                  mapLocation.text = formattedAddress.value;
                                  Get.back();
                                  submitFieldData();
                                },
                              ),
                              const SpaceVertical(0.01),
                              CustomSquareCornerButton(
                                buttonText: AppStringKey.cancel.tr,
                                buttonTextColor: AppColors.colorBlack,
                                backgroundColor: AppColors.colorBackground,
                                height: 0.07,
                                width: 0.45,
                                textSize: 13,
                                isResponsive: false,
                                callBack: () {
                                  textEditingController.clear();
                                  placeData.clear();
                                  Get.back();
                                },
                              ),
                            ],
                          )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    ).then(
      (value) {
        textEditingController.clear();
        placeData.clear();
      },
    );
  }

  void clearData() {
    garageController.clear();
    garageEmailController.clear();
    garagePhoneNumber1.clear();
    garagePhoneNumber2.clear();
    selectedLocation!.value = const LatLng(23.0225, 72.5714);
    logoUrls.clear();
    registrationUrls.clear();
    licenseUrls.clear();
    computerCardUrls.clear();
    openingHour = '';
    closingHour = '';
    startTime.value = AppStringKey.start_operation_time.tr;
    endTime.value = AppStringKey.end_operation_time.tr;
    dateOfEstablishment = '';
    garageAddress1.clear();
    garageAddress2.clear();
    pickupSlotLimit.clear();
    mapLocation.clear();
    zipCode.clear();
    formattedAddress.value = '';
    firstName.clear();
    lastName.clear();
    email.clear();
    phoneNumber.clear();
    updateNotifier.value++;
  }
}
