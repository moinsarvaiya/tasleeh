import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../Utils/utilities.dart';
import '../../common/custom_start_time_end_time_picker.dart';
import '../../common/date_picker.dart';
import '../../common/input_text_field.dart';
import '../../core/constant/app_color.dart';
import '../../core/constant/base_style.dart';
import '../../core/constant/ui_constants.dart';
import '../../core/extensions/validation_function.dart';
import '../../custom_classes/custom_dropdown_field.dart';
import '../../l10n/app_string_key.dart';
import '../common/image_upload/file_upload.dart';
import 'basic_details_controller.dart';

@override
class BasicDetailsMobileView extends StatelessWidget {
  final BasicDetailsController controller;
  final bool isReadOnly;
  final bool enableSync;

  const BasicDetailsMobileView({
    super.key,
    required this.controller,
    required this.isReadOnly,
    required this.enableSync,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          AppStringKey.garage_details.tr,
          style: Theme.of(context).textTheme.headlineSmall,
        ),
        const SizedBox(
          height: 32,
        ),
        Obx(
          () => FileUpload(
            title: AppStringKey.logo.tr,
            acceptedFileDescription: AppStringKey.file_upload_format.tr,
            sizeDescription: "${AppStringKey.upload_max_size_mb.tr} 10${AppStringKey.mb}",
            buttonContent: AppStringKey.click_to_upload.tr,
            // ignore: invalid_use_of_protected_member
            imageUrlsList: controller.logoUrls.value,
            isReadOnly: isReadOnly,
            onImagePick: (image) {
              controller.imageUpload(context, image, controller.logoUrls);
            },
            onRemoveImage: (index) {
              controller.logoUrls.removeAt(index);
            },
          ),
        ),
        const SizedBox(
          height: 24,
        ),
        Obx(
          () => FileUpload(
            title: AppStringKey.trade_license.tr,
            acceptedFileDescription: AppStringKey.file_upload_format.tr,
            sizeDescription: "${AppStringKey.upload_max_size_mb.tr} 10${AppStringKey.mb}",
            buttonContent: AppStringKey.click_to_upload.tr,
            isReadOnly: isReadOnly,
            // ignore: invalid_use_of_protected_member
            imageUrlsList: controller.licenseUrls.value,
            onImagePick: (image) {
              controller.imageUpload(context, image, controller.licenseUrls);
            },
            onRemoveImage: (index) {
              controller.licenseUrls.removeAt(index);
            },
          ),
        ),
        const SizedBox(
          height: 24,
        ),
        Obx(
          () => FileUpload(
            title: AppStringKey.commercial_registration.tr,
            acceptedFileDescription: AppStringKey.file_upload_format.tr,
            sizeDescription: "${AppStringKey.upload_max_size_mb.tr} 10${AppStringKey.mb}",
            buttonContent: AppStringKey.click_to_upload.tr,
            isReadOnly: isReadOnly,
            // ignore: invalid_use_of_protected_member
            imageUrlsList: controller.registrationUrls.value,
            onImagePick: (image) {
              controller.imageUpload(context, image, controller.registrationUrls);
            },
            onRemoveImage: (index) {
              controller.registrationUrls.removeAt(index);
            },
          ),
        ),
        const SizedBox(
          height: 24,
        ),
        Obx(
          () => FileUpload(
            title: AppStringKey.computer_card.tr,
            acceptedFileDescription: AppStringKey.file_upload_format.tr,
            sizeDescription: "${AppStringKey.upload_max_size_mb.tr} 10${AppStringKey.mb}",
            buttonContent: AppStringKey.click_to_upload.tr,
            isReadOnly: isReadOnly,
            // ignore: invalid_use_of_protected_member
            imageUrlsList: controller.computerCardUrls.value,
            onImagePick: (image) {
              controller.imageUpload(context, image, controller.computerCardUrls);
            },
            onRemoveImage: (index) {
              controller.computerCardUrls.removeAt(index);
            },
          ),
        ),
        const SizedBox(
          height: 24,
        ),
        InputTextField(
          hintText: AppStringKey.enter_garage_name.tr,
          label: AppStringKey.garage_name.tr,
          isRequireField: true,
          focusScope: controller.garageFocusNode,
          textEditingController: controller.garageController,
          isReadOnly: isReadOnly,
          readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
          onChangeWithValidationStatus: (value, isValid) {
            if (enableSync && isValid) {
              Utilities.customDebounce(customDebounceTime, () {
                controller.submitFieldData();
              });
            }
          },
        ),
        const SizedBox(
          height: 24,
        ),
        InputTextField(
          hintText: AppStringKey.enter_email.tr,
          label: AppStringKey.email.tr,
          textInputType: TextInputType.emailAddress,
          isRequireField: true,
          focusScope: controller.garageEmailFocusNode,
          textEditingController: controller.garageEmailController,
          isReadOnly: isReadOnly,
          readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
          onChangeWithValidationStatus: (value, isValid) {
            if (enableSync && isValid) {
              Utilities.customDebounce(customDebounceTime, () {
                controller.submitFieldData();
              });
            }
          },
          validationFunction: (val) {
            return Validation.isValidEmail(val);
          },
          validationMessage: (val) {
            return AppStringKey.validation_invalid_email.tr;
          },
        ),
        const SizedBox(
          height: 24,
        ),
        InputTextField(
          hintText: "${AppStringKey.enter_phone_no.tr} 1",
          label: " ${AppStringKey.phone_no.tr} 1",
          inputFormatter: [FilteringTextInputFormatter.digitsOnly],
          isPhoneNo: true,
          isRequireField: true,
          focusScope: controller.garagePhoneNumber1FocusNode,
          textEditingController: controller.garagePhoneNumber1,
          isReadOnly: isReadOnly,
          readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
          onChangeWithValidationStatus: (value, isValid) {
            if (enableSync && isValid) {
              Utilities.customDebounce(customDebounceTime, () {
                controller.submitFieldData();
              });
            }
          },
          validationFunction: (val) {
            return Validation.isNumberWithinRange(val);
          },
          validationMessage: (val) {
            return AppStringKey.validation_invalid_number.tr;
          },
        ),
        const SizedBox(
          height: 24,
        ),
        InputTextField(
          hintText: "${AppStringKey.enter_phone_no.tr} 2",
          label: " ${AppStringKey.phone_no.tr} 2",
          inputFormatter: [FilteringTextInputFormatter.digitsOnly],
          isPhoneNo: true,
          isRequireField: false,
          focusScope: controller.garagePhoneNumber2FocusNode,
          textEditingController: controller.garagePhoneNumber2,
          isReadOnly: isReadOnly,
          readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
          onChangeWithValidationStatus: (value, isValid) {
            if (enableSync && isValid) {
              Utilities.customDebounce(customDebounceTime, () {
                controller.submitFieldData();
              });
            }
          },
          validationFunction: (val) {
            return Validation.isNumberWithinRange(val);
          },
          validationMessage: (val) {
            return AppStringKey.validation_invalid_number.tr;
          },
        ),
        const SizedBox(
          height: 24,
        ),
        CustomStartTimeEndTimePicker(
          hintText: AppStringKey.start_date.tr,
          label: AppStringKey.operation_hours.tr,
          isRequireField: true,
          textEditingController: controller.operationalHours,
          isReadOnly: isReadOnly,
          onChange: (value) {
            // Utilities.customDebounce(customDebounceTime, () {
            //   controller.submitFieldData();
            // });
          },
        ),
        const SizedBox(
          height: 24,
        ),
        Text(
          AppStringKey.date_of_establishment.tr,
          style: BaseStyle.textStyleRobotoRegular(14, AppColors.primaryTextColor),
        ),
        const SizedBox(
          height: 8,
        ),
        DatePicker(
          dateSetter: (date) {
            controller.dateOfEstablishment = date;
            controller.submitFieldData();
          },
          isReadOnly: isReadOnly,
          textEditingController: controller.dateOfEstablishmentController,
        ),
        const SizedBox(
          height: 24,
        ),
        InputTextField(
          hintText: AppStringKey.building_number.tr,
          label: AppStringKey.enter_building_number.tr,
          isRequireField: true,
          focusScope: controller.buildingNumberFocusNode,
          textEditingController: controller.buildingNumber,
          isReadOnly: isReadOnly,
          readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
          onChangeWithValidationStatus: (value, isValid) {
            if (enableSync && isValid) {
              Utilities.customDebounce(customDebounceTime, () {
                controller.submitFieldData();
              });
            }
          },
        ),
        const SizedBox(
          height: 24,
        ),
        InputTextField(
          hintText: "${AppStringKey.enter_address_line.tr} 1",
          label: "${AppStringKey.address_line.tr} 1",
          textEditingController: controller.garageAddress1,
          isRequireField: true,
          focusScope: controller.garageAddress1FocusNode,
          isReadOnly: isReadOnly,
          readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
          onChangeWithValidationStatus: (value, isValid) {
            if (enableSync && isValid) {
              Utilities.customDebounce(customDebounceTime, () {
                controller.submitFieldData();
              });
            }
          },
        ),
        const SizedBox(
          height: 24,
        ),
        InputTextField(
          hintText: "${AppStringKey.enter_address_line.tr} 2",
          label: "${AppStringKey.address_line.tr} 2",
          isRequireField: false,
          focusScope: controller.garageAddress2FocusNode,
          textEditingController: controller.garageAddress2,
          isReadOnly: isReadOnly,
          readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
          onChangeWithValidationStatus: (value, isValid) {
            if (enableSync && isValid) {
              Utilities.customDebounce(customDebounceTime, () {
                controller.submitFieldData();
              });
            }
          },
        ),
        const SizedBox(
          height: 24,
        ),
        InputTextField(
          hintText: AppStringKey.enter_max_number_of_pickups.tr,
          label: AppStringKey.pickup_slot_limit.tr,
          inputFormatter: [FilteringTextInputFormatter.digitsOnly],
          isRequireField: true,
          focusScope: controller.pickupSlotFocusNode,
          textEditingController: controller.pickupSlotLimit,
          isReadOnly: isReadOnly,
          readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
          onChangeWithValidationStatus: (value, isValid) {
            if (enableSync && isValid) {
              Utilities.customDebounce(customDebounceTime, () {
                controller.submitFieldData();
              });
            }
          },
        ),
        const SizedBox(
          height: 24,
        ),
        Obx(
          () => CustomDropDownField(
            labelVisibility: true,
            labelText: AppStringKey.city.tr,
            selectedValue: controller.city.text,
            validationText: AppStringKey.select_city.tr,
            hintTitle: AppStringKey.select_city.tr,
            onChanged: (countryValue) {},
            isReadOnly: isReadOnly,
            backgroundColor: isReadOnly ? AppColors.tertiaryBackgroundColor : AppColors.colorWhite,
            items: controller.cityList.map((city) => city.name!).toList(),
          ),
        ),
        const SizedBox(
          height: 24,
        ),
        InputTextField(
          hintText: AppStringKey.enter_zip_code.tr,
          label: AppStringKey.zip_code.tr,
          isRequireField: true,
          focusScope: controller.zipCodeFocusNode,
          inputFormatter: [FilteringTextInputFormatter.digitsOnly],
          textEditingController: controller.zipCode,
          isReadOnly: isReadOnly,
          readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
          onChangeWithValidationStatus: (value, isValid) {
            if (enableSync && isValid) {
              Utilities.customDebounce(customDebounceTime, () {
                controller.submitFieldData();
              });
            }
          },
        ),
        const SizedBox(
          height: 24,
        ),
        InputTextField(
          hintText: AppStringKey.pin_map_location.tr,
          label: AppStringKey.map_location.tr,
          isRequireField: true,
          focusScope: controller.mapLocationFocusNode,
          textEditingController: controller.mapLocation,
          icon: const Icon(
            Icons.location_on_outlined,
            color: AppColors.strokeColor,
          ),
          onTap: isReadOnly
              ? null
              : () {
                  controller.mapDialog(context);
                },
          readOnlyBackgroundColor: isReadOnly ? AppColors.tertiaryBackgroundColor : AppColors.colorWhite,
          isReadOnly: true,
        ),
        const SizedBox(
          height: 48,
        ),
        Text(
          AppStringKey.contact_person_details.tr,
          style: Theme.of(context).textTheme.headlineSmall,
        ),
        const SizedBox(
          height: 32,
        ),
        InputTextField(
          hintText: AppStringKey.enter_first_name.tr,
          label: AppStringKey.first_name.tr,
          isRequireField: true,
          textInputType: TextInputType.text,
          focusScope: controller.firstNameFocusNode,
          textEditingController: controller.firstName,
          isReadOnly: isReadOnly,
          readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
          onChangeWithValidationStatus: (value, isValid) {
            if (enableSync && isValid) {
              Utilities.customDebounce(customDebounceTime, () {
                controller.submitFieldData();
              });
            }
          },
        ),
        const SizedBox(
          height: 24,
        ),
        InputTextField(
          hintText: AppStringKey.enter_last_name.tr,
          label: AppStringKey.last_name.tr,
          isRequireField: true,
          textInputType: TextInputType.text,
          focusScope: controller.lastNameFocusNode,
          textEditingController: controller.lastName,
          isReadOnly: isReadOnly,
          readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
          onChangeWithValidationStatus: (value, isValid) {
            if (enableSync && isValid) {
              Utilities.customDebounce(customDebounceTime, () {
                controller.submitFieldData();
              });
            }
          },
        ),
        const SizedBox(
          height: 24,
        ),
        InputTextField(
          hintText: AppStringKey.enter_phone_no.tr,
          label: AppStringKey.phone_no.tr,
          inputFormatter: [FilteringTextInputFormatter.digitsOnly],
          isPhoneNo: true,
          isRequireField: true,
          focusScope: controller.phoneNumberFocusNode,
          textEditingController: controller.phoneNumber,
          isReadOnly: isReadOnly,
          readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
          onChangeWithValidationStatus: (value, isValid) {
            if (enableSync && isValid) {
              Utilities.customDebounce(customDebounceTime, () {
                controller.submitFieldData();
              });
            }
          },
          validationFunction: (val) {
            return Validation.isNumberWithinRange(val);
          },
          validationMessage: (val) {
            return AppStringKey.validation_invalid_number.tr;
          },
        ),
        const SizedBox(
          height: 24,
        ),
        InputTextField(
          hintText: AppStringKey.enter_email.tr,
          label: AppStringKey.email.tr,
          textInputType: TextInputType.emailAddress,
          isRequireField: true,
          focusScope: controller.emailFocusNode,
          textEditingController: controller.email,
          isReadOnly: isReadOnly,
          readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
          onChangeWithValidationStatus: (value, isValid) {
            if (enableSync && isValid) {
              Utilities.customDebounce(customDebounceTime, () {
                controller.submitFieldData();
              });
            }
          },
          validationFunction: (val) {
            return Validation.isValidEmail(val);
          },
          validationMessage: (val) {
            return AppStringKey.validation_invalid_email.tr;
          },
        ),
        const SizedBox(
          height: 24,
        ),
      ],
    );
  }
}
