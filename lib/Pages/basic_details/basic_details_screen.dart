import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../core/app_preference/app_preferences.dart';
import '../../core/app_preference/storage_keys.dart';
import '../../core/constant/ui_constants.dart';
import '../../core/extensions/common_extension.dart';
import 'basic_details_controller.dart';
import 'basic_details_mobile_view.dart';
import 'basic_details_web_view.dart';

class BasicDetailsScreen extends StatefulWidget {
  final bool isReadOnly;
  final bool enableSync;

  const BasicDetailsScreen({
    super.key,
    this.isReadOnly = false,
    this.enableSync = true,
  });

  @override
  State<BasicDetailsScreen> createState() => _BasicDetailsScreenState();
}

class _BasicDetailsScreenState extends State<BasicDetailsScreen> {
  final controller = Get.find<BasicDetailsController>();

  @override
  void initState() {
    stepperPosition = StepperPosition.basicDetails;
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      if (!widget.isReadOnly && !controller.isApiCalled) {
        controller.isApiCalled = true;
        controller.getApiKey();
        controller.dateOfEstablishment = controller.getCurrentDate();
        if (AppPreferences.sharedPrefRead(StorageKeys.addBranchesFrom) == addBranch) {
          controller.getBranchId();
        } else {
          controller.getBranches();
        }
        controller.getCityData();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ValueListenableBuilder<int>(
          valueListenable: controller.updateNotifier,
          builder: (context, _, __) => Container(
            decoration: const BoxDecoration(
              color: Colors.white,
            ),
            child: context.isDesktop
                ? BasicDetailWebAppView(
                    controller: controller,
                    isReadOnly: widget.isReadOnly,
                    enableSync: widget.enableSync,
                  )
                : BasicDetailsMobileView(
                    controller: controller,
                    isReadOnly: widget.isReadOnly,
                    enableSync: widget.enableSync,
                  ),
          ),
        )
      ],
    );
  }
}
