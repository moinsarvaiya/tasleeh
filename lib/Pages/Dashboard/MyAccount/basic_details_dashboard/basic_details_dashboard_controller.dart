import 'package:al_baida_garage_fe/api/api_interface.dart';
import 'package:al_baida_garage_fe/core/app_preference/app_preferences.dart';
import 'package:al_baida_garage_fe/core/app_preference/storage_keys.dart';
import 'package:get/get.dart';

import '../../../../api/api_end_point.dart';
import '../../../../api/api_presenter.dart';

class BasicDetailsDashboardController extends GetxController
    implements ApiCallBacks {
  RxString garageController = ''.obs;
  RxString garageEmailController = ''.obs;
  RxString garagePhoneNumber = ''.obs;
  RxString dateOfEstablishment = ''.obs;
  RxString operationalHours = ''.obs;
  RxString garageAddress = ''.obs;
  RxString zipCode = ''.obs;
  RxString mapLocation = ''.obs;
  RxString name = ''.obs;
  RxString email = ''.obs;
  RxString phoneNumber = ''.obs;
  RxString garageLogoImage = ''.obs;

  void getBranches() {
    ApiPresenter(this).getBranches();
  }

  void getBasicDetailsData() {
    ApiPresenter(this)
        .setBranchData(AppPreferences.sharedPrefRead(StorageKeys.garageId));
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {}

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {}

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.getBranchId:
        getBasicDetailsData();
        break;
      case ApiEndPoints.getBranchData:
        if (object['data'].length > 0) {
          garageController.value = object['data']['garageName'];
          garageEmailController.value = object['data']['email'];
          if (object['data']['phone1'] != '' &&
              object['data']['phone2'] != '') {
            garagePhoneNumber.value =
                ("+${object['data']['countryCode1']} ${object['data']['phone1']}, +${object['data']['countryCode2']} ${object['data']['phone2']}");
          } else if (object['data']['phone1'] != '') {
            garagePhoneNumber.value =
                ("+${object['data']['countryCode1']} ${object['data']['phone1']}");
          } else if (object['data']['phone2'] != '') {
            garagePhoneNumber.value =
                ("+${object['data']['countryCode1']} ${object['data']['phone2']}");
          }
          operationalHours.value =
              ("${object['data']['openingHour']} - ${object['data']['closingHour']}");
          garageAddress.value =
              ("${object['data']['addressLine1']} ${object['data']['addressLine2']}");
          zipCode.value = object['data']['zip'];
          mapLocation.value = object['data']['mapLocation'];
          name.value =
              ("${object['data']['contactPersonFirstName']} ${object['data']['contactPersonLastName']}");
          email.value = object['data']['contactPersonEmail'];
          dateOfEstablishment.value = object['data']['dateOfEstablishment'];
          garageLogoImage.value = object['data']['logo'];
          if (object['data']['contactPersonPhone'] != '') {
            phoneNumber.value =
                ("+${object['data']['contactPersonCountryCode']} ${object['data']['contactPersonPhone']}");
          }
        }
        break;
    }
  }

  void clearData() {
    garageController.value = '';
    garageEmailController.value = '';
    garagePhoneNumber.value = '';
    dateOfEstablishment.value = '';
    operationalHours.value = '';
    garageAddress.value = '';
    zipCode.value = '';
    mapLocation.value = '';
    name.value = '';
    email.value = '';
    phoneNumber.value = '';
  }
}
