import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_network/image_network.dart';

import '../../../../Utils/dimensions.dart';
import '../../../../common/custom_dialog.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/app_images.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../l10n/app_string_key.dart';
import '../../../basic_details/basic_details_controller.dart';
import '../../../basic_details/basic_details_screen.dart';
import 'basic_details_dashboard_controller.dart';

class BasicDetailsDashboardMobile extends StatelessWidget {
  final BasicDetailsDashboardController controller;

  const BasicDetailsDashboardMobile({required this.controller, super.key});

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    double screenWidth = mediaQuery.size.width;
    return SingleChildScrollView(
      child: IntrinsicHeight(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 16),
          decoration: BoxDecoration(
            border: Border.all(width: 1, color: AppColors.borderColor),
            borderRadius: BorderRadius.circular(4),
            color: Colors.white,
          ),
          child: Obx(
            () => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Flexible(
                  fit: FlexFit.tight,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            fit: FlexFit.tight,
                            child: Column(
                              children: [
                                controller.garageLogoImage.value != ''
                                    ? ImageNetwork(
                                        image: controller.garageLogoImage.value,
                                        height: 80,
                                        width: 80,
                                        fitWeb: BoxFitWeb.cover,
                                        key: ValueKey(controller.garageLogoImage.value),
                                      )
                                    : Image.asset(
                                        AppImages.sampleProfileImage,
                                        width: 80,
                                        height: 80,
                                        fit: BoxFit.cover,
                                      ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  controller.garageController.value,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 4,
                                  textAlign: TextAlign.center,
                                  style: BaseStyle.textStyleRobotoMediumBold(
                                    16,
                                    AppColors.primaryTextColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return CustomDialog(
                                    padding: const EdgeInsets.all(16),
                                    content: SingleChildScrollView(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          const BasicDetailsScreen(
                                            enableSync: false,
                                          ),
                                          const SizedBox(
                                            height: 18,
                                          ),
                                          Wrap(
                                            alignment: WrapAlignment.center,
                                            spacing: 18,
                                            runSpacing: 12,
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    width: 1,
                                                    color: AppColors.primaryTextColor,
                                                  ),
                                                ),
                                                child: TextButton(
                                                  onPressed: () {
                                                    Navigator.pop(context);
                                                  },
                                                  style: TextButton.styleFrom(
                                                    padding: const EdgeInsets.symmetric(
                                                      horizontal: 28,
                                                      vertical: 8,
                                                    ),
                                                    textStyle: BaseStyle.textStyleRobotoMedium(
                                                      14,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                    backgroundColor: AppColors.colorWhite,
                                                    foregroundColor: AppColors.primaryTextColor,
                                                  ),
                                                  child: Text(AppStringKey.cancel.tr),
                                                ),
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    width: 1,
                                                    color: AppColors.primaryTextColor,
                                                  ),
                                                ),
                                                child: TextButton(
                                                  onPressed: () {
                                                    isUpdatingMyAccounts = true;
                                                    Get.find<BasicDetailsController>().submitFieldData();
                                                    Navigator.pop(context);
                                                  },
                                                  style: TextButton.styleFrom(
                                                    padding: const EdgeInsets.symmetric(
                                                      horizontal: 28,
                                                      vertical: 8,
                                                    ),
                                                    textStyle: BaseStyle.textStyleRobotoMedium(
                                                      14,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                    backgroundColor: AppColors.primaryTextColor,
                                                    foregroundColor: AppColors.colorWhite,
                                                    shape: const BeveledRectangleBorder(),
                                                  ),
                                                  child: Text(AppStringKey.save.tr),
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                },
                              );
                            },
                            splashRadius: 1,
                            padding: EdgeInsets.zero,
                            icon: const Icon(
                              Icons.edit_outlined,
                              size: 24,
                              color: AppColors.secondaryTextColor,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 26,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Flexible(
                            fit: FlexFit.tight,
                            child: Row(
                              children: [
                                const Icon(
                                  Icons.email_outlined,
                                  color: AppColors.secondaryIconColor,
                                  size: 12,
                                ),
                                const SizedBox(
                                  width: 6,
                                ),
                                Text(
                                  AppStringKey.email.tr,
                                  style: BaseStyle.textStyleRobotoMediumBold(
                                    12,
                                    AppColors.secondaryIconColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                            fit: FlexFit.tight,
                            child: Text(
                              controller.garageEmailController.value,
                              style: BaseStyle.textStyleRobotoMediumBold(
                                14,
                                AppColors.primaryTextColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 28,
                      ),
                      Row(
                        children: [
                          Flexible(
                            fit: FlexFit.tight,
                            child: Row(
                              children: [
                                const Icon(
                                  Icons.phone,
                                  color: AppColors.secondaryIconColor,
                                  size: 12,
                                ),
                                const SizedBox(
                                  width: 6,
                                ),
                                Text(
                                  AppStringKey.phone_no.tr,
                                  style: BaseStyle.textStyleRobotoMediumBold(
                                    12,
                                    AppColors.secondaryIconColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                            fit: FlexFit.tight,
                            child: Text(
                              controller.garagePhoneNumber.value,
                              style: BaseStyle.textStyleRobotoMediumBold(
                                14,
                                AppColors.primaryTextColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 28,
                      ),
                      Row(
                        children: [
                          Flexible(
                            fit: FlexFit.tight,
                            child: Row(
                              children: [
                                const Icon(
                                  Icons.calendar_month,
                                  color: AppColors.secondaryIconColor,
                                  size: 12,
                                ),
                                SizedBox(
                                  width: screenWidth > Dimensions.lg ? 6 : 2,
                                ),
                                Flexible(
                                  fit: FlexFit.tight,
                                  child: Text(
                                    AppStringKey.date_of_establishment.tr,
                                    style: BaseStyle.textStyleRobotoMediumBold(
                                      12,
                                      AppColors.secondaryIconColor,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                            fit: FlexFit.tight,
                            child: Text(
                              controller.dateOfEstablishment.value,
                              style: BaseStyle.textStyleRobotoMediumBold(
                                14,
                                AppColors.primaryTextColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 28,
                      ),
                      Row(
                        children: [
                          Flexible(
                            fit: FlexFit.tight,
                            child: Row(
                              children: [
                                const Icon(
                                  Icons.lock_clock,
                                  color: AppColors.secondaryIconColor,
                                  size: 12,
                                ),
                                const SizedBox(
                                  width: 6,
                                ),
                                Container(
                                  constraints: BoxConstraints(maxWidth: screenWidth > Dimensions.lg ? 170 : 100),
                                  child: Text(
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    AppStringKey.operation_hours.tr,
                                    style: BaseStyle.textStyleRobotoMediumBold(
                                      12,
                                      AppColors.secondaryIconColor,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                            fit: FlexFit.tight,
                            child: Text(
                              controller.operationalHours.value,
                              style: BaseStyle.textStyleRobotoMediumBold(
                                14,
                                AppColors.primaryTextColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 28,
                      ),
                      Row(
                        children: [
                          Flexible(
                            fit: FlexFit.tight,
                            child: Row(
                              children: [
                                const Icon(
                                  Icons.home,
                                  color: AppColors.secondaryIconColor,
                                  size: 12,
                                ),
                                const SizedBox(
                                  width: 6,
                                ),
                                Text(
                                  AppStringKey.address_line.tr,
                                  style: BaseStyle.textStyleRobotoMediumBold(
                                    12,
                                    AppColors.secondaryIconColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                            fit: FlexFit.tight,
                            child: Text(
                              controller.garageAddress.value,
                              style: BaseStyle.textStyleRobotoMediumBold(
                                14,
                                AppColors.primaryTextColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 28,
                      ),
                      Row(
                        children: [
                          Flexible(
                            fit: FlexFit.tight,
                            child: Row(
                              children: [
                                SvgPicture.asset(
                                  AppImages.emailIcon,
                                  height: 12,
                                  width: 12,
                                  color: AppColors.secondaryIconColor,
                                ),
                                const SizedBox(
                                  width: 6,
                                ),
                                Text(
                                  AppStringKey.zip_code.tr,
                                  style: BaseStyle.textStyleRobotoMediumBold(
                                    12,
                                    AppColors.secondaryIconColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                            fit: FlexFit.tight,
                            child: Text(
                              controller.zipCode.value,
                              style: BaseStyle.textStyleRobotoMediumBold(
                                14,
                                AppColors.primaryTextColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 28,
                      ),
                      Row(
                        children: [
                          Flexible(
                            fit: FlexFit.tight,
                            child: Row(
                              children: [
                                const Icon(
                                  Icons.location_city,
                                  color: AppColors.secondaryIconColor,
                                  size: 12,
                                ),
                                const SizedBox(
                                  width: 6,
                                ),
                                Text(
                                  AppStringKey.map_location.tr,
                                  style: BaseStyle.textStyleRobotoMediumBold(
                                    12,
                                    AppColors.secondaryIconColor,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                            fit: FlexFit.tight,
                            child: Text(
                              controller.mapLocation.value,
                              style: BaseStyle.textStyleRobotoMediumBold(
                                14,
                                AppColors.primaryTextColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 28,
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 24),
                  width: 1,
                  decoration: const BoxDecoration(border: BorderDirectional(end: BorderSide(width: 1, color: AppColors.borderColor))),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppStringKey.contact_person_details.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              16,
                              AppColors.primaryTextColor,
                            ),
                          ),
                          Text(
                            overflow: TextOverflow.ellipsis,
                            maxLines: 4,
                            AppStringKey.contact_person_description.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              14,
                              AppColors.secondaryTextColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 24,
                ),
                Row(
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            AppImages.emailIcon,
                            height: 12,
                            width: 12,
                            color: AppColors.secondaryIconColor,
                          ),
                          const SizedBox(
                            width: 6,
                          ),
                          Text(
                            AppStringKey.name.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              12,
                              AppColors.secondaryIconColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.tight,
                      child: Text(
                        controller.name.value,
                        style: BaseStyle.textStyleRobotoMediumBold(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      ),
                    )
                  ],
                ),
                const SizedBox(
                  height: 28,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      child: Row(
                        children: [
                          const Icon(
                            Icons.email_outlined,
                            color: AppColors.secondaryIconColor,
                            size: 12,
                          ),
                          const SizedBox(
                            width: 6,
                          ),
                          Text(
                            AppStringKey.email.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              12,
                              AppColors.secondaryIconColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.tight,
                      child: Text(
                        controller.email.value,
                        style: BaseStyle.textStyleRobotoMediumBold(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 28,
                ),
                Row(
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      child: Row(
                        children: [
                          const Icon(
                            Icons.phone,
                            color: AppColors.secondaryIconColor,
                            size: 12,
                          ),
                          const SizedBox(
                            width: 6,
                          ),
                          Text(
                            AppStringKey.phone_no.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              12,
                              AppColors.secondaryIconColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.tight,
                      child: Text(
                        controller.phoneNumber.value,
                        style: BaseStyle.textStyleRobotoMediumBold(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
