import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/extensions/common_extension.dart';
import 'basic_details_dashboard_controller.dart';
import 'basic_details_dashboard_mobile_view.dart';
import 'basic_details_dashboard_web_view.dart';

class BasicDetailsDashboard extends StatefulWidget {
  const BasicDetailsDashboard({super.key});

  @override
  State<BasicDetailsDashboard> createState() => _BasicDetailsDashboardState();
}

class _BasicDetailsDashboardState extends State<BasicDetailsDashboard> {
  final controller = Get.find<BasicDetailsDashboardController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.getBranches();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(
            color: Colors.white,
          ),
          child: context.isDesktop
              ? BasicDetailsDashboardWeb(controller: controller)
              : BasicDetailsDashboardMobile(controller: controller),
        )
      ],
    );
  }
}
