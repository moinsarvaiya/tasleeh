import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:al_baida_garage_fe/l10n/app_string_key.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/constant/app_color.dart';
import '../../../core/constant/base_style.dart';
import '../../../custom_classes/custom_dropdown_field.dart';
import 'my_account_dashboard_controller.dart';

class MyAccountDashBoardMobile extends StatelessWidget {
  final MyAccountDashboardController controller;

  const MyAccountDashBoardMobile({required this.controller, super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: const BoxDecoration(
          color: AppColors.primaryBackgroundColor,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  AppStringKey.my_account.tr,
                  style: BaseStyle.textStyleRobotoSemiBold(
                    24,
                    AppColors.primaryTextColor,
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Text(
                  "List of payments cleared from company side.",
                  style: BaseStyle.textStyleRobotoMedium(
                    14,
                    AppColors.primaryTextColor,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 12,
            ),
            CustomDropDownField(
              width: context.width * 0.5,
              labelVisibility: false,
              labelText: AppStringKey.basic.tr,
              validationText: AppStringKey.basic_details.tr,
              hintTitle: AppStringKey.basic_details.tr,
              onChanged: (value) {
                var index = controller.menuList.indexOf(value ?? '');
                controller.selectedIndex.value = index;
              },
              items: controller.menuList,
            ),
            const SizedBox(
              height: 16,
            ),
            Obx(
              () => SizedBox(
                height: context.screenHeight * 0.62,
                child: controller.stepper[controller.selectedIndex.value].childPage,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
