import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../staff/add_staff_controller.dart';
import '../../../staff/add_staff_desktop.dart';
import '../../../staff/add_staff_mobile.dart';

class StaffDashboard extends StatefulWidget {
  const StaffDashboard({super.key});

  @override
  State<StaffDashboard> createState() => _StaffDashboardState();
}

class _StaffDashboardState extends State<StaffDashboard> {
  final controller = Get.find<AddStaffController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.getEmployeeRecord();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: Colors.white,
          padding: const EdgeInsets.all(16),
          child: context.isDesktop
              ? AddStaffDesktopView(
                  controller: controller,
                  isReadOnly: false,
                )
              : AddStaffMobileView(
                  controller: controller,
                  isReadOnly: false,
                ),
        )
      ],
    );
  }
}
