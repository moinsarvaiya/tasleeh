import 'package:flutter/material.dart';

import '../../../core/constant/app_color.dart';
import '../../../core/constant/base_style.dart';
import '../../custom_stepper/models/my_account_model.dart';

class MenuItemWidget extends StatelessWidget {
  final List<MyAccountDashboardModel> stepperList;
  final int index;
  final Function(int selectedValue) onTap;

  const MenuItemWidget({
    super.key,
    required this.stepperList,
    required this.index,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Material(
          color: AppColors.primaryBackgroundColor,
          child: Row(
            children: [
              TextButton(
                style: TextButton.styleFrom(
                  minimumSize: Size.zero,
                  padding: EdgeInsets.zero,
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  foregroundColor: AppColors.primaryTextColor,
                ),
                onPressed: () {
                  onTap(index);
                },
                child: Text(
                  stepperList[index].title ?? '',
                  style: stepperList[index].isSelected!
                      ? BaseStyle.textStyleRobotoSemiBold(
                          18,
                          AppColors.colorBlack,
                        )
                      : BaseStyle.textStyleRobotoMedium(
                          18,
                          AppColors.colorBlack,
                        ),
                ),
              ),
              const SizedBox(
                width: 32,
              )
            ],
          ),
        ),
      ],
    );
  }
}
