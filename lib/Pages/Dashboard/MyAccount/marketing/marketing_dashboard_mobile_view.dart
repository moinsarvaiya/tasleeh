import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:image_network/image_network.dart';

import '../../../../Utils/utilities.dart';
import '../../../../common/add_more_card.dart';
import '../../../../common/custom_dialog.dart';
import '../../../../common/input_text_field.dart';
import '../../../../common/space_horizontal.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/app_images.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../l10n/app_string_key.dart';
import '../../../common/image_upload/file_upload.dart';
import 'marketing_dashboard_controller.dart';

class MarketingDashboardMobileView extends StatelessWidget {
  final MarketingDashboardController controller;

  const MarketingDashboardMobileView({required this.controller, super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: const BoxDecoration(color: AppColors.primaryBackgroundColor),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            //Garage
            Text(
              AppStringKey.garage_images.tr,
              style: BaseStyle.textStyleRobotoSemiBold(14, AppColors.primaryTextColor),
            ),
            const SizedBox(
              height: 8,
            ),
            Wrap(
              spacing: 16,
              runSpacing: 16,
              alignment: WrapAlignment.center,
              runAlignment: WrapAlignment.center,
              children: [
                Obx(
                  () => AddCard(
                    content: AppStringKey.add_images.tr,
                    height: 192,
                    width: 342,
                    onTap: () {
                      controller.uploadOnClick();
                    },
                    borderColor: controller.isFileError.value ? AppColors.errorTextColor : AppColors.borderColor,
                  ),
                ),
                Obx(
                  () => GridView.builder(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: context.width > 500 ? 2 : 1,
                      childAspectRatio: 1.0,
                      mainAxisSpacing: 4,
                      crossAxisSpacing: 16,
                      mainAxisExtent: 200.0,
                    ),
                    itemCount: controller.garageImages.length,
                    shrinkWrap: true,
                    itemBuilder: (context, index) {
                      return Container(
                        height: 192,
                        padding: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4.0),
                          border: Border.all(
                            width: 1,
                            color: AppColors.borderColor,
                          ),
                          color: Colors.white,
                        ),
                        child: Stack(
                          children: [
                            ImageNetwork(
                              image: controller.garageImages[index],
                              height: 160,
                              width: 160,
                              fitWeb: BoxFitWeb.cover,
                              key: ValueKey(controller.garageImages[index]),
                            ),
                            Align(
                              alignment: Alignment.topRight,
                              child: Container(
                                height: 14,
                                width: 14,
                                decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: AppColors.dividerColor,
                                ),
                                child: PopupMenuButton<String>(
                                  padding: EdgeInsets.zero,
                                  splashRadius: 1,
                                  elevation: 5,
                                  icon: const Icon(
                                    Icons.more_vert,
                                    size: 10,
                                    color: Colors.black, // Set your desired icon color
                                  ),
                                  surfaceTintColor: AppColors.colorWhite,
                                  onSelected: (value) {
                                    if (value == 'delete') {
                                      controller.deleteDroppedImage(
                                        controller.garageImages[index],
                                      );
                                    } else if (value == 'makeCoverImage') {
                                      controller.setCoverImage(
                                        controller.garageImages[index],
                                      );
                                    }
                                  },
                                  itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
                                    PopupMenuItem<String>(
                                      value: 'makeCoverImage',
                                      height: 32,
                                      padding: const EdgeInsets.symmetric(
                                        vertical: 5,
                                        horizontal: 12,
                                      ),
                                      child: Row(
                                        children: [
                                          SvgPicture.asset(
                                            AppImages.leftPaneIcon,
                                            height: 16,
                                          ),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          Text(
                                            AppStringKey.make_cover_photo.tr,
                                            style: BaseStyle.textStyleRobotoRegular(
                                              14,
                                              AppColors.primaryTextColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    PopupMenuItem<String>(
                                      value: 'delete',
                                      height: 32,
                                      padding: const EdgeInsets.symmetric(
                                        vertical: 5,
                                        horizontal: 12,
                                      ),
                                      child: Row(
                                        children: [
                                          const Icon(
                                            Icons.delete_outline_rounded,
                                            size: 16,
                                          ),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          Text(
                                            AppStringKey.delete.tr,
                                            style: BaseStyle.textStyleRobotoRegular(
                                              14,
                                              AppColors.primaryTextColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            controller.coverImageUrl.value == controller.garageImages[index]
                                ? Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 12,
                                        vertical: 1,
                                      ),
                                      decoration: BoxDecoration(
                                        color: AppColors.lightBlueBorderColor,
                                        borderRadius: BorderRadius.circular(1),
                                        border: Border.all(
                                          color: AppColors.lightBlueBackgroundColor,
                                        ),
                                      ),
                                      child: Text(
                                        AppStringKey.cover_image.tr,
                                        style: BaseStyle.textStyleRobotoRegular(
                                          10,
                                          AppColors.highlightedBlueText,
                                        ),
                                      ),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              AppStringKey.customer_testimonials.tr,
              style: BaseStyle.textStyleRobotoSemiBold(14, AppColors.primaryTextColor),
            ),
            const SizedBox(
              height: 8,
            ),
            Container(
              constraints: const BoxConstraints(minHeight: 220),
              child: Wrap(
                spacing: 16,
                runSpacing: 16,
                alignment: WrapAlignment.center,
                runAlignment: WrapAlignment.center,
                children: [
                  AddCard(
                    content: AppStringKey.add_another_testimonial.tr,
                    width: 348,
                    height: 220,
                    onTap: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return Obx(
                            () => CustomDialog(
                              padding: const EdgeInsets.all(16),
                              maxWidth: 500,
                              content: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  FileUpload(
                                    title: AppStringKey.client_photo_or_video.tr,
                                    acceptedFileDescription: AppStringKey.file_upload_format.tr,
                                    sizeDescription: "${AppStringKey.upload_max_size_mb.tr} 10${AppStringKey.mb}",
                                    buttonContent: AppStringKey.click_to_upload.tr,
                                    imageUrlsList: controller.clientFeedbackPhotos.value != '' ? [controller.clientFeedbackPhotos.value] : [],
                                    onImagePick: (image) {
                                      controller.imageUpload(context, image);
                                    },
                                    onRemoveImage: (index) {
                                      controller.clientFeedbackPhotos.value = '';
                                    },
                                  ),
                                  InputTextField(
                                    hintText: AppStringKey.enter_client_name.tr,
                                    label: AppStringKey.client_name.tr,
                                    isRequireField: false,
                                    maxLines: 1,
                                    textInputType: TextInputType.text,
                                    textEditingController: controller.clientNameController,
                                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                                  ),
                                  const SizedBox(
                                    height: 12,
                                  ),
                                  InputTextField(
                                    hintText: AppStringKey.enter_feedback.tr,
                                    label: AppStringKey.feedback.tr,
                                    isRequireField: false,
                                    maxLength: 500,
                                    maxLines: 5,
                                    textInputType: TextInputType.multiline,
                                    textEditingController: controller.feedbackController,
                                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                                  ),
                                  const SizedBox(
                                    height: 16,
                                  ),
                                  Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            width: 1,
                                            color: AppColors.primaryTextColor,
                                          ),
                                        ),
                                        width: double.maxFinite,
                                        child: TextButton(
                                          onPressed: () {
                                            Navigator.pop(context);
                                          },
                                          style: TextButton.styleFrom(
                                            padding: const EdgeInsets.symmetric(
                                              horizontal: 28,
                                              vertical: 8,
                                            ),
                                            textStyle: BaseStyle.textStyleRobotoMedium(
                                              14,
                                              AppColors.primaryTextColor,
                                            ),
                                            backgroundColor: AppColors.colorWhite,
                                            foregroundColor: AppColors.primaryTextColor,
                                          ),
                                          child: Text(AppStringKey.cancel.tr),
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 16,
                                      ),
                                      Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            width: 1,
                                            color: AppColors.primaryTextColor,
                                          ),
                                        ),
                                        width: double.maxFinite,
                                        child: TextButton(
                                          onPressed: () {
                                            controller.submitTestimonial();
                                            Navigator.pop(context);
                                          },
                                          style: TextButton.styleFrom(
                                            padding: const EdgeInsets.symmetric(
                                              horizontal: 28,
                                              vertical: 8,
                                            ),
                                            textStyle: BaseStyle.textStyleRobotoMedium(
                                              14,
                                              AppColors.primaryTextColor,
                                            ),
                                            backgroundColor: AppColors.primaryTextColor,
                                            foregroundColor: AppColors.colorWhite,
                                            shape: const BeveledRectangleBorder(),
                                          ),
                                          child: Text(AppStringKey.save.tr),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    },
                  ),
                  Obx(
                    () => GridView.builder(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: context.width > 500 ? 2 : 1,
                        childAspectRatio: 1.0,
                        mainAxisSpacing: 4,
                        crossAxisSpacing: 16,
                        mainAxisExtent: 200.0,
                      ),
                      itemCount: controller.testimonialData.length,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return Container(
                          constraints: const BoxConstraints(maxWidth: 350),
                          padding: const EdgeInsets.symmetric(
                            vertical: 25,
                            horizontal: 26,
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(
                                width: 1,
                                color: AppColors.borderColor,
                              ),
                              color: Colors.white),
                          child: Stack(
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    controller.testimonialData[index].comment.toString(),
                                    style: BaseStyle.textStyleRobotoMediumBold(12, AppColors.cardTextColor),
                                    textAlign: TextAlign.center,
                                  ),
                                  const SizedBox(
                                    height: 12,
                                  ),
                                  ImageNetwork(
                                    image: controller.testimonialData[index].clientImageUrl.toString(),
                                    height: 40,
                                    width: 40,
                                    borderRadius: BorderRadius.circular(48),
                                    fitWeb: BoxFitWeb.fill,
                                    key: ValueKey(controller.testimonialData[index].clientImageUrl.toString()),
                                  ),
                                  const SizedBox(
                                    height: 4,
                                  ),
                                  Text(
                                    controller.testimonialData[index].name.toString(),
                                    style: BaseStyle.textStyleRobotoSemiBold(14, Colors.black),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                              Align(
                                alignment: Alignment.bottomRight,
                                child: PopupMenuButton<String>(
                                  splashRadius: 5,
                                  offset: const Offset(20, 40),
                                  padding: EdgeInsets.zero,
                                  surfaceTintColor: AppColors.colorWhite,
                                  onSelected: (value) {
                                    if (value == AppStringKey.edit.tr) {
                                      controller.clientNameController.text = controller.testimonialData[index].name!;
                                      controller.feedbackController.text = controller.testimonialData[index].comment!;
                                      controller.clientFeedbackPhotos.value = controller.testimonialData[index].clientImageUrl!;
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return Obx(
                                            () => CustomDialog(
                                              padding: const EdgeInsets.all(16),
                                              maxWidth: 500,
                                              content: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  FileUpload(
                                                    title: AppStringKey.client_photo_or_video.tr,
                                                    acceptedFileDescription: AppStringKey.file_upload_format.tr,
                                                    sizeDescription: "${AppStringKey.upload_max_size_mb.tr} 10${AppStringKey.mb}",
                                                    buttonContent: AppStringKey.click_to_upload.tr,
                                                    imageUrlsList:
                                                        controller.clientFeedbackPhotos.value != '' ? [controller.clientFeedbackPhotos.value] : [],
                                                    onImagePick: (image) {
                                                      controller.imageUpload(context, image);
                                                    },
                                                    onRemoveImage: (index) {
                                                      controller.clientFeedbackPhotos.value = '';
                                                    },
                                                  ),
                                                  InputTextField(
                                                    hintText: AppStringKey.enter_client_name.tr,
                                                    label: AppStringKey.client_name.tr,
                                                    isRequireField: false,
                                                    maxLines: 1,
                                                    textInputType: TextInputType.text,
                                                    textEditingController: controller.clientNameController,
                                                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                                                  ),
                                                  const SizedBox(
                                                    height: 12,
                                                  ),
                                                  InputTextField(
                                                    hintText: AppStringKey.enter_feedback.tr,
                                                    label: AppStringKey.feedback.tr,
                                                    isRequireField: false,
                                                    maxLength: 500,
                                                    maxLines: 5,
                                                    textInputType: TextInputType.multiline,
                                                    textEditingController: controller.feedbackController,
                                                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                                                  ),
                                                  const SizedBox(
                                                    height: 16,
                                                  ),
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    children: [
                                                      Container(
                                                        decoration: BoxDecoration(
                                                          border: Border.all(
                                                            width: 1,
                                                            color: AppColors.primaryTextColor,
                                                          ),
                                                        ),
                                                        child: TextButton(
                                                          onPressed: () {
                                                            Navigator.pop(context);
                                                          },
                                                          style: TextButton.styleFrom(
                                                            padding: const EdgeInsets.symmetric(
                                                              horizontal: 28,
                                                              vertical: 8,
                                                            ),
                                                            textStyle: BaseStyle.textStyleRobotoMedium(
                                                              14,
                                                              AppColors.primaryTextColor,
                                                            ),
                                                            backgroundColor: AppColors.colorWhite,
                                                            foregroundColor: AppColors.primaryTextColor,
                                                          ),
                                                          child: Text(AppStringKey.cancel.tr),
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        width: 18,
                                                      ),
                                                      Container(
                                                        decoration: BoxDecoration(
                                                          border: Border.all(
                                                            width: 1,
                                                            color: AppColors.primaryTextColor,
                                                          ),
                                                        ),
                                                        child: TextButton(
                                                          onPressed: () {
                                                            controller.patchTestimonial(
                                                              index,
                                                            );
                                                            Navigator.pop(context);
                                                          },
                                                          style: TextButton.styleFrom(
                                                            padding: const EdgeInsets.symmetric(
                                                              horizontal: 28,
                                                              vertical: 8,
                                                            ),
                                                            textStyle: BaseStyle.textStyleRobotoMedium(
                                                              14,
                                                              AppColors.primaryTextColor,
                                                            ),
                                                            backgroundColor: AppColors.primaryTextColor,
                                                            foregroundColor: AppColors.colorWhite,
                                                            shape: const BeveledRectangleBorder(),
                                                          ),
                                                          child: Text(
                                                            AppStringKey.save.tr,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    } else if (value == AppStringKey.delete.tr) {
                                      controller.deleteTestimonialData(index);
                                    }
                                  },
                                  child: const Icon(
                                    Icons.more_vert,
                                    size: 20,
                                    weight: 400,
                                  ),
                                  itemBuilder: (BuildContext context) => [
                                    PopupMenuItem<String>(
                                      value: AppStringKey.edit.tr,
                                      height: 32,
                                      padding: const EdgeInsets.symmetric(
                                        vertical: 5,
                                        horizontal: 12,
                                      ),
                                      child: Row(
                                        children: [
                                          const Icon(
                                            Icons.edit_outlined,
                                            size: 16,
                                          ),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          Text(
                                            AppStringKey.edit_details.tr,
                                            style: BaseStyle.textStyleRobotoRegular(
                                              14,
                                              AppColors.primaryTextColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    PopupMenuItem<String>(
                                      value: AppStringKey.delete.tr,
                                      height: 32,
                                      padding: const EdgeInsets.symmetric(
                                        vertical: 5,
                                        horizontal: 12,
                                      ),
                                      child: Row(
                                        children: [
                                          const Icon(
                                            Icons.delete_outline,
                                            size: 16,
                                          ),
                                          const SizedBox(
                                            width: 8,
                                          ),
                                          Text(
                                            AppStringKey.delete.tr,
                                            style: BaseStyle.textStyleRobotoRegular(
                                              14,
                                              AppColors.primaryTextColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 32,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppStringKey.social_links.tr,
                  style: BaseStyle.textStyleRobotoSemiBold(
                    14,
                    AppColors.primaryTextColor,
                  ),
                ),
                IconButton(
                  onPressed: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return CustomDialog(
                          padding: const EdgeInsets.all(16),
                          content: Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  InputTextField(
                                    hintText: AppStringKey.enter_instagram_handle.tr,
                                    label: AppStringKey.instagram_link.tr,
                                    isRequireField: false,
                                    textEditingController: controller.instagramHandleInputController,
                                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                                  ),
                                  const SizedBox(
                                    height: 24,
                                  ),
                                  InputTextField(
                                    hintText: AppStringKey.enter_facebook_handle.tr,
                                    label: AppStringKey.facebook_link.tr,
                                    isRequireField: false,
                                    textEditingController: controller.facebookHandleInputController,
                                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                                  ),
                                  const SizedBox(
                                    height: 24,
                                  ),
                                  InputTextField(
                                    hintText: AppStringKey.enter_website_handle.tr,
                                    label: AppStringKey.website_link.tr,
                                    isRequireField: false,
                                    textEditingController: controller.websiteInputController,
                                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                                  ),
                                  const SpaceHorizontal(0.04),
                                  InputTextField(
                                    hintText: AppStringKey.enter_twitter_handle.tr,
                                    label: AppStringKey.twitter_link.tr,
                                    isRequireField: false,
                                    textEditingController: controller.twitterHandleInputController,
                                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                                  ),
                                  const SizedBox(
                                    height: 24,
                                  ),
                                  InputTextField(
                                    hintText: AppStringKey.enter_tik_tok_handle.tr,
                                    label: AppStringKey.tik_tok_link.tr,
                                    isRequireField: false,
                                    textEditingController: controller.tikTokHandleInputController,
                                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        width: 1,
                                        color: AppColors.primaryTextColor,
                                      ),
                                    ),
                                    child: TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      style: TextButton.styleFrom(
                                        padding: const EdgeInsets.symmetric(
                                          horizontal: 28,
                                          vertical: 8,
                                        ),
                                        textStyle: BaseStyle.textStyleRobotoMedium(
                                          14,
                                          AppColors.primaryTextColor,
                                        ),
                                        backgroundColor: AppColors.colorWhite,
                                        foregroundColor: AppColors.primaryTextColor,
                                      ),
                                      child: Text(AppStringKey.cancel.tr),
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 18,
                                  ),
                                  Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        width: 1,
                                        color: AppColors.primaryTextColor,
                                      ),
                                    ),
                                    child: TextButton(
                                      onPressed: () {
                                        controller.submitSocialMediaLinks();
                                        Navigator.pop(context);
                                      },
                                      style: TextButton.styleFrom(
                                        padding: const EdgeInsets.symmetric(
                                          horizontal: 28,
                                          vertical: 8,
                                        ),
                                        textStyle: BaseStyle.textStyleRobotoMedium(
                                          14,
                                          AppColors.primaryTextColor,
                                        ),
                                        backgroundColor: AppColors.primaryTextColor,
                                        foregroundColor: AppColors.colorWhite,
                                        shape: const BeveledRectangleBorder(),
                                      ),
                                      child: Text(AppStringKey.save.tr),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  },
                  padding: EdgeInsets.zero,
                  splashRadius: 1,
                  iconSize: 16,
                  color: AppColors.secondaryTextColor,
                  icon: const Icon(
                    Icons.edit_outlined,
                  ),
                )
              ],
            ),
            const SizedBox(
              height: 8,
            ),
            Obx(
              () => GridView.builder(
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 65,
                  childAspectRatio: 1.0,
                  mainAxisSpacing: 8,
                ),
                itemCount: controller.socialLink.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Utilities.openNewUrl(
                        controller.socialLink[index].link!,
                      );
                    },
                    child: Container(
                      margin: const EdgeInsets.only(right: 8),
                      padding: const EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(4.0),
                        border: Border.all(
                          width: 1.5,
                          color: AppColors.borderColor,
                        ),
                      ),
                      child: Ink(
                        child: InkWell(
                          onTap: () {},
                          child: SvgPicture.asset(
                            controller.socialLink[index].type == socialMediaTypes[SocialMediaTypes.instagram]
                                ? AppImages.instagramIcon
                                : controller.socialLink[index].type == socialMediaTypes[SocialMediaTypes.facebook]
                                    ? AppImages.facebookLogo
                                    : controller.socialLink[index].type == socialMediaTypes[SocialMediaTypes.twitter]
                                        ? AppImages.twitterIcon
                                        : controller.socialLink[index].type == socialMediaTypes[SocialMediaTypes.tiktok]
                                            ? AppImages.tikTokIcon
                                            : AppImages.websiteIcon,
                            width: 48,
                            height: 48,
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
