import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'marketing_dashboard_controller.dart';
import 'marketing_dashboard_mobile_view.dart';
import 'marketing_dashboard_web_view.dart';

class MarketingDashboard extends StatefulWidget {
  const MarketingDashboard({super.key});

  @override
  State<MarketingDashboard> createState() => _MarketingDashboardState();
}

class _MarketingDashboardState extends State<MarketingDashboard> {
  final controller = Get.find<MarketingDashboardController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.getBranches();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        context.isDesktop
            ? MarketingDashboardWebView(
                controller: controller,
              )
            : MarketingDashboardMobileView(
                controller: controller,
              )
      ],
    );
  }
}
