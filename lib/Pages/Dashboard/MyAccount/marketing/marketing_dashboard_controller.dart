import 'dart:convert';
// ignore: avoid_web_libraries_in_flutter
import 'dart:html' as html;
import 'dart:typed_data';

import 'package:al_baida_garage_fe/Utils/app_constants.dart';
import 'package:al_baida_garage_fe/Utils/utilities.dart';
import 'package:al_baida_garage_fe/api/web_fields_key.dart';
import 'package:al_baida_garage_fe/common/common_function.dart';
import 'package:al_baida_garage_fe/common/custom_loader.dart';
import 'package:al_baida_garage_fe/core/app_preference/app_preferences.dart';
import 'package:al_baida_garage_fe/core/app_preference/storage_keys.dart';
import 'package:al_baida_garage_fe/core/constant/app_color.dart';
import 'package:al_baida_garage_fe/core/constant/base_extension.dart';
import 'package:al_baida_garage_fe/core/constant/ui_constants.dart';
import 'package:al_baida_garage_fe/l10n/app_string_key.dart';
import 'package:al_baida_garage_fe/models/garage_id_model.dart';
import 'package:al_baida_garage_fe/models/garage_image_data_model.dart';
import 'package:al_baida_garage_fe/models/garage_social_links_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import '../../../../api/api_end_point.dart';
import '../../../../api/api_interface.dart';
import '../../../../api/api_presenter.dart';
import '../../../../models/testimonial_data_model.dart';

class MarketingDashboardController extends GetxController
    implements ApiCallBacks {
  RxBool isDragEnable = false.obs;
  RxBool isFileError = false.obs;
  RxString errorText = RxString("");
  RxString coverImageUrl = RxString('');

  RxList<TestimonialDataModel> testimonialData = RxList();
  RxList<SocialLinksModel> socialLink = RxList();
  RxList<dynamic> garageImages = RxList();
  String branchId = '';
  List<String> garageImageId = [];

  TextEditingController instagramHandleInputController =
      TextEditingController();
  TextEditingController facebookHandleInputController = TextEditingController();
  TextEditingController twitterHandleInputController = TextEditingController();
  TextEditingController tikTokHandleInputController = TextEditingController();
  TextEditingController websiteInputController = TextEditingController();

  TextEditingController clientNameController = TextEditingController();
  TextEditingController feedbackController = TextEditingController();
  RxString clientFeedbackPhotos = RxString('');
  List<String> testimonialId = [];

  void getBranches() async {
    branchId = await AppPreferences.sharedPrefRead(StorageKeys.garageId);
    getGarageImages();
    getTestimonial();
    getSocialMediaLinks();
  }

  void getGarageImages() {
    socialMediaLinksFrom = SocialMediaLinksFrom.get;
    ApiPresenter(this).getGarageImage(branchId);
  }

  void getGarageImageId(String url, bool isCoverImage) {
    garageImageFrom = GarageImageFrom.post;
    ApiPresenter(this)
        .garageImage(garageImageModel(url, isCoverImage).toJson());
  }

  void uploadDroppedImage(html.File file) async {
    var headers = {
      'Content-Type': 'multipart/form-data',
      'Authorization':
          'Bearer ${AppPreferences.sharedPrefRead(StorageKeys.accessToken)}',
    };

    var request = http.MultipartRequest(
      'POST',
      Uri.parse('${ApiEndPoints.baseUrl}${ApiEndPoints.imageUpload}'),
    );
    request.headers.addAll(headers);

    var reader = html.FileReader();
    reader.readAsDataUrl(file); // Read the file as data URL

    await reader.onLoad.first; // Wait for the reader to load the file

    var encodedData = reader.result
        .toString()
        .split(',')[1]; // Extract the base64 encoded data

    // Convert the base64 encoded data to bytes
    List<int> bytes = base64.decode(encodedData);

    var stream = http.ByteStream.fromBytes(bytes);
    var length = bytes.length;

    request.files.add(
      http.MultipartFile(
        'file', // Replace with your file key
        stream,
        length,
        filename: file.name,
      ),
    );
    request.fields['folder'] = 'garage'; // Add additional fields if needed
    try {
      var response = await request.send();
      if (response.statusCode == 200 || response.statusCode == 201) {
        List<int> chunks = [];
        await for (var chunk in response.stream) {
          chunks.addAll(chunk);
        }
        final object = jsonDecode(utf8.decode(chunks));
        getGarageImageId(object['data']['url'], false);
        getGarageImages();
      } else {
        AppStringKey.api_fail_error.tr.showErrorSnackBar();
      }
    } catch (e) {
      e.toString().showErrorSnackBar();
    }
  }

  void processFile(html.File file) {
    if (file.size < AppConstants.fileMaxSize) {
      final supportedExtensions = ['.png', '.jpg', '.mp4'];
      final extension = file.name.split('.').last.toLowerCase();

      if (supportedExtensions.contains('.$extension')) {
        uploadDroppedImage(file);
        isFileError = false.obs;
      } else {
        errorText = RxString(AppStringKey.unsupported_file_extension.tr);
        isFileError = true.obs;
        Utilities.showToast(
          errorText.value,
          fontSize: 14,
          webPosition: 'center',
          webBgColor: '#FF4D4F',
          textColor: AppColors.colorWhite,
        );
      }
    } else {
      errorText = RxString(AppStringKey.file_size_exceeds.tr);
      isFileError = true.obs;
      Utilities.showToast(
        errorText.value,
        fontSize: 14,
        webPosition: 'center',
        webBgColor: '#FF4D4F',
        textColor: AppColors.colorWhite,
      );
    }
  }

  void uploadOnClick() {
    html.InputElement uploadInput = html.InputElement(type: 'file');
    uploadInput.multiple = true;
    uploadInput.click();

    uploadInput.onChange.listen((e) {
      final List<html.File> files = uploadInput.files!;
      for (final file in files) {
        processFile(file);
      }
    });
  }

  void setCoverImage(String url) {
    garageImageFrom = GarageImageFrom.patch;
    ApiPresenter(this).updateGarageImage(
      {
        'isCoverImage': true,
      },
      garageImageId[garageImages.indexOf(url)],
    );
    coverImageUrl.value = url;
  }

  void deleteDroppedImage(String url) {
    ApiPresenter(this).deleteGarageImage(
      garageImageId[garageImages.indexOf(url)],
    );
    garageImageId.removeAt(garageImages.indexOf(url));
    garageImages.remove(url);
    garageImages.refresh();
    if (coverImageUrl.value == url) {
      coverImageUrl.value = '';
    }
  }

  void getTestimonial() {
    testimonialFrom = TestimonialFrom.get;
    ApiPresenter(this).getTestimonialData(branchId);
  }

  void submitTestimonial() {
    testimonialFrom = TestimonialFrom.post;
    ApiPresenter(this).garageTestimonialData(testimonialDataModel().toJson());
  }

  void patchTestimonial(int index) {
    testimonialFrom = TestimonialFrom.patch;
    ApiPresenter(this).updateGarageTestimonialData(
      testimonialDataModel().toJson(),
      testimonialId[index],
    );
  }

  void deleteTestimonialData(int index) {
    testimonialFrom = TestimonialFrom.delete;
    ApiPresenter(this).deleteGarageTestimonialData(testimonialId[index]);
    testimonialId.removeAt(index);
  }

  void imageUpload(
      BuildContext context, Map<String, Uint8List> imageList) async {
    try {
      var headers = {
        WebFieldKey.strContentType: WebFieldKey.strMultipartFormData,
        WebFieldKey.strAuthorization:
            'Bearer ${AppPreferences.sharedPrefRead(StorageKeys.accessToken)}',
      };

      for (var entry in imageList.entries) {
        var request = http.MultipartRequest(
          WebFieldKey.strPostMethod,
          Uri.parse('${ApiEndPoints.baseUrl}${ApiEndPoints.imageUpload}'),
        );
        request.headers.addAll(headers);
        var stream = http.ByteStream.fromBytes(entry.value);
        var length = entry.value.lengthInBytes;
        request.files.add(
          http.MultipartFile(WebFieldKey.strFile, stream, length,
              filename: '${entry.key}.jpg'),
        );
        request.fields[WebFieldKey.strFolder] = 'garage';
        http.StreamedResponse response = await request.send();
        final responseBody = await response.stream.bytesToString();
        final parsedJson = jsonDecode(responseBody);
        if (parsedJson['code'] == 200 || parsedJson['code'] == 201) {
          clientFeedbackPhotos.value = (parsedJson['data']['url'].toString());
        } else {
          // ignore: use_build_context_synchronously
          CommonFunction.showCustomSnackBar(
            context: context,
            message: parsedJson['error'],
            duration: const Duration(seconds: 3),
            actionLabel: 'Close',
          );
        }
      }
      LoadingDialog.closeFullScreenDialog();
    } catch (e) {
      LoadingDialog.closeFullScreenDialog();
      // ignore: use_build_context_synchronously
      CommonFunction.showCustomSnackBar(
        context: context,
        message: e.toString(),
        duration: const Duration(seconds: 3),
        actionLabel: 'Close',
      );
    }
  }

  void getSocialMediaLinks() {
    ApiPresenter(this).getSocialMediaLinks(branchId);
  }

  void submitSocialMediaLinks() {
    socialMediaLinksFrom = SocialMediaLinksFrom.post;
    ApiPresenter(this)
        .submitSocialMediaLinks({'socialLink': socialLinksModel()});
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {}

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {}

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.garageTestimonial:
        if (testimonialFrom == TestimonialFrom.post) {
          testimonialData.clear();
          getTestimonial();
          clearData();
        } else if (testimonialFrom == TestimonialFrom.get) {
          testimonialData.clear();
          if (object['data'].length > 0) {
            for (var item in object['data']) {
              if (item['name'] != "" ||
                  item['clientImageUrl'] != "" ||
                  item['comment'] != "") {
                testimonialData.add(
                  TestimonialDataModel(
                    name: item['name'],
                    clientImageUrl: item['clientImageUrl'],
                    comment: item['comment'],
                  ),
                );
              }
              testimonialId.add(item['id']);
            }
          }
        } else if (testimonialFrom == TestimonialFrom.delete) {
          testimonialData.clear();
          getTestimonial();
        } else if (testimonialFrom == TestimonialFrom.patch) {
          testimonialData.clear();
          getTestimonial();
          clearData();
        }
        break;

      case ApiEndPoints.socialMediaLinks:
        socialLink.clear();
        if (socialMediaLinksFrom == SocialMediaLinksFrom.post) {
          socialMediaLinksFrom = SocialMediaLinksFrom.get;
          getSocialMediaLinks();
        } else {
          if (object['data'].length > 0) {
            for (var item in object['data']) {
              if (item['link'] != "") {
                if (item['type'] ==
                    socialMediaTypes[SocialMediaTypes.facebook]) {
                  facebookHandleInputController.text = item['link'];
                  socialLink.add(
                    SocialLinksModel(
                      type: item['type'],
                      link: facebookHandleInputController.text,
                      garage: Garage(id: branchId),
                    ),
                  );
                } else if (item['type'] ==
                    socialMediaTypes[SocialMediaTypes.twitter]) {
                  twitterHandleInputController.text = item['link'];
                  socialLink.add(
                    SocialLinksModel(
                      type: item['type'],
                      link: twitterHandleInputController.text,
                      garage: Garage(id: branchId),
                    ),
                  );
                } else if (item['type'] ==
                    socialMediaTypes[SocialMediaTypes.instagram]) {
                  instagramHandleInputController.text = item['link'];
                  socialLink.add(
                    SocialLinksModel(
                      type: item['type'],
                      link: instagramHandleInputController.text,
                      garage: Garage(id: branchId),
                    ),
                  );
                } else if (item['type'] ==
                    socialMediaTypes[SocialMediaTypes.tiktok]) {
                  tikTokHandleInputController.text = item['link'];
                  socialLink.add(
                    SocialLinksModel(
                      type: item['type'],
                      link: tikTokHandleInputController.text,
                      garage: Garage(id: branchId),
                    ),
                  );
                } else if (item['type'] ==
                    socialMediaTypes[SocialMediaTypes.website]) {
                  websiteInputController.text = item['link'];
                  socialLink.add(
                    SocialLinksModel(
                      type: item['type'],
                      link: websiteInputController.text,
                      garage: Garage(id: branchId),
                    ),
                  );
                }
              }
            }
          }
        }
        break;

      case ApiEndPoints.getGarageImage:
        garageImages.clear();
        if (object['data'].length > 0) {
          for (var item in object['data']) {
            garageImageId.add(item['id']);
            garageImages.add(item['imageUrl']);
            if (item['isCoverImage']) {
              coverImageUrl.value = item['imageUrl'];
            }
          }
        }
        break;
    }
  }

  GarageImageModel garageImageModel(String url, bool isCoverImage) =>
      GarageImageModel(
        garage: Garage(id: branchId),
        imageUrl: url,
        isCoverImage: isCoverImage,
      );

  List<SocialLinksModel> socialLinksModel() => [
        SocialLinksModel(
          garage: Garage(id: branchId),
          type: socialMediaTypes[SocialMediaTypes.facebook],
          link: facebookHandleInputController.text,
        ),
        SocialLinksModel(
          garage: Garage(id: branchId),
          type: socialMediaTypes[SocialMediaTypes.instagram],
          link: instagramHandleInputController.text,
        ),
        SocialLinksModel(
          garage: Garage(id: branchId),
          type: socialMediaTypes[SocialMediaTypes.website],
          link: websiteInputController.text,
        ),
        SocialLinksModel(
          garage: Garage(id: branchId),
          type: socialMediaTypes[SocialMediaTypes.tiktok],
          link: tikTokHandleInputController.text,
        ),
        SocialLinksModel(
          garage: Garage(id: branchId),
          type: socialMediaTypes[SocialMediaTypes.twitter],
          link: twitterHandleInputController.text,
        )
      ];

  TestimonialDataModel testimonialDataModel() => TestimonialDataModel(
        garage: Garage(id: branchId),
        name: clientNameController.text,
        comment: feedbackController.text,
        clientImageUrl: clientFeedbackPhotos.value,
      );

  void clearData() {
    testimonialId = [];
    clientNameController.clear();
    feedbackController.clear();
    clientFeedbackPhotos = RxString('');
    testimonialData.clear();
  }
}
