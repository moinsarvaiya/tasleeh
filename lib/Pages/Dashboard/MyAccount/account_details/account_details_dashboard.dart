import 'package:al_baida_garage_fe/Pages/account_detail/account_detail_screen.dart';
import 'package:al_baida_garage_fe/Pages/account_detail/account_details_controller.dart';
import 'package:al_baida_garage_fe/common/custom_dialog.dart';
import 'package:al_baida_garage_fe/core/constant/ui_constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/extensions/common_extension.dart';
import '../../../../l10n/app_string_key.dart';
import 'accounts_details_controller.dart';

class AccountDetailsDashboard extends StatefulWidget {
  const AccountDetailsDashboard({
    super.key,
  });

  @override
  State<AccountDetailsDashboard> createState() =>
      _AccountDetailsDashboardState();
}

class _AccountDetailsDashboardState extends State<AccountDetailsDashboard> {
  final controller = Get.find<AccountDetailsDashboardController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.getBranches();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Column(
          children: [
            Container(
              constraints: const BoxConstraints(maxWidth: 1000),
              padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 32),
              decoration: BoxDecoration(
                border: Border.all(width: 1, color: AppColors.borderColor),
                color: Colors.white,
              ),
              child: Obx(
                () => Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppStringKey.account_detail.tr,
                              style: BaseStyle.textStyleRobotoMediumBold(
                                context.isDesktop ? 20 : 16,
                                AppColors.primaryTextColor,
                              ),
                            ),
                          ],
                        ),
                        PopupMenuButton<String>(
                          splashRadius: 5,
                          offset: const Offset(20, 40),
                          padding: EdgeInsets.zero,
                          surfaceTintColor: AppColors.colorWhite,
                          onSelected: (choice) => {
                            if (choice == AppStringKey.edit.tr)
                              {
                                Get.put(AccountDetailsController()),
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return CustomDialog(
                                      padding: const EdgeInsets.all(16),
                                      content: SingleChildScrollView(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            const AccountDetailScreen(
                                              enableSync: false,
                                            ),
                                            const SizedBox(
                                              height: 18,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      width: 1,
                                                      color: AppColors
                                                          .primaryTextColor,
                                                    ),
                                                  ),
                                                  child: TextButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    style: TextButton.styleFrom(
                                                      padding: const EdgeInsets
                                                          .symmetric(
                                                        horizontal: 28,
                                                        vertical: 8,
                                                      ),
                                                      textStyle: BaseStyle
                                                          .textStyleRobotoMedium(
                                                        14,
                                                        AppColors
                                                            .primaryTextColor,
                                                      ),
                                                      backgroundColor:
                                                          AppColors.colorWhite,
                                                      foregroundColor: AppColors
                                                          .primaryTextColor,
                                                    ),
                                                    child: Text(
                                                        AppStringKey.cancel.tr),
                                                  ),
                                                ),
                                                const SizedBox(
                                                  width: 18,
                                                ),
                                                Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      width: 1,
                                                      color: AppColors
                                                          .primaryTextColor,
                                                    ),
                                                  ),
                                                  child: TextButton(
                                                    onPressed: () {
                                                      isUpdatingMyAccounts =
                                                          true;
                                                      Get.find<
                                                              AccountDetailsController>()
                                                          .submitAccountDetailsData();
                                                      Navigator.pop(context);
                                                    },
                                                    style: TextButton.styleFrom(
                                                      padding: const EdgeInsets
                                                          .symmetric(
                                                        horizontal: 28,
                                                        vertical: 8,
                                                      ),
                                                      textStyle: BaseStyle
                                                          .textStyleRobotoMedium(
                                                        14,
                                                        AppColors
                                                            .primaryTextColor,
                                                      ),
                                                      backgroundColor: AppColors
                                                          .primaryTextColor,
                                                      foregroundColor:
                                                          AppColors.colorWhite,
                                                      shape:
                                                          const BeveledRectangleBorder(),
                                                    ),
                                                    child: Text(
                                                        AppStringKey.save.tr),
                                                  ),
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                )
                              },
                          },
                          child: const Icon(
                            Icons.more_vert,
                            size: 20,
                            weight: 400,
                          ),
                          itemBuilder: (BuildContext context) => [
                            PopupMenuItem<String>(
                              value: AppStringKey.edit.tr,
                              height: 32,
                              padding: const EdgeInsets.symmetric(
                                vertical: 5,
                                horizontal: 12,
                              ),
                              child: Row(
                                children: [
                                  const Icon(
                                    Icons.edit_outlined,
                                    size: 16,
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    AppStringKey.edit_bank_details.tr,
                                    style: BaseStyle.textStyleRobotoRegular(
                                      14,
                                      AppColors.primaryTextColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            PopupMenuItem<String>(
                              value: AppStringKey.delete.tr,
                              height: 32,
                              padding: const EdgeInsets.symmetric(
                                vertical: 5,
                                horizontal: 12,
                              ),
                              child: Row(
                                children: [
                                  const Icon(
                                    Icons.delete_outline,
                                    size: 16,
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Text(
                                    AppStringKey.delete.tr,
                                    style: BaseStyle.textStyleRobotoRegular(
                                      14,
                                      AppColors.primaryTextColor,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(
                            AppStringKey.bank_name.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              context.isDesktop ? 16 : 12,
                              AppColors.secondaryIconColor,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          child: Text(
                            controller.bankName.value,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              context.isDesktop ? 16 : 12,
                              AppColors.primaryTextColor,
                            ),
                            textAlign: TextAlign.right,
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 28,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(
                            AppStringKey.account_holder_name.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              context.isDesktop ? 16 : 12,
                              AppColors.secondaryIconColor,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          child: Text(
                            controller.accountHolderName.value,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              context.isDesktop ? 16 : 12,
                              AppColors.primaryTextColor,
                            ),
                            textAlign: TextAlign.right,
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 28,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(
                            AppStringKey.account_no.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              context.isDesktop ? 16 : 12,
                              AppColors.secondaryIconColor,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          child: Text(
                            controller.accountNumber.value,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              context.isDesktop ? 16 : 12,
                              AppColors.primaryTextColor,
                            ),
                            textAlign: TextAlign.right,
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 28,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(
                            AppStringKey.bank_address.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              context.isDesktop ? 16 : 12,
                              AppColors.secondaryIconColor,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          child: Text(
                            controller.bankAddress.value,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              context.isDesktop ? 16 : 12,
                              AppColors.primaryTextColor,
                            ),
                            textAlign: TextAlign.right,
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 28,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(
                            AppStringKey.IBAN_code.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              context.isDesktop ? 16 : 12,
                              AppColors.secondaryIconColor,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          child: Text(
                            controller.ibanCode.value,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              context.isDesktop ? 16 : 12,
                              AppColors.primaryTextColor,
                            ),
                            textAlign: TextAlign.right,
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 28,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: Text(
                            AppStringKey.swift_code.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              context.isDesktop ? 16 : 12,
                              AppColors.secondaryIconColor,
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          child: Text(
                            controller.swiftCode.value,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              context.isDesktop ? 16 : 12,
                              AppColors.primaryTextColor,
                            ),
                            textAlign: TextAlign.right,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
