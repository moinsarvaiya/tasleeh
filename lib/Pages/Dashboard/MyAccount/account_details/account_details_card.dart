import 'package:flutter/material.dart';
import 'package:get/get_utils/get_utils.dart';

import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/extensions/common_extension.dart';
import '../../../../l10n/app_string_key.dart';

class AccountDetailsDashboardCard extends StatelessWidget {
  final String bankName;
  final String accountNumber;
  final String ibanCode;
  final String accountHolderName;

  const AccountDetailsDashboardCard({
    required this.bankName,
    required this.accountHolderName,
    required this.accountNumber,
    required this.ibanCode,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      // constraints: const BoxConstraints(maxWidth: 432),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4.0),
        color: Colors.white,
        border: Border.all(
          width: 1,
          color: AppColors.borderColor,
        ),
      ),
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                bankName,
                style: BaseStyle.textStyleRobotoSemiBold(
                  context.isDesktop ? 18 : 16,
                  AppColors.primaryTextColor,
                ),
              ),
              PopupMenuButton<String>(
                splashRadius: 5,
                offset: const Offset(20, 40),
                padding: EdgeInsets.zero,
                surfaceTintColor: AppColors.colorWhite,
                // onSelected: (choice) => controller.onSelected(choice, i),
                child: const Icon(
                  Icons.more_vert,
                  size: 20,
                  weight: 400,
                ),
                itemBuilder: (BuildContext context) => [
                  PopupMenuItem<String>(
                    value: AppStringKey.rename.tr,
                    height: 32,
                    padding: const EdgeInsets.symmetric(
                      vertical: 5,
                      horizontal: 12,
                    ),
                    child: Row(
                      children: [
                        const Icon(
                          Icons.edit_outlined,
                          size: 16,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        Text(
                          AppStringKey.edit_bank_details.tr,
                          style: BaseStyle.textStyleRobotoRegular(
                            14,
                            AppColors.primaryTextColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                  PopupMenuItem<String>(
                    value: AppStringKey.delete.tr,
                    height: 32,
                    padding: const EdgeInsets.symmetric(
                      vertical: 5,
                      horizontal: 12,
                    ),
                    child: Row(
                      children: [
                        const Icon(
                          Icons.delete_outline,
                          size: 16,
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        Text(
                          AppStringKey.delete.tr,
                          style: BaseStyle.textStyleRobotoRegular(
                            14,
                            AppColors.primaryTextColor,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 44,
          ),
          Row(
            children: [
              Text(
                accountNumber,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                style: BaseStyle.textStyleRobotoMedium(
                  context.isDesktop ? 24 : 18,
                  AppColors.primaryTextColor,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 24,
          ),
          Row(
            children: [
              Text(
                ibanCode,
                style: BaseStyle.textStyleRobotoMedium(
                  context.isDesktop ? 16 : 14,
                  AppColors.primaryTextColor,
                ),
              )
            ],
          ),
          const SizedBox(
            height: 36,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                accountHolderName,
                style: BaseStyle.textStyleRobotoMedium(
                  context.isDesktop ? 16 : 14,
                  AppColors.secondaryTextColor,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
