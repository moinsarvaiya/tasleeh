import 'package:al_baida_garage_fe/core/app_preference/app_preferences.dart';
import 'package:al_baida_garage_fe/core/app_preference/storage_keys.dart';
import 'package:get/get.dart';

import '../../../../api/api_end_point.dart';
import '../../../../api/api_interface.dart';
import '../../../../api/api_presenter.dart';
import '../../../../models/success_account_data_model.dart';

class AccountDetailsDashboardController extends GetxController
    implements ApiCallBacks {
  RxString accountHolderName = ''.obs;
  RxString accountNumber = ''.obs;
  RxString bankName = ''.obs;
  RxString ibanCode = ''.obs;
  RxString bankAddress = ''.obs;
  RxString swiftCode = ''.obs;

  SuccessAccountModel successAccountModel = SuccessAccountModel();
  String branchId = '';

  void getBranches() {
    branchId = AppPreferences.sharedPrefRead(StorageKeys.garageId);
    getBankData();
  }

  void getBankData() {
    ApiPresenter(this).getAccountDetails(branchId);
  }

  void setCurrentData() {
    accountHolderName.value = successAccountModel.accountHolderName!;
    accountNumber.value = successAccountModel.accountNumber!;
    bankName.value = successAccountModel.bankName!;
    ibanCode.value = successAccountModel.IBAN!;
    swiftCode.value = successAccountModel.swift!;
    bankAddress.value = successAccountModel.address!;
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {}

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {}

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.getAccountDetails:
        if (object['data'].length > 0) {
          successAccountModel = SuccessAccountModel.fromJson(object['data'][0]);
          setCurrentData();
        }
    }
  }
}
