import 'package:al_baida_garage_fe/Pages/Dashboard/MyAccount/services/services_dashboard_controller.dart';
import 'package:al_baida_garage_fe/Pages/services/service_screen.dart';
import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:image_network/image_network.dart';

import '../../../../Utils/app_constants.dart';
import '../../../../Utils/utilities.dart';
import '../../../../common/common_function.dart';
import '../../../../common/custom_dialog.dart';
import '../../../../common/input_text_field.dart';
import '../../../../common/space_horizontal.dart';
import '../../../../common/space_vertical.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../custom_classes/custom_dropdown_field.dart';
import '../../../../custom_classes/custom_text.dart';
import '../../../../l10n/app_string_key.dart';
import '../../../services/models/missing_value.dart';
import '../../../services/models/sub_service_data_model.dart';
import '../../../services/service_controller.dart';
import 'service_dashboard_card_mobile_view.dart';
import 'services_dashboard_card.dart';

class ServicesDashboard extends StatefulWidget {
  const ServicesDashboard({super.key});

  @override
  State<ServicesDashboard> createState() => _ServicesDashboardState();
}

class _ServicesDashboardState extends State<ServicesDashboard> {
  final controller = Get.find<ServicesDashboardController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.getBranches();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          context.isDesktop
              ? Obx(
                  () => Visibility(
                    visible: controller.submittedServiceList.isNotEmpty,
                    child: Column(
                      children: [
                        ...controller.submittedServiceList.indexed.map(
                          (e) {
                            var submittedServiceDataModel = e.$2;
                            return Column(
                              children: [
                                ServicesDashboardCard(
                                  title: submittedServiceDataModel.category!.serviceName ?? '',
                                  description: AppStringKey.submitted_category_desc.tr,
                                  subCategory: submittedServiceDataModel.category!.subCategory ?? [],
                                  subCategoryCallBack: (String subCategoryId) {
                                    controller.getSubServiceData(subCategoryId);
                                  },
                                ),
                                const SizedBox(
                                  height: 24,
                                ),
                              ],
                            );
                          },
                        ),
                        const SpaceVertical(0.03),
                      ],
                    ),
                  ),
                )
              : Obx(
                  () => Visibility(
                    visible: controller.submittedServiceList.isNotEmpty,
                    child: Column(
                      children: [
                        ...controller.submittedServiceList.indexed.map(
                          (e) {
                            var submittedServiceDataModel = e.$2;
                            return Column(
                              children: [
                                ServicesDashboardCardMobile(
                                  title: submittedServiceDataModel.category!.serviceName ?? '',
                                  description: AppStringKey.submitted_category_desc.tr,
                                  subCategory: submittedServiceDataModel.category!.subCategory ?? [],
                                  subCategoryCallBack: (String subCategoryId) {
                                    controller.getSubServiceData(subCategoryId);
                                  },
                                ),
                                const SizedBox(
                                  height: 24,
                                ),
                              ],
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
          TextButton(
            style: TextButton.styleFrom(
              minimumSize: Size.zero,
              padding: EdgeInsets.zero,
              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
              foregroundColor: const Color.fromARGB(255, 181, 170, 170),
            ),
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return CustomDialog(
                    padding: const EdgeInsets.all(16),
                    content: SingleChildScrollView(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          const ServiceScreen(
                            isReadOnly: false,
                            serviceRedirectionFrom: ServiceRedirectionFrom.myAccount,
                          ),
                          const SizedBox(
                            height: 18,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1,
                                    color: AppColors.primaryTextColor,
                                  ),
                                ),
                                child: TextButton(
                                  onPressed: () {
                                    controller.clearServiceData();
                                    Get.back();
                                  },
                                  style: TextButton.styleFrom(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 28,
                                      vertical: 8,
                                    ),
                                    textStyle: BaseStyle.textStyleRobotoMedium(
                                      14,
                                      AppColors.primaryTextColor,
                                    ),
                                    backgroundColor: AppColors.colorWhite,
                                    foregroundColor: AppColors.primaryTextColor,
                                  ),
                                  child: Text(AppStringKey.cancel.tr),
                                ),
                              ),
                              const SizedBox(
                                width: 18,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1,
                                    color: AppColors.primaryTextColor,
                                  ),
                                ),
                                child: TextButton(
                                  onPressed: () {
                                    List<MissingValue> missingValues =
                                        Get.find<ServiceController>().findMissingValues(Get.find<ServiceController>().categoryResponseModel);
                                    if (missingValues.isNotEmpty) {
                                      for (var missingValue in missingValues) {
                                        Utilities.showToast(missingValue.message);
                                        break;
                                      }
                                    } else {
                                      Get.find<ServiceController>().submitServiceData();
                                    }
                                  },
                                  style: TextButton.styleFrom(
                                    padding: const EdgeInsets.symmetric(
                                      horizontal: 28,
                                      vertical: 8,
                                    ),
                                    textStyle: BaseStyle.textStyleRobotoMedium(
                                      14,
                                      AppColors.primaryTextColor,
                                    ),
                                    backgroundColor: AppColors.primaryTextColor,
                                    foregroundColor: AppColors.colorWhite,
                                    shape: const BeveledRectangleBorder(),
                                  ),
                                  child: Text(AppStringKey.save.tr),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                },
              ).then((value) {
                controller.clearServiceData();
              });
            },
            child: Text(
              "+ ${AppStringKey.add_another_service.tr}",
              style: BaseStyle.textStyleRobotoSemiBold(
                16,
                AppColors.primaryTextColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

void editMyAccountSubServiceData(BuildContext context, RxList<SubServiceDataModel> subServiceDataModel) {
  var controller = Get.find<ServicesDashboardController>();
  showDialog(
    context: context,
    builder: (BuildContext context) => IntrinsicHeight(
      child: IntrinsicWidth(
        child: AlertDialog(
          backgroundColor: Colors.transparent,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(0)),
          contentPadding: EdgeInsets.zero,
          content: GestureDetector(
            onTap: () {
              controller.isLevelSelected.value = false;
            },
            child: IntrinsicHeight(
              child: Column(
                children: subServiceDataModel.indexed.map(
                  (subCategory) {
                    int subCategoryIndex = subCategory.$1;
                    Rx<SubServiceDataModel> subServiceDataModel = subCategory.$2.obs;
                    return Stack(
                      children: [
                        Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(left: 32, right: context.isMobile || context.isTabletView ? 32 : 48, top: 32, bottom: 32),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: AppColors.colorDragImageBack,
                                border: Border.all(
                                  width: 1,
                                  color: AppColors.strokeColor,
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            subServiceDataModel.value.category!.subcategoryName ?? '',
                                            style: BaseStyle.textStyleRobotoBold(
                                              22,
                                              AppColors.colorBlack,
                                            ),
                                          ),
                                          const SpaceVertical(0.02),
                                          Material(
                                            color: Colors.transparent,
                                            child: Ink(
                                              child: InkWell(
                                                onTap: () {
                                                  var subCategoryId = subServiceDataModel.value.category!.subcategoryId;
                                                  if (subCategoryId != '') {
                                                    controller.getSelectedCars(subCategoryId!);
                                                  } else {
                                                    Utilities.showToast(AppStringKey.select_sub_category.tr);
                                                  }
                                                },
                                                child: CustomText(
                                                  text: AppStringKey.view_added_card.tr,
                                                  textStyle: BaseStyle.underLineTextStyle(
                                                    7,
                                                    AppColors.primaryTextColor,
                                                    fontWeight: FontWeight.normal,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                          const SpaceVertical(0.02),
                                          context.isMobile || context.isTabletView
                                              ? Obx(
                                                  () => Visibility(
                                                    visible: subServiceDataModel.value.category!.serviceDetailImage != null,
                                                    child: Container(
                                                      width: context.isMobile || context.isTabletView ? context.width * 0.20 : context.width * 0.09,
                                                      height: context.isMobile || context.isTabletView ? context.height * 0.1 : context.height * 0.2,
                                                      decoration: BoxDecoration(
                                                        color: AppColors.strokeColor,
                                                        borderRadius: BorderRadius.circular(5),
                                                      ),
                                                      child: Stack(
                                                        children: [
                                                          subServiceDataModel.value.category!.serviceDetailImage != null
                                                              ? Positioned(
                                                                  top: 0,
                                                                  bottom: 0,
                                                                  left: 0,
                                                                  right: 0,
                                                                  child: MouseRegion(
                                                                    cursor: SystemMouseCursors.click,
                                                                    child: ImageNetwork(
                                                                      key: ValueKey(subServiceDataModel.value.category!.serviceDetailImage),
                                                                      image: subServiceDataModel.value.category!.serviceDetailImage ?? '',
                                                                      fitWeb: BoxFitWeb.cover,
                                                                      width: context.width * 0.09,
                                                                      height: context.height * 0.2,
                                                                      onTap: () {
                                                                        CommonFunction.pickImages(0).then((value) {
                                                                          controller.imageUpload(context, value.entries.first.value, subCategoryIndex,
                                                                              isFromEdit: true, subServiceDataModel: subServiceDataModel);
                                                                        });
                                                                      },
                                                                    ),
                                                                  ),
                                                                )
                                                              : const SizedBox.shrink(),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              : const SizedBox.shrink(),
                                          context.isMobile || context.isTabletView ? const SpaceVertical(0.02) : const SizedBox.shrink(),
                                          SizedBox(
                                            width: context.isDesktop ? context.screenWidth * 0.3 : context.screenWidth * 0.55,
                                            child: Column(
                                              children: [
                                                CustomDropDownField(
                                                  isReadOnly: false,
                                                  labelVisibility: false,
                                                  textStyle: BaseStyle.textStyleRobotoRegular(
                                                    16,
                                                    AppColors.inputFieldHintTextColor,
                                                  ),
                                                  labelText: AppStringKey.added_levels.tr,
                                                  buttonPadding: EdgeInsets.zero,
                                                  validationText: AppStringKey.added_levels.tr,
                                                  hintTitle: AppStringKey.added_levels.tr,
                                                  onChanged: (levelName) {
                                                    controller.findSubServiceByName(levelName ?? '');
                                                  },
                                                  items: subServiceDataModel.value.category!.serviceDetailLevel!.map((e) => e.name ?? '').toList(),
                                                ),
                                                Obx(
                                                  () => Visibility(
                                                    visible: controller.isLevelSelected.value,
                                                    child: Column(
                                                      children: [
                                                        InputTextField(
                                                          textInputType: TextInputType.phone,
                                                          keyBoardAction: TextInputAction.next,
                                                          inputFormatter: [
                                                            FilteringTextInputFormatter.digitsOnly,
                                                          ],
                                                          label: '',
                                                          hintText: AppStringKey.enter_amount.tr,
                                                          isPhoneNo: false,
                                                          isLabelVisible: false,
                                                          isRequireField: false,
                                                          textEditingController: controller.editLevelPriceController,
                                                          onChangeWithValidationStatus: (val, isValid) {},
                                                        ),
                                                        InputTextField(
                                                          textInputType: TextInputType.phone,
                                                          keyBoardAction: TextInputAction.next,
                                                          inputFormatter: [
                                                            FilteringTextInputFormatter.digitsOnly,
                                                          ],
                                                          label: '',
                                                          hintText: "${AppStringKey.enter_discount_amount.tr} ${AppConstants.currencyString}",
                                                          isPhoneNo: false,
                                                          isLabelVisible: false,
                                                          isRequireField: false,
                                                          textEditingController: controller.editLevelDiscountedPriceController,
                                                          onChangeWithValidationStatus: (val, isValid) {},
                                                        ),
                                                        InputTextField(
                                                          label: '',
                                                          hintText: AppStringKey.enter_level_desc.tr,
                                                          isPhoneNo: false,
                                                          isLabelVisible: false,
                                                          isRequireField: false,
                                                          textEditingController: controller.editLevelDescriptionController,
                                                          maxLines: 3,
                                                          onChangeWithValidationStatus: (val, isValid) {},
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      const SpaceHorizontal(0.02),
                                      context.isMobile || context.isTabletView
                                          ? const SizedBox.shrink()
                                          : Obx(
                                              () => Visibility(
                                                visible: subServiceDataModel.value.category!.serviceDetailImage != null,
                                                child: Container(
                                                  width: context.width * 0.09,
                                                  height: context.height * 0.2,
                                                  decoration: BoxDecoration(
                                                    color: AppColors.strokeColor,
                                                    borderRadius: BorderRadius.circular(5),
                                                  ),
                                                  child: Stack(
                                                    children: [
                                                      subServiceDataModel.value.category!.serviceDetailImage != null
                                                          ? Positioned(
                                                              top: 0,
                                                              bottom: 0,
                                                              left: 0,
                                                              right: 0,
                                                              child: MouseRegion(
                                                                cursor: SystemMouseCursors.click,
                                                                child: ImageNetwork(
                                                                  key: ValueKey(subServiceDataModel.value.category!.serviceDetailImage),
                                                                  image: subServiceDataModel.value.category!.serviceDetailImage ?? '',
                                                                  fitWeb: BoxFitWeb.cover,
                                                                  width: context.width * 0.09,
                                                                  height: context.height * 0.2,
                                                                  onTap: () {
                                                                    CommonFunction.pickImages(0).then((value) {
                                                                      controller.imageUpload(context, value.entries.first.value, subCategoryIndex,
                                                                          isFromEdit: true, subServiceDataModel: subServiceDataModel);
                                                                    });
                                                                  },
                                                                ),
                                                              ),
                                                            )
                                                          : const SizedBox.shrink(),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                    ],
                                  ),
                                  const SpaceVertical(0.03),
                                  Obx(
                                    () => controller.isLevelSelected.value
                                        ? context.isMobile || context.isTabletView
                                            ? Column(
                                                children: [
                                                  Container(
                                                    width: context.screenWidth * 0.55,
                                                    decoration: BoxDecoration(
                                                      border: Border.all(
                                                        width: 1,
                                                        color: AppColors.primaryTextColor,
                                                      ),
                                                    ),
                                                    child: TextButton(
                                                      onPressed: () {
                                                        controller.isLevelSelected.value = false;
                                                        Get.back();
                                                      },
                                                      style: TextButton.styleFrom(
                                                        padding: const EdgeInsets.symmetric(horizontal: 28, vertical: 8),
                                                        textStyle: BaseStyle.textStyleRobotoMedium(14, AppColors.primaryTextColor),
                                                        backgroundColor: AppColors.colorWhite,
                                                        foregroundColor: AppColors.primaryTextColor,
                                                      ),
                                                      child: Text(AppStringKey.cancel.tr),
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    height: 10,
                                                  ),
                                                  Container(
                                                    width: context.screenWidth * 0.55,
                                                    decoration: BoxDecoration(
                                                      border: Border.all(
                                                        width: 1,
                                                        color: AppColors.primaryTextColor,
                                                      ),
                                                    ),
                                                    child: TextButton(
                                                      onPressed: () {
                                                        if (double.parse(controller.editLevelDiscountedPriceController.text) <
                                                            double.parse(controller.editLevelPriceController.text)) {
                                                          controller.editLevelData(
                                                            subServiceDataModel
                                                                    .value.category!.serviceDetailLevel![controller.selectedLevelIndex].levelId ??
                                                                '',
                                                            double.parse(controller.editLevelPriceController.text),
                                                            double.parse(controller.editLevelDiscountedPriceController.text),
                                                            controller.editLevelDescriptionController.text,
                                                          );
                                                        } else {
                                                          Utilities.showToast(AppStringKey.enter_valid_discount_price.tr);
                                                        }
                                                      },
                                                      style: TextButton.styleFrom(
                                                        padding: const EdgeInsets.symmetric(horizontal: 28, vertical: 8),
                                                        textStyle: BaseStyle.textStyleRobotoMedium(14, AppColors.primaryTextColor),
                                                        backgroundColor: AppColors.primaryTextColor,
                                                        foregroundColor: AppColors.colorWhite,
                                                        shape: const BeveledRectangleBorder(),
                                                      ),
                                                      child: Text(AppStringKey.save.tr),
                                                    ),
                                                  ),
                                                ],
                                              )
                                            : Row(
                                                mainAxisAlignment:
                                                    context.isMobile || context.isTabletView ? MainAxisAlignment.center : MainAxisAlignment.end,
                                                children: [
                                                  Container(
                                                    width: 120,
                                                    decoration: BoxDecoration(
                                                      border: Border.all(
                                                        width: 1,
                                                        color: AppColors.primaryTextColor,
                                                      ),
                                                    ),
                                                    child: TextButton(
                                                      onPressed: () {
                                                        controller.isLevelSelected.value = false;
                                                        Get.back();
                                                      },
                                                      style: TextButton.styleFrom(
                                                        padding: const EdgeInsets.symmetric(horizontal: 28, vertical: 8),
                                                        textStyle: BaseStyle.textStyleRobotoMedium(14, AppColors.primaryTextColor),
                                                        backgroundColor: AppColors.colorWhite,
                                                        foregroundColor: AppColors.primaryTextColor,
                                                      ),
                                                      child: Text(AppStringKey.cancel.tr),
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    width: 18,
                                                  ),
                                                  Container(
                                                    width: 120,
                                                    decoration: BoxDecoration(
                                                      border: Border.all(
                                                        width: 1,
                                                        color: AppColors.primaryTextColor,
                                                      ),
                                                    ),
                                                    child: TextButton(
                                                      onPressed: () {
                                                        if (double.parse(controller.editLevelDiscountedPriceController.text) <
                                                            double.parse(controller.editLevelPriceController.text)) {
                                                          controller.editLevelData(
                                                            subServiceDataModel
                                                                    .value.category!.serviceDetailLevel![controller.selectedLevelIndex].levelId ??
                                                                '',
                                                            double.parse(controller.editLevelPriceController.text),
                                                            double.parse(controller.editLevelDiscountedPriceController.text),
                                                            controller.editLevelDescriptionController.text,
                                                          );
                                                        } else {
                                                          Utilities.showToast(AppStringKey.enter_valid_discount_price.tr);
                                                        }
                                                      },
                                                      style: TextButton.styleFrom(
                                                        padding: const EdgeInsets.symmetric(horizontal: 28, vertical: 8),
                                                        textStyle: BaseStyle.textStyleRobotoMedium(14, AppColors.primaryTextColor),
                                                        backgroundColor: AppColors.primaryTextColor,
                                                        foregroundColor: AppColors.colorWhite,
                                                        shape: const BeveledRectangleBorder(),
                                                      ),
                                                      child: Text(AppStringKey.save.tr),
                                                    ),
                                                  ),
                                                ],
                                              )
                                        : const SizedBox.shrink(),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        Positioned(
                          right: 10,
                          top: 10,
                          child: Material(
                            color: Colors.transparent,
                            child: Ink(
                              child: InkWell(
                                onTap: () {
                                  controller.isLevelSelected.value = false;
                                  Get.back();
                                },
                                child: const Icon(
                                  Icons.close,
                                  size: 20,
                                  color: AppColors.colorBlack,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    );
                  },
                ).toList(),
              ),
            ),
          ),
        ),
      ),
    ),
  ).then((value) {
    controller.isLevelSelected.value = false;
  });
}
