import 'package:al_baida_garage_fe/custom_classes/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:image_network/image_network.dart';

import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/base_style.dart';
import '../../../services/models/get_service_data_model.dart';

class ServicesDashboardCard extends StatelessWidget {
  final String title;
  final String description;
  final List<SubmittedSubCategory> subCategory;
  final Function(String subCategoryId) subCategoryCallBack;

  const ServicesDashboardCard({
    required this.title,
    required this.description,
    required this.subCategory,
    required this.subCategoryCallBack,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 40),
      decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: AppColors.borderColor,
          ),
          color: Colors.white),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title,
                  style: BaseStyle.textStyleRobotoSemiBold(
                    24,
                    AppColors.primaryTextColor,
                  ),
                ),
                Text(
                  description,
                  style: BaseStyle.textStyleRobotoMediumBold(
                    14,
                    AppColors.inCompletedStepTextColor,
                  ),
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 32),
            width: 2,
            decoration: BoxDecoration(border: Border.all(width: 2, color: AppColors.borderColor)),
          ),
          Expanded(
            flex: 3,
            child: GridView.count(
              crossAxisCount: 2,
              shrinkWrap: true,
              childAspectRatio: 10,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              children: List.generate(
                subCategory.length,
                (index) {
                  return Container(
                    margin: const EdgeInsets.all(5),
                    child: Row(
                      children: [
                        ImageNetwork(
                          image: subCategory[index].serviceDetailImage ?? '',
                          width: 40,
                          height: 40,
                          fitWeb: BoxFitWeb.cover,
                          key: ValueKey(subCategory[index].serviceDetailImage),
                        ),
                        const SizedBox(
                          width: 12,
                        ),
                        CustomText(
                          text: subCategory[index].name ?? '',
                          textStyle: BaseStyle.textStyleRobotoMediumBold(
                            8,
                            AppColors.inCompletedStepTextColor,
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        Material(
                          child: Ink(
                            child: InkWell(
                              onTap: () {
                                subCategoryCallBack(subCategory[index].serviceDetailId ?? '');
                              },
                              child: const Icon(
                                Icons.edit,
                                size: 12,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
