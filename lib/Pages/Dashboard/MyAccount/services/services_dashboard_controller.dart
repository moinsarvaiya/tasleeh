import 'dart:convert';
import 'dart:typed_data';

import 'package:al_baida_garage_fe/Pages/services/service_controller.dart';
import 'package:al_baida_garage_fe/api/api_interface.dart';
import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import '../../../../Utils/utilities.dart';
import '../../../../api/api_end_point.dart';
import '../../../../api/api_presenter.dart';
import '../../../../api/web_fields_key.dart';
import '../../../../common/common_function.dart';
import '../../../../common/custom_loader.dart';
import '../../../../core/app_preference/app_preferences.dart';
import '../../../../core/app_preference/storage_keys.dart';
import '../../../../l10n/app_string_key.dart';
import '../../../Dashboard/MyAccount/services/services_dashboard.dart';
import '../../../services/models/car_model.dart';
import '../../../services/models/get_service_data_model.dart';
import '../../../services/models/sub_service_data_model.dart';
import '../../../services/service_mobile_view.dart';
import '../../../services/service_web_view.dart';

class ServicesDashboardController extends GetxController
    implements ApiCallBacks {
  RxList<SubmittedServiceDataModel> submittedServiceList = RxList();
  String branchId = '';
  RxList<SelectedCarModel> selectedCarList = RxList();
  RxList<SubServiceDataModel> subServiceDataModel = RxList();
  TextEditingController editLevelPriceController = TextEditingController();
  TextEditingController editLevelDiscountedPriceController =
      TextEditingController();
  TextEditingController editLevelDescriptionController =
      TextEditingController();
  RxBool isLevelSelected = false.obs;
  bool subServiceUpdateFromGet = true;
  int selectedLevelIndex = -1;

  void getBranches() async {
    branchId = await AppPreferences.sharedPrefRead(StorageKeys.garageId);
    getServiceData();
  }

  void getServiceData() {
    ApiPresenter(this).getServiceData(branchId);
  }

  void getCategories() {
    ApiPresenter(this).getCategories();
  }

  void getSubServiceData(String subServiceId) {
    subServiceUpdateFromGet = true;
    ApiPresenter(this).getSubServiceData(subServiceId);
  }

  void updateSubmittedServiceList(dynamic object) {
    submittedServiceList.clear();
    submittedServiceList.addAll((object['data'] as List)
        .map((e) => SubmittedServiceDataModel.fromJson(e)));
    submittedServiceList.refresh();
    getCategories();
  }

  void getSelectedCars(String subCategoryId) {
    ApiPresenter(this).getSelectedCars(subCategoryId);
  }

  void updateSubServiceData(
    String subServiceId,
    String subCategoryId,
    String imageUrl,
  ) {
    subServiceUpdateFromGet = false;
    ApiPresenter(this).updateSubServiceData(
      subServiceId,
      subCategoryId,
      imageUrl,
    );
  }

  void editLevelData(
    String levelId,
    double price,
    double discountedPrice,
    String description,
  ) {
    ApiPresenter(this).editLevelData(
      levelId,
      price,
      discountedPrice,
      description,
    );
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {}

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {}

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.getServiceData:
        updateSubmittedServiceList(object);
        break;
      case ApiEndPoints.getSelectedCarData:
        updateSelectedCarModel(object);
        break;
      case ApiEndPoints.subServiceData:
        if (subServiceUpdateFromGet) {
          manageSubServiceData(object);
        } else {
          Future.delayed(const Duration(seconds: 5));
          getServiceData();
        }
        break;
      case ApiEndPoints.editLevelData:
        manageEditLevelData(object);
        break;
      default:
        break;
    }
  }

  void manageEditLevelData(dynamic object) {
    isLevelSelected.value = false;
    Get.back();
  }

  void manageSubServiceData(dynamic object) {
    subServiceDataModel.clear();
    subServiceDataModel.addAll((object['data'] as List)
        .map((e) => SubServiceDataModel.fromJson(e))
        .toList());
    subServiceDataModel.refresh();
    editMyAccountSubServiceData(Get.context!, subServiceDataModel);
  }

  void updateSelectedCarModel(dynamic object) {
    selectedCarList.clear();
    selectedCarList.addAll(
        (object['data'] as List).map((e) => SelectedCarModel.fromJson(e)));
    selectedCarList.refresh();
    if (selectedCarList.isNotEmpty) {
      Get.put(ServiceController());
      if (Get.context!.isDesktop) {
        selectedCarDialog(Get.context!,selectedCarList);
      } else {
        selectedCarMobileDialog(Get.context!,selectedCarList);
      }
    } else {
      Utilities.showToast(AppStringKey.selected_car_not_available.tr);
    }
  }

  void imageUpload(
    BuildContext context,
    Uint8List imageFile,
    int subCategoryIndex, {
    bool isFromEdit = false,
    int fromEditListIndex = 0,
    Rx<SubServiceDataModel>? subServiceDataModel,
  }) async {
    try {
      var headers = {
        WebFieldKey.strContentType: WebFieldKey.strMultipartFormData,
        WebFieldKey.strAuthorization:
            'Bearer ${AppPreferences.sharedPrefRead(StorageKeys.accessToken)}',
      };

      var request = http.MultipartRequest(
        WebFieldKey.strPostMethod,
        Uri.parse('${ApiEndPoints.baseUrl}${ApiEndPoints.imageUpload}'),
      );
      request.headers.addAll(headers);
      var stream = http.ByteStream.fromBytes(imageFile);
      var length = imageFile.lengthInBytes;
      request.files.add(
        http.MultipartFile(WebFieldKey.strFile, stream, length,
            filename: 'sub_category.jpg'),
      );
      request.fields[WebFieldKey.strFolder] = 'garage';
      http.StreamedResponse response = await request.send();
      final responseBody = await response.stream.bytesToString();
      final parsedJson = jsonDecode(responseBody);
      if (parsedJson['code'] == 200 || parsedJson['code'] == 201) {
        subServiceDataModel!.value.category!.serviceDetailImage =
            parsedJson['data']['url'];
        subServiceDataModel.refresh();
        imageCache.clear();
        updateSubServiceData(
          subServiceDataModel.value.category!.serviceDetailId ?? '',
          subServiceDataModel.value.category!.subcategoryId ?? '',
          subServiceDataModel.value.category!.serviceDetailImage ?? '',
        );
      } else {
        CommonFunction.showCustomSnackBar(
          context: context,
          message: parsedJson['error'],
          duration: const Duration(seconds: 3),
          actionLabel: 'Close',
        );
      }
      LoadingDialog.closeFullScreenDialog();
    } catch (e) {
      LoadingDialog.closeFullScreenDialog();
      CommonFunction.showCustomSnackBar(
        context: context,
        message: e.toString(),
        duration: const Duration(seconds: 3),
        actionLabel: 'Close',
      );
    }
  }

  void findSubServiceByName(String paramName) {
    for (int i = 0; i < subServiceDataModel.length; i++) {
      var subServiceData = subServiceDataModel[i];
      if (subServiceData.category != null &&
          subServiceData.category!.serviceDetailLevel != null) {
        for (int j = 0;
            j < subServiceData.category!.serviceDetailLevel!.length;
            j++) {
          var serviceDetailLevel =
              subServiceData.category!.serviceDetailLevel![j];
          if (serviceDetailLevel.name == paramName) {
            editLevelPriceController.text =
                double.parse(serviceDetailLevel.actualPrice ?? '')
                    .round()
                    .toString();
            editLevelDiscountedPriceController.text =
                double.parse(serviceDetailLevel.discountedPrice ?? '')
                    .round()
                    .toString();
            editLevelDescriptionController.text =
                serviceDetailLevel.description ?? '';
            isLevelSelected.value = true;
            selectedLevelIndex = j;
            return;
          }
        }
      }
    }
    // No match found, reset values and set index to -1
    editLevelPriceController.text = '';
    editLevelDiscountedPriceController.text = '';
    editLevelDescriptionController.text = '';
    isLevelSelected.value = false;
    selectedLevelIndex = -1;
  }

  void clearServiceData(){
    if (Get.isRegistered<ServiceController>()) {
      var controller = Get.find<ServiceController>();
      controller.isServiceCalled = true;
      controller.categorySelectedValue.clear();
      controller.subCategorySelectedValue.clear();
      controller.addCategoryList.clear();
      controller.categoryResponseModel.clear();
    }
  }
}
