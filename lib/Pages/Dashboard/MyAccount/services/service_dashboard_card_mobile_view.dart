import 'package:al_baida_garage_fe/Pages/services/models/get_service_data_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/get_utils.dart';
import 'package:image_network/image_network.dart';

import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../l10n/app_string_key.dart';

class ServicesDashboardCardMobile extends StatefulWidget {
  final String title;
  final String description;
  final List<SubmittedSubCategory> subCategory;
  final Function(String subCategoryId) subCategoryCallBack;

  const ServicesDashboardCardMobile({
    required this.title,
    required this.description,
    required this.subCategory,
    required this.subCategoryCallBack,
    Key? key,
  }) : super(key: key);

  @override
  State<ServicesDashboardCardMobile> createState() => _ServicesDashboardCardMobileState();
}

class _ServicesDashboardCardMobileState extends State<ServicesDashboardCardMobile> {
  bool showAllItems = false;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border.all(
            width: 1,
            color: AppColors.borderColor,
          ),
          color: Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.title,
                      overflow: TextOverflow.ellipsis,
                      style: BaseStyle.textStyleRobotoSemiBold(
                        14,
                        AppColors.primaryTextColor,
                      ),
                    ),
                    Container(
                      constraints: const BoxConstraints(maxWidth: 220),
                      child: Text(
                        widget.description,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 4,
                        style: BaseStyle.textStyleRobotoMediumBold(
                          12,
                          AppColors.inCompletedStepTextColor,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 12),
              decoration: BoxDecoration(
                border: Border.all(color: AppColors.borderColor, width: 0.8),
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              itemCount: showAllItems
                  ? widget.subCategory.length
                  : widget.subCategory.length > 4
                      ? 4
                      : widget.subCategory.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: const EdgeInsets.only(bottom: 8),
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: 1,
                      color: AppColors.borderColor,
                    ),
                  ),
                  padding: const EdgeInsets.all(6.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          ImageNetwork(
                            image: widget.subCategory[index].serviceDetailImage ?? '',
                            width: 32,
                            height: 32,
                            fitWeb: BoxFitWeb.cover,
                            key: ValueKey(widget.subCategory[index].serviceDetailImage),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Text(
                            widget.subCategory[index].name ?? '',
                            style: BaseStyle.textStyleRobotoMediumBold(
                              12,
                              AppColors.inCompletedStepTextColor,
                            ),
                          ),
                        ],
                      ),
                      Material(
                        child: Ink(
                          child: InkWell(
                            onTap: () {
                              widget.subCategoryCallBack(widget.subCategory[index].serviceDetailId ?? '');
                            },
                            child: const Icon(
                              Icons.edit,
                              size: 16.0,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                );
              },
            ),
            if (!showAllItems && widget.subCategory.length - 4 > 0)
              TextButton(
                style: TextButton.styleFrom(
                  minimumSize: Size.zero,
                  padding: EdgeInsets.zero,
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  foregroundColor: AppColors.primaryTextColor,
                ),
                onPressed: () {
                  setState(() {
                    showAllItems = true;
                  });
                },
                child: Text(
                  '+ ${AppStringKey.view_more.tr} ${widget.subCategory.length - 4}',
                  style: BaseStyle.textStyleRobotoSemiBold(12, AppColors.primaryTextColor),
                ),
              ),
            if (showAllItems)
              TextButton(
                style: TextButton.styleFrom(
                  minimumSize: Size.zero,
                  padding: EdgeInsets.zero,
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  foregroundColor: AppColors.primaryTextColor,
                ),
                onPressed: () {
                  setState(() {
                    showAllItems = false;
                  });
                },
                child: Text(
                  AppStringKey.view_less.tr,
                  style: BaseStyle.textStyleRobotoSemiBold(12, AppColors.primaryTextColor),
                ),
              ),
            const SizedBox(
              height: 8,
            )
          ],
        ),
      ),
    );
  }
}
