import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'my_account_dashboard_controller.dart';
import 'my_account_mobile_view.dart';
import 'my_account_web_view.dart';

class MyAccountDashBoardScreen extends StatefulWidget {
  const MyAccountDashBoardScreen({super.key});

  @override
  State<MyAccountDashBoardScreen> createState() => _MyAccountDashBoardScreenState();
}

class _MyAccountDashBoardScreenState extends State<MyAccountDashBoardScreen> {
  final controller = Get.find<MyAccountDashboardController>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: context.isDesktop
          ? MyAccountDashBoardWeb(
              controller: controller,
            )
          : MyAccountDashBoardMobile(
              controller: controller,
            ),
    );
  }
}
