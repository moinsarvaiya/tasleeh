import 'package:get/get.dart';

import '../../../l10n/app_string_key.dart';
import '../../custom_stepper/models/my_account_model.dart';
import 'account_details/account_details_dashboard.dart';
import 'basic_details_dashboard/basic_details_dashboard.dart';
import 'marketing/marketing_dashboard.dart';
import 'services/services_dashboard.dart';
import 'staff/staff_dashboard.dart';

class MyAccountDashboardController extends GetxController {
  RxList<MyAccountDashboardModel> stepper = RxList();
  RxInt selectedIndex = 0.obs;

  List<String> menuList = [
    AppStringKey.basic.tr,
    AppStringKey.accounts.tr,
    AppStringKey.marketing.tr,
    AppStringKey.stepper_services.tr,
    AppStringKey.staff.tr
  ];

  @override
  void onInit() {
    super.onInit();
    stepper.add(
      MyAccountDashboardModel(
        title: AppStringKey.basic.tr,
        isSelected: true,
        childPage: const BasicDetailsDashboard(),
      ),
    );
    stepper.add(
      MyAccountDashboardModel(
        title: AppStringKey.accounts.tr,
        isSelected: false,
        childPage: const AccountDetailsDashboard(),
      ),
    );
    stepper.add(
      MyAccountDashboardModel(
        title: AppStringKey.stepper_marketing.tr,
        isSelected: false,
        childPage: const MarketingDashboard(),
      ),
    );
    stepper.add(
      MyAccountDashboardModel(
        title: AppStringKey.stepper_services.tr,
        isSelected: false,
        childPage: const ServicesDashboard(),
      ),
    );
    stepper.add(
      MyAccountDashboardModel(
        title: AppStringKey.stepper_staff.tr,
        isSelected: false,
        childPage: const StaffDashboard(),
      ),
    );
  }
}
