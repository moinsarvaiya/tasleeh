import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/constant/app_color.dart';
import '../../../core/constant/base_style.dart';
import '../../../core/extensions/common_extension.dart';
import '../../../l10n/app_string_key.dart';
import 'menu_item_widget.dart';
import 'my_account_dashboard_controller.dart';

class MyAccountDashBoardWeb extends StatelessWidget {
  final MyAccountDashboardController controller;

  const MyAccountDashBoardWeb({required this.controller, super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: AppColors.primaryBackgroundColor),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    AppStringKey.my_account.tr,
                    style: BaseStyle.textStyleRobotoSemiBold(
                      24,
                      AppColors.primaryTextColor,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                "Sorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate libero et velit interdum, ac aliquet odio mattis.",
                style: BaseStyle.textStyleRobotoMedium(
                  14,
                  AppColors.primaryTextColor,
                ),
              )
            ],
          ),
          const SizedBox(
            height: 24,
          ),
          Obx(
            () => SizedBox(
              height: context.screenHeight * 0.062,
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: controller.stepper.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return MenuItemWidget(
                    stepperList: controller.stepper,
                    index: index,
                    onTap: (int selectedValue) {
                      controller.selectedIndex.value = selectedValue;
                      for (var element in controller.stepper) {
                        element.isSelected = false;
                      }
                      controller.stepper[index].isSelected = true;
                      controller.stepper.refresh();
                    },
                  );
                },
              ),
            ),
          ),
          Obx(
            () => SizedBox(
              height: context.screenHeight * 0.65,
              child: SingleChildScrollView(
                child: controller.stepper[controller.selectedIndex.value].childPage,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
