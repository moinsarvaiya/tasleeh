import 'package:al_baida_garage_fe/Pages/Dashboard/custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import 'package:al_baida_garage_fe/api/api_end_point.dart';
import 'package:al_baida_garage_fe/api/api_interface.dart';
import 'package:al_baida_garage_fe/core/app_preference/app_preferences.dart';
import 'package:al_baida_garage_fe/core/app_preference/storage_keys.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../api/api_presenter.dart';
import '../../../common/submission_failed_modal.dart';
import '../../../models/comments_model.dart';
import 'models/multi_branch_model.dart';

class MultiBranchHandlerController extends GetxController implements ApiCallBacks {
  RxList<MultiBranchModel> multiBranchList = RxList();
  int deletedBranchIndex = 0;

  void getBranches() {
    ApiPresenter(this).getMultiBranches();
  }

  void getBranchReviews(String branchId) {
    ApiPresenter(this).getBranchReviews(branchId);
  }

  void deleteBranch(int index) {
    deletedBranchIndex = index;
    ApiPresenter(this).deleteBranch(multiBranchList[deletedBranchIndex].id);
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    // TODO: implement onConnectionError
  }

  @override
  void onError(String errorMsg, responseCode, String apiEndPoint) {
    // TODO: implement onError
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {
    // TODO: implement onLoading
  }

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.getMultiBranch:
        final List<dynamic> branches = object['data'];
        multiBranchList.clear();
        for (var element in branches) {
          multiBranchList.add(
            MultiBranchModel(
                id: element['id'],
                garageName: element['garageName'],
                garageStatus: element['garageStatus'],
                contactPersonName: element['contactPersonName'],
                isGarageSelected: false,
                branchName: element['branchName'],
                garageImage: element['garageImage'],
                isHeadBranch: element['isHeadBranch']),
          );
        }
        multiBranchList[multiBranchList.indexWhere((branch) => branch.isHeadBranch)].isGarageSelected = true;
        multiBranchList.refresh();
        AppPreferences.sharedPrefWrite(StorageKeys.garageId, multiBranchList.firstWhere((branch) => branch.isHeadBranch == true).id);
        var controller = Get.find<CustomDashboardStepperController>();
        controller.getGarageProfile();
        controller.getOrdersSummary();
        controller.getServiceLevelData();
        break;
      case ApiEndPoints.getBranchReviews:
        List<CommentModel> reviews = [];
        reviews.addAll((object['data'] as List).map((e) => CommentModel.fromJson(e)));
        showDialog(
          context: Get.context!,
          builder: (BuildContext context) {
            return SubmissionFailedModal(reviews: reviews);
          },
        );
        break;
      case ApiEndPoints.deleteBranch:
        multiBranchList.removeAt(deletedBranchIndex);
        multiBranchList.refresh();
        break;
    }
  }
}
