class MultiBranchModel {
  String id;
  String garageName;
  String branchName;
  String contactPersonName;
  String garageImage;
  String garageStatus;
  bool isHeadBranch;
  bool isGarageSelected;

  MultiBranchModel({
    required this.id,
    required this.garageName,
    required this.branchName,
    required this.contactPersonName,
    required this.garageImage,
    required this.garageStatus,
    required this.isHeadBranch,
    required this.isGarageSelected,
  });
}
