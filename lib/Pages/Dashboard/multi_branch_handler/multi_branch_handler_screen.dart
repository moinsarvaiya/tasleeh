import 'package:al_baida_garage_fe/Pages/services/service_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_network/image_network.dart';

import '../../../common/common_modal_widget.dart';
import '../../../common/space_horizontal.dart';
import '../../../common/space_vertical.dart';
import '../../../core/app_preference/app_preferences.dart';
import '../../../core/app_preference/storage_keys.dart';
import '../../../core/constant/app_color.dart';
import '../../../core/constant/app_images.dart';
import '../../../core/constant/base_style.dart';
import '../../../core/constant/ui_constants.dart';
import '../../../core/extensions/common_extension.dart';
import '../../../core/routes/route_paths.dart';
import '../../../l10n/app_string_key.dart';
import '../../basic_details/basic_details_controller.dart';
import '../../custom_stepper/widgets/branch_handler/branch_delete_dialog.dart';
import '../custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import 'multi_branch_handler_controller.dart';

class MultiBranchHandlerScreen extends StatefulWidget {
  const MultiBranchHandlerScreen({super.key});

  @override
  State<MultiBranchHandlerScreen> createState() => _MultiBranchHandlerScreenState();
}

class _MultiBranchHandlerScreenState extends State<MultiBranchHandlerScreen> {
  final controller = Get.find<MultiBranchHandlerController>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: AppColors.primaryBackgroundColor),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          context.isDesktop
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    titleWidget(),
                    addBranchButtonWidget(),
                  ],
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    titleWidget(),
                    const SpaceVertical(0.02),
                    addBranchButtonWidget(),
                  ],
                ),
          SizedBox(
            height: context.isDesktop ? 32 : 20,
          ),
          Expanded(
            child: Obx(
              () => GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: context.isDesktop ? 4 : 1,
                  childAspectRatio: 4.0,
                  mainAxisSpacing: 16,
                  crossAxisSpacing: 16,
                  mainAxisExtent: 200.0,
                ),
                itemCount: controller.multiBranchList.length,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Material(
                    key: UniqueKey(),
                    borderRadius: BorderRadius.circular(12),
                    color: Colors.transparent,
                    child: Ink(
                      child: InkWell(
                        onTap: () {
                          handleBranchClick(index);
                        },
                        child: Stack(
                          children: [
                            Container(
                              width: 432,
                              height: 252,
                              padding: const EdgeInsets.all(1),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                border: Border.all(
                                  width: controller.multiBranchList[index].isGarageSelected ? 2 : 0,
                                  color: controller.multiBranchList[index].isGarageSelected ? AppColors.colorBlack : Colors.transparent,
                                ),
                                color: AppColors.colorWhite,
                              ),
                              child: Container(
                                padding: const EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                ),
                                child: ImageNetwork(
                                  onTap: () {
                                    handleBranchClick(index);
                                  },
                                  borderRadius: BorderRadius.circular(12),
                                  image: controller.multiBranchList[index].garageImage,
                                  width: 432,
                                  height: 200,
                                  fitWeb: BoxFitWeb.fill,
                                ),
                              ),
                            ),
                            controller.multiBranchList[index].isGarageSelected
                                ? const SizedBox.shrink()
                                : Positioned(
                                    right: 10,
                                    top: 10,
                                    child: Container(
                                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(3), color: AppColors.inActiveCtaTextColor),
                                      child: PopupMenuButton<String>(
                                        splashRadius: 5,
                                        offset: const Offset(0, 25),
                                        padding: EdgeInsets.zero,
                                        surfaceTintColor: AppColors.colorWhite,
                                        onSelected: (choice) => {
                                          showDialog(
                                            context: context,
                                            builder: (_) {
                                              return BranchDeleteDialog(
                                                branchName: controller.multiBranchList[index].garageName,
                                                index: index,
                                                dialogFrom: 'dashboard',
                                              );
                                            },
                                          )
                                        },
                                        child: const Icon(
                                          Icons.more_vert,
                                          size: 20,
                                          weight: 400,
                                        ),
                                        itemBuilder: (BuildContext context) => [
                                          PopupMenuItem<String>(
                                            value: AppStringKey.delete.tr,
                                            height: 32,
                                            padding: const EdgeInsets.symmetric(
                                              vertical: 5,
                                              horizontal: 12,
                                            ),
                                            child: Row(
                                              children: [
                                                const Icon(
                                                  Icons.delete_outline,
                                                  size: 16,
                                                ),
                                                const SizedBox(
                                                  width: 8,
                                                ),
                                                Text(
                                                  AppStringKey.delete.tr,
                                                  style: BaseStyle.textStyleRobotoRegular(
                                                    14,
                                                    AppColors.primaryTextColor,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                            Positioned(
                              bottom: 0,
                              right: 0,
                              left: 0,
                              top: 130,
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.only(
                                    bottomLeft: Radius.circular(12),
                                    bottomRight: Radius.circular(12),
                                  ),
                                  gradient: const LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [Colors.white12, Colors.white24, Colors.white, Colors.white, Colors.white],
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 1,
                                      blurRadius: 5,
                                      offset: const Offset(0, 0),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 12, right: 12, bottom: 4),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    controller.multiBranchList[index].garageName,
                                    style: BaseStyle.textStyleRobotoBold(
                                      16,
                                      AppColors.primaryTextColor,
                                    ),
                                  ),
                                  const SpaceVertical(0.002),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        controller.multiBranchList[index].contactPersonName,
                                        style: BaseStyle.textStyleRobotoMedium(
                                          13,
                                          AppColors.primaryTextColor,
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          Container(
                                            width: 6,
                                            height: 6,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(10),
                                              color: textColor(controller.multiBranchList[index].garageStatus),
                                            ),
                                          ),
                                          const SpaceHorizontal(0.003),
                                          Text(
                                            controller.multiBranchList[index].garageStatus
                                                .replaceRange(0, 1, controller.multiBranchList[index].garageStatus[0].toUpperCase()),
                                            style: BaseStyle.textStyleRobotoSemiBold(
                                              14,
                                              textColor(controller.multiBranchList[index].garageStatus),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Color textColor(String garageStatus) {
    if (garageStatus == garageStatusType[GarageStatusType.incomplete]) {
      return garageStatusColor[GarageStatusType.incomplete] ?? Colors.black;
    } else if (garageStatus == garageStatusType[GarageStatusType.reviewed]) {
      return garageStatusColor[GarageStatusType.reviewed] ?? Colors.black;
    } else if (garageStatus == garageStatusType[GarageStatusType.newStatus]) {
      return garageStatusColor[GarageStatusType.newStatus] ?? Colors.black;
    } else if (garageStatus == garageStatusType[GarageStatusType.accepted]) {
      return garageStatusColor[GarageStatusType.accepted] ?? Colors.black;
    } else if (garageStatus == garageStatusType[GarageStatusType.rejected]) {
      return garageStatusColor[GarageStatusType.rejected] ?? Colors.black;
    } else {
      return garageStatusColor[GarageStatusType.blacklisted] ?? Colors.black;
    }
  }

  Widget titleWidget() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            AppStringKey.branch_handler.tr,
            style: BaseStyle.textStyleRobotoSemiBold(
              24,
              AppColors.primaryTextColor,
            ),
          ),
          Text(
            AppStringKey.branch_handler_desc.tr,
            style: BaseStyle.textStyleRobotoMediumBold(
              14,
              AppColors.primaryTextColor,
            ),
          )
        ],
      );

  Widget addBranchButtonWidget() => Row(
        children: [
          TextButton(
            onPressed: () {
              AppPreferences.sharedPrefWrite(StorageKeys.garageId, controller.multiBranchList.firstWhere((branch) => branch.isHeadBranch == true).id);
              for (var element in controller.multiBranchList) {
                element.isGarageSelected = false;
              }
              controller.multiBranchList[controller.multiBranchList.indexWhere((branch) => branch.isHeadBranch)].isGarageSelected = true;
              controller.multiBranchList.refresh();
              Get.find<CustomDashboardStepperController>().getGarageProfile();
            }, // temporary disabled
            style: TextButton.styleFrom(
              textStyle: BaseStyle.textStyleRobotoSemiBold(
                14,
                AppColors.primaryTextColor,
              ),
              shape: const BeveledRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(2),
                ),
              ),
              padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 15),
              foregroundColor: AppColors.primaryTextColor,
              side: const BorderSide(
                color: AppColors.strokeColor,
                width: 1,
              ),
            ),
            child: Text(AppStringKey.switch_to_main_branch.tr),
          ),
          SpaceHorizontal(context.isDesktop ? 0.005 : 0.02),
          TextButton(
            onPressed: () {
              AppPreferences.sharedPrefWrite(StorageKeys.addBranchesFrom, addBranch);
              AppPreferences.sharedPrefWrite(StorageKeys.isAddBranchesFromAuth, false);
              Get.toNamed(RoutePaths.CUSTOM_STEPPER);
            }, // temporary disabled
            style: TextButton.styleFrom(
              textStyle: BaseStyle.textStyleRobotoSemiBold(
                14,
                AppColors.primaryTextColor,
              ),
              shape: const BeveledRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(2),
                ),
              ),
              padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 15),
              foregroundColor: AppColors.primaryTextColor,
              side: const BorderSide(
                color: AppColors.strokeColor,
                width: 1,
              ),
            ),
            child: Text('+${AppStringKey.add_branch.tr}'),
          ),
        ],
      );

  void handleBranchClick(int index) {
    AppPreferences.sharedPrefWrite(StorageKeys.garageId, controller.multiBranchList[index].id);
    if (controller.multiBranchList[index].garageStatus == garageStatusType[GarageStatusType.incomplete]) {
      if (Get.isRegistered<ServiceController>()) {
        var controller = Get.find<ServiceController>();
        controller.categorySelectedValue.clear();
        controller.subCategorySelectedValue.clear();
        controller.addCategoryList.clear();
        controller.categoryResponseModel.clear();
      }
      AppPreferences.sharedPrefWrite(StorageKeys.addBranchesFrom, editBranch);
      AppPreferences.sharedPrefWrite(StorageKeys.isAddBranchesFromAuth, false);
      Get.find<BasicDetailsController>().isApiCalled = false;
      Get.toNamed(RoutePaths.CUSTOM_STEPPER);
    } else if (controller.multiBranchList[index].garageStatus == garageStatusType[GarageStatusType.accepted]) {
      for (var element in controller.multiBranchList) {
        element.isGarageSelected = false;
      }
      controller.multiBranchList[index].isGarageSelected = true;
      controller.multiBranchList.refresh();
      Get.find<CustomDashboardStepperController>().getGarageProfile();
    } else if (controller.multiBranchList[index].garageStatus == garageStatusType[GarageStatusType.newStatus]) {
      showDialog(
        context: Get.context!,
        builder: (BuildContext context) {
          return CommonModalWidget(
            modalIcon: AppImages.warning,
            title: AppStringKey.application_review.tr,
            description: AppStringKey.application_review_description.tr,
            firstButtonContent: AppStringKey.explore_izhal.tr,
            firstButtonOnTap: () {
              Navigator.pop(context);
            },
          );
        },
      );
    } else if (controller.multiBranchList[index].garageStatus == garageStatusType[GarageStatusType.reviewed]) {
      controller.getBranchReviews(controller.multiBranchList[index].id);
    } else if (controller.multiBranchList[index].garageStatus == garageStatusType[GarageStatusType.rejected]) {
      showDialog(
        context: Get.context!,
        builder: (BuildContext context) {
          return CommonModalWidget(
            modalIcon: AppImages.failed,
            title: AppStringKey.rejected_application.tr,
            description: AppStringKey.rejected_application_description.tr,
            firstButtonContent: AppStringKey.explore_izhal.tr,
            firstButtonOnTap: () {
              Navigator.pop(context);
            },
          );
        },
      );
    } else if (controller.multiBranchList[index].garageStatus == garageStatusType[GarageStatusType.blacklisted]) {
      showDialog(
        context: Get.context!,
        builder: (BuildContext context) {
          return CommonModalWidget(
            modalIcon: AppImages.failed,
            title: AppStringKey.garage_blacklist.tr,
            description: AppStringKey.garage_blacklist_description.tr,
            firstButtonContent: AppStringKey.explore_izhal.tr,
            firstButtonOnTap: () {
              Navigator.pop(context);
            },
          );
        },
      );
    }
  }
}
