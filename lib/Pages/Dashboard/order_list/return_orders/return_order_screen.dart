import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../Utils/pagination_helper.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../core/extensions/common_extension.dart';
import 'return_order_controller.dart';
import 'return_order_mobile.dart';
import 'return_order_web.dart';

class ReturnOrderScreen extends StatefulWidget {
  const ReturnOrderScreen({super.key});

  @override
  State<ReturnOrderScreen> createState() => _ReturnOrderScreenState();
}

class _ReturnOrderScreenState extends State<ReturnOrderScreen> {
  final controller = Get.find<ReturnOrderController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.getReturnOrderData();
    });

    controller.paginationHelper = PaginationHelper(
      loadData: (isOngoing, page, limit, refresh) {
        controller.paginationHelper.pageNumber++;
        controller.getReturnOrderData();
      },
      limit: pageLimit,
    );
  }

  @override
  Widget build(BuildContext context) {
    return context.isDesktop
        ? ReturnOrderWeb(
            controller: controller,
          )
        : ReturnOrderMobile(
            controller: controller,
          );
  }
}
