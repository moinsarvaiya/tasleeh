import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../../Utils/utilities.dart';
import '../../../../common/common_order_card.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/app_images.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../l10n/app_string_key.dart';
import '../../custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import '../requested_order/order_controller.dart';
import '../requested_order/requested_order_mobile_screen.dart';
import 'return_order_controller.dart';

class ReturnOrderMobile extends StatelessWidget {
  final ReturnOrderController controller;

  const ReturnOrderMobile({
    super.key,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStringKey.return_order_list.tr,
                    style: BaseStyle.textStyleRobotoBold(
                      18,
                      AppColors.inputFieldHeading,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    AppStringKey.order_list_desc.tr,
                    style: BaseStyle.textStyleRobotoRegular(
                      12,
                      AppColors.inputFieldHeading,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Theme(
                    data: context.theme.copyWith(useMaterial3: false),
                    child: IntrinsicWidth(
                      child: Material(
                        color: Colors.transparent,
                        child: Ink(
                          child: InkWell(
                            onTap: () {
                              filterBottomSheet(context, OrderStatus.returned);
                            },
                            child: Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.black,
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.circular(4.0),
                              ),
                              child: SvgPicture.asset(
                                AppImages.filter,
                                width: 20,
                                height: 20,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Ink(
                    child: InkWell(
                      onTap: () {
                        Get.find<OrderController>().isSearching.value = !Get.find<OrderController>().isSearching.value;
                      },
                      child: Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: AppColors.primaryTextColor,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        child: Icon(
                          Get.find<OrderController>().isSearching.value ? Icons.close : Icons.search,
                          size: 20,
                          color: AppColors.primaryTextColor,
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
          Get.find<OrderController>().isSearching.value
              ? const SizedBox(
                  height: 16,
                )
              : const SizedBox.shrink(),
          Get.find<OrderController>().isSearching.value
              ? TextFormField(
                  controller: controller.searchOrderId,
                  style: BaseStyle.textStyleRobotoRegular(
                    10,
                    AppColors.primaryTextColor,
                  ),
                  decoration: InputDecoration(
                    fillColor: AppColors.colorWhite,
                    filled: true,
                    contentPadding: const EdgeInsets.symmetric(
                      vertical: 4,
                      horizontal: 8,
                    ),
                    constraints: const BoxConstraints(maxHeight: 40),
                    suffixIcon: MouseRegion(
                      cursor: SystemMouseCursors.click,
                      child: GestureDetector(
                        onTap: () {
                          Get.find<OrderController>().isSearching.value = !Get.find<OrderController>().isSearching.value;
                          controller.searchOrderId.clear();
                        },
                        child: const Icon(
                          Icons.close,
                          size: 12,
                        ),
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(2),
                      borderSide: const BorderSide(
                        color: AppColors.borderColor,
                        width: 0,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(2),
                      borderSide: const BorderSide(
                        color: AppColors.borderColor,
                        width: 0,
                      ),
                    ),
                  ),
                  focusNode: Get.find<OrderController>().orderSearchFocus,
                  autofocus: true,
                  onChanged: (value) {
                    Utilities.customDebounce(customDebounceTime, () {
                      controller.searchReturnOrderData();
                    });
                  },
                )
              : const SizedBox.shrink(),
          const SizedBox(
            height: 16,
          ),
          Expanded(
            child: controller.returnOrderData.isNotEmpty
                ? Stack(
                    children: [
                      ListView.builder(
                        controller: controller.paginationHelper.scrollController,
                        shrinkWrap: true,
                        itemCount: controller.returnOrderData.length,
                        itemBuilder: (context, index) {
                          return GestureDetector(
                            onTap: () {
                              var controller = Get.find<CustomDashboardStepperController>();
                              controller.isFromActiveOrder = false;
                              if (controller.customStepperList[controller.stepperSelectedIndex.value]
                                      .subList[controller.orderListItemSelectedIndex.value].childWidget.length >
                                  1) {
                                controller.customStepperList[controller.stepperSelectedIndex.value]
                                    .subList[controller.orderListItemSelectedIndex.value].childActiveIndex++;
                                controller.customStepperList.refresh();
                              }
                            },
                            child: CommonOrderCard(
                              topLeftHeading: controller.returnOrderData[index].customerName!,
                              topLeftSubHeading: "#${controller.returnOrderData[index].orderId!}",
                              topRightHeading: controller.returnOrderData[index].servicePrice!,
                              topRightSubHeading: controller.returnOrderData[index].orderTime!,
                              centerLeftIcon: const Icon(
                                Icons.warehouse_outlined,
                                size: 12,
                                color: AppColors.secondaryTextColor,
                              ),
                              centerLeftHeading: AppStringKey.stepper_services.tr,
                              centerLeftContent: controller.returnOrderData[index].serviceSelected!,
                              centerRightIcon: const Icon(
                                Icons.warehouse_outlined,
                                size: 12,
                                color: AppColors.secondaryTextColor,
                              ),
                              centerRightHeading: AppStringKey.vehicle_model.tr,
                              centerRightContent:
                                  "${controller.returnOrderData[index].vehicleMake} ${controller.returnOrderData[index].vehicleModel}",
                              showBottomContent: true,
                              bottomIcon: SvgPicture.asset(
                                AppImages.remarks,
                                width: 12,
                                height: 12,
                                color: AppColors.secondaryTextColor,
                              ),
                              bottomContent: controller.returnOrderData[index].remarks,
                              bottomHeading: AppStringKey.remarks.tr,
                            ),
                          );
                        },
                      ),
                      Obx(
                        () => Get.find<ReturnOrderController>().paginationHelper.isLoading.value
                            ? Positioned(
                                bottom: 0,
                                left: 0,
                                right: 0,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const SizedBox(
                                      width: 20,
                                      height: 20,
                                      child: CircularProgressIndicator(
                                        color: AppColors.primaryCtaColor,
                                        strokeWidth: 3,
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      AppStringKey.pagination_loading.tr,
                                      style: BaseStyle.textStyleRobotoMedium(
                                        16,
                                        AppColors.primaryCtaColor,
                                      ),
                                    )
                                  ],
                                ),
                              )
                            : const SizedBox.expand(),
                      ),
                    ],
                  )
                : noDataFound(),
          ),
        ],
      ),
    );
  }
}
