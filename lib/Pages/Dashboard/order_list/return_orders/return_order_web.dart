import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../../Utils/utilities.dart';
import '../../../../common/common_order_table.dart';
import '../../../../common/custom_dialog.dart';
import '../../../../common/input_text_field.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/app_images.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../l10n/app_string_key.dart';
import '../../custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import '../../custom_dashboard_drawer/widgets/summary_widget.dart';
import '../requested_order/order_controller.dart';
import 'return_order_controller.dart';

class ReturnOrderWeb extends StatelessWidget {
  final ReturnOrderController controller;

  const ReturnOrderWeb({
    super.key,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SummaryWidget(),
        const SizedBox(
          height: 8,
        ),
        Text(
          AppStringKey.above_data_from_the_past.tr,
          style: BaseStyle.textStyleRobotoMediumBold(
            12,
            AppColors.inputFieldHeading,
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              AppStringKey.return_order_list.tr,
              style: BaseStyle.textStyleRobotoSemiBold(
                24,
                AppColors.colorBlack,
              ),
            ),
            IntrinsicWidth(
              child: Material(
                color: Colors.transparent,
                child: Ink(
                  child: InkWell(
                    onTap: () {
                      dashboardOrderListDrawerKey.currentState?.openEndDrawer();
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 15,
                        vertical: 6.5,
                      ),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 1.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            AppImages.filter,
                            width: 24,
                            height: 24,
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Text(
                            AppStringKey.filter.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              16,
                              AppColors.primaryTextColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 24,
        ),
        Obx(
          () => Expanded(
            child: CommonOrderTable(
              scrollController: controller.paginationHelper.scrollController,
              columnSpacing: 10,
              padding: const EdgeInsets.all(0),
              columns: [
                DataColumn2(
                  fixedWidth: 132,
                  label: Obx(
                    () => Get.find<OrderController>().isSearching.value
                        ? TextFormField(
                            controller: controller.searchOrderId,
                            style: BaseStyle.textStyleRobotoRegular(
                              10,
                              AppColors.primaryTextColor,
                            ),
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric(
                                vertical: 4,
                                horizontal: 8,
                              ),
                              constraints: const BoxConstraints(maxHeight: 40),
                              suffixIcon: MouseRegion(
                                cursor: SystemMouseCursors.click,
                                child: GestureDetector(
                                  onTap: () {
                                    Get.find<OrderController>().isSearching.value = !Get.find<OrderController>().isSearching.value;
                                    controller.searchOrderId.clear();
                                  },
                                  child: const Icon(
                                    Icons.close,
                                    size: 12,
                                  ),
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(2),
                                borderSide: const BorderSide(
                                  color: AppColors.borderColor,
                                  width: 0,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(2),
                                borderSide: const BorderSide(
                                  color: AppColors.borderColor,
                                  width: 0,
                                ),
                              ),
                            ),
                            autofocus: true,
                            onChanged: (value) {
                              Utilities.customDebounce(customDebounceTime, () {
                                controller.searchReturnOrderData();
                              });
                            },
                          )
                        : Row(
                            children: [
                              Text(
                                AppStringKey.order_details.tr,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              ),
                              const SizedBox(
                                width: 4,
                              ),
                              Material(
                                child: Ink(
                                  child: InkWell(
                                    onTap: () {
                                      Get.find<OrderController>().isSearching.value = !Get.find<OrderController>().isSearching.value;
                                    },
                                    child: const Icon(
                                      Icons.search,
                                      size: 20,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.vehicle_model.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.service_selected.tr.capitalizeFirst!,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.pickup_drop_details.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Container(
                    margin: const EdgeInsets.only(left: 15),
                    child: Text(
                      AppStringKey.remarks.tr,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  fixedWidth: 100,
                  label: Text(
                    AppStringKey.status.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
              ],
              data: List<DataRow2>.generate(
                controller.returnOrderData.length,
                (index) => DataRow2(
                  cells: [
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          MouseRegion(
                            cursor: SystemMouseCursors.click,
                            child: GestureDetector(
                              onTap: () {
                                var controller = Get.find<CustomDashboardStepperController>();
                                controller.isFromActiveOrder = false;

                                controller.orderId = this.controller.returnOrderData[index].orderId!;
                                if (controller.customStepperList[controller.stepperSelectedIndex.value]
                                        .subList[controller.orderListItemSelectedIndex.value].childWidget.length >
                                    1) {
                                  controller.customStepperList[controller.stepperSelectedIndex.value]
                                      .subList[controller.orderListItemSelectedIndex.value].childActiveIndex++;
                                  controller.customStepperList.refresh();
                                }
                              },
                              child: Text(
                                '#${controller.returnOrderData[index].orderId!}',
                                style: BaseStyle.textStyleRobotoSemiBold(
                                  14,
                                  AppColors.primaryTextColor,
                                ).copyWith(
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                          ),
                          Text(
                            "${controller.returnOrderData[index].orderTime} Hrs, ${controller.returnOrderData[index].orderDate}",
                            style: BaseStyle.textStyleRobotoMediumBold(
                              12,
                              AppColors.primaryTextColor,
                            ),
                          )
                        ],
                      ),
                    ),
                    DataCell(
                      Text(
                        "${controller.returnOrderData[index].vehicleMake} ${controller.returnOrderData[index].vehicleModel}",
                        style: BaseStyle.textStyleRobotoMediumBold(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      ),
                    ),
                    DataCell(
                      Text(
                        controller.returnOrderData[index].serviceSelected!,
                        style: BaseStyle.textStyleRobotoMediumBold(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      ),
                    ),
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller.returnOrderData[index].pickUpDropAddress!,
                            style: BaseStyle.textStyleRobotoSemiBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                          ),
                          Text(
                            "${controller.returnOrderData[index].timeSlot}, ${controller.returnOrderData[index].pickUpDropDate}",
                            style: BaseStyle.textStyleRobotoMediumBold(
                              12,
                              AppColors.primaryTextColor,
                            ),
                          )
                        ],
                      ),
                    ),
                    DataCell(
                      Container(
                        margin: const EdgeInsets.only(left: 15),
                        child: Text(
                          controller.returnOrderData[index].remarks!,
                          style: BaseStyle.textStyleRobotoMediumBold(
                            14,
                            AppColors.primaryTextColor,
                          ),
                        ),
                      ),
                    ),
                    DataCell(
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return CustomDialog(
                                    padding: const EdgeInsets.only(
                                      left: 32,
                                      right: 32,
                                      top: 32,
                                      bottom: 24,
                                    ),
                                    maxHeight: 250,
                                    content: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Flexible(
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    '${AppStringKey.pickup_for_return_order.tr} #${controller.returnOrderData[index].orderId}',
                                                    style: BaseStyle.textStyleRobotoSemiBold(
                                                      16,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                    overflow: TextOverflow.ellipsis,
                                                  ),
                                                  const SizedBox(
                                                    height: 8,
                                                  ),
                                                  Text(
                                                    AppStringKey.will_be_able_to_pick_order.tr,
                                                    style: BaseStyle.textStyleRobotoRegular(
                                                      14,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                    overflow: TextOverflow.ellipsis,
                                                  ),
                                                  Text(
                                                    "${AppStringKey.pickup.tr}: ${controller.returnOrderData[index].pickUpDropAddress}",
                                                    style: BaseStyle.textStyleRobotoRegular(
                                                      14,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                  ),
                                                  Text(
                                                    "${AppStringKey.slot.tr}: ${controller.returnOrderData[index].timeSlot}",
                                                    style: BaseStyle.textStyleRobotoRegular(
                                                      14,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                    overflow: TextOverflow.ellipsis,
                                                  ),
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              width: 16,
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          height: 24,
                                        ),
                                        Wrap(
                                          spacing: 8,
                                          alignment: WrapAlignment.start,
                                          children: [
                                            TextButton(
                                              onPressed: () {
                                                controller.acceptOrder(controller.returnOrderData[index].orderId!, true);
                                                Navigator.pop(context);
                                                Utilities.showToast(
                                                  "${AppStringKey.dashboard_return_order.tr} : ${AppStringKey.pickup.tr}\n${AppStringKey.you_need_to_pickup_order.tr} ${controller.returnOrderData[index].orderId}\n${AppStringKey.at.tr} ${controller.returnOrderData[index].pickUpDropAddress} ${AppStringKey.between.tr} ${controller.returnOrderData[index].timeSlot}.",
                                                  fontSize: 16,
                                                  webPosition: 'right',
                                                  webBgColor: '#DDF3EAFF',
                                                  textColor: AppColors.secondaryTextColor,
                                                );
                                              },
                                              style: TextButton.styleFrom(
                                                textStyle: BaseStyle.textStyleRobotoRegular(
                                                  14,
                                                  const Color.fromARGB(255, 22, 11, 11),
                                                ),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(
                                                    2,
                                                  ),
                                                ),
                                                foregroundColor: AppColors.colorWhite,
                                                backgroundColor: AppColors.primaryTextColor,
                                              ),
                                              child: Text(AppStringKey.yes.tr),
                                            ),
                                            TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);
                                                showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return CustomDialog(
                                                      padding: const EdgeInsets.only(
                                                        left: 32,
                                                        top: 32,
                                                        right: 32,
                                                        bottom: 24,
                                                      ),
                                                      maxWidth: 416,
                                                      maxHeight: 280,
                                                      content: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          Row(
                                                            mainAxisSize: MainAxisSize.min,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: [
                                                              Flexible(
                                                                child: Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  children: [
                                                                    Text(
                                                                      AppStringKey.cancel_pickup_for_return_order.tr,
                                                                      style: BaseStyle.textStyleRobotoSemiBold(
                                                                        16,
                                                                        AppColors.primaryTextColor,
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                      '#${controller.returnOrderData[index].orderId}',
                                                                      style: BaseStyle.textStyleRobotoSemiBold(
                                                                        16,
                                                                        AppColors.primaryTextColor,
                                                                      ),
                                                                    ),
                                                                    const SizedBox(
                                                                      height: 8,
                                                                    ),
                                                                    Text(
                                                                      AppStringKey.are_you_sure_cancel_return_order.tr,
                                                                      style: BaseStyle.textStyleRobotoRegular(
                                                                        14,
                                                                        AppColors.primaryTextColor,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                              const SizedBox(
                                                                width: 16,
                                                              ),
                                                            ],
                                                          ),
                                                          const SizedBox(
                                                            height: 24,
                                                          ),
                                                          TextButton(
                                                            onPressed: () {
                                                              controller.acceptOrder(controller.returnOrderData[index].orderId!, false);
                                                              Navigator.pop(context);
                                                              Utilities.showToast(
                                                                "${AppStringKey.dashboard_return_order.tr} : ${AppStringKey.third_party.tr}\n${AppStringKey.you_need_to_pickup_order.tr} ${controller.returnOrderData[index].orderId}\n${AppStringKey.at.tr} ${controller.returnOrderData[index].pickUpDropAddress} ${AppStringKey.between.tr} ${controller.returnOrderData[index].timeSlot}.",
                                                                fontSize: 16,
                                                                webPosition: 'right',
                                                                webBgColor: '#DDF3EAFF',
                                                                textColor: AppColors.secondaryTextColor,
                                                              );
                                                            },
                                                            style: TextButton.styleFrom(
                                                              textStyle: BaseStyle.textStyleRobotoRegular(
                                                                14,
                                                                AppColors.colorWhite,
                                                              ),
                                                              shape: RoundedRectangleBorder(
                                                                borderRadius: BorderRadius.circular(
                                                                  2,
                                                                ),
                                                              ),
                                                              foregroundColor: AppColors.colorWhite,
                                                              backgroundColor: AppColors.primaryTextColor,
                                                            ),
                                                            child: Text(AppStringKey.confirm.tr),
                                                          )
                                                        ],
                                                      ),
                                                    );
                                                  },
                                                );
                                              },
                                              style: TextButton.styleFrom(
                                                textStyle: BaseStyle.textStyleRobotoRegular(
                                                  14,
                                                  AppColors.colorWhite,
                                                ),
                                                side: const BorderSide(
                                                  color: AppColors.strokeColor,
                                                  width: 1,
                                                ),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius: BorderRadius.circular(
                                                    2,
                                                  ),
                                                ),
                                                foregroundColor: AppColors.primaryTextColor,
                                                backgroundColor: AppColors.colorWhite,
                                              ),
                                              child: Text(AppStringKey.no.tr),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  );
                                },
                              );
                            },
                            splashRadius: 1,
                            iconSize: 30,
                            color: AppColors.successTextGreenColor,
                            icon: SvgPicture.asset(AppImages.check),
                          ),
                          IconButton(
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return CustomDialog(
                                    padding: const EdgeInsets.only(
                                      top: 32,
                                      right: 32,
                                      bottom: 24,
                                      left: 32,
                                    ),
                                    maxWidth: 420,
                                    maxHeight: 350,
                                    content: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Flexible(
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    AppStringKey.decline_return_service_for_order.tr,
                                                    style: BaseStyle.textStyleRobotoSemiBold(
                                                      16,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                  ),
                                                  Text(
                                                    '#${controller.returnOrderData[index].orderId}',
                                                    style: BaseStyle.textStyleRobotoSemiBold(
                                                      16,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    height: 8,
                                                  ),
                                                  Text(
                                                      "${AppStringKey.specify_reason_for_order_decline.tr} #${controller.returnOrderData[index].orderId}")
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              width: 16,
                                            ),
                                          ],
                                        ),
                                        InputTextField(
                                          hintText: AppStringKey.enter_reason.tr,
                                          label: '',
                                          maxLength: 100,
                                          maxLines: 3,
                                          textInputType: TextInputType.multiline,
                                          isRequireField: false,
                                          textEditingController: controller.orderRejectedRemarks,
                                        ),
                                        const SizedBox(
                                          height: 24,
                                        ),
                                        TextButton(
                                          onPressed: () {
                                            controller.rejectOrder(controller.returnOrderData[index].orderId!);
                                            Navigator.pop(context);
                                          },
                                          style: TextButton.styleFrom(
                                            textStyle: BaseStyle.textStyleRobotoRegular(
                                              14,
                                              AppColors.colorWhite,
                                            ),
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(
                                                2,
                                              ),
                                            ),
                                            foregroundColor: AppColors.colorWhite,
                                            backgroundColor: AppColors.primaryTextColor,
                                          ),
                                          child: Text(AppStringKey.confirm.tr),
                                        )
                                      ],
                                    ),
                                  );
                                },
                              );
                            },
                            splashRadius: 1,
                            iconSize: 30,
                            color: AppColors.errorTextColor,
                            icon: SvgPicture.asset(AppImages.cross),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
