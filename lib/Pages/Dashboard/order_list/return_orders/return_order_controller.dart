import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../Utils/pagination_helper.dart';
import '../../../../api/api_end_point.dart';
import '../../../../api/api_interface.dart';
import '../../../../api/api_presenter.dart';
import '../../../../core/app_preference/app_preferences.dart';
import '../../../../core/app_preference/storage_keys.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../models/accept_order_data_model.dart';
import '../../../../models/return_order_data_model.dart';
import '../../custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';

class ReturnOrderController extends GetxController implements ApiCallBacks {
  late PaginationHelper paginationHelper;

  TextEditingController orderRejectedRemarks = TextEditingController();

  RxList<ReturnOrderDataModel> returnOrderData = RxList([]);

  AcceptOrderDataModel acceptOrderData = AcceptOrderDataModel();

  TextEditingController searchOrderId = TextEditingController();

  void getReturnOrderData() {
    ApiPresenter(this).getReturnOrderData(
      AppPreferences.sharedPrefRead(StorageKeys.garageId),
      paginationHelper.pageNumber.toString(),
      pageLimit.toString(),
      '',
    );
  }

  void searchReturnOrderData() {
    ApiPresenter(this).getReturnOrderData(
      AppPreferences.sharedPrefRead(StorageKeys.garageId),
      paginationHelper.pageNumber.toString(),
      pageLimit.toString(),
      searchOrderId.text,
    );
  }

  void acceptOrder(String orderId, bool isPickup) {
    acceptOrderData.status = workStatusType[WorkStatusType.accepted];
    acceptOrderData.remarks = "";
    acceptOrderData.isThirdPartyAssigned = !isPickup;
    ApiPresenter(this).acceptReturnOrder(acceptOrderData.toJson(), orderId);
  }

  void rejectOrder(String orderId) {
    acceptOrderData.status = workStatusType[WorkStatusType.rejected];
    acceptOrderData.remarks = orderRejectedRemarks.text;
    acceptOrderData.isThirdPartyAssigned = false;
    ApiPresenter(this).acceptReturnOrder(acceptOrderData.toJson(), orderId);
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {}

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {}

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.getReturnOrderData:
        if (paginationHelper.pageNumber == 1) {
          returnOrderData.clear();
        }
        RxList<ReturnOrderDataModel> pageRequestedList = RxList();
        for (var item in object['data']['results']) {
          pageRequestedList.add(
            ReturnOrderDataModel(
              customerName: item['name'],
              orderId: item['bookingId'],
              orderDate: item['bookingDate'],
              orderTime: '11:58',
              vehicleMake: item['make'],
              vehicleModel: item['model'],
              serviceSelected: item['services'].length > 1
                  ? "${item['services'][0]['serviceDetail']} + ${item['services'].length - 1}"
                  : "${item['services'][0]['serviceDetail']}",
              pickUpDropAddress: "${item['distance']}m | ${item['pickupAddress']}",
              timeSlot: item['timeSlot'],
              pickUpDropDate: item['pickupDate'],
              remarks: item['remarks'],
              servicePrice: item['servicePrice'],
            ),
          );
        }
        if (pageRequestedList.isNotEmpty) {
          if (pageRequestedList.length < pageLimit) {
            paginationHelper.isLastPage = true;
          }
          returnOrderData.addAll(pageRequestedList);
          paginationHelper.isLoading.value = false;
          returnOrderData.refresh();
        } else {
          paginationHelper.isLastPage = true;
        }
        break;

      case ApiEndPoints.acceptReturnOrder:
        getReturnOrderData();
        Get.find<CustomDashboardStepperController>().updateGarageProfile();
        break;
    }
  }

  void clearData() {
    returnOrderData = RxList([]);
  }
}
