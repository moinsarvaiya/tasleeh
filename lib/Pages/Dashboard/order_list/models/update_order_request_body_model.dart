class UpdateOrderServiceData {
  ServiceDetail serviceDetail;
  List<ServiceDetailLevel> serviceDetailLevel;

  UpdateOrderServiceData({required this.serviceDetail, required this.serviceDetailLevel});

  factory UpdateOrderServiceData.fromJson(Map<String, dynamic> json) {
    return UpdateOrderServiceData(
      serviceDetail: ServiceDetail.fromJson(json['serviceDetail']),
      serviceDetailLevel: (json['serviceDetailLevel'] as List).map((level) => ServiceDetailLevel.fromJson(level)).toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'serviceDetail': serviceDetail.toJson(),
      'serviceDetailLevel': serviceDetailLevel.map((level) => level.toJson()).toList(),
    };
  }
}

class ServiceDetail {
  String id;

  ServiceDetail({required this.id});

  factory ServiceDetail.fromJson(Map<String, dynamic> json) {
    return ServiceDetail(id: json['id']);
  }

  Map<String, dynamic> toJson() {
    return {'id': id};
  }
}

class ServiceDetailLevel {
  String id;

  ServiceDetailLevel({required this.id});

  factory ServiceDetailLevel.fromJson(Map<String, dynamic> json) {
    return ServiceDetailLevel(id: json['id']);
  }

  Map<String, dynamic> toJson() {
    return {'id': id};
  }
}
