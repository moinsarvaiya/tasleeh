import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../api/api_end_point.dart';
import '../../../../api/api_interface.dart';
import '../../../../api/api_presenter.dart';
import '../../../../core/app_preference/app_preferences.dart';
import '../../../../core/app_preference/storage_keys.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../l10n/app_string_key.dart';
import '../../../../models/accept_order_data_model.dart';
import '../../custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import '../active_orders/models/update_order_category_model.dart';
import '../active_orders/models/update_order_sub_category_level_model.dart';
import '../active_orders/models/update_order_sub_category_model.dart';
import '../models/update_order_request_body_model.dart';
import 'order_status_model.dart';

class ReturnOrderDetailController extends GetxController implements ApiCallBacks {
  String workStatus = AppStringKey.vehicle_received.tr;

  RxString customerName = RxString('');
  RxString contactNumber = RxString('');
  RxString pickUpDropAddress = RxString('');
  RxString timeSlot = RxString('');
  RxString returnRequestDateTime = RxString('');
  RxString serviceRequestDateTime = RxString('');
  RxString remarks = RxString('');
  RxString vehicleModel = RxString('');
  RxList<String> vehicleImages = RxList([]);
  TextEditingController orderRejectedRemarks = TextEditingController();

  RxList<UpdateOrderCategoryModel> serviceCategoryList = RxList();
  RxList<UpdateOrderSubCategoryModel> serviceSubCategoryList = RxList();
  RxList<UpdateOrderSubCategoryLevelModel> serviceLevelList = RxList();

  RxList<String> serviceSelectedCategoryList = RxList();
  RxList<String> serviceSelectedSubCategoryList = RxList();
  RxList<String> serviceSelectedLevelList = RxList();
  Rx<TextEditingController> orderTotalAmountController = TextEditingController().obs;
  FocusNode orderTotalAmountFocusNode = FocusNode();

  RxDouble discountedPrice = 0.0.obs;

  RxList services = RxList();

  RxString totalAmount = RxString('');

  AcceptOrderDataModel acceptOrderData = AcceptOrderDataModel();
  UpdateOrderServiceData updateOrderServiceData = UpdateOrderServiceData(
    serviceDetail: ServiceDetail(id: ''),
    serviceDetailLevel: [],
  );
  bool isFromGet = true;
  RxString selectedOrderStatusValue = AppStringKey.vehicle_in_transit.tr.obs;
  RxBool isOrderStatusWidgetVisible = false.obs;
  int selectedOrderStatusIndex = 0;
  String selectedOrderId = '';
  RxList<OrderStatusModel> orderStatusList = RxList();
  OverlayPortalController overlayPortalController = OverlayPortalController();
  LayerLink layerLink = LayerLink();
  FocusNode focusNode = FocusNode();
  bool isPaymentStatusFromGet = true;
  bool isPaymentUpdated = false;

  void setOrderStatusInModel(String status) {
    orderStatusList.add(
      OrderStatusModel(
        orderStatusKey: Get.find<CustomDashboardStepperController>().isReturnOrder
            ? workStatusType[WorkStatusType.warrantyClaimed]!
            : workStatusType[WorkStatusType.accepted]!,
        orderStatusValue: AppStringKey.vehicle_in_transit.tr,
        isOrderStatusCompleted: false,
      ),
    );
    orderStatusList.add(
      OrderStatusModel(
        orderStatusKey: workStatusType[WorkStatusType.vehicleReached]!,
        orderStatusValue: AppStringKey.vehicle_reached.tr,
        isOrderStatusCompleted: false,
      ),
    );
    orderStatusList.add(
      OrderStatusModel(
        orderStatusKey: workStatusType[WorkStatusType.serviceStarted]!,
        orderStatusValue: AppStringKey.service_started.tr,
        isOrderStatusCompleted: false,
      ),
    );
    if (!Get.find<CustomDashboardStepperController>().isReturnOrder) {
      orderStatusList.add(
        OrderStatusModel(
          orderStatusKey: workStatusType[WorkStatusType.paymentPending]!,
          orderStatusValue: AppStringKey.payment_pending.tr,
          isOrderStatusCompleted: false,
        ),
      );
    }
    orderStatusList.add(
      OrderStatusModel(
        orderStatusKey: workStatusType[WorkStatusType.outForDelivery]!,
        orderStatusValue: AppStringKey.out_for_delivery.tr,
        isOrderStatusCompleted: false,
      ),
    );
    orderStatusList.add(
      OrderStatusModel(
        orderStatusKey: workStatusType[WorkStatusType.delivered]!,
        orderStatusValue: AppStringKey.delivered.tr,
        isOrderStatusCompleted: false,
      ),
    );
    selectedOrderStatusValue.value = orderStatusList.firstWhere((element) => element.orderStatusKey == status).orderStatusValue;
    updateOrderStatusInModel(status);
  }

  void getOrderDetails() {
    isFromGet = true;
    if (Get.find<CustomDashboardStepperController>().isFromActiveOrder) {
      if (Get.find<CustomDashboardStepperController>().isReturnOrder) {
        ApiPresenter(this).getReturnOrderDetails(Get.find<CustomDashboardStepperController>().orderId);
      } else {
        ApiPresenter(this).getActiveOrderDetailsData(Get.find<CustomDashboardStepperController>().orderId);
      }
 
    } else {
      ApiPresenter(this).getReturnOrderDetails(Get.find<CustomDashboardStepperController>().orderId);
    }
  }

  void updateOrder() {
    isFromGet = false;
    ApiPresenter(this).updateOrderDetail(
      Get.find<CustomDashboardStepperController>().orderId,
      updateOrderServiceData.toJson(),
    );
  }

  void acceptOrder(String orderId, bool isPickup) {
    acceptOrderData.status = workStatusType[WorkStatusType.accepted];
    acceptOrderData.remarks = "";
    acceptOrderData.isThirdPartyAssigned = !isPickup;
    ApiPresenter(this).acceptReturnOrder(acceptOrderData.toJson(), orderId);
  }

  void rejectOrder(String orderId) {
    acceptOrderData.status = workStatusType[WorkStatusType.rejected];
    acceptOrderData.remarks = orderRejectedRemarks.text;
    acceptOrderData.isThirdPartyAssigned = false;
    ApiPresenter(this).acceptReturnOrder(acceptOrderData.toJson(), orderId);
  }

  void updateOrderStatus(String orderId, String status) {
    acceptOrderData.status = orderUpdateStatus(status);
    acceptOrderData.remarks = orderRejectedRemarks.text;
    acceptOrderData.isThirdPartyAssigned = false;
    ApiPresenter(this).acceptRequestOrder(acceptOrderData.toJson(), orderId);
  }

  void updateReturnOrderStatus(String orderId, String status) {
    acceptOrderData.status = orderUpdateStatus(status);
    acceptOrderData.remarks = orderRejectedRemarks.text;
    acceptOrderData.isThirdPartyAssigned = false;
    ApiPresenter(this).updateReturnOrder(acceptOrderData.toJson(), orderId);
  }

  String? orderUpdateStatus(String value) {
    if (value == AppStringKey.vehicle_received.tr) {
      return workStatusType[WorkStatusType.vehicleReached];
    } else if (value == AppStringKey.service_started.tr) {
      return workStatusType[WorkStatusType.serviceStarted];
    } else if (value == AppStringKey.vehicle_in_transit.tr) {
      return workStatusType[WorkStatusType.vehicleInTransit];
    } else if (value == AppStringKey.service_completed.tr) {
      return workStatusType[WorkStatusType.delivered];
    } else if (value == AppStringKey.delivered.tr) {
      return workStatusType[WorkStatusType.delivered];
    } else if (value == AppStringKey.payment_pending.tr) {
      return workStatusType[WorkStatusType.paymentPending];
    } else if (value == AppStringKey.out_for_delivery.tr) {
      return workStatusType[WorkStatusType.outForDelivery];
    } else {
      return workStatusType[WorkStatusType.vehicleReached];
    }
  }

  void getCategoryData() {
    ApiPresenter(this).getUpdateOrderCategoryData(AppPreferences.sharedPrefRead(StorageKeys.garageId));
  }

  void getSubCategoryData(String categoryId) {
    ApiPresenter(this).getUpdateOrderSubCategoryData(categoryId);
  }

  void getSubCategoryLevelData(String subCategoryId) {
    ApiPresenter(this).getUpdateOrderSubCategoryLevelData(subCategoryId);
  }

  void getPaymentStatus(String orderID) {
    isPaymentStatusFromGet = true;
    ApiPresenter(this).getPaymentStatus(orderID);
  }

  void updatePaymentStatus(String orderID) {
    isPaymentStatusFromGet = false;
    ApiPresenter(this).updatePaymentStatus(orderID);
  }

  void calculateDiscountedPrice(String price) {
    discountedPrice.value = discountedPrice.value + double.parse(price);
    orderTotalAmountController.value.text = discountedPrice.value.toString();
  }

  void removeDiscountedPrice(String price) {
    discountedPrice.value = discountedPrice.value - double.parse(price);
    orderTotalAmountController.value.text = discountedPrice.value.toString();
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {}

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {}

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.getReturnOrderDetail:
        var data = object['data'];
        customerName.value = data['name'];
        contactNumber.value = data['number'];
        pickUpDropAddress.value = data['pickupAddress'];
        returnRequestDateTime.value = data['warrantyClaimDate'];
        serviceRequestDateTime.value = data['serviceRequestedDate'];
        remarks.value = data['customerComment'];
        vehicleModel.value = "${data['make']} ${data['model']}";
        timeSlot.value = data['timeSlot'];
        for (var url in data['images']) {
          vehicleImages.add(url);
        }
        services = RxList(data['services']);
        totalAmount.value = data['totalPrice'].toString();
        setOrderStatusInModel(data['warrantyStatus']);
        break;
      case ApiEndPoints.getActiveOrderDetail:
        var data = object['data'];
        customerName.value = data['name'];
        contactNumber.value = data['number'];
        pickUpDropAddress.value = data['pickupAddress'];
        serviceRequestDateTime.value = data['timeSlot'];
        vehicleModel.value = "${data['make']} ${data['model']}";
        timeSlot.value = data['timeSlot'];
        services = RxList(data['services']);
        totalAmount.value = data['totalPrice'].toString();
        setOrderStatusInModel(data['status']);
        break;
      case ApiEndPoints.getUpdateOrderCategory:
        serviceCategoryList.clear();
        serviceCategoryList.addAll((object['data'] as List).map((e) => UpdateOrderCategoryModel.fromJson(e)).toList());
        serviceCategoryList.refresh();
        break;
      case ApiEndPoints.updateOrderData:
        getOrderDetails();
        break;
      case ApiEndPoints.getPaymentStatus:
        if (isPaymentStatusFromGet) {
          var isPaymentReceived = object['data']['isPaymentReceived'];
          if (isPaymentReceived) {
            var customStepperController = Get.find<CustomDashboardStepperController>();
            customStepperController.isReturnOrder
                ? updateReturnOrderStatus(
                    selectedOrderId,
                    orderStatusList[selectedOrderStatusIndex].orderStatusValue,
                  )
                : updateOrderStatus(
                    selectedOrderId,
                    orderStatusList[selectedOrderStatusIndex].orderStatusValue,
                  );
          } else {
            showDialog(
                context: Get.context!,
                builder: (BuildContext context) {
                  return Dialog(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16),
                    ),
                    elevation: 0.0,
                    backgroundColor: Colors.transparent,
                    child: IntrinsicWidth(
                      child: Container(
                        margin: allPadding16,
                        padding: allPadding24,
                        constraints: BoxConstraints(
                          maxWidth: (context.isDesktop ? 712 : double.infinity),
                        ),
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              AppStringKey.payment.tr,
                              style: BaseStyle.textStyleRobotoSemiBold(
                                context.isDesktop ? 24 : 18,
                                AppColors.primaryTextColor,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Text(
                              AppStringKey.paymentDesc.tr,
                              style: BaseStyle.textStyleRobotoRegular(
                                14,
                                AppColors.primaryTextColor,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            Row(
                              children: [
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    fixedSize: const Size(140, 45),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(0),
                                    ),
                                    backgroundColor: AppColors.colorBackground,
                                  ),
                                  onPressed: () {
                                    Get.back();
                                  },
                                  child: Text(
                                    AppStringKey.no.tr,
                                    style: BaseStyle.textStyleRobotoSemiBold(
                                      14,
                                      AppColors.colorBlack,
                                    ),
                                  ),
                                ),
                                horizontalMargin16,
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    fixedSize: const Size(140, 45),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(0),
                                    ),
                                    backgroundColor: AppColors.errorTextColor,
                                  ),
                                  onPressed: () {
                                    updatePaymentStatus(selectedOrderId);
                                    Get.back();
                                  },
                                  child: Text(
                                    AppStringKey.yes.tr,
                                    style: BaseStyle.textStyleRobotoSemiBold(
                                      14,
                                      AppColors.colorBackground,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                });
          }
        } else {
          isPaymentUpdated = true;
          var customStepperController = Get.find<CustomDashboardStepperController>();
          customStepperController.isReturnOrder
              ? updateReturnOrderStatus(
                  selectedOrderId,
                  orderStatusList[selectedOrderStatusIndex].orderStatusValue,
                )
              : updateOrderStatus(
                  selectedOrderId,
                  orderStatusList[selectedOrderStatusIndex].orderStatusValue,
                );
        }
        break;
      case ApiEndPoints.getUpdateOrderSubCategory:
        serviceSubCategoryList.clear();
        serviceSubCategoryList.addAll((object['data'] as List).map((e) => UpdateOrderSubCategoryModel.fromJson(e)).toList());
        serviceSubCategoryList.refresh();
        break;
      case ApiEndPoints.getUpdateOrderSubCategoryLevel:
        serviceLevelList.clear();
        serviceLevelList.addAll((object['data'] as List).map((e) => UpdateOrderSubCategoryLevelModel.fromJson(e)).toList());
        serviceLevelList.refresh();
        break;
      case ApiEndPoints.acceptRequestOrder:
        isOrderStatusWidgetVisible.value = false;
        selectedOrderStatusValue.value = orderStatusList[selectedOrderStatusIndex].orderStatusValue;
        updateOrderStatusInModel(orderStatusList[selectedOrderStatusIndex].orderStatusKey);
        if (overlayPortalController.isShowing) {
          overlayPortalController.hide();
 
        }
        Future(() {
          if (acceptOrderData.status == workStatusType[WorkStatusType.delivered]) {
            var controller = Get.find<CustomDashboardStepperController>();
            controller.updateGarageProfile();
            if (controller
                .customStepperList[controller.stepperSelectedIndex.value].subList[controller.orderListItemSelectedIndex.value].childActiveIndex >
                0) {
              controller.updateGarageProfile();
              controller
                  .customStepperList[controller.stepperSelectedIndex.value].subList[controller.orderListItemSelectedIndex.value].childActiveIndex--;
              controller.customStepperList.refresh();
            }
          }
        });

        break;

      case ApiEndPoints.acceptReturnOrder:
        if (acceptOrderData.status == workStatusType[WorkStatusType.delivered]) {
          var controller = Get.find<CustomDashboardStepperController>();
          controller.updateGarageProfile();
          if (controller
                  .customStepperList[controller.stepperSelectedIndex.value].subList[controller.orderListItemSelectedIndex.value].childActiveIndex >
              0) {
            controller.updateGarageProfile();
            controller
                .customStepperList[controller.stepperSelectedIndex.value].subList[controller.orderListItemSelectedIndex.value].childActiveIndex--;
            controller.customStepperList.refresh();
          }
        }
        break;

      case ApiEndPoints.acceptReturnOrder:
        var controller = Get.find<CustomDashboardStepperController>();
        controller.updateGarageProfile();
        if (controller
                .customStepperList[controller.stepperSelectedIndex.value].subList[controller.orderListItemSelectedIndex.value].childActiveIndex >
            0) {
          controller.updateGarageProfile();
          controller.customStepperList[controller.stepperSelectedIndex.value].subList[controller.orderListItemSelectedIndex.value].childActiveIndex--;
          controller.customStepperList.refresh();
        }
        break;
    }
  }

  void addServiceDetailId(String newId) {
    updateOrderServiceData.serviceDetail = ServiceDetail(id: newId);
  }

  void addServiceDetailLevelIdAtPosition(String newId) {
    updateOrderServiceData.serviceDetailLevel.add(ServiceDetailLevel(id: newId));
  }

  void removeServiceDetailId() {
    updateOrderServiceData.serviceDetail.id = '';
  }

  void removeServiceDetailLevelIdAtPosition(int position) {
    if (position >= 0 && position < updateOrderServiceData.serviceDetailLevel.length) {
      updateOrderServiceData.serviceDetailLevel.removeAt(position);
    }
  }

  void updateOrderStatusInModel(String orderKey) {
    bool foundOrderKey = false;
    for (int i = 0; i < orderStatusList.length; i++) {
      OrderStatusModel orderStatus = orderStatusList[i];
      if (orderStatus.orderStatusKey == orderKey) {
        foundOrderKey = true;
        orderStatusList[i] = OrderStatusModel(
          orderStatusKey: orderStatus.orderStatusKey,
          orderStatusValue: orderStatus.orderStatusValue,
          isOrderStatusCompleted: true,
        );
      } else {
        if (!foundOrderKey) {
          orderStatusList[i] = OrderStatusModel(
            orderStatusKey: orderStatus.orderStatusKey,
            orderStatusValue: orderStatus.orderStatusValue,
            isOrderStatusCompleted: true,
          );
        } else {
          orderStatusList[i] = OrderStatusModel(
            orderStatusKey: orderStatus.orderStatusKey,
            orderStatusValue: orderStatus.orderStatusValue,
            isOrderStatusCompleted: false,
          );
        }
      }
    }
    orderStatusList.refresh();
  }
}
