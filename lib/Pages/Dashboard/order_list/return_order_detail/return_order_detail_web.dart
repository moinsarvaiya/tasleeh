import 'package:al_baida_garage_fe/common/space_horizontal.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_network/image_network.dart';

import '../../../../Utils/app_constants.dart';
import '../../../../Utils/utilities.dart';
import '../../../../common/custom_dialog.dart';
import '../../../../common/input_text_field.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../l10n/app_string_key.dart';
import '../../custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import 'return_order_detail_controller.dart';
import 'total_bill_accordian.dart';

class ReturnOrderDetailWeb extends StatelessWidget {
  final String orderId;
  final ReturnOrderDetailController controller;

  const ReturnOrderDetailWeb({
    super.key,
    required this.orderId,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    var customStepperController = Get.find<CustomDashboardStepperController>();
    return Scaffold(
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: GestureDetector(
          onTap: () {
            controller.overlayPortalController.hide();
          },
          child: Container(
            color: AppColors.primaryBackgroundColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                MouseRegion(
                  cursor: SystemMouseCursors.click,
                  child: GestureDetector(
                    onTap: () {
                      if (controller.overlayPortalController.isShowing) {
                        controller.overlayPortalController.hide();
                      } else {
                        var customDashboardStepperController =
                            Get.find<CustomDashboardStepperController>();
                        if (customDashboardStepperController
                                .customStepperList[
                                    customDashboardStepperController
                                        .stepperSelectedIndex.value]
                                .subList[customDashboardStepperController
                                    .orderListItemSelectedIndex.value]
                                .childActiveIndex >
                            0) {
                          customDashboardStepperController
                              .customStepperList[
                                  customDashboardStepperController
                                      .stepperSelectedIndex.value]
                              .subList[customDashboardStepperController
                                  .orderListItemSelectedIndex.value]
                              .childActiveIndex--;
                          customDashboardStepperController.customStepperList
                              .refresh();
                        }
                      }
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const Icon(
                          Icons.chevron_left,
                          color: AppColors.cardTextColor,
                          size: 16,
                        ),
                        const SizedBox(
                          width: 4,
                        ),
                        Text(
                          AppStringKey.go_back_to_dashboard.tr,
                          style: BaseStyle.textStyleRobotoRegular(
                            14,
                            AppColors.cardTextColor,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 32,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      "${customStepperController.isFromActiveOrder ? AppStringKey.active_order_detail.tr : AppStringKey.return_order_detail.tr} #$orderId",
                      style: BaseStyle.textStyleRobotoSemiBold(
                        24,
                        AppColors.primaryTextColor,
                      ),
                    ),
                    Row(
                      children: [
                        Get.find<CustomDashboardStepperController>()
                                .isFromActiveOrder
                            ? Wrap(
                                alignment: WrapAlignment.center,
                                crossAxisAlignment: WrapCrossAlignment.center,
                                spacing: 16,
                                children: [
                                  Text(
                                    AppStringKey.work_status.tr,
                                    style: BaseStyle.textStyleRobotoSemiBold(
                                      16,
                                      AppColors.cardTextColor,
                                    ),
                                  ),
                                  OrderStatusWidget(
                                    controller: controller,
                                    orderId: orderId,
                                    customStepperController:
                                        customStepperController,
                                  )
                                ],
                              )
                            : const SizedBox.shrink(),
                        const SpaceHorizontal(0.001)
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  height: 32,
                ),
                Obx(
                  () => Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                            vertical: 32,
                            horizontal: 24,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: AppColors.strokeColor,
                              width: 1,
                            ),
                            color: AppColors.colorWhite,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppStringKey.customer_info.tr,
                                style: BaseStyle.textStyleRobotoSemiBold(
                                  20,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Text(
                                AppStringKey
                                    .below_contains_customer_detail_info.tr,
                                style: BaseStyle.textStyleRobotoRegular(
                                  14,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              const SizedBox(
                                height: 12,
                              ),
                              Wrap(
                                spacing: 18,
                                runSpacing: 18,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        AppStringKey.name.tr,
                                        style: BaseStyle.textStyleRobotoRegular(
                                          14,
                                          AppColors.secondaryTextColor,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 7,
                                      ),
                                      Text(
                                        controller.customerName.value,
                                        style: BaseStyle.textStyleRobotoRegular(
                                          16,
                                          AppColors.primaryTextColor,
                                        ),
                                      )
                                    ],
                                  ),
                                  Container(
                                    width: 1,
                                    height: 48,
                                    color: AppColors.dividerColor,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        AppStringKey.contact_number.tr,
                                        style: BaseStyle.textStyleRobotoRegular(
                                          14,
                                          AppColors.secondaryTextColor,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 7,
                                      ),
                                      Text(
                                        controller.contactNumber.value,
                                        style: BaseStyle.textStyleRobotoRegular(
                                          16,
                                          AppColors.primaryTextColor,
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 18,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    AppStringKey.pickup_address.tr,
                                    style: BaseStyle.textStyleRobotoRegular(
                                      14,
                                      AppColors.secondaryTextColor,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 7,
                                  ),
                                  Text(
                                    controller.pickUpDropAddress.value,
                                    style: BaseStyle.textStyleRobotoRegular(
                                      16,
                                      AppColors.primaryTextColor,
                                    ),
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Container(
                                color: AppColors.dividerColor,
                                height: 1,
                                width: double.maxFinite,
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Text(
                                AppStringKey.service_info.tr,
                                style: BaseStyle.textStyleRobotoSemiBold(
                                  20,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Text(
                                AppStringKey
                                    .below_contains_service_detail_info.tr,
                                style: BaseStyle.textStyleRobotoRegular(
                                  14,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Wrap(
                                spacing:
                                    customStepperController.isFromActiveOrder
                                        ? 0
                                        : 18,
                                runSpacing: 18,
                                children: [
                                  customStepperController.isFromActiveOrder
                                      ? const SizedBox.shrink()
                                      : Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              AppStringKey.return_request.tr,
                                              style: BaseStyle
                                                  .textStyleRobotoRegular(
                                                14,
                                                AppColors.secondaryTextColor,
                                              ),
                                            ),
                                            const SizedBox(
                                              height: 7,
                                            ),
                                            Text(
                                              controller
                                                  .returnRequestDateTime.value,
                                              style: BaseStyle
                                                  .textStyleRobotoRegular(
                                                16,
                                                AppColors.primaryTextColor,
                                              ),
                                            )
                                          ],
                                        ),
                                  customStepperController.isFromActiveOrder
                                      ? const SizedBox.shrink()
                                      : Container(
                                          width: 1,
                                          height: 48,
                                          color: AppColors.dividerColor,
                                        ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        AppStringKey.service_request.tr,
                                        style: BaseStyle.textStyleRobotoRegular(
                                          14,
                                          AppColors.secondaryTextColor,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 7,
                                      ),
                                      Text(
                                        controller.serviceRequestDateTime.value,
                                        style: BaseStyle.textStyleRobotoRegular(
                                          16,
                                          AppColors.primaryTextColor,
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 18,
                              ),
                              customStepperController.isFromActiveOrder
                                  ? const SizedBox.shrink()
                                  : Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          AppStringKey.remarks.tr,
                                          style:
                                              BaseStyle.textStyleRobotoRegular(
                                            14,
                                            AppColors.secondaryTextColor,
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 7,
                                        ),
                                        Text(
                                          controller.remarks.value,
                                          style:
                                              BaseStyle.textStyleRobotoRegular(
                                            16,
                                            AppColors.primaryTextColor,
                                          ),
                                        )
                                      ],
                                    ),
                              const SizedBox(
                                height: 16,
                              ),
                              Container(
                                color: AppColors.dividerColor,
                                height: 1,
                                width: double.maxFinite,
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Text(
                                AppStringKey.vehicle_info.tr,
                                style: BaseStyle.textStyleRobotoSemiBold(
                                  20,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Text(
                                AppStringKey
                                    .below_contains_vehicle_detail_info.tr,
                                style: BaseStyle.textStyleRobotoRegular(
                                  14,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              const SizedBox(
                                height: 24,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    AppStringKey.vehicle_model.tr,
                                    style: BaseStyle.textStyleRobotoRegular(
                                      14,
                                      AppColors.secondaryTextColor,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 7,
                                  ),
                                  Text(
                                    controller.vehicleModel.value,
                                    style: BaseStyle.textStyleRobotoRegular(
                                      16,
                                      AppColors.primaryTextColor,
                                    ),
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              customStepperController.isFromActiveOrder
                                  ? const SizedBox.shrink()
                                  : Text(
                                      "${AppStringKey.vehicle_images.tr}(${controller.vehicleImages.length})",
                                      style: BaseStyle.textStyleRobotoSemiBold(
                                        20,
                                        AppColors.primaryTextColor,
                                      ),
                                    ),
                              customStepperController.isFromActiveOrder
                                  ? const SizedBox.shrink()
                                  : const SizedBox(
                                      height: 29,
                                    ),
                              customStepperController.isFromActiveOrder
                                  ? const SizedBox.shrink()
                                  : Wrap(
                                      spacing: 8,
                                      runSpacing: 8,
                                      children: List.generate(
                                        controller.vehicleImages.length,
                                        (index) => Container(
                                          width: 104,
                                          height: 104,
                                          padding: const EdgeInsets.all(10),
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              width: 1,
                                              color: AppColors.strokeColor,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(2),
                                          ),
                                          child: ImageNetwork(
                                            image:
                                                controller.vehicleImages[index],
                                            height: 104,
                                            width: 104,
                                            key: ValueKey(controller
                                                .vehicleImages[index]),
                                          ),
                                        ),
                                      ),
                                    )
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                            vertical: 32,
                            horizontal: 24,
                          ),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: AppColors.strokeColor,
                              width: 1,
                            ),
                            color: AppColors.colorWhite,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: double.maxFinite,
                                child: Wrap(
                                  alignment: WrapAlignment.spaceBetween,
                                  spacing: 30,
                                  runSpacing: 20,
                                  children: [
                                    Text(
                                      "${AppStringKey.booking_id.tr} $orderId",
                                      style: BaseStyle.textStyleRobotoSemiBold(
                                        20,
                                        AppColors.primaryTextColor,
                                      ),
                                    ),
                                    Get.find<CustomDashboardStepperController>()
                                            .isFromActiveOrder
                                        ? Container(
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                color: AppColors.colorWhite,
                                                width: 1,
                                              ),
                                            ),
                                            child: TextButton(
                                              onPressed: () {
                                                updateOrderDrawerKey
                                                    .currentState
                                                    ?.openEndDrawer();
                                              },
                                              style: TextButton.styleFrom(
                                                backgroundColor:
                                                    AppColors.primaryCtaColor,
                                                foregroundColor:
                                                    AppColors.colorWhite,
                                                shape:
                                                    const BeveledRectangleBorder(),
                                                textStyle: BaseStyle
                                                    .textStyleRobotoRegular(
                                                  16,
                                                  AppColors.colorWhite,
                                                ),
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                  vertical: 7,
                                                  horizontal: 15,
                                                ),
                                              ),
                                              child: Text(
                                                  AppStringKey.edit_order.tr),
                                            ),
                                          )
                                        : Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Container(
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    color: AppColors
                                                        .errorTextColor,
                                                    width: 1,
                                                  ),
                                                ),
                                                child: TextButton(
                                                  onPressed: () {
                                                    showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                          context) {
                                                        return CustomDialog(
                                                          padding:
                                                              const EdgeInsets
                                                                  .only(
                                                            top: 32,
                                                            right: 32,
                                                            bottom: 24,
                                                            left: 32,
                                                          ),
                                                          maxWidth: 420,
                                                          maxHeight: 350,
                                                          content: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Row(
                                                                mainAxisSize:
                                                                    MainAxisSize
                                                                        .min,
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .start,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Flexible(
                                                                    child:
                                                                        Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Text(
                                                                          AppStringKey
                                                                              .decline_return_service_for_order
                                                                              .tr,
                                                                          style:
                                                                              BaseStyle.textStyleRobotoSemiBold(
                                                                            16,
                                                                            AppColors.primaryTextColor,
                                                                          ),
                                                                        ),
                                                                        Text(
                                                                          '#$orderId',
                                                                          style:
                                                                              BaseStyle.textStyleRobotoSemiBold(
                                                                            16,
                                                                            AppColors.primaryTextColor,
                                                                          ),
                                                                        ),
                                                                        const SizedBox(
                                                                          height:
                                                                              8,
                                                                        ),
                                                                        Text(
                                                                            "${AppStringKey.specify_reason_for_order_decline.tr} #$orderId")
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                              InputTextField(
                                                                hintText:
                                                                    AppStringKey
                                                                        .enter_reason
                                                                        .tr,
                                                                label: '',
                                                                maxLength: 100,
                                                                maxLines: 3,
                                                                textInputType:
                                                                    TextInputType
                                                                        .multiline,
                                                                isRequireField:
                                                                    false,
                                                                textEditingController:
                                                                    controller
                                                                        .orderRejectedRemarks,
                                                              ),
                                                              const SizedBox(
                                                                height: 24,
                                                              ),
                                                              TextButton(
                                                                onPressed: () {
                                                                  controller
                                                                      .rejectOrder(
                                                                          orderId);
                                                                  Navigator.pop(
                                                                      context);
                                                                },
                                                                style: TextButton
                                                                    .styleFrom(
                                                                  textStyle:
                                                                      BaseStyle
                                                                          .textStyleRobotoRegular(
                                                                    14,
                                                                    AppColors
                                                                        .colorWhite,
                                                                  ),
                                                                  shape:
                                                                      RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(
                                                                      2,
                                                                    ),
                                                                  ),
                                                                  foregroundColor:
                                                                      AppColors
                                                                          .colorWhite,
                                                                  backgroundColor:
                                                                      AppColors
                                                                          .primaryTextColor,
                                                                ),
                                                                child: Text(
                                                                    AppStringKey
                                                                        .confirm
                                                                        .tr),
                                                              )
                                                            ],
                                                          ),
                                                        );
                                                      },
                                                    );
                                                  },
                                                  style: TextButton.styleFrom(
                                                    backgroundColor: AppColors
                                                        .lightRedBackgroundColor,
                                                    foregroundColor: AppColors
                                                        .errorTextColor,
                                                    shape:
                                                        const BeveledRectangleBorder(),
                                                    textStyle: BaseStyle
                                                        .textStyleRobotoRegular(
                                                      16,
                                                      AppColors.errorTextColor,
                                                    ),
                                                    padding: const EdgeInsets
                                                        .symmetric(
                                                      vertical: 7,
                                                      horizontal: 15,
                                                    ),
                                                  ),
                                                  child: Text(
                                                      AppStringKey.reject.tr),
                                                ),
                                              ),
                                              const SizedBox(
                                                width: 8,
                                              ),
                                              Container(
                                                decoration: BoxDecoration(
                                                  border: Border.all(
                                                    color: AppColors
                                                        .successTextGreenColor,
                                                    width: 1,
                                                  ),
                                                ),
                                                child: TextButton(
                                                  onPressed: () {
                                                    showDialog(
                                                      context: context,
                                                      builder: (BuildContext
                                                          context) {
                                                        return CustomDialog(
                                                          padding:
                                                              const EdgeInsets
                                                                  .only(
                                                            left: 32,
                                                            right: 32,
                                                            top: 32,
                                                            bottom: 24,
                                                          ),
                                                          maxHeight: 250,
                                                          content: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Row(
                                                                mainAxisSize:
                                                                    MainAxisSize
                                                                        .min,
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                children: [
                                                                  Flexible(
                                                                    child:
                                                                        Column(
                                                                      crossAxisAlignment:
                                                                          CrossAxisAlignment
                                                                              .start,
                                                                      mainAxisAlignment:
                                                                          MainAxisAlignment
                                                                              .start,
                                                                      children: [
                                                                        Text(
                                                                          '${AppStringKey.pickup_for_return_order.tr} #$orderId',
                                                                          style:
                                                                              BaseStyle.textStyleRobotoSemiBold(
                                                                            16,
                                                                            AppColors.primaryTextColor,
                                                                          ),
                                                                          overflow:
                                                                              TextOverflow.ellipsis,
                                                                        ),
                                                                        const SizedBox(
                                                                          height:
                                                                              8,
                                                                        ),
                                                                        Text(
                                                                          AppStringKey
                                                                              .will_be_able_to_pick_order
                                                                              .tr,
                                                                          style:
                                                                              BaseStyle.textStyleRobotoRegular(
                                                                            14,
                                                                            AppColors.primaryTextColor,
                                                                          ),
                                                                          overflow:
                                                                              TextOverflow.ellipsis,
                                                                        ),
                                                                        Text(
                                                                          "${AppStringKey.pickup.tr}: ${controller.pickUpDropAddress}",
                                                                          style:
                                                                              BaseStyle.textStyleRobotoRegular(
                                                                            14,
                                                                            AppColors.primaryTextColor,
                                                                          ),
                                                                        ),
                                                                        Text(
                                                                          "${AppStringKey.slot.tr}: ${controller.timeSlot}",
                                                                          style:
                                                                              BaseStyle.textStyleRobotoRegular(
                                                                            14,
                                                                            AppColors.primaryTextColor,
                                                                          ),
                                                                          overflow:
                                                                              TextOverflow.ellipsis,
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                              const SizedBox(
                                                                height: 24,
                                                              ),
                                                              Wrap(
                                                                spacing: 8,
                                                                alignment:
                                                                    WrapAlignment
                                                                        .start,
                                                                children: [
                                                                  TextButton(
                                                                    onPressed:
                                                                        () {
                                                                      controller.acceptOrder(
                                                                          orderId,
                                                                          true);
                                                                      Navigator.pop(
                                                                          context);
                                                                      Utilities
                                                                          .showToast(
                                                                        "${AppStringKey.dashboard_return_order.tr} : ${AppStringKey.pickup.tr}\n${AppStringKey.you_need_to_pickup_order.tr} $orderId\n${AppStringKey.at.tr} ${controller.pickUpDropAddress} ${AppStringKey.between.tr} ${controller.timeSlot}.",
                                                                        fontSize:
                                                                            16,
                                                                        webPosition:
                                                                            'right',
                                                                        webBgColor:
                                                                            '#DDF3EAFF',
                                                                        textColor:
                                                                            AppColors.secondaryTextColor,
                                                                      );
                                                                    },
                                                                    style: TextButton
                                                                        .styleFrom(
                                                                      textStyle:
                                                                          BaseStyle
                                                                              .textStyleRobotoRegular(
                                                                        14,
                                                                        const Color
                                                                            .fromARGB(
                                                                            255,
                                                                            22,
                                                                            11,
                                                                            11),
                                                                      ),
                                                                      shape:
                                                                          RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                          2,
                                                                        ),
                                                                      ),
                                                                      foregroundColor:
                                                                          AppColors
                                                                              .colorWhite,
                                                                      backgroundColor:
                                                                          AppColors
                                                                              .primaryTextColor,
                                                                    ),
                                                                    child: Text(
                                                                        AppStringKey
                                                                            .yes
                                                                            .tr),
                                                                  ),
                                                                  TextButton(
                                                                    onPressed:
                                                                        () {
                                                                      Navigator.pop(
                                                                          context);
                                                                      showDialog(
                                                                        context:
                                                                            context,
                                                                        builder:
                                                                            (BuildContext
                                                                                context) {
                                                                          return CustomDialog(
                                                                            padding:
                                                                                const EdgeInsets.only(
                                                                              left: 32,
                                                                              top: 32,
                                                                              right: 32,
                                                                              bottom: 24,
                                                                            ),
                                                                            maxWidth:
                                                                                416,
                                                                            maxHeight:
                                                                                280,
                                                                            content:
                                                                                Column(
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                                                Row(
                                                                                  mainAxisSize: MainAxisSize.min,
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: [
                                                                                    Flexible(
                                                                                      child: Column(
                                                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                                                        children: [
                                                                                          Text(
                                                                                            AppStringKey.cancel_pickup_for_return_order.tr,
                                                                                            style: BaseStyle.textStyleRobotoSemiBold(
                                                                                              16,
                                                                                              AppColors.primaryTextColor,
                                                                                            ),
                                                                                          ),
                                                                                          Text(
                                                                                            '#$orderId',
                                                                                            style: BaseStyle.textStyleRobotoSemiBold(
                                                                                              16,
                                                                                              AppColors.primaryTextColor,
                                                                                            ),
                                                                                          ),
                                                                                          const SizedBox(
                                                                                            height: 8,
                                                                                          ),
                                                                                          Text(
                                                                                            AppStringKey.are_you_sure_cancel_return_order.tr,
                                                                                            style: BaseStyle.textStyleRobotoRegular(
                                                                                              14,
                                                                                              AppColors.primaryTextColor,
                                                                                            ),
                                                                                          ),
                                                                                        ],
                                                                                      ),
                                                                                    ),
                                                                                  ],
                                                                                ),
                                                                                const SizedBox(
                                                                                  height: 24,
                                                                                ),
                                                                                TextButton(
                                                                                  onPressed: () {
                                                                                    controller.acceptOrder(orderId, false);
                                                                                    Navigator.pop(context);
                                                                                    Utilities.showToast(
                                                                                      "${AppStringKey.dashboard_return_order.tr} : ${AppStringKey.third_party.tr}\n${AppStringKey.you_need_to_pickup_order.tr} $orderId\n${AppStringKey.at.tr} ${controller.pickUpDropAddress} ${AppStringKey.between.tr} ${controller.timeSlot}.",
                                                                                      fontSize: 16,
                                                                                      webPosition: 'right',
                                                                                      webBgColor: '#DDF3EAFF',
                                                                                      textColor: AppColors.secondaryTextColor,
                                                                                    );
                                                                                  },
                                                                                  style: TextButton.styleFrom(
                                                                                    textStyle: BaseStyle.textStyleRobotoRegular(
                                                                                      14,
                                                                                      AppColors.colorWhite,
                                                                                    ),
                                                                                    shape: RoundedRectangleBorder(
                                                                                      borderRadius: BorderRadius.circular(
                                                                                        2,
                                                                                      ),
                                                                                    ),
                                                                                    foregroundColor: AppColors.colorWhite,
                                                                                    backgroundColor: AppColors.primaryTextColor,
                                                                                  ),
                                                                                  child: Text(AppStringKey.confirm.tr),
                                                                                )
                                                                              ],
                                                                            ),
                                                                          );
                                                                        },
                                                                      );
                                                                    },
                                                                    style: TextButton
                                                                        .styleFrom(
                                                                      textStyle:
                                                                          BaseStyle
                                                                              .textStyleRobotoRegular(
                                                                        14,
                                                                        AppColors
                                                                            .colorWhite,
                                                                      ),
                                                                      side:
                                                                          const BorderSide(
                                                                        color: AppColors
                                                                            .strokeColor,
                                                                        width:
                                                                            1,
                                                                      ),
                                                                      shape:
                                                                          RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(
                                                                          2,
                                                                        ),
                                                                      ),
                                                                      foregroundColor:
                                                                          AppColors
                                                                              .primaryTextColor,
                                                                      backgroundColor:
                                                                          AppColors
                                                                              .colorWhite,
                                                                    ),
                                                                    child: Text(
                                                                        AppStringKey
                                                                            .no
                                                                            .tr),
                                                                  )
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                        );
                                                      },
                                                    );
                                                  },
                                                  style: TextButton.styleFrom(
                                                    backgroundColor: AppColors
                                                        .greenBackgroundColor,
                                                    foregroundColor: AppColors
                                                        .successTextGreenColor,
                                                    shape:
                                                        const BeveledRectangleBorder(),
                                                    textStyle: BaseStyle
                                                        .textStyleRobotoRegular(
                                                      16,
                                                      AppColors
                                                          .successTextGreenColor,
                                                    ),
                                                    padding: const EdgeInsets
                                                        .symmetric(
                                                      vertical: 7,
                                                      horizontal: 15,
                                                    ),
                                                  ),
                                                  child: Text(
                                                      AppStringKey.accept.tr),
                                                ),
                                              )
                                            ],
                                          )
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 7,
                              ),
                              Text(
                                AppStringKey.list_of_sub_category_added.tr,
                                style: BaseStyle.textStyleRobotoRegular(
                                  14,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              const SizedBox(
                                height: 28,
                              ),
                              Container(
                                width: double.maxFinite,
                                height: 1,
                                color: AppColors.dividerColor,
                              ),
                              const SizedBox(
                                height: 26,
                              ),
                              Text(
                                AppStringKey.total_bill.tr,
                                style: BaseStyle.textStyleRobotoSemiBold(
                                  20,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              TotalBillAccordian(
                                servicesBreakdown: controller.services,
                              ),
                              const SizedBox(
                                height: 66,
                              ),
                              Container(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 12,
                                  vertical: 20,
                                ),
                                decoration: const BoxDecoration(
                                  color: AppColors.primaryBackgroundColor,
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      AppStringKey.total_amount.tr,
                                      style: BaseStyle.textStyleRobotoSemiBold(
                                        16,
                                        AppColors.primaryTextColor,
                                      ),
                                    ),
                                    Text(
                                      "${controller.totalAmount} ${AppConstants.currencyString}",
                                      style: BaseStyle.textStyleRobotoSemiBold(
                                        16,
                                        AppColors.primaryTextColor,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class OrderStatusWidget extends StatefulWidget {
  final ReturnOrderDetailController controller;
  final String orderId;
  final CustomDashboardStepperController customStepperController;

  const OrderStatusWidget({
    super.key,
    required this.controller,
    required this.orderId,
    required this.customStepperController,
  });

  @override
  State<OrderStatusWidget> createState() => _OrderStatusWidgetState();
}

class _OrderStatusWidgetState extends State<OrderStatusWidget> {
  @override
  Widget build(BuildContext context) {
    return CompositedTransformTarget(
      link: widget.controller.layerLink,
      child: OverlayPortal(
        controller: widget.controller.overlayPortalController,
        overlayChildBuilder: (BuildContext context) {
          return CompositedTransformFollower(
            link: widget.controller.layerLink,
            targetAnchor: Alignment.bottomLeft,
            child: Align(
              alignment: AlignmentDirectional.topStart,
              child: Container(
                width: 220,
                height: 180,
                decoration: BoxDecoration(
                  color: AppColors.colorWhite,
                  borderRadius: BorderRadius.circular(3),
                  border: Border.all(
                    color: AppColors.borderColor,
                    width: 0,
                  ),
                ),
                child: SingleChildScrollView(
                  child: Obx(
                    () => Column(
                      children: List.generate(
                        widget.controller.orderStatusList.length,
                        (index) => Material(
                          color: Colors.transparent,
                          child: Ink(
                            child: InkWell(
                              onTap: widget.controller.orderStatusList[index]
                                      .isOrderStatusCompleted
                                  ? null
                                  : () {
                                      widget.controller
                                          .selectedOrderStatusIndex = index;
                                      widget.controller.selectedOrderId =
                                          widget.orderId;
                                      if (!widget.controller.isPaymentUpdated &&
                                          !widget.customStepperController
                                              .isReturnOrder &&
                                          index >
                                              widget.controller.orderStatusList
                                                  .indexWhere((status) =>
                                                      status.orderStatusValue ==
                                                      AppStringKey
                                                          .payment_pending
                                                          .tr)) {
                                        widget.controller
                                            .getPaymentStatus(widget.orderId);
                                      } else {
                                        widget.customStepperController
                                                .isReturnOrder
                                            ? widget.controller
                                                .updateReturnOrderStatus(
                                                widget.orderId,
                                                widget
                                                    .controller
                                                    .orderStatusList[index]
                                                    .orderStatusValue,
                                              )
                                            : widget.controller
                                                .updateOrderStatus(
                                                widget.orderId,
                                                widget
                                                    .controller
                                                    .orderStatusList[index]
                                                    .orderStatusValue,
                                              );
                                      }
                                    },
                              child: Column(
                                children: [
                                  Container(
                                    padding: const EdgeInsets.only(
                                        left: 12,
                                        top: 12,
                                        bottom: 12,
                                        right: 18),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          widget
                                              .controller
                                              .orderStatusList[index]
                                              .orderStatusValue,
                                          style:
                                              BaseStyle.textStyleRobotoRegular(
                                            15,
                                            widget
                                                    .controller
                                                    .orderStatusList[index]
                                                    .isOrderStatusCompleted
                                                ? AppColors.inActiveCtaTextColor
                                                : AppColors.colorBlack,
                                          ),
                                        ),
                                        Visibility(
                                          visible: widget
                                              .controller
                                              .orderStatusList[index]
                                              .isOrderStatusCompleted,
                                          child: const SizedBox(
                                            height: 20,
                                            width: 20,
                                            child: Icon(
                                              Icons.done,
                                              size: 20,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    width: 220,
                                    height: 1,
                                    color: AppColors.dividerColor,
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        },
        child: Material(
          color: Colors.transparent,
          child: Ink(
            child: InkWell(
              onTap: () {
                widget.controller.isOrderStatusWidgetVisible.value =
                    !widget.controller.isOrderStatusWidgetVisible.value;
                if (widget.controller.overlayPortalController.isShowing) {
                  widget.controller.overlayPortalController.hide();
                } else {
                  widget.controller.overlayPortalController.show();
                }
              },
              child: Container(
                width: 220,
                height: 50,
                padding: const EdgeInsets.symmetric(horizontal: 12),
                decoration: BoxDecoration(
                  color: AppColors.colorWhite,
                  borderRadius: BorderRadius.circular(3),
                  border: Border.all(
                    color: AppColors.borderColor,
                    width: 0,
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Obx(
                      () => Text(
                        widget.controller.selectedOrderStatusValue.value,
                        style: BaseStyle.textStyleRobotoRegular(
                          15,
                          AppColors.colorBlack,
                        ),
                      ),
                    ),
                    Obx(
                      () => SizedBox(
                        height: 20,
                        width: 20,
                        child: Icon(
                          widget.controller.isOrderStatusWidgetVisible.value
                              ? Icons.keyboard_arrow_up
                              : Icons.keyboard_arrow_down,
                          size: 20,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
