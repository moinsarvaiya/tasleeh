import 'package:al_baida_garage_fe/common/input_text_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../Utils/utilities.dart';
import '../../../../common/custom_dialog.dart';
import '../../../../common/space_horizontal.dart';
import '../../../../common/space_vertical.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/extensions/common_extension.dart';
import '../../../../custom_classes/custom_dropdown_field.dart';
import '../../../../l10n/app_string_key.dart';
import '../../../custom_stepper/widgets/custom_square_corner_button.dart';
import '../../custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import 'return_order_detail_controller.dart';
import 'total_bill_accordian.dart';

class ReturnOrderDetailMobile extends StatelessWidget {
  final String orderId;
  final ReturnOrderDetailController controller;

  const ReturnOrderDetailMobile({
    super.key,
    required this.orderId,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
          color: AppColors.colorWhite,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "${Get.find<CustomDashboardStepperController>().isFromActiveOrder ? AppStringKey.active_order_detail.tr : AppStringKey.return_order_detail.tr} #$orderId",
                        style: BaseStyle.textStyleRobotoBold(
                          18,
                          AppColors.inputFieldHeading,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              const SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  GestureDetector(
                    onTap: () {
                      var controller = Get.find<CustomDashboardStepperController>();
                      if (controller.customStepperList[controller.stepperSelectedIndex.value].subList[controller.orderListItemSelectedIndex.value]
                              .childActiveIndex >
                          0) {
                        controller.customStepperList[controller.stepperSelectedIndex.value].subList[controller.orderListItemSelectedIndex.value]
                            .childActiveIndex--;
                        controller.customStepperList.refresh();
                      }
                    },
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 1,
                          color: AppColors.strokeColor,
                        ),
                        borderRadius: BorderRadius.circular(2),
                      ),
                      child: const Icon(
                        Icons.arrow_back,
                        size: 12,
                        color: AppColors.primaryTextColor,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Obx(
                        () => Text(
                          controller.customerName.value,
                          style: BaseStyle.textStyleRobotoBold(
                            14,
                            AppColors.primaryTextColor,
                          ),
                        ),
                      ),
                      Text(
                        "#$orderId",
                        style: BaseStyle.textStyleRobotoRegular(
                          10,
                          AppColors.secondaryTextColor,
                        ),
                      ),
                    ],
                  )
                ],
              ),
              const SpaceVertical(0.015),
              Container(
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      width: 1,
                      color: AppColors.tertiaryBackgroundColor,
                    ),
                    bottom: BorderSide(
                      width: 1,
                      color: AppColors.tertiaryBackgroundColor,
                    ),
                  ),
                ),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 12),
                        decoration: const BoxDecoration(
                          border: Border(
                            right: BorderSide(
                              width: 1,
                              color: AppColors.tertiaryBackgroundColor,
                            ),
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                const Icon(
                                  Icons.warehouse_outlined,
                                  size: 12,
                                  color: AppColors.secondaryTextColor,
                                ),
                                const SizedBox(
                                  width: 4,
                                ),
                                Text(
                                  AppStringKey.stepper_services.tr,
                                  style: BaseStyle.textStyleRobotoRegular(
                                    12,
                                    AppColors.secondaryTextColor,
                                  ),
                                )
                              ],
                            ),
                            Obx(
                              () => controller.services.length > 1
                                  ? Text(
                                      "${controller.services[0]['category']} + ${controller.services.length - 1}",
                                      style: BaseStyle.textStyleRobotoRegular(
                                        14,
                                        AppColors.primaryTextColor,
                                      ),
                                    )
                                  : controller.services.isNotEmpty
                                      ? Text(
                                          "${controller.services[0]['category']}",
                                          style: BaseStyle.textStyleRobotoRegular(
                                            14,
                                            AppColors.primaryTextColor,
                                          ),
                                        )
                                      : const SizedBox.shrink(),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              const Icon(
                                Icons.car_repair_outlined,
                                size: 12,
                                color: AppColors.secondaryTextColor,
                              ),
                              const SizedBox(
                                width: 4,
                              ),
                              Text(
                                AppStringKey.vehicle_model.tr,
                                style: BaseStyle.textStyleRobotoRegular(
                                  12,
                                  AppColors.secondaryTextColor,
                                ),
                              )
                            ],
                          ),
                          Obx(
                            () => Text(
                              controller.vehicleModel.value,
                              style: BaseStyle.textStyleRobotoRegular(
                                14,
                                AppColors.primaryTextColor,
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        const Icon(
                          Icons.location_pin,
                          size: 12,
                          color: AppColors.secondaryTextColor,
                        ),
                        const SizedBox(
                          width: 4,
                        ),
                        Text(
                          AppStringKey.pickup_address.tr,
                          style: BaseStyle.textStyleRobotoRegular(
                            12,
                            AppColors.secondaryTextColor,
                          ),
                        )
                      ],
                    ),
                    Obx(
                      () => Text(
                        "${controller.pickUpDropAddress}",
                        style: BaseStyle.textStyleRobotoRegular(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      ),
                    ),
                    Obx(
                      () => Text(
                        "${controller.timeSlot}",
                        style: BaseStyle.textStyleRobotoRegular(
                          12,
                          AppColors.secondaryTextColor,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      width: 1,
                      color: AppColors.tertiaryBackgroundColor,
                    ),
                    bottom: BorderSide(
                      width: 1,
                      color: AppColors.tertiaryBackgroundColor,
                    ),
                  ),
                ),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        decoration: const BoxDecoration(
                          border: Border(
                            right: BorderSide(
                              width: 1,
                              color: AppColors.tertiaryBackgroundColor,
                            ),
                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                const Icon(
                                  Icons.ballot_outlined,
                                  size: 12,
                                  color: AppColors.secondaryTextColor,
                                ),
                                const SizedBox(
                                  width: 4,
                                ),
                                Text(
                                  AppStringKey.order_id.tr,
                                  style: BaseStyle.textStyleRobotoRegular(
                                    12,
                                    AppColors.secondaryTextColor,
                                  ),
                                )
                              ],
                            ),
                            Text(
                              "#$orderId",
                              style: BaseStyle.textStyleRobotoRegular(
                                14,
                                AppColors.primaryTextColor,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Container(
                        padding: const EdgeInsets.fromLTRB(12, 20, 0, 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                const Icon(
                                  Icons.shopping_bag_outlined,
                                  size: 12,
                                  color: AppColors.secondaryTextColor,
                                ),
                                const SizedBox(
                                  width: 4,
                                ),
                                Text(
                                  AppStringKey.total_amount.tr,
                                  style: BaseStyle.textStyleRobotoRegular(
                                    12,
                                    AppColors.secondaryTextColor,
                                  ),
                                )
                              ],
                            ),
                            Obx(
                              () => Text(
                                controller.totalAmount.value,
                                style: BaseStyle.textStyleRobotoRegular(
                                  14,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              const SpaceVertical(0.012),
              Text(
                AppStringKey.total_bill.tr,
                style: BaseStyle.textStyleRobotoRegular(
                  12,
                  AppColors.secondaryTextColor,
                ),
              ),
              const SpaceVertical(0.012),
              TotalBillAccordian(
                servicesBreakdown: controller.services,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.symmetric(
          horizontal: context.screenWidth * 0.001,
          vertical: 16,
        ),
        decoration: const BoxDecoration(
          color: AppColors.primaryBackgroundColor,
        ),
        height: context.screenHeight * 0.1,
        child: Get.find<CustomDashboardStepperController>().isFromActiveOrder
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomSquareCornerButton(
                    buttonText: AppStringKey.edit_order.tr,
                    buttonTextColor: AppColors.colorWhite,
                    backgroundColor: AppColors.colorBlack,
                    isBorderVisible: true,
                    borderColor: AppColors.colorWhite,
                    borderWidth: 1,
                    height: 0.07,
                    width: 0.45,
                    textSize: 14,
                    callBack: () {
                      updateBottomSheet(context);
                    },
                  ),
                ],
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CustomSquareCornerButton(
                    buttonText: AppStringKey.reject.tr,
                    buttonTextColor: AppColors.errorTextColor,
                    backgroundColor: AppColors.lightRedBackgroundColor,
                    isBorderVisible: true,
                    borderColor: AppColors.errorTextColor,
                    borderWidth: 1,
                    height: 0.07,
                    width: 0.30,
                    textSize: 16,
                    callBack: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return CustomDialog(
                            padding: const EdgeInsets.only(
                              top: 32,
                              right: 32,
                              bottom: 24,
                              left: 32,
                            ),
                            content: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Row(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Flexible(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            AppStringKey.decline_return_service_for_order.tr,
                                            style: BaseStyle.textStyleRobotoSemiBold(
                                              16,
                                              AppColors.primaryTextColor,
                                            ),
                                          ),
                                          Text(
                                            '#$orderId',
                                            style: BaseStyle.textStyleRobotoSemiBold(
                                              16,
                                              AppColors.primaryTextColor,
                                            ),
                                          ),
                                          const SizedBox(
                                            height: 8,
                                          ),
                                          Text("${AppStringKey.specify_reason_for_order_decline.tr} #$orderId")
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 16,
                                    ),
                                  ],
                                ),
                                InputTextField(
                                  hintText: AppStringKey.enter_reason.tr,
                                  label: '',
                                  maxLength: 100,
                                  maxLines: 3,
                                  textInputType: TextInputType.multiline,
                                  isRequireField: false,
                                  textEditingController: controller.orderRejectedRemarks,
                                ),
                                const SizedBox(
                                  height: 24,
                                ),
                                TextButton(
                                  onPressed: () {
                                    controller.rejectOrder(orderId);
                                    Navigator.pop(context);
                                  },
                                  style: TextButton.styleFrom(
                                    textStyle: BaseStyle.textStyleRobotoRegular(
                                      14,
                                      AppColors.colorWhite,
                                    ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                        2,
                                      ),
                                    ),
                                    foregroundColor: AppColors.colorWhite,
                                    backgroundColor: AppColors.primaryTextColor,
                                  ),
                                  child: Text(AppStringKey.confirm.tr),
                                )
                              ],
                            ),
                          );
                        },
                      );
                    },
                  ),
                  const SpaceHorizontal(0.05),
                  CustomSquareCornerButton(
                    buttonText: AppStringKey.accept.tr,
                    buttonTextColor: AppColors.successTextGreenColor,
                    backgroundColor: AppColors.greenBackgroundColor,
                    isBorderVisible: true,
                    borderColor: AppColors.successTextGreenColor,
                    borderWidth: 1,
                    height: 0.07,
                    width: 0.30,
                    textSize: 13,
                    callBack: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return CustomDialog(
                            padding: const EdgeInsets.only(
                              left: 32,
                              right: 32,
                              top: 32,
                              bottom: 24,
                            ),
                            content: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisSize: MainAxisSize.min,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Flexible(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            '${AppStringKey.pickup_for_return_order.tr} #$orderId',
                                            style: BaseStyle.textStyleRobotoSemiBold(
                                              16,
                                              AppColors.primaryTextColor,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          const SizedBox(
                                            height: 8,
                                          ),
                                          Text(
                                            AppStringKey.will_be_able_to_pick_order.tr,
                                            style: BaseStyle.textStyleRobotoRegular(
                                              14,
                                              AppColors.primaryTextColor,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                          Text(
                                            "${AppStringKey.pickup.tr}: ${controller.pickUpDropAddress}",
                                            style: BaseStyle.textStyleRobotoRegular(
                                              14,
                                              AppColors.primaryTextColor,
                                            ),
                                          ),
                                          Text(
                                            "${AppStringKey.slot.tr}: ${controller.timeSlot}",
                                            style: BaseStyle.textStyleRobotoRegular(
                                              14,
                                              AppColors.primaryTextColor,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ],
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 16,
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 24,
                                ),
                                Wrap(
                                  spacing: 8,
                                  alignment: WrapAlignment.start,
                                  children: [
                                    TextButton(
                                      onPressed: () {
                                        controller.acceptOrder(orderId, true);
                                        Navigator.pop(context);
                                        Utilities.showToast(
                                          "${AppStringKey.dashboard_return_order.tr} : ${AppStringKey.pickup.tr}\n${AppStringKey.you_need_to_pickup_order.tr} $orderId\n${AppStringKey.at.tr} ${controller.pickUpDropAddress} ${AppStringKey.between.tr} ${controller.timeSlot}.",
                                          fontSize: 16,
                                          webPosition: 'right',
                                          webBgColor: '#DDF3EAFF',
                                          textColor: AppColors.secondaryTextColor,
                                        );
                                      },
                                      style: TextButton.styleFrom(
                                        textStyle: BaseStyle.textStyleRobotoRegular(
                                          14,
                                          const Color.fromARGB(255, 22, 11, 11),
                                        ),
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                            2,
                                          ),
                                        ),
                                        foregroundColor: AppColors.colorWhite,
                                        backgroundColor: AppColors.primaryTextColor,
                                      ),
                                      child: Text(AppStringKey.yes.tr),
                                    ),
                                    TextButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return CustomDialog(
                                              padding: const EdgeInsets.only(
                                                left: 32,
                                                top: 32,
                                                right: 32,
                                                bottom: 24,
                                              ),
                                              maxWidth: 416,
                                              maxHeight: 280,
                                              content: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Row(
                                                    mainAxisSize: MainAxisSize.min,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      Flexible(
                                                        child: Column(
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          children: [
                                                            Text(
                                                              AppStringKey.cancel_pickup_for_return_order.tr,
                                                              style: BaseStyle.textStyleRobotoSemiBold(
                                                                16,
                                                                AppColors.primaryTextColor,
                                                              ),
                                                            ),
                                                            Text(
                                                              '#$orderId',
                                                              style: BaseStyle.textStyleRobotoSemiBold(
                                                                16,
                                                                AppColors.primaryTextColor,
                                                              ),
                                                            ),
                                                            const SizedBox(
                                                              height: 8,
                                                            ),
                                                            Text(
                                                              AppStringKey.are_you_sure_cancel_return_order.tr,
                                                              style: BaseStyle.textStyleRobotoRegular(
                                                                14,
                                                                AppColors.primaryTextColor,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      const SizedBox(
                                                        width: 16,
                                                      ),
                                                    ],
                                                  ),
                                                  const SizedBox(
                                                    height: 24,
                                                  ),
                                                  TextButton(
                                                    onPressed: () {
                                                      controller.acceptOrder(orderId, false);
                                                      Navigator.pop(context);
                                                      Utilities.showToast(
                                                        "${AppStringKey.dashboard_return_order.tr} : ${AppStringKey.third_party.tr}\n${AppStringKey.you_need_to_pickup_order.tr} $orderId\n${AppStringKey.at.tr} ${controller.pickUpDropAddress} ${AppStringKey.between.tr} ${controller.timeSlot}.",
                                                        fontSize: 16,
                                                        webPosition: 'right',
                                                        webBgColor: '#DDF3EAFF',
                                                        textColor: AppColors.secondaryTextColor,
                                                      );
                                                    },
                                                    style: TextButton.styleFrom(
                                                      textStyle: BaseStyle.textStyleRobotoRegular(
                                                        14,
                                                        AppColors.colorWhite,
                                                      ),
                                                      shape: RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.circular(
                                                          2,
                                                        ),
                                                      ),
                                                      foregroundColor: AppColors.colorWhite,
                                                      backgroundColor: AppColors.primaryTextColor,
                                                    ),
                                                    child: Text(AppStringKey.confirm.tr),
                                                  )
                                                ],
                                              ),
                                            );
                                          },
                                        );
                                      },
                                      style: TextButton.styleFrom(
                                        textStyle: BaseStyle.textStyleRobotoRegular(
                                          14,
                                          AppColors.colorWhite,
                                        ),
                                        side: const BorderSide(
                                          color: AppColors.strokeColor,
                                          width: 1,
                                        ),
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                            2,
                                          ),
                                        ),
                                        foregroundColor: AppColors.primaryTextColor,
                                        backgroundColor: AppColors.colorWhite,
                                      ),
                                      child: Text(AppStringKey.no.tr),
                                    )
                                  ],
                                )
                              ],
                            ),
                          );
                        },
                      );
                    },
                  ),
                ],
              ),
      ),
    );
  }
}

void updateBottomSheet(BuildContext context) {
  var controller = Get.find<ReturnOrderDetailController>();
  showModalBottomSheet(
    backgroundColor: Colors.white,
    constraints: BoxConstraints(maxHeight: context.screenHeight * 0.95),
    isScrollControlled: true,
    context: context,
    builder: (builder) {
      return GestureDetector(
        onTap: () {
          Get.back();
        },
        child: ClipRect(
          clipBehavior: Clip.antiAlias,
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(0),
                topRight: Radius.circular(0),
              ),
            ),
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
              child: Stack(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: SingleChildScrollView(
                          child: SizedBox(
                            width: double.infinity,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  AppStringKey.update_order_detail.tr,
                                  style: BaseStyle.textStyleRobotoBold(
                                    20,
                                    AppColors.primaryTextColor,
                                  ),
                                ),
                                const SpaceVertical(0.01),
                                Text(
                                  '${AppStringKey.service_no.tr} #${Get.find<CustomDashboardStepperController>().orderId}',
                                  style: BaseStyle.textStyleRobotoRegular(
                                    12,
                                    AppColors.primaryTextColor,
                                  ),
                                  maxLines: 2,
                                ),
                                const SpaceVertical(0.02),
                                Container(
                                  width: double.infinity,
                                  height: 2,
                                  color: AppColors.dividerColor,
                                ),
                                const SpaceVertical(0.05),
                                Obx(
                                  () => CustomDropDownField(
                                    buttonPadding: const EdgeInsets.only(left: 0),
                                    labelVisibility: true,
                                    hintTitle: AppStringKey.select_service_category.tr,
                                    items: controller.serviceCategoryList.map((element) => element.category ?? '').toList(),
                                    validationText: '',
                                    onChanged: (val) {
                                      controller.serviceSelectedLevelList.clear();
                                      controller.orderTotalAmountController.value.clear();
                                      controller.serviceSelectedLevelList.refresh();
                                      controller.getSubCategoryData(
                                          controller.serviceCategoryList.firstWhere((element) => element.category == val).serviceId ?? '');
                                    },
                                    labelText: AppStringKey.service_category.tr,
                                    selectedValue: '',
                                    textStyle: BaseStyle.textStyleRobotoRegular(
                                      16,
                                      AppColors.inputFieldHintTextColor,
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 16,
                                ),
                                Obx(
                                  () => CustomDropDownField(
                                    buttonPadding: const EdgeInsets.only(left: 0),
                                    labelVisibility: true,
                                    hintTitle: AppStringKey.select_sub_service_category.tr,
                                    items: controller.serviceSubCategoryList.map((element) => element.subcategory ?? '').toList(),
                                    validationText: '',
                                    onChanged: (val) {
                                      controller.addServiceDetailId(
                                          controller.serviceSubCategoryList.firstWhere((element) => element.subcategory == val).serviceDetailId ??
                                              '');
                                      controller.getSubCategoryLevelData(
                                          controller.serviceSubCategoryList.firstWhere((element) => element.subcategory == val).serviceDetailId ??
                                              '');
                                    },
                                    labelText: AppStringKey.service_sub_category.tr,
                                    selectedValue: '',
                                    textStyle: BaseStyle.textStyleRobotoRegular(
                                      16,
                                      AppColors.inputFieldHintTextColor,
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 16,
                                ),
                                Obx(
                                  () => CustomDropDownField(
                                    buttonPadding: const EdgeInsets.only(left: 0),
                                    labelVisibility: true,
                                    hintTitle: AppStringKey.service_service_type.tr,
                                    items: controller.serviceLevelList.map((element) => element.level ?? '').toList(),
                                    validationText: '',
                                    onChanged: (val) {
                                      if (!controller.serviceSelectedLevelList.contains(val)) {
                                        controller.serviceSelectedLevelList.add(val!);
                                        controller.serviceSelectedLevelList.refresh();
                                        controller.calculateDiscountedPrice(
                                            controller.serviceLevelList.firstWhere((element) => element.level == val).discountedPrice ?? '');
                                        controller.addServiceDetailLevelIdAtPosition(
                                            controller.serviceLevelList.firstWhere((element) => element.level == val).serviceDetailLevelId ?? '');
                                      }
                                    },
                                    labelText: AppStringKey.service_level.tr,
                                    selectedValue: '',
                                    textStyle: BaseStyle.textStyleRobotoRegular(
                                      16,
                                      AppColors.inputFieldHintTextColor,
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                Obx(
                                  () => Wrap(
                                    children: [
                                      ...controller.serviceSelectedLevelList.indexed.map(
                                        (e) {
                                          var index = e.$1;
                                          var item = e.$2;
                                          return Container(
                                            margin: const EdgeInsets.only(right: 5),
                                            padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                width: 0,
                                                color: AppColors.borderColor,
                                              ),
                                            ),
                                            child: Row(
                                              mainAxisSize: MainAxisSize.min,
                                              children: [
                                                Center(
                                                  child: Text(
                                                    item,
                                                    style: BaseStyle.textStyleRobotoRegular(
                                                      14,
                                                      AppColors.primaryCtaColor,
                                                    ),
                                                  ),
                                                ),
                                                const SizedBox(
                                                  width: 5,
                                                ),
                                                Material(
                                                  child: Ink(
                                                    child: InkWell(
                                                      onTap: () {
                                                        controller.removeDiscountedPrice(controller.serviceLevelList
                                                                .firstWhere((element) => element.level == controller.serviceSelectedLevelList[index])
                                                                .discountedPrice ??
                                                            '');
                                                        controller.removeServiceDetailLevelIdAtPosition(index);
                                                        controller.serviceSelectedLevelList.removeAt(index);
                                                        controller.serviceSelectedLevelList.refresh();
                                                      },
                                                      child: const Icon(
                                                        Icons.close,
                                                        size: 15,
                                                        color: AppColors.colorBlack,
                                                      ),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          );
                                        },
                                      )
                                    ],
                                  ),
                                ),
                                const SizedBox(
                                  height: 16,
                                ),
                                Obx(
                                  () => InputTextField(
                                    hintText: AppStringKey.total_amount.tr,
                                    label: AppStringKey.total_amount.tr,
                                    isRequireField: true,
                                    focusScope: controller.orderTotalAmountFocusNode,
                                    textEditingController: controller.orderTotalAmountController.value,
                                    isReadOnly: true,
                                    readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                                    onChangeWithValidationStatus: (value, isValid) {},
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Column(
                        children: [
                          Container(
                            width: double.infinity,
                            height: 2,
                            color: AppColors.dividerColor,
                          ),
                          const SpaceVertical(0.02),
                          Obx(
                            () => CustomSquareCornerButton(
                              buttonText: AppStringKey.update_order.tr,
                              buttonTextColor:
                                  controller.serviceSelectedLevelList.isNotEmpty ? AppColors.colorWhite : AppColors.inCompletedStepTextColor,
                              backgroundColor: controller.serviceSelectedLevelList.isNotEmpty ? AppColors.colorBlack : AppColors.colorGrey,
                              height: 0.07,
                              width: 0.40,
                              textSize: context.isDesktop ? 7 : 12,
                              callBack: controller.serviceSelectedLevelList.isNotEmpty ? () {} : null,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Positioned(
                    right: Directionality.of(context) == TextDirection.ltr ? 00 : null,
                    left: Directionality.of(context) == TextDirection.rtl ? 00 : null,
                    top: 00,
                    child: Material(
                      color: Colors.transparent,
                      child: Ink(
                        child: InkWell(
                          onTap: () {
                            Get.back();
                          },
                          child: const Icon(
                            Icons.close,
                            color: AppColors.colorBlack,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    },
  );
}
