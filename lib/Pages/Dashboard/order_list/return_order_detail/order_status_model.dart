class OrderStatusModel {
  String orderStatusKey;
  String orderStatusValue;
  bool isOrderStatusCompleted;

  OrderStatusModel({
    required this.orderStatusKey,
    required this.orderStatusValue,
    required this.isOrderStatusCompleted,
  });
}
