import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../Utils/app_constants.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/base_style.dart';

class TotalBillAccordian extends StatefulWidget {
  final List servicesBreakdown;

  const TotalBillAccordian({super.key, required this.servicesBreakdown});

  @override
  State<TotalBillAccordian> createState() => _TotalBillAccordianState();
}

class _TotalBillAccordianState extends State<TotalBillAccordian> {
  RxInt expandedCategoryIndex = RxInt(-1);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: AppColors.strokeColor,
          width: 1,
        ),
      ),
      child: Column(
        children: List.generate(
          widget.servicesBreakdown.length,
          (categoryIndex) => Column(
            children: [
              Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 12,
                  horizontal: 16,
                ),
                decoration: BoxDecoration(
                  color: AppColors.colorWhite,
                  border: categoryIndex == widget.servicesBreakdown.length - 1
                      ? const Border()
                      : const Border(
                          bottom: BorderSide(
                            color: AppColors.strokeColor,
                            width: 1,
                          ),
                        ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    MouseRegion(
                      cursor: SystemMouseCursors.click,
                      child: GestureDetector(
                        onTap: () {
                          expandedCategoryIndex.value = (expandedCategoryIndex.value == categoryIndex ? -1 : categoryIndex);
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            widget.servicesBreakdown[categoryIndex]['subcategories'].length > 0
                                ? const Icon(
                                    Icons.arrow_drop_down_sharp,
                                    size: 16,
                                    color: AppColors.primaryTextColor,
                                  )
                                : const SizedBox(width: 20),
                            const SizedBox(
                              width: 12,
                            ),
                            Text(widget.servicesBreakdown[categoryIndex]['category']),
                          ],
                        ),
                      ),
                    ),
                    Text("${widget.servicesBreakdown[categoryIndex]['totalCategoryPrice']} ${AppConstants.currencyString}"),
                  ],
                ),
              ),
              Obx(
                () => Visibility(
                  visible: categoryIndex == expandedCategoryIndex.value,
                  child: Container(
                    padding: const EdgeInsets.all(16),
                    decoration: const BoxDecoration(
                      color: AppColors.primaryBackgroundColor,
                    ),
                    child: Column(
                      children: List.generate(
                        widget.servicesBreakdown[categoryIndex]['subcategories'].length,
                        (subCategoryIndex) => Container(
                          padding: const EdgeInsets.symmetric(
                            vertical: 4,
                            horizontal: 8,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                widget.servicesBreakdown[categoryIndex]['subcategories'][subCategoryIndex]['subCategory'],
                                style: BaseStyle.textStyleRobotoRegular(
                                  14,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              Text(
                                "${widget.servicesBreakdown[categoryIndex]['subcategories'][subCategoryIndex]['price']} ${AppConstants.currencyString}",
                                style: BaseStyle.textStyleRobotoRegular(
                                  14,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
