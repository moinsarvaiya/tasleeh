import 'package:al_baida_garage_fe/Pages/Dashboard/custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/extensions/common_extension.dart';
import 'return_order_detail_controller.dart';
import 'return_order_detail_mobile.dart';
import 'return_order_detail_web.dart';

class ReturnOrderDetailScreen extends StatefulWidget {
  const ReturnOrderDetailScreen({
    super.key,
  });

  @override
  State<ReturnOrderDetailScreen> createState() => _ReturnOrderDetailScreenState();
}

class _ReturnOrderDetailScreenState extends State<ReturnOrderDetailScreen> {
  final ReturnOrderDetailController controller = ReturnOrderDetailController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.getOrderDetails();
    });
  }

  @override
  Widget build(BuildContext context) {
    return context.isDesktop
        ? ReturnOrderDetailWeb(
            orderId: Get.find<CustomDashboardStepperController>().orderId,
            controller: controller,
          )
        : ReturnOrderDetailMobile(
            orderId: Get.find<CustomDashboardStepperController>().orderId,
            controller: controller,
          );
  }
}
