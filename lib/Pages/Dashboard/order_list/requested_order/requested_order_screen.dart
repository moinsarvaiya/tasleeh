import 'package:al_baida_garage_fe/Pages/Dashboard/order_list/requested_order/requested_order_mobile_screen.dart';
import 'package:al_baida_garage_fe/Pages/Dashboard/order_list/requested_order/requested_order_web_screen.dart';
import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../Utils/pagination_helper.dart';
import '../../../../core/constant/ui_constants.dart';
import 'order_controller.dart';

class RequestedOrderScreen extends StatefulWidget {
  const RequestedOrderScreen({super.key});

  @override
  State<RequestedOrderScreen> createState() => _RequestedOrderScreenState();
}

class _RequestedOrderScreenState extends State<RequestedOrderScreen> {
  final controller = Get.find<OrderController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.getOrderData(OrderStatus.requested);
    });

    controller.paginationHelper = PaginationHelper(
      loadData: (isOngoing, page, limit, refresh) {
        controller.paginationHelper.pageNumber++;
        controller.getOrderData(OrderStatus.requested);
      },
      limit: pageLimit,
    );
  }

  @override
  Widget build(BuildContext context) {
    return context.isDesktop
        ? RequestedOrderWebScreen(
            controller: controller,
          )
        : RequestedOrderMobileScreen(
            controller: controller,
          );
  }
}
