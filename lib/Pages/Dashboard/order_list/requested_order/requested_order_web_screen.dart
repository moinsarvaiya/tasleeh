import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../../Utils/utilities.dart';
import '../../../../common/common_order_table.dart';
import '../../../../common/custom_dialog.dart';
import '../../../../common/input_text_field.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/app_images.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../l10n/app_string_key.dart';
import '../../custom_dashboard_drawer/widgets/summary_widget.dart';
import 'order_controller.dart';

class RequestedOrderWebScreen extends StatelessWidget {
  final OrderController controller;

  const RequestedOrderWebScreen({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SummaryWidget(),
        const SizedBox(
          height: 8,
        ),
        Text(
          AppStringKey.above_data_from_the_past.tr,
          style: BaseStyle.textStyleRobotoMediumBold(
            12,
            AppColors.inputFieldHeading,
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              AppStringKey.requested_order_list_title.tr,
              style: BaseStyle.textStyleRobotoSemiBold(
                24,
                AppColors.colorBlack,
              ),
            ),
            IntrinsicWidth(
              child: Material(
                color: Colors.transparent,
                child: Ink(
                  child: InkWell(
                    onTap: () {
                      orderType = OrderStatus.requested;
                      dashboardOrderListDrawerKey.currentState?.openEndDrawer();
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 15,
                        vertical: 6.5,
                      ),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 1.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            AppImages.filter,
                            width: 24,
                            height: 24,
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Text(
                            AppStringKey.filter.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              16,
                              AppColors.primaryTextColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 24,
        ),
        Obx(
          () => Expanded(
            child: CommonOrderTable(
              scrollController: controller.paginationHelper.scrollController,
              columnSpacing: 10,
              padding: const EdgeInsets.all(0),
              columns: [
                DataColumn2(
                  fixedWidth: 132,
                  label: Obx(
                    () => controller.isSearching.value
                        ? TextFormField(
                            controller: controller.searchOrderId,
                            style: BaseStyle.textStyleRobotoRegular(
                              10,
                              AppColors.primaryTextColor,
                            ),
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric(
                                vertical: 4,
                                horizontal: 8,
                              ),
                              constraints: const BoxConstraints(maxHeight: 40),
                              suffixIcon: MouseRegion(
                                cursor: SystemMouseCursors.click,
                                child: GestureDetector(
                                  onTap: () {
                                    controller.isSearching.value = !controller.isSearching.value;
                                    controller.searchOrderId.clear();
                                  },
                                  child: const Icon(
                                    Icons.close,
                                    size: 12,
                                  ),
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(2),
                                borderSide: const BorderSide(
                                  color: AppColors.borderColor,
                                  width: 0,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(2),
                                borderSide: const BorderSide(
                                  color: AppColors.borderColor,
                                  width: 0,
                                ),
                              ),
                            ),
                            focusNode: controller.orderSearchFocus,
                            autofocus: true,
                            onChanged: (value) {
                              Utilities.customDebounce(customDebounceTime, () {
                                controller.searchOrder(OrderStatus.requested);
                              });
                            },
                          )
                        : Row(
                            children: [
                              Text(
                                AppStringKey.order_details.tr,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              ),
                              const SizedBox(
                                width: 4,
                              ),
                              Material(
                                child: Ink(
                                  child: InkWell(
                                    onTap: () {
                                      controller.isSearching.value = !controller.isSearching.value;
                                    },
                                    child: const Icon(
                                      Icons.search,
                                      size: 20,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.customer_info.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.requested_vehicle_detail_title.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.requested_title_service_selected.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.requested_title_pickup_detail.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  fixedWidth: 100,
                  label: Text(
                    AppStringKey.requested_title_amount.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  fixedWidth: 100,
                  label: Text(
                    AppStringKey.status.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
              ],
              data: List<DataRow2>.generate(
                controller.requestedOrderListData.length,
                (index) => DataRow2(
                  cells: [
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '#${controller.requestedOrderListData[index].bookingId!}',
                            style: BaseStyle.textStyleRobotoSemiBold(
                              14,
                              AppColors.primaryTextColor,
                            ).copyWith(
                              decoration: TextDecoration.underline,
                            ),
                          ),
                          Text(
                            "${controller.requestedOrderListData[index].bookingDate}",
                            style: BaseStyle.textStyleRobotoMediumBold(
                              14,
                              AppColors.secondaryTextColor,
                            ),
                          )
                        ],
                      ),
                    ),
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${controller.requestedOrderListData[index].name} ",
                            style: BaseStyle.textStyleRobotoMediumBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(
                            " ${controller.requestedOrderListData[index].number}",
                            style: BaseStyle.textStyleRobotoMediumBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller.requestedOrderListData[index].make!,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(
                            controller.requestedOrderListData[index].model!,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller.requestedOrderListData[index].services!.length > 1
                                ? "${controller.requestedOrderListData[index].services![0].serviceDetail} +${controller.requestedOrderListData[index].services!.length}"
                                : '${controller.requestedOrderListData[index].services![0].serviceDetail}',
                            style: BaseStyle.textStyleRobotoMediumBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(
                            "${controller.requestedOrderListData[index].timeSlot}",
                            style: BaseStyle.textStyleRobotoMediumBold(
                              12,
                              AppColors.secondaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          )
                        ],
                      ),
                    ),
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller.requestedOrderListData[index].pickupAddress!.isEmpty
                                ? AppStringKey.self_drop.tr
                                : controller.requestedOrderListData[index].pickupAddress!,
                            style: BaseStyle.textStyleRobotoSemiBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(
                            controller.requestedOrderListData[index].timeSlot!,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                    DataCell(
                      Text(
                        controller.requestedOrderListData[index].totalPrice.toString(),
                        style: BaseStyle.textStyleRobotoMediumBold(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      ),
                    ),
                    DataCell(
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(
                            onPressed: () {
                              controller.requestedOrderListData[index].pickupAddress!.isEmpty
                                  ? controller.acceptOrder(controller.requestedOrderListData[index].bookingId!, false)
                                  : showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return CustomDialog(
                                          padding: const EdgeInsets.only(
                                            left: 32,
                                            right: 32,
                                            top: 32,
                                            bottom: 24,
                                          ),
                                          maxHeight: 250,
                                          content: Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                mainAxisSize: MainAxisSize.min,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Flexible(
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: [
                                                        Text(
                                                          '${AppStringKey.pick_for_requested_order.tr} #${controller.requestedOrderListData[index].bookingId}',
                                                          style: BaseStyle.textStyleRobotoSemiBold(
                                                            16,
                                                            AppColors.primaryTextColor,
                                                          ),
                                                          overflow: TextOverflow.ellipsis,
                                                        ),
                                                        const SizedBox(
                                                          height: 8,
                                                        ),
                                                        Text(
                                                          AppStringKey.will_be_able_to_pick_order.tr,
                                                          style: BaseStyle.textStyleRobotoRegular(
                                                            14,
                                                            AppColors.primaryTextColor,
                                                          ),
                                                          overflow: TextOverflow.ellipsis,
                                                        ),
                                                        Text(
                                                          "${AppStringKey.pickup.tr}: ${controller.requestedOrderListData[index].pickupDate}",
                                                          style: BaseStyle.textStyleRobotoRegular(
                                                            14,
                                                            AppColors.primaryTextColor,
                                                          ),
                                                        ),
                                                        Text(
                                                          "${AppStringKey.slot.tr}: ${controller.requestedOrderListData[index].timeSlot}",
                                                          style: BaseStyle.textStyleRobotoRegular(
                                                            14,
                                                            AppColors.primaryTextColor,
                                                          ),
                                                          overflow: TextOverflow.ellipsis,
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    width: 16,
                                                  ),
                                                ],
                                              ),
                                              const SizedBox(
                                                height: 24,
                                              ),
                                              Wrap(
                                                spacing: 8,
                                                alignment: WrapAlignment.start,
                                                children: [
                                                  TextButton(
                                                    onPressed: () {
                                                      controller.acceptOrder(controller.requestedOrderListData[index].bookingId!, true);
                                                      Navigator.pop(context);
                                                      Utilities.showToast(
                                                        "${AppStringKey.order_accepted.tr} : ${AppStringKey.pickup.tr}\n${AppStringKey.you_need_to_pickup_order.tr} ${controller.requestedOrderListData[index].bookingId}\n${AppStringKey.at.tr} ${controller.requestedOrderListData[index].pickupAddress} ${AppStringKey.between.tr} ${controller.requestedOrderListData[index].timeSlot}.",
                                                        fontSize: 16,
                                                        webPosition: 'right',
                                                        webBgColor: '#DDF3EAFF',
                                                        textColor: AppColors.secondaryTextColor,
                                                      );
                                                    },
                                                    style: TextButton.styleFrom(
                                                      textStyle: BaseStyle.textStyleRobotoRegular(
                                                        14,
                                                        const Color.fromARGB(255, 22, 11, 11),
                                                      ),
                                                      shape: RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.circular(
                                                          2,
                                                        ),
                                                      ),
                                                      foregroundColor: AppColors.colorWhite,
                                                      backgroundColor: AppColors.primaryTextColor,
                                                    ),
                                                    child: Text(AppStringKey.yes.tr),
                                                  ),
                                                  TextButton(
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                      showDialog(
                                                        context: context,
                                                        builder: (BuildContext context) {
                                                          return CustomDialog(
                                                            padding: const EdgeInsets.only(
                                                              left: 32,
                                                              top: 32,
                                                              right: 32,
                                                              bottom: 24,
                                                            ),
                                                            maxWidth: 416,
                                                            maxHeight: 280,
                                                            content: Column(
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              children: [
                                                                Row(
                                                                  mainAxisSize: MainAxisSize.min,
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: [
                                                                    Flexible(
                                                                      child: Column(
                                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                                        children: [
                                                                          Text(
                                                                            AppStringKey.cancel_pick_for_requested_order.tr,
                                                                            style: BaseStyle.textStyleRobotoSemiBold(
                                                                              16,
                                                                              AppColors.primaryTextColor,
                                                                            ),
                                                                          ),
                                                                          Text(
                                                                            '#${controller.requestedOrderListData[index].bookingId}',
                                                                            style: BaseStyle.textStyleRobotoSemiBold(
                                                                              16,
                                                                              AppColors.primaryTextColor,
                                                                            ),
                                                                          ),
                                                                          const SizedBox(
                                                                            height: 8,
                                                                          ),
                                                                          Text(
                                                                            AppStringKey.are_you_sure_cancel_return_order.tr,
                                                                            style: BaseStyle.textStyleRobotoRegular(
                                                                              14,
                                                                              AppColors.primaryTextColor,
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    const SizedBox(
                                                                      width: 16,
                                                                    ),
                                                                  ],
                                                                ),
                                                                const SizedBox(
                                                                  height: 24,
                                                                ),
                                                                TextButton(
                                                                  onPressed: () {
                                                                    controller.acceptOrder(
                                                                        controller.requestedOrderListData[index].bookingId!, false);
                                                                    Navigator.pop(context);
                                                                    Utilities.showToast(
                                                                      "${AppStringKey.order_accepted.tr} : ${AppStringKey.third_party.tr}\n${AppStringKey.you_need_to_pickup_order.tr} ${controller.requestedOrderListData[index].bookingId}\n${AppStringKey.at.tr} ${controller.requestedOrderListData[index].pickupAddress} ${AppStringKey.between.tr} ${controller.requestedOrderListData[index].timeSlot}.",
                                                                      fontSize: 16,
                                                                      webPosition: 'right',
                                                                      webBgColor: '#DDF3EAFF',
                                                                      textColor: AppColors.secondaryTextColor,
                                                                    );
                                                                  },
                                                                  style: TextButton.styleFrom(
                                                                    textStyle: BaseStyle.textStyleRobotoRegular(
                                                                      14,
                                                                      AppColors.colorWhite,
                                                                    ),
                                                                    shape: RoundedRectangleBorder(
                                                                      borderRadius: BorderRadius.circular(
                                                                        2,
                                                                      ),
                                                                    ),
                                                                    foregroundColor: AppColors.colorWhite,
                                                                    backgroundColor: AppColors.primaryTextColor,
                                                                  ),
                                                                  child: Text(AppStringKey.confirm.tr),
                                                                )
                                                              ],
                                                            ),
                                                          );
                                                        },
                                                      );
                                                    },
                                                    style: TextButton.styleFrom(
                                                      textStyle: BaseStyle.textStyleRobotoRegular(
                                                        14,
                                                        AppColors.colorWhite,
                                                      ),
                                                      side: const BorderSide(
                                                        color: AppColors.strokeColor,
                                                        width: 1,
                                                      ),
                                                      shape: RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.circular(
                                                          2,
                                                        ),
                                                      ),
                                                      foregroundColor: AppColors.primaryTextColor,
                                                      backgroundColor: AppColors.colorWhite,
                                                    ),
                                                    child: Text(AppStringKey.no.tr),
                                                  )
                                                ],
                                              )
                                            ],
                                          ),
                                        );
                                      },
                                    );
                            },
                            splashRadius: 1,
                            iconSize: 30,
                            color: AppColors.successTextGreenColor,
                            icon: SvgPicture.asset(AppImages.check),
                          ),
                          IconButton(
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return CustomDialog(
                                    padding: const EdgeInsets.only(
                                      top: 32,
                                      right: 32,
                                      bottom: 24,
                                      left: 32,
                                    ),
                                    maxWidth: 420,
                                    maxHeight: 350,
                                    content: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisSize: MainAxisSize.min,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Flexible(
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    AppStringKey.decline_return_service_for_order.tr,
                                                    style: BaseStyle.textStyleRobotoSemiBold(
                                                      16,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                  ),
                                                  Text(
                                                    '#${controller.requestedOrderListData[index].bookingId}',
                                                    style: BaseStyle.textStyleRobotoSemiBold(
                                                      16,
                                                      AppColors.primaryTextColor,
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    height: 8,
                                                  ),
                                                  Text(
                                                      "${AppStringKey.specify_reason_for_order_decline.tr} #${controller.requestedOrderListData[index].bookingId}")
                                                ],
                                              ),
                                            ),
                                            const SizedBox(
                                              width: 16,
                                            ),
                                          ],
                                        ),
                                        InputTextField(
                                          hintText: AppStringKey.enter_reason.tr,
                                          label: '',
                                          maxLength: 100,
                                          maxLines: 3,
                                          textInputType: TextInputType.multiline,
                                          isRequireField: false,
                                          textEditingController: controller.orderRejectedRemarks,
                                        ),
                                        const SizedBox(
                                          height: 24,
                                        ),
                                        TextButton(
                                          onPressed: () {
                                            controller.rejectOrder(controller.requestedOrderListData[index].bookingId!);
                                            Navigator.pop(context);
                                          },
                                          style: TextButton.styleFrom(
                                            textStyle: BaseStyle.textStyleRobotoRegular(
                                              14,
                                              AppColors.colorWhite,
                                            ),
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(
                                                2,
                                              ),
                                            ),
                                            foregroundColor: AppColors.colorWhite,
                                            backgroundColor: AppColors.primaryTextColor,
                                          ),
                                          child: Text(AppStringKey.confirm.tr),
                                        )
                                      ],
                                    ),
                                  );
                                },
                              );
                            },
                            splashRadius: 1,
                            iconSize: 30,
                            color: AppColors.errorTextColor,
                            icon: SvgPicture.asset(AppImages.cross),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ).obs,
            ),
          ),
        )
      ],
    );
  }
}
