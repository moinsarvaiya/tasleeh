class WorkStatusModel {
  String statusKey;
  String statusTitle;

  WorkStatusModel({
    required this.statusKey,
    required this.statusTitle,
  });
}
