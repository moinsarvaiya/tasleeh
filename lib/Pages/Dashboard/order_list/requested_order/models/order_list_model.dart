import 'package:json_annotation/json_annotation.dart';

part 'order_list_model.g.dart';

@JsonSerializable()
class OrderListDataModel {
  String? bookingId;
  String? bookingDate;
  String? name;
  String? number;
  String? make;
  String? model;
  List<OrderServices>? services;
  double? distance;
  String? bookingStatus;
  bool? isReturnedOrder;
  String? pickupAddress;
  String? timeSlot;
  String? pickupDate;
  String? totalPrice;

  OrderListDataModel(
      {this.bookingId,
      this.bookingDate,
      this.name,
      this.number,
      this.make,
      this.model,
      this.services,
      this.distance,
      this.bookingStatus,
      this.isReturnedOrder,
      this.pickupAddress,
      this.timeSlot,
      this.pickupDate,
      this.totalPrice});

  factory OrderListDataModel.fromJson(Map<String, dynamic> json) => _$OrderListDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$OrderListDataModelToJson(this);
}

@JsonSerializable()
class OrderServices {
  String? serviceDetail;
  String? serviceDetailLevel;
  String? level;

  OrderServices({this.serviceDetail, this.serviceDetailLevel, this.level});

  factory OrderServices.fromJson(Map<String, dynamic> json) => _$OrderServicesFromJson(json);

  Map<String, dynamic> toJson() => _$OrderServicesToJson(this);
}
