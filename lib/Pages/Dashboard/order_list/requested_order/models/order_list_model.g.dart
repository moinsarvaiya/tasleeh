// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderListDataModel _$OrderListDataModelFromJson(Map<String, dynamic> json) => OrderListDataModel(
      bookingId: json['bookingId'] as String?,
      bookingDate: json['bookingDate'] as String?,
      name: json['name'] as String?,
      number: json['number'] as String?,
      make: json['make'] as String?,
      model: json['model'] as String?,
      services: (json['services'] as List<dynamic>?)?.map((e) => OrderServices.fromJson(e as Map<String, dynamic>)).toList(),
      distance: (json['distance'] as num?)?.toDouble(),
      bookingStatus: json['bookingStatus'] as String?,
      isReturnedOrder: json['isReturnedOrder'] as bool?,
      pickupAddress: json['pickupAddress'] as String?,
      timeSlot: json['timeSlot'] as String?,
      pickupDate: json['pickupDate'] as String?,
      totalPrice: json['totalPrice'] as String?,
    );

Map<String, dynamic> _$OrderListDataModelToJson(OrderListDataModel instance) => <String, dynamic>{
      'bookingId': instance.bookingId,
      'bookingDate': instance.bookingDate,
      'name': instance.name,
      'number': instance.number,
      'make': instance.make,
      'model': instance.model,
      'services': instance.services,
      'distance': instance.distance,
      'bookingStatus': instance.bookingStatus,
      'isReturnedOrder': instance.isReturnedOrder,
      'pickupAddress': instance.pickupAddress,
      'timeSlot': instance.timeSlot,
      'pickupDate': instance.pickupDate,
      'totalPrice': instance.totalPrice,
    };

OrderServices _$OrderServicesFromJson(Map<String, dynamic> json) => OrderServices(
      serviceDetail: json['serviceDetail'] as String?,
      serviceDetailLevel: json['serviceDetailLevel'] as String?,
      level: json['level'] as String?,
    );

Map<String, dynamic> _$OrderServicesToJson(OrderServices instance) => <String, dynamic>{
      'serviceDetail': instance.serviceDetail,
      'serviceDetailLevel': instance.serviceDetailLevel,
      'level': instance.level,
    };
