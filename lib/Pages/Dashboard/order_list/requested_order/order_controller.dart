import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../../Utils/pagination_helper.dart';
import '../../../../api/api_end_point.dart';
import '../../../../api/api_interface.dart';
import '../../../../api/api_presenter.dart';
import '../../../../core/app_preference/app_preferences.dart';
import '../../../../core/app_preference/storage_keys.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../models/accept_order_data_model.dart';
import '../../../../models/active_order_data_model.dart';
import '../../custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import 'models/order_list_model.dart';

class OrderController extends GetxController implements ApiCallBacks {
  String branchId = '3';

  // String page = '1';
  RxList<OrderListDataModel> requestedOrderListData = RxList();
  RxList<ActiveOrderDataModel> activeOrderData = RxList();
  RxList<OrderListDataModel> completedOrderListData = RxList();
  OrderStatus? orderType;
  TextEditingController orderRejectedRemarks = TextEditingController();
  AcceptOrderDataModel acceptOrderData = AcceptOrderDataModel();
  late PaginationHelper paginationHelper;
  bool isRequestedApiCalled = false;
  bool isCompletedApiCalled = false;
  bool isActiveApiCalled = false;
  bool isFromGet = true;

  RxBool isSearching = false.obs;
  TextEditingController searchOrderId = TextEditingController();
  FocusNode orderSearchFocus = FocusNode();

  void getOrderData(OrderStatus status) {
    isFromGet = true;
    orderType = status;
    var controller = Get.find<CustomDashboardStepperController>();
    ApiPresenter(this).getOrderData(
      AppPreferences.sharedPrefRead(StorageKeys.garageId),
      controller.orderListType[status]!,
      controller.orderDate.value,
      controller.pickUpDate.value,
      controller.serviceLevelList.where((item) => controller.selectedServiceLevel.contains(item.title)).map((item) => item.id).toList().join(','),
      controller.minPriceController.text,
      controller.manPriceController.text,
      controller.workStatus.value,
      paginationHelper.pageNumber.toString(),
      pageLimit.toString(),
      '',
    );
  }

  void acceptOrder(String orderId, bool isPickup) {
    acceptOrderData.status = workStatusType[WorkStatusType.accepted];
    isFromGet = false;
    acceptOrderData.remarks = "";
    acceptOrderData.isThirdPartyAssigned = !isPickup;
    ApiPresenter(this).acceptRequestOrder(acceptOrderData.toJson(), orderId);
  }

  void rejectOrder(String orderId) {
    acceptOrderData.status = workStatusType[WorkStatusType.rejected];
    isFromGet = false;
    acceptOrderData.remarks = orderRejectedRemarks.text;
    acceptOrderData.isThirdPartyAssigned = false;
    ApiPresenter(this).acceptRequestOrder(acceptOrderData.toJson(), orderId);
  }

  void searchOrder(OrderStatus status) {
    isFromGet = true;
    orderType = status;
    var controller = Get.find<CustomDashboardStepperController>();
    ApiPresenter(this).getOrderData(
      branchId,
      controller.orderListType[status]!,
      controller.orderDate.value,
      controller.pickUpDate.value,
      controller.serviceLevelList.where((item) => controller.selectedServiceLevel.contains(item.title)).map((item) => item.id).toList().join(','),
      controller.minPriceController.text,
      controller.manPriceController.text,
      controller.workStatus.value,
      paginationHelper.pageNumber.toString(),
      pageLimit.toString(),
      searchOrderId.text,
    );
  }

  WorkStatusType? getWorkStatusColor(String status) {
    for (var entry in workStatusType.entries) {
      if (entry.value == status) {
        return entry.key;
      }
    }
    return null;
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {}

  @override
  void onError(String errorMsg, responseCode, String apiEndPoint) {}

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.getOrderData:
        if (isFromGet) {
          if (orderType == OrderStatus.requested) {
            if (paginationHelper.pageNumber == 1) {
              requestedOrderListData.clear();
            }
            RxList<OrderListDataModel> pageRequestedList = RxList();
            pageRequestedList.addAll((object['data']['results'] as List).map((e) => OrderListDataModel.fromJson(e)));
            if (pageRequestedList.isNotEmpty) {
              if (pageRequestedList.length < pageLimit) {
                paginationHelper.isLastPage = true;
              }
              requestedOrderListData.addAll(pageRequestedList);
              paginationHelper.isLoading.value = false;
              requestedOrderListData.refresh();
            } else {
              paginationHelper.isLastPage = true;
            }
          } else if (orderType == OrderStatus.active) {
            if (paginationHelper.pageNumber == 1) {
              activeOrderData.clear();
            }
            RxList<ActiveOrderDataModel> pageActiveList = RxList();
            pageActiveList.addAll((object['data']['results'] as List).map((e) => ActiveOrderDataModel.fromJson(e)));
            if (pageActiveList.isNotEmpty) {
              if (pageActiveList.length < pageLimit) {
                paginationHelper.isLastPage = true;
              }
              activeOrderData.addAll(pageActiveList);
              paginationHelper.isLoading.value = false;
              activeOrderData.refresh();
            } else {
              paginationHelper.isLastPage = true;
            }
          } else {
            if (paginationHelper.pageNumber == 1) {
              completedOrderListData.clear();
            }
            RxList<OrderListDataModel> pageCompletedList = RxList();
            pageCompletedList.addAll((object['data']['results'] as List).map((e) => OrderListDataModel.fromJson(e)));
            if (pageCompletedList.isNotEmpty) {
              if (pageCompletedList.length < pageLimit) {
                paginationHelper.isLastPage = true;
              }
              completedOrderListData.addAll(pageCompletedList);
              paginationHelper.isLoading.value = false;
              completedOrderListData.refresh();
            } else {
              paginationHelper.isLastPage = true;
            }
          }
        } else {
          getOrderData(orderType!);
        }
        // orderSearchFocus.requestFocus();
        FocusScope.of(Get.context!).requestFocus(orderSearchFocus);
        break;
    }
  }

  void resetPagination() {
    if (paginationHelper != null) {
      paginationHelper.isLastPage = false;
      paginationHelper.pageNumber = 1;
      paginationHelper.isLoading.value = false;
    }
  }
}
