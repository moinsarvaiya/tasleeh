import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../../Utils/utilities.dart';
import '../../../../common/common_order_card.dart';
import '../../../../common/custom_dialog.dart';
import '../../../../common/date_picker.dart';
import '../../../../common/input_text_field.dart';
import '../../../../common/space_horizontal.dart';
import '../../../../common/space_vertical.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/app_images.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../core/constant/widget_functions.dart';
import '../../../../core/extensions/common_extension.dart';
import '../../../../custom_classes/custom_dropdown_field.dart';
import '../../../../l10n/app_string_key.dart';
import '../../../custom_stepper/widgets/custom_square_corner_button.dart';
import '../../custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import 'order_controller.dart';

class RequestedOrderMobileScreen extends StatelessWidget {
  final OrderController controller;

  const RequestedOrderMobileScreen({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStringKey.requested_order_list_title.tr,
                    style: BaseStyle.textStyleRobotoBold(
                      18,
                      AppColors.inputFieldHeading,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    AppStringKey.order_list_desc.tr,
                    style: BaseStyle.textStyleRobotoRegular(
                      12,
                      AppColors.inputFieldHeading,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Theme(
                    data: context.theme.copyWith(useMaterial3: false),
                    child: IntrinsicWidth(
                      child: Material(
                        color: Colors.transparent,
                        child: Ink(
                          child: InkWell(
                            onTap: () {
                              filterBottomSheet(context, OrderStatus.requested);
                            },
                            child: Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: AppColors.primaryTextColor,
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.circular(4.0),
                              ),
                              child: SvgPicture.asset(
                                AppImages.filter,
                                width: 20,
                                height: 20,
                                color: AppColors.primaryTextColor,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Ink(
                    child: InkWell(
                      onTap: () {
                        controller.isSearching.value = !controller.isSearching.value;
                      },
                      child: Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: AppColors.primaryTextColor,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        child: Icon(
                          controller.isSearching.value ? Icons.close : Icons.search,
                          size: 20,
                          color: AppColors.primaryTextColor,
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
          controller.isSearching.value
              ? const SizedBox(
                  height: 16,
                )
              : const SizedBox.shrink(),
          controller.isSearching.value
              ? TextFormField(
                  controller: controller.searchOrderId,
                  style: BaseStyle.textStyleRobotoRegular(
                    10,
                    AppColors.primaryTextColor,
                  ),
                  decoration: InputDecoration(
                    fillColor: AppColors.colorWhite,
                    filled: true,
                    contentPadding: const EdgeInsets.symmetric(
                      vertical: 4,
                      horizontal: 8,
                    ),
                    constraints: const BoxConstraints(maxHeight: 40),
                    suffixIcon: MouseRegion(
                      cursor: SystemMouseCursors.click,
                      child: GestureDetector(
                        onTap: () {
                          controller.isSearching.value = !controller.isSearching.value;
                          controller.searchOrderId.clear();
                        },
                        child: const Icon(
                          Icons.close,
                          size: 12,
                        ),
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(2),
                      borderSide: const BorderSide(
                        color: AppColors.borderColor,
                        width: 0,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(2),
                      borderSide: const BorderSide(
                        color: AppColors.borderColor,
                        width: 0,
                      ),
                    ),
                  ),
                  focusNode: controller.orderSearchFocus,
                  autofocus: true,
                  onChanged: (value) {
                    Utilities.customDebounce(customDebounceTime, () {
                      controller.searchOrder(OrderStatus.requested);
                    });
                  },
                )
              : const SizedBox.shrink(),
          const SizedBox(
            height: 16,
          ),
          Expanded(
            child: controller.requestedOrderListData.isNotEmpty
                ? Stack(
                    children: [
                      ListView.builder(
                        shrinkWrap: true,
                        controller: controller.paginationHelper.scrollController,
                        itemCount: controller.requestedOrderListData.length,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: const EdgeInsets.only(bottom: 10),
                            child: Column(
                              children: [
                                CommonOrderCard(
                                  bottomPadding: 0,
                                  topLeftHeading: controller.requestedOrderListData[index].name!,
                                  topLeftSubHeading: "#${controller.requestedOrderListData[index].bookingId!}",
                                  topRightHeading: controller.requestedOrderListData[index].totalPrice!,
                                  topRightSubHeading: controller.requestedOrderListData[index].bookingDate!,
                                  centerLeftIcon: const Icon(
                                    Icons.warehouse_outlined,
                                    size: 12,
                                    color: AppColors.secondaryTextColor,
                                  ),
                                  centerLeftHeading: AppStringKey.stepper_services.tr,
                                  centerLeftContent: controller.requestedOrderListData[index].services!.length > 1
                                      ? "${controller.requestedOrderListData[index].services![0].serviceDetail} +${controller.requestedOrderListData[index].services!.length}"
                                      : '${controller.requestedOrderListData[index].services![0].serviceDetail}',
                                  centerRightIcon: const Icon(
                                    Icons.warehouse_outlined,
                                    size: 12,
                                    color: AppColors.secondaryTextColor,
                                  ),
                                  centerRightHeading: AppStringKey.vehicle_model.tr,
                                  centerRightContent:
                                      "${controller.requestedOrderListData[index].make} ${controller.requestedOrderListData[index].model}",
                                  showBottomContent: false,
                                  bottomIcon: SvgPicture.asset(
                                    AppImages.remarks,
                                    width: 12,
                                    height: 12,
                                    color: AppColors.secondaryTextColor,
                                  ),
                                  bottomContent: controller.requestedOrderListData[index].pickupAddress!.isEmpty
                                      ? AppStringKey.self_drop.tr
                                      : controller.requestedOrderListData[index].pickupAddress!,
                                  bottomHeading: AppStringKey.remarks.tr,
                                ),
                                Container(
                                  padding: const EdgeInsets.symmetric(vertical: 8),
                                  decoration: BoxDecoration(
                                    border: LRTB_Border(1, 1, 0.5, 1, AppColors.strokeColor),
                                    borderRadius: BorderRadius.circular(4),
                                    color: AppColors.colorWhite,
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      CustomSquareCornerButton(
                                        buttonText: AppStringKey.reject.tr,
                                        buttonTextColor: AppColors.errorTextColor,
                                        backgroundColor: AppColors.lightRedBackgroundColor,
                                        isBorderVisible: true,
                                        borderColor: AppColors.errorTextColor,
                                        borderWidth: 1,
                                        height: 0.05,
                                        width: 0.20,
                                        textSize: 12,
                                        callBack: () {
                                          showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return CustomDialog(
                                                padding: const EdgeInsets.only(
                                                  top: 32,
                                                  right: 32,
                                                  bottom: 24,
                                                  left: 32,
                                                ),
                                                content: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  mainAxisSize: MainAxisSize.min,
                                                  children: [
                                                    Row(
                                                      mainAxisSize: MainAxisSize.min,
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Flexible(
                                                          child: Column(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            children: [
                                                              Text(
                                                                AppStringKey.decline_return_service_for_order.tr,
                                                                style: BaseStyle.textStyleRobotoSemiBold(
                                                                  16,
                                                                  AppColors.primaryTextColor,
                                                                ),
                                                              ),
                                                              Text(
                                                                '#${controller.requestedOrderListData[index].bookingId!}',
                                                                style: BaseStyle.textStyleRobotoSemiBold(
                                                                  16,
                                                                  AppColors.primaryTextColor,
                                                                ),
                                                              ),
                                                              const SizedBox(
                                                                height: 8,
                                                              ),
                                                              Text(
                                                                  "${AppStringKey.specify_reason_for_order_decline.tr} #${controller.requestedOrderListData[index].bookingId!}")
                                                            ],
                                                          ),
                                                        ),
                                                        const SizedBox(
                                                          width: 16,
                                                        ),
                                                        IconButton(
                                                          onPressed: () {
                                                            Navigator.pop(context);
                                                          },
                                                          color: AppColors.primaryTextColor,
                                                          splashRadius: 1,
                                                          padding: EdgeInsets.zero,
                                                          icon: const Icon(
                                                            Icons.close,
                                                            size: 24,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    InputTextField(
                                                      hintText: AppStringKey.enter_reason.tr,
                                                      label: '',
                                                      maxLength: 100,
                                                      maxLines: 3,
                                                      textInputType: TextInputType.multiline,
                                                      isRequireField: false,
                                                      textEditingController: controller.orderRejectedRemarks,
                                                    ),
                                                    const SizedBox(
                                                      height: 24,
                                                    ),
                                                    TextButton(
                                                      onPressed: () {
                                                        controller.rejectOrder(controller.requestedOrderListData[index].bookingId!);
                                                        Navigator.pop(context);
                                                      },
                                                      style: TextButton.styleFrom(
                                                        textStyle: BaseStyle.textStyleRobotoRegular(
                                                          14,
                                                          AppColors.colorWhite,
                                                        ),
                                                        shape: RoundedRectangleBorder(
                                                          borderRadius: BorderRadius.circular(
                                                            2,
                                                          ),
                                                        ),
                                                        foregroundColor: AppColors.colorWhite,
                                                        backgroundColor: AppColors.primaryTextColor,
                                                      ),
                                                      child: Text(AppStringKey.confirm.tr),
                                                    )
                                                  ],
                                                ),
                                              );
                                            },
                                          );
                                        },
                                      ),
                                      const SpaceHorizontal(0.02),
                                      CustomSquareCornerButton(
                                        buttonText: AppStringKey.accept.tr,
                                        buttonTextColor: AppColors.successTextGreenColor,
                                        backgroundColor: AppColors.greenBackgroundColor,
                                        isBorderVisible: true,
                                        borderColor: AppColors.successTextGreenColor,
                                        borderWidth: 1,
                                        height: 0.05,
                                        width: 0.20,
                                        textSize: 12,
                                        callBack: () {
                                          controller.requestedOrderListData[index].pickupAddress!.isEmpty
                                              ? controller.acceptOrder(controller.requestedOrderListData[index].bookingId!, false)
                                              : showDialog(
                                                  context: context,
                                                  builder: (BuildContext context) {
                                                    return CustomDialog(
                                                      padding: const EdgeInsets.only(
                                                        left: 32,
                                                        right: 32,
                                                        top: 32,
                                                        bottom: 24,
                                                      ),
                                                      content: Column(
                                                        mainAxisSize: MainAxisSize.min,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          Row(
                                                            mainAxisSize: MainAxisSize.min,
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            children: [
                                                              Flexible(
                                                                child: Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  children: [
                                                                    Text(
                                                                      '${AppStringKey.pick_for_requested_order.tr} #${controller.requestedOrderListData[index].bookingId!}',
                                                                      style: BaseStyle.textStyleRobotoSemiBold(
                                                                        16,
                                                                        AppColors.primaryTextColor,
                                                                      ),
                                                                      overflow: TextOverflow.ellipsis,
                                                                    ),
                                                                    const SizedBox(
                                                                      height: 8,
                                                                    ),
                                                                    Text(
                                                                      AppStringKey.will_be_able_to_pick_order.tr,
                                                                      style: BaseStyle.textStyleRobotoRegular(
                                                                        14,
                                                                        AppColors.primaryTextColor,
                                                                      ),
                                                                      overflow: TextOverflow.ellipsis,
                                                                    ),
                                                                    Text(
                                                                      "${AppStringKey.pickup.tr}: ${controller.requestedOrderListData[index].pickupAddress!}",
                                                                      style: BaseStyle.textStyleRobotoRegular(
                                                                        14,
                                                                        AppColors.primaryTextColor,
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                      "${AppStringKey.slot.tr}: ${controller.requestedOrderListData[index].timeSlot!}",
                                                                      style: BaseStyle.textStyleRobotoRegular(
                                                                        14,
                                                                        AppColors.primaryTextColor,
                                                                      ),
                                                                      overflow: TextOverflow.ellipsis,
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                              const SizedBox(
                                                                width: 16,
                                                              ),
                                                            ],
                                                          ),
                                                          const SizedBox(
                                                            height: 24,
                                                          ),
                                                          Wrap(
                                                            spacing: 8,
                                                            alignment: WrapAlignment.start,
                                                            children: [
                                                              TextButton(
                                                                onPressed: () {
                                                                  controller.acceptOrder(controller.requestedOrderListData[index].bookingId!, true);
                                                                  Navigator.pop(context);
                                                                  Utilities.showToast(
                                                                    "${AppStringKey.dashboard_return_order.tr} : ${AppStringKey.pickup.tr}\n${AppStringKey.you_need_to_pickup_order.tr} ${controller.requestedOrderListData[index].bookingId!}\n${AppStringKey.at.tr} ${controller.requestedOrderListData[index].pickupAddress} ${AppStringKey.between.tr} ${controller.requestedOrderListData[index].timeSlot!}.",
                                                                    fontSize: 16,
                                                                    webPosition: 'right',
                                                                    webBgColor: '#DDF3EAFF',
                                                                    textColor: AppColors.secondaryTextColor,
                                                                  );
                                                                },
                                                                style: TextButton.styleFrom(
                                                                  textStyle: BaseStyle.textStyleRobotoRegular(
                                                                    14,
                                                                    const Color.fromARGB(255, 22, 11, 11),
                                                                  ),
                                                                  shape: RoundedRectangleBorder(
                                                                    borderRadius: BorderRadius.circular(
                                                                      2,
                                                                    ),
                                                                  ),
                                                                  foregroundColor: AppColors.colorWhite,
                                                                  backgroundColor: AppColors.primaryTextColor,
                                                                ),
                                                                child: Text(AppStringKey.yes.tr),
                                                              ),
                                                              TextButton(
                                                                onPressed: () {
                                                                  Navigator.pop(context);
                                                                  showDialog(
                                                                    context: context,
                                                                    builder: (BuildContext context) {
                                                                      return CustomDialog(
                                                                        padding: const EdgeInsets.only(
                                                                          left: 32,
                                                                          top: 32,
                                                                          right: 32,
                                                                          bottom: 24,
                                                                        ),
                                                                        maxWidth: 416,
                                                                        maxHeight: 280,
                                                                        content: Column(
                                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                                          children: [
                                                                            Row(
                                                                              mainAxisSize: MainAxisSize.min,
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                                                Flexible(
                                                                                  child: Column(
                                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                                                    children: [
                                                                                      Text(
                                                                                        AppStringKey.cancel_pick_for_requested_order.tr,
                                                                                        style: BaseStyle.textStyleRobotoSemiBold(
                                                                                          16,
                                                                                          AppColors.primaryTextColor,
                                                                                        ),
                                                                                      ),
                                                                                      Text(
                                                                                        '#${controller.requestedOrderListData[index].bookingId!}',
                                                                                        style: BaseStyle.textStyleRobotoSemiBold(
                                                                                          16,
                                                                                          AppColors.primaryTextColor,
                                                                                        ),
                                                                                      ),
                                                                                      const SizedBox(
                                                                                        height: 8,
                                                                                      ),
                                                                                      Text(
                                                                                        AppStringKey.are_you_sure_cancel_return_order.tr,
                                                                                        style: BaseStyle.textStyleRobotoRegular(
                                                                                          14,
                                                                                          AppColors.primaryTextColor,
                                                                                        ),
                                                                                      ),
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                                const SizedBox(
                                                                                  width: 16,
                                                                                ),
                                                                                IconButton(
                                                                                  onPressed: () {},
                                                                                  color: AppColors.primaryTextColor,
                                                                                  splashRadius: 1,
                                                                                  padding: EdgeInsets.zero,
                                                                                  icon: const Icon(
                                                                                    Icons.close,
                                                                                    size: 24,
                                                                                  ),
                                                                                )
                                                                              ],
                                                                            ),
                                                                            const SizedBox(
                                                                              height: 24,
                                                                            ),
                                                                            TextButton(
                                                                              onPressed: () {
                                                                                controller.acceptOrder(
                                                                                    controller.requestedOrderListData[index].bookingId!, false);
                                                                                Navigator.pop(context);
                                                                                Utilities.showToast(
                                                                                  "${AppStringKey.dashboard_return_order.tr} : ${AppStringKey.third_party.tr}\n${AppStringKey.you_need_to_pickup_order.tr} ${controller.requestedOrderListData[index].bookingId!}\n${AppStringKey.at.tr} ${controller.requestedOrderListData[index].pickupAddress!} ${AppStringKey.between.tr} ${controller.requestedOrderListData[index].timeSlot}.",
                                                                                  fontSize: 16,
                                                                                  webPosition: 'right',
                                                                                  webBgColor: '#DDF3EAFF',
                                                                                  textColor: AppColors.secondaryTextColor,
                                                                                );
                                                                              },
                                                                              style: TextButton.styleFrom(
                                                                                textStyle: BaseStyle.textStyleRobotoRegular(
                                                                                  14,
                                                                                  AppColors.colorWhite,
                                                                                ),
                                                                                shape: RoundedRectangleBorder(
                                                                                  borderRadius: BorderRadius.circular(
                                                                                    2,
                                                                                  ),
                                                                                ),
                                                                                foregroundColor: AppColors.colorWhite,
                                                                                backgroundColor: AppColors.primaryTextColor,
                                                                              ),
                                                                              child: Text(AppStringKey.confirm.tr),
                                                                            )
                                                                          ],
                                                                        ),
                                                                      );
                                                                    },
                                                                  );
                                                                },
                                                                style: TextButton.styleFrom(
                                                                  textStyle: BaseStyle.textStyleRobotoRegular(
                                                                    14,
                                                                    AppColors.colorWhite,
                                                                  ),
                                                                  side: const BorderSide(
                                                                    color: AppColors.strokeColor,
                                                                    width: 1,
                                                                  ),
                                                                  shape: RoundedRectangleBorder(
                                                                    borderRadius: BorderRadius.circular(
                                                                      2,
                                                                    ),
                                                                  ),
                                                                  foregroundColor: AppColors.primaryTextColor,
                                                                  backgroundColor: AppColors.colorWhite,
                                                                ),
                                                                child: Text(AppStringKey.no.tr),
                                                              )
                                                            ],
                                                          )
                                                        ],
                                                      ),
                                                    );
                                                  },
                                                );
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                      Obx(
                        () => Get.find<OrderController>().paginationHelper.isLoading.value
                            ? Positioned(
                                bottom: 0,
                                left: 0,
                                right: 0,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const SizedBox(
                                      width: 20,
                                      height: 20,
                                      child: CircularProgressIndicator(
                                        color: AppColors.primaryCtaColor,
                                        strokeWidth: 3,
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                      AppStringKey.pagination_loading.tr,
                                      style: BaseStyle.textStyleRobotoMedium(
                                        16,
                                        AppColors.primaryCtaColor,
                                      ),
                                    )
                                  ],
                                ),
                              )
                            : const SizedBox.expand(),
                      ),
                    ],
                  )
                : noDataFound(),
          ),
        ],
      ),
    );
  }
}

void filterBottomSheet(BuildContext context, OrderStatus status) {
  var controller = Get.find<CustomDashboardStepperController>();
  showModalBottomSheet(
    backgroundColor: Colors.white,
    constraints: BoxConstraints(maxHeight: context.screenHeight * 0.95),
    isScrollControlled: true,
    context: context,
    builder: (builder) {
      return GestureDetector(
        onTap: () {
          Get.back();
        },
        child: ClipRect(
          clipBehavior: Clip.antiAlias,
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(0),
                topRight: Radius.circular(0),
              ),
            ),
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        child: SizedBox(
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppStringKey.filter.tr,
                                style: BaseStyle.textStyleRobotoBold(
                                  20,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              const SpaceVertical(0.01),
                              Text(
                                AppStringKey.order_list_filter.tr,
                                style: BaseStyle.textStyleRobotoRegular(
                                  12,
                                  AppColors.primaryTextColor,
                                ),
                                maxLines: 2,
                              ),
                              const SpaceVertical(0.02),
                              Container(
                                width: double.infinity,
                                height: 2,
                                color: AppColors.dividerColor,
                              ),
                              const SpaceVertical(0.05),
                              Text(
                                AppStringKey.order_date.tr,
                                style: BaseStyle.textStyleRobotoRegular(14, AppColors.primaryTextColor),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              DatePicker(
                                dateSetter: (date) {
                                  controller.orderDate.value = date;
                                },
                                textEditingController: controller.orderDateFieldController,
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Text(
                                AppStringKey.pick_up_date.tr,
                                style: BaseStyle.textStyleRobotoRegular(14, AppColors.primaryTextColor),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              DatePicker(
                                dateSetter: (date) {
                                  controller.pickUpDate.value = date;
                                },
                                textEditingController: controller.pickupDateFieldController,
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              CustomDropDownField(
                                buttonPadding: const EdgeInsets.only(left: 0),
                                labelVisibility: true,
                                hintTitle: AppStringKey.select_service_levels.tr,
                                items: controller.serviceLevelList.map((element) => element.title).toList(),
                                validationText: '',
                                onChanged: (val) {
                                  if (!controller.selectedServiceLevel.contains(val)) {
                                    controller.selectedServiceLevel.add(val!);
                                    controller.selectedServiceLevel.refresh();
                                  }
                                },
                                labelText: AppStringKey.label_select_service_levels.tr,
                                selectedValue: '',
                                textStyle: BaseStyle.textStyleRobotoRegular(
                                  16,
                                  AppColors.inputFieldHintTextColor,
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Obx(
                                () => Wrap(
                                  children: [
                                    ...controller.selectedServiceLevel.indexed.map(
                                      (e) {
                                        var index = e.$1;
                                        var item = e.$2;
                                        return Container(
                                          margin: const EdgeInsets.only(right: 5),
                                          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              width: 0,
                                              color: AppColors.borderColor,
                                            ),
                                          ),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Center(
                                                child: Text(
                                                  item,
                                                  style: BaseStyle.textStyleRobotoRegular(
                                                    14,
                                                    AppColors.primaryCtaColor,
                                                  ),
                                                ),
                                              ),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              Material(
                                                child: Ink(
                                                  child: InkWell(
                                                    onTap: () {
                                                      controller.selectedServiceLevel.removeAt(index);
                                                      controller.selectedServiceLevel.refresh();
                                                    },
                                                    child: const Icon(
                                                      Icons.close,
                                                      size: 15,
                                                      color: AppColors.colorBlack,
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        );
                                      },
                                    )
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Expanded(
                                    child: InputTextField(
                                      hintText: AppStringKey.hint_min_price.tr,
                                      label: AppStringKey.label_price_range.tr,
                                      isRequireField: false,
                                      inputFormatter: [FilteringTextInputFormatter.digitsOnly],
                                      focusScope: controller.minPriceFocusNode,
                                      textEditingController: controller.minPriceController,
                                      isReadOnly: false,
                                      readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                                      onChangeWithValidationStatus: (value, isValid) {},
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  Expanded(
                                    child: InputTextField(
                                      hintText: AppStringKey.hint_max_price.tr,
                                      label: AppStringKey.label_price_range.tr,
                                      inputFormatter: [FilteringTextInputFormatter.digitsOnly],
                                      isLabelVisible: false,
                                      isRequireField: false,
                                      focusScope: controller.manPriceFocusNode,
                                      textEditingController: controller.manPriceController,
                                      isReadOnly: false,
                                      readOnlyBackgroundColor: AppColors.tertiaryBackgroundColor,
                                      onChangeWithValidationStatus: (value, isValid) {},
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              CustomDropDownField(
                                buttonPadding: const EdgeInsets.only(left: 0),
                                labelVisibility: true,
                                hintTitle: AppStringKey.select_work_status.tr,
                                items: controller.workStatusList.map((e) => e.statusTitle).toList(),
                                validationText: '',
                                onChanged: (val) {
                                  controller.workStatus.value =
                                      controller.workStatusList.firstWhere((element) => element.statusTitle == val).statusKey;
                                },
                                labelText: AppStringKey.work_status.tr,
                                selectedValue: '',
                                textStyle: BaseStyle.textStyleRobotoRegular(
                                  16,
                                  AppColors.inputFieldHintTextColor,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CustomSquareCornerButton(
                              buttonText: AppStringKey.apply_filter.tr,
                              buttonTextColor: AppColors.colorWhite,
                              backgroundColor: AppColors.colorBlack,
                              height: 0.07,
                              width: 0.7,
                              textSize: 12,
                              callBack: () {
                                Get.find<OrderController>().resetPagination();
                                Get.find<OrderController>().getOrderData(status);
                                Get.back();
                              },
                            ),
                            const SpaceVertical(0.01),
                            Material(
                              child: Ink(
                                child: InkWell(
                                  onTap: () {
                                    controller.clearData();
                                    Get.find<OrderController>().resetPagination();
                                    Get.find<OrderController>().getOrderData(status);
                                    Get.back();
                                  },
                                  child: Text(
                                    AppStringKey.reset.tr,
                                    style: BaseStyle.textStyleRobotoMedium(
                                      14,
                                      AppColors.colorBlack,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Positioned(
                  right: 00,
                  top: 00,
                  child: Material(
                    color: Colors.transparent,
                    child: Ink(
                      child: InkWell(
                        onTap: () {
                          Get.back();
                        },
                        child: const Icon(
                          Icons.close,
                          color: AppColors.colorBlack,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );
    },
  );
}

Widget noDataFound() {
  return Center(
    child: Text(
      AppStringKey.no_data_found.tr,
      style: BaseStyle.textStyleRobotoSemiBold(
        16,
        AppColors.colorBlack,
      ),
    ),
  );
}
