import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../../Utils/utilities.dart';
import '../../../../common/common_order_table.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/app_images.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../l10n/app_string_key.dart';
import '../../custom_dashboard_drawer/widgets/summary_widget.dart';
import '../requested_order/order_controller.dart';

class CompletedOrderWebScreen extends StatelessWidget {
  final OrderController controller;

  const CompletedOrderWebScreen({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SummaryWidget(),
        const SizedBox(
          height: 8,
        ),
        Text(
          AppStringKey.above_data_from_the_past.tr,
          style: BaseStyle.textStyleRobotoMediumBold(
            12,
            AppColors.inputFieldHeading,
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              AppStringKey.completed_order_list_title.tr,
              style: BaseStyle.textStyleRobotoSemiBold(
                24,
                AppColors.colorBlack,
              ),
            ),
            IntrinsicWidth(
              child: Material(
                color: Colors.transparent,
                child: Ink(
                  child: InkWell(
                    onTap: () {
                      dashboardCompletedOrderListDrawerKey.currentState?.openEndDrawer();
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 15,
                        vertical: 6.5,
                      ),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 1.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            AppImages.filter,
                            width: 24,
                            height: 24,
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Text(
                            AppStringKey.filter.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              16,
                              AppColors.primaryTextColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 24,
        ),
        Obx(
          () => Expanded(
            child: CommonOrderTable(
              scrollController: controller.paginationHelper.scrollController,
              columnSpacing: 10,
              padding: const EdgeInsets.all(0),
              columns: [
                DataColumn2(
                  fixedWidth: 132,
                  label: Obx(
                    () => controller.isSearching.value
                        ? TextFormField(
                            controller: Get.find<OrderController>().searchOrderId,
                            style: BaseStyle.textStyleRobotoRegular(
                              10,
                              AppColors.primaryTextColor,
                            ),
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.symmetric(
                                vertical: 4,
                                horizontal: 8,
                              ),
                              constraints: const BoxConstraints(maxHeight: 40),
                              suffixIcon: MouseRegion(
                                cursor: SystemMouseCursors.click,
                                child: GestureDetector(
                                  onTap: () {
                                    Get.find<OrderController>().isSearching.value = !Get.find<OrderController>().isSearching.value;
                                    Get.find<OrderController>().searchOrderId.clear();
                                  },
                                  child: const Icon(
                                    Icons.close,
                                    size: 12,
                                  ),
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(2),
                                borderSide: const BorderSide(
                                  color: AppColors.borderColor,
                                  width: 0,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(2),
                                borderSide: const BorderSide(
                                  color: AppColors.borderColor,
                                  width: 0,
                                ),
                              ),
                            ),
                            autofocus: true,
                            onChanged: (value) {
                              Utilities.customDebounce(customDebounceTime, () {
                                Get.find<OrderController>().searchOrder(OrderStatus.completed);
                              });
                            },
                          )
                        : Row(
                            children: [
                              Text(
                                AppStringKey.order_details.tr,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                              ),
                              const SizedBox(
                                width: 4,
                              ),
                              Material(
                                child: Ink(
                                  child: InkWell(
                                    onTap: () {
                                      Get.find<OrderController>().isSearching.value = !Get.find<OrderController>().isSearching.value;
                                    },
                                    child: const Icon(
                                      Icons.search,
                                      size: 20,
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.customer_info.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.vehicle_model.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.requested_title_service_selected.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.requested_title_pickup_detail.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  fixedWidth: 100,
                  label: Text(
                    AppStringKey.service_completed.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  fixedWidth: 100,
                  label: Text(
                    AppStringKey.requested_title_amount.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
              ],
              data: List<DataRow2>.generate(
                controller.completedOrderListData.length,
                (index) => DataRow2(
                  cells: [
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '#${controller.completedOrderListData[index].bookingId!}',
                            style: BaseStyle.textStyleRobotoSemiBold(
                              14,
                              AppColors.primaryTextColor,
                            ).copyWith(
                              decoration: TextDecoration.underline,
                            ),
                          ),
                          Text(
                            "${controller.completedOrderListData[index].bookingDate}",
                            style: BaseStyle.textStyleRobotoMediumBold(
                              12,
                              AppColors.primaryTextColor,
                            ),
                          )
                        ],
                      ),
                    ),
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${controller.completedOrderListData[index].name} ",
                            style: BaseStyle.textStyleRobotoMediumBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(
                            " ${controller.completedOrderListData[index].number}",
                            style: BaseStyle.textStyleRobotoMediumBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller.completedOrderListData[index].make!,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(
                            controller.completedOrderListData[index].model!,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller.completedOrderListData[index].services!.length > 1
                                ? "${controller.completedOrderListData[index].services![0].serviceDetail} +${controller.completedOrderListData[index].services!.length}"
                                : '${controller.completedOrderListData[index].services![0].serviceDetail}',
                            style: BaseStyle.textStyleRobotoSemiBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(
                            "${controller.completedOrderListData[index].timeSlot}",
                            style: BaseStyle.textStyleRobotoMediumBold(
                              12,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          )
                        ],
                      ),
                    ),
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller.completedOrderListData[index].pickupAddress!.isEmpty
                                ? AppStringKey.self_drop.tr
                                : controller.completedOrderListData[index].pickupAddress!,
                            style: BaseStyle.textStyleRobotoSemiBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(
                            controller.completedOrderListData[index].timeSlot!,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              14,
                              AppColors.primaryTextColor,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                    DataCell(
                      Text(
                        controller.completedOrderListData[index].pickupDate.toString(),
                        style: BaseStyle.textStyleRobotoMediumBold(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      ),
                    ),
                    DataCell(
                      Text(
                        controller.completedOrderListData[index].totalPrice.toString(),
                        style: BaseStyle.textStyleRobotoMediumBold(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
