import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../../Utils/utilities.dart';
import '../../../../common/common_order_card.dart';
import '../../../../common/date_picker.dart';
import '../../../../common/space_vertical.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/app_images.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../core/extensions/common_extension.dart';
import '../../../../custom_classes/custom_dropdown_field.dart';
import '../../../../l10n/app_string_key.dart';
import '../../../custom_stepper/widgets/custom_square_corner_button.dart';
import '../../custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import '../requested_order/order_controller.dart';
import '../requested_order/requested_order_mobile_screen.dart';

class CompletedOrderMobileScreen extends StatelessWidget {
  final OrderController controller;

  const CompletedOrderMobileScreen({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStringKey.completed_order_list_title.tr,
                    style: BaseStyle.textStyleRobotoBold(
                      18,
                      AppColors.inputFieldHeading,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(
                    AppStringKey.order_list_desc.tr,
                    style: BaseStyle.textStyleRobotoRegular(
                      12,
                      AppColors.inputFieldHeading,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Theme(
                    data: context.theme.copyWith(useMaterial3: false),
                    child: IntrinsicWidth(
                      child: Material(
                        color: Colors.transparent,
                        child: Ink(
                          child: InkWell(
                            onTap: () {
                              completedOrderFilterBottomSheet(context, OrderStatus.completed);
                            },
                            child: Container(
                              padding: const EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Colors.black,
                                  width: 1,
                                ),
                                borderRadius: BorderRadius.circular(4.0),
                              ),
                              child: SvgPicture.asset(
                                AppImages.filter,
                                width: 20,
                                height: 20,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  Ink(
                    child: InkWell(
                      onTap: () {
                        controller.isSearching.value = !controller.isSearching.value;
                      },
                      child: Container(
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: AppColors.primaryTextColor,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        child: Icon(
                          controller.isSearching.value ? Icons.close : Icons.search,
                          size: 20,
                          color: AppColors.primaryTextColor,
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
          controller.isSearching.value
              ? const SizedBox(
                  height: 16,
                )
              : const SizedBox.shrink(),
          controller.isSearching.value
              ? TextFormField(
                  controller: controller.searchOrderId,
                  style: BaseStyle.textStyleRobotoRegular(
                    10,
                    AppColors.primaryTextColor,
                  ),
                  decoration: InputDecoration(
                    fillColor: AppColors.colorWhite,
                    filled: true,
                    contentPadding: const EdgeInsets.symmetric(
                      vertical: 4,
                      horizontal: 8,
                    ),
                    constraints: const BoxConstraints(maxHeight: 40),
                    suffixIcon: MouseRegion(
                      cursor: SystemMouseCursors.click,
                      child: GestureDetector(
                        onTap: () {
                          controller.isSearching.value = !controller.isSearching.value;
                          controller.searchOrderId.clear();
                        },
                        child: const Icon(
                          Icons.close,
                          size: 12,
                        ),
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(2),
                      borderSide: const BorderSide(
                        color: AppColors.borderColor,
                        width: 0,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(2),
                      borderSide: const BorderSide(
                        color: AppColors.borderColor,
                        width: 0,
                      ),
                    ),
                  ),
                  focusNode: controller.orderSearchFocus,
                  autofocus: true,
                  onChanged: (value) {
                    Utilities.customDebounce(customDebounceTime, () {
                      controller.searchOrder(OrderStatus.completed);
                    });
                  },
                )
              : const SizedBox.shrink(),
          const SizedBox(
            height: 16,
          ),
          Obx(
            () => Expanded(
              child: controller.completedOrderListData.isNotEmpty
                  ? Stack(
                      children: [
                        ListView.builder(
                          shrinkWrap: true,
                          controller: controller.paginationHelper.scrollController,
                          itemCount: controller.completedOrderListData.length,
                          itemBuilder: (context, index) {
                            return CommonOrderCard(
                              topLeftHeading: controller.completedOrderListData[index].name!,
                              topLeftSubHeading: "#${controller.completedOrderListData[index].bookingId!}",
                              topRightHeading: controller.completedOrderListData[index].totalPrice!,
                              topRightSubHeading: controller.completedOrderListData[index].bookingDate!,
                              centerLeftIcon: const Icon(
                                Icons.warehouse_outlined,
                                size: 12,
                                color: AppColors.secondaryTextColor,
                              ),
                              centerLeftHeading: AppStringKey.stepper_services.tr,
                              centerLeftContent: controller.completedOrderListData[index].services!.length > 1
                                  ? "${controller.completedOrderListData[index].services![0].serviceDetail} +${controller.completedOrderListData[index].services!.length}"
                                  : '${controller.completedOrderListData[index].services![0].serviceDetail}',
                              centerRightIcon: const Icon(
                                Icons.warehouse_outlined,
                                size: 12,
                                color: AppColors.secondaryTextColor,
                              ),
                              centerRightHeading: AppStringKey.vehicle_model.tr,
                              centerRightContent:
                                  "${controller.completedOrderListData[index].make} ${controller.completedOrderListData[index].model}",
                              showBottomContent: true,
                              bottomIcon: SvgPicture.asset(
                                AppImages.serviceCompleted,
                                width: 12,
                                height: 12,
                                color: AppColors.secondaryTextColor,
                              ),
                              bottomContent: controller.completedOrderListData[index].pickupDate,
                              bottomHeading: AppStringKey.service_completed.tr,
                            );
                          },
                        ),
                        Obx(
                          () => Get.find<OrderController>().paginationHelper.isLoading.value
                              ? Positioned(
                                  bottom: 0,
                                  left: 0,
                                  right: 0,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const SizedBox(
                                        width: 20,
                                        height: 20,
                                        child: CircularProgressIndicator(
                                          color: AppColors.primaryCtaColor,
                                          strokeWidth: 3,
                                        ),
                                      ),
                                      const SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        AppStringKey.pagination_loading.tr,
                                        style: BaseStyle.textStyleRobotoMedium(
                                          16,
                                          AppColors.primaryCtaColor,
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              : const SizedBox.expand(),
                        ),
                      ],
                    )
                  : noDataFound(),
            ),
          ),
        ],
      ),
    );
  }
}

void completedOrderFilterBottomSheet(BuildContext context, OrderStatus status) {
  var controller = Get.find<CustomDashboardStepperController>();
  showModalBottomSheet(
    backgroundColor: Colors.white,
    constraints: BoxConstraints(maxHeight: context.screenHeight * 0.95),
    isScrollControlled: true,
    context: context,
    builder: (builder) {
      return GestureDetector(
        onTap: () {
          Get.back();
        },
        child: ClipRect(
          clipBehavior: Clip.antiAlias,
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(0),
                topRight: Radius.circular(0),
              ),
            ),
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        child: SizedBox(
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppStringKey.filter.tr,
                                style: BaseStyle.textStyleRobotoBold(
                                  20,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              const SpaceVertical(0.01),
                              Text(
                                AppStringKey.order_list_filter.tr,
                                style: BaseStyle.textStyleRobotoRegular(
                                  12,
                                  AppColors.primaryTextColor,
                                ),
                                maxLines: 2,
                              ),
                              const SpaceVertical(0.02),
                              Container(
                                width: double.infinity,
                                height: 2,
                                color: AppColors.dividerColor,
                              ),
                              const SpaceVertical(0.05),
                              Text(
                                AppStringKey.order_date.tr,
                                style: BaseStyle.textStyleRobotoRegular(14, AppColors.primaryTextColor),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              DatePicker(
                                dateSetter: (date) {
                                  controller.orderDate.value = date;
                                },
                                textEditingController: controller.orderDateFieldController,
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Text(
                                AppStringKey.pick_up_date.tr,
                                style: BaseStyle.textStyleRobotoRegular(14, AppColors.primaryTextColor),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              DatePicker(
                                dateSetter: (date) {
                                  controller.pickUpDate.value = date;
                                },
                                textEditingController: controller.pickupDateFieldController,
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              CustomDropDownField(
                                buttonPadding: const EdgeInsets.only(left: 0),
                                labelVisibility: true,
                                hintTitle: AppStringKey.select_service_levels.tr,
                                items: controller.serviceLevelList.map((element) => element.title).toList(),
                                validationText: '',
                                onChanged: (val) {
                                  if (!controller.selectedServiceLevel.contains(val)) {
                                    controller.selectedServiceLevel.add(val!);
                                    controller.selectedServiceLevel.refresh();
                                  }
                                },
                                labelText: AppStringKey.label_select_service_levels.tr,
                                selectedValue: '',
                                textStyle: BaseStyle.textStyleRobotoRegular(
                                  16,
                                  AppColors.inputFieldHintTextColor,
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Obx(
                                () => Wrap(
                                  children: [
                                    ...controller.selectedServiceLevel.indexed.map(
                                      (e) {
                                        var index = e.$1;
                                        var item = e.$2;
                                        return Container(
                                          margin: const EdgeInsets.only(right: 5),
                                          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              width: 0,
                                              color: AppColors.borderColor,
                                            ),
                                          ),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Center(
                                                child: Text(
                                                  item,
                                                  style: BaseStyle.textStyleRobotoRegular(
                                                    14,
                                                    AppColors.primaryCtaColor,
                                                  ),
                                                ),
                                              ),
                                              const SizedBox(
                                                width: 5,
                                              ),
                                              Material(
                                                child: Ink(
                                                  child: InkWell(
                                                    onTap: () {
                                                      controller.selectedServiceLevel.removeAt(index);
                                                      controller.selectedServiceLevel.refresh();
                                                    },
                                                    child: const Icon(
                                                      Icons.close,
                                                      size: 15,
                                                      color: AppColors.colorBlack,
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        );
                                      },
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CustomSquareCornerButton(
                              buttonText: AppStringKey.apply_filter.tr,
                              buttonTextColor: AppColors.colorWhite,
                              backgroundColor: AppColors.colorBlack,
                              height: 0.07,
                              width: 0.7,
                              textSize: 12,
                              callBack: () {
                                Get.find<OrderController>().resetPagination();
                                Get.find<OrderController>().getOrderData(status);
                                Get.back();
                              },
                            ),
                            const SpaceVertical(0.01),
                            Material(
                              child: Ink(
                                child: InkWell(
                                  onTap: () {
                                    controller.clearData();
                                    Get.find<OrderController>().resetPagination();
                                    Get.find<OrderController>().getOrderData(status);
                                    Get.back();
                                  },
                                  child: Text(
                                    AppStringKey.reset.tr,
                                    style: BaseStyle.textStyleRobotoMedium(
                                      14,
                                      AppColors.colorBlack,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                Positioned(
                  right: 00,
                  top: 00,
                  child: Material(
                    color: Colors.transparent,
                    child: Ink(
                      child: InkWell(
                        onTap: () {
                          Get.back();
                        },
                        child: const Icon(
                          Icons.close,
                          color: AppColors.colorBlack,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );
    },
  );
}
