import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../Utils/pagination_helper.dart';
import '../../../../core/constant/ui_constants.dart';
import '../requested_order/order_controller.dart';
import 'completed_order_mobile_screen.dart';
import 'completed_order_web_screen.dart';

class CompletedOrderScreen extends StatefulWidget {
  const CompletedOrderScreen({super.key});

  @override
  State<CompletedOrderScreen> createState() => _CompletedOrderScreenState();
}

class _CompletedOrderScreenState extends State<CompletedOrderScreen> {
  final controller = Get.find<OrderController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.getOrderData(OrderStatus.completed);
    });

    controller.paginationHelper = PaginationHelper(
      loadData: (isOngoing, page, limit, refresh) {
        controller.paginationHelper.pageNumber++;
        controller.getOrderData(OrderStatus.completed);
      },
      limit: pageLimit,
    );
  }

  @override
  Widget build(BuildContext context) {
    return context.isDesktop
        ? CompletedOrderWebScreen(
            controller: controller,
          )
        : CompletedOrderMobileScreen(
            controller: controller,
          );
  }
}
