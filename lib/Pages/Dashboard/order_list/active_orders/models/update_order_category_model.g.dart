// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_order_category_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateOrderCategoryModel _$UpdateOrderCategoryModelFromJson(Map<String, dynamic> json) => UpdateOrderCategoryModel(
      serviceId: json['serviceId'] as String?,
      category: json['category'] as String?,
    );

Map<String, dynamic> _$UpdateOrderCategoryModelToJson(UpdateOrderCategoryModel instance) => <String, dynamic>{
      'serviceId': instance.serviceId,
      'category': instance.category,
    };
