import 'package:json_annotation/json_annotation.dart';

part 'update_order_sub_category_model.g.dart';

@JsonSerializable()
class UpdateOrderSubCategoryModel {
  String? serviceDetailId;
  String? subcategory;

  UpdateOrderSubCategoryModel({this.serviceDetailId, this.subcategory});

  factory UpdateOrderSubCategoryModel.fromJson(Map<String, dynamic> json) => _$UpdateOrderSubCategoryModelFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateOrderSubCategoryModelToJson(this);
}
