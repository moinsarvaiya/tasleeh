// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_order_sub_category_level_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateOrderSubCategoryLevelModel _$UpdateOrderSubCategoryLevelModelFromJson(Map<String, dynamic> json) => UpdateOrderSubCategoryLevelModel(
      serviceDetailLevelId: json['serviceDetailLevelId'] as String?,
      level: json['level'] as String?,
      actualPrice: json['actualPrice'] as String?,
      discountedPrice: json['discountedPrice'] as String?,
    );

Map<String, dynamic> _$UpdateOrderSubCategoryLevelModelToJson(UpdateOrderSubCategoryLevelModel instance) => <String, dynamic>{
      'serviceDetailLevelId': instance.serviceDetailLevelId,
      'level': instance.level,
      'actualPrice': instance.actualPrice,
      'discountedPrice': instance.discountedPrice,
    };
