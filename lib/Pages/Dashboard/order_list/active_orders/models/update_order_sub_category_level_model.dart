import 'package:json_annotation/json_annotation.dart';

part 'update_order_sub_category_level_model.g.dart';

@JsonSerializable()
class UpdateOrderSubCategoryLevelModel {
  String? serviceDetailLevelId;
  String? level;
  String? actualPrice;
  String? discountedPrice;

  UpdateOrderSubCategoryLevelModel({this.serviceDetailLevelId, this.level, this.actualPrice, this.discountedPrice});

  factory UpdateOrderSubCategoryLevelModel.fromJson(Map<String, dynamic> json) => _$UpdateOrderSubCategoryLevelModelFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateOrderSubCategoryLevelModelToJson(this);
}
