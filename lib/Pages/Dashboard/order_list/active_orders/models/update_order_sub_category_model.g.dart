// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_order_sub_category_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UpdateOrderSubCategoryModel _$UpdateOrderSubCategoryModelFromJson(Map<String, dynamic> json) => UpdateOrderSubCategoryModel(
      serviceDetailId: json['serviceDetailId'] as String?,
      subcategory: json['subcategory'] as String?,
    );

Map<String, dynamic> _$UpdateOrderSubCategoryModelToJson(UpdateOrderSubCategoryModel instance) => <String, dynamic>{
      'serviceDetailId': instance.serviceDetailId,
      'subcategory': instance.subcategory,
    };
