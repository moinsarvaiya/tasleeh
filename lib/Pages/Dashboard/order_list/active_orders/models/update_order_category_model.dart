import 'package:json_annotation/json_annotation.dart';

part 'update_order_category_model.g.dart';

@JsonSerializable()
class UpdateOrderCategoryModel {
  String? serviceId;
  String? category;

  UpdateOrderCategoryModel({this.serviceId, this.category});

  factory UpdateOrderCategoryModel.fromJson(Map<String, dynamic> json) => _$UpdateOrderCategoryModelFromJson(json);

  Map<String, dynamic> toJson() => _$UpdateOrderCategoryModelToJson(this);
}
