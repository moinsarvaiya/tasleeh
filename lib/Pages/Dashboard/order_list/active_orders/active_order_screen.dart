import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../Utils/pagination_helper.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../core/extensions/common_extension.dart';
import '../requested_order/order_controller.dart';
import 'active_order_mobile.dart';
import 'active_order_web.dart';

class ActiveOrderScreen extends StatefulWidget {
  const ActiveOrderScreen({super.key});

  @override
  State<ActiveOrderScreen> createState() => _ActiveOrderScreenState();
}

class _ActiveOrderScreenState extends State<ActiveOrderScreen> {
  final controller = Get.find<OrderController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.getOrderData(OrderStatus.active);
    });

    controller.paginationHelper = PaginationHelper(
      loadData: (isOngoing, page, limit, refresh) {
        controller.paginationHelper.pageNumber++;
        controller.getOrderData(OrderStatus.active);
      },
      limit: pageLimit,
    );
  }

  @override
  Widget build(BuildContext context) {
    return context.isDesktop
        ? ActiveOrderWeb(
            controller: controller,
          )
        : ActiveOrderMobile(
            controller: controller,
          );
  }
}
