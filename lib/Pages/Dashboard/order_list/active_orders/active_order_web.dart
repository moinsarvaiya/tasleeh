import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../../Utils/utilities.dart';
import '../../../../common/common_order_table.dart';
import '../../../../core/constant/app_color.dart';
import '../../../../core/constant/app_images.dart';
import '../../../../core/constant/base_style.dart';
import '../../../../core/constant/ui_constants.dart';
import '../../../../l10n/app_string_key.dart';
import '../../custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import '../../custom_dashboard_drawer/widgets/summary_widget.dart';
import '../requested_order/order_controller.dart';

class ActiveOrderWeb extends StatelessWidget {
  final OrderController controller;

  const ActiveOrderWeb({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SummaryWidget(),
        const SizedBox(
          height: 8,
        ),
        Text(
          AppStringKey.above_data_from_the_past.tr,
          style: BaseStyle.textStyleRobotoMediumBold(
            12,
            AppColors.inputFieldHeading,
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              AppStringKey.active_order_list.tr,
              style: BaseStyle.textStyleRobotoSemiBold(
                24,
                AppColors.colorBlack,
              ),
            ),
            IntrinsicWidth(
              child: Material(
                color: Colors.transparent,
                child: Ink(
                  child: InkWell(
                    onTap: () {
                      orderType = OrderStatus.active;
                      dashboardOrderListDrawerKey.currentState?.openEndDrawer();
                    },
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 15,
                        vertical: 6.5,
                      ),
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.black, width: 1.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            AppImages.filter,
                            width: 24,
                            height: 24,
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Text(
                            AppStringKey.filter.tr,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              16,
                              AppColors.primaryTextColor,
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        const SizedBox(
          height: 24,
        ),
        Obx(
              () => Expanded(
            child: CommonOrderTable(
              scrollController: controller.paginationHelper.scrollController,
              columnSpacing: 10,
              padding: const EdgeInsets.all(0),
              columns: [
                DataColumn2(
                  label: Obx(
                        () => controller.isSearching.value
                        ? TextFormField(
                      controller: controller.searchOrderId,
                      style: BaseStyle.textStyleRobotoRegular(
                        10,
                        AppColors.primaryTextColor,
                      ),
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(
                          vertical: 4,
                          horizontal: 8,
                        ),
                        constraints: const BoxConstraints(maxHeight: 40),
                        suffixIcon: MouseRegion(
                          cursor: SystemMouseCursors.click,
                          child: GestureDetector(
                            onTap: () {
                              controller.isSearching.value = !controller.isSearching.value;
                              controller.searchOrderId.clear();
                            },
                            child: const Icon(
                              Icons.close,
                              size: 12,
                            ),
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(2),
                          borderSide: const BorderSide(
                            color: AppColors.borderColor,
                            width: 0,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(2),
                          borderSide: const BorderSide(
                            color: AppColors.borderColor,
                            width: 0,
                          ),
                        ),
                      ),
                      autofocus: true,
                      onChanged: (value) {
                        Utilities.customDebounce(customDebounceTime, () {
                          controller.searchOrder(OrderStatus.active);
                        });
                      },
                    )
                        : Row(
                      children: [
                        Text(
                          AppStringKey.order_details.tr,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                        const SizedBox(
                          width: 4,
                        ),
                        Material(
                          child: Ink(
                            child: InkWell(
                              onTap: () {
                                controller.isSearching.value = !controller.isSearching.value;
                              },
                              child: const Icon(
                                Icons.search,
                                size: 20,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.customer_info.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.vehicle_model.tr.capitalizeFirst!,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.service_selected.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[1],
                ),
                DataColumn2(
                  label: Container(
                    margin: const EdgeInsets.only(left: 15),
                    child: Text(
                      AppStringKey.pickup_drop_details.tr,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                  size: ColumnSize.values[2],
                ),
                DataColumn2(
                  label: Text(
                    AppStringKey.status.tr,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  size: ColumnSize.values[0],
                ),
              ],
              data: List<DataRow2>.generate(
                controller.activeOrderData.length,
                    (index) => DataRow2(
                  cells: [
                    DataCell(
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          MouseRegion(
                            cursor: SystemMouseCursors.click,
                            child: GestureDetector(
                              onTap: () {
                                var controller = Get.find<CustomDashboardStepperController>();

                                controller.isFromActiveOrder = true;

                                controller.isReturnOrder = this.controller.activeOrderData[index].isReturnedOrder!;

                                controller.orderId = this.controller.activeOrderData[index].bookingId!;

                                if (controller.customStepperList[controller.stepperSelectedIndex.value]
                                    .subList[controller.orderListItemSelectedIndex.value].childWidget.length >
                                    1) {
                                  controller.customStepperList[controller.stepperSelectedIndex.value]
                                      .subList[controller.orderListItemSelectedIndex.value].childActiveIndex++;
                                  controller.customStepperList.refresh();
                                }
                              },
                              child: Text(
                                '#${controller.activeOrderData[index].bookingId!}',
                                style: BaseStyle.textStyleRobotoSemiBold(
                                  14,
                                  AppColors.primaryTextColor,
                                ).copyWith(
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                          ),
                          Text(
                            controller.activeOrderData[index].bookingDate!,
                            style: BaseStyle.textStyleRobotoMediumBold(
                              12,
                              AppColors.secondaryTextColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller.activeOrderData[index].name!,
                            style: BaseStyle.textStyleRobotoRegular(
                              14,
                              AppColors.primaryTextColor,
                            ),
                          ),
                          Text(
                            controller.activeOrderData[index].number!,
                            style: BaseStyle.textStyleRobotoRegular(
                              14,
                              AppColors.primaryTextColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                    DataCell(
                      Text(
                        "${controller.activeOrderData[index].make} ${controller.activeOrderData[index].model}",
                        style: BaseStyle.textStyleRobotoRegular(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      ),
                    ),
                    DataCell(
                      Text(
                        controller.activeOrderData[index].services!.length > 1
                            ? "${controller.activeOrderData[index].services![0]['serviceDetail']} +${controller.activeOrderData[index].services!.length - 1}"
                            : "${controller.activeOrderData[index].services![0]['serviceDetail']}",
                        style: BaseStyle.textStyleRobotoMediumBold(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      ),
                    ),
                    DataCell(
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            controller.activeOrderData[index].pickupAddress!.isEmpty
                                ? AppStringKey.self_drop.tr
                                : "${controller.activeOrderData[index].distance}m | ${controller.activeOrderData[index].pickupAddress!}",
                            style: BaseStyle.textStyleRobotoRegular(
                              14,
                              AppColors.primaryTextColor,
                            ),
                          ),
                          Flexible(
                            child: Text(
                              "${controller.activeOrderData[index].timeSlot}, ${controller.activeOrderData[index].pickupDate}",
                              style: BaseStyle.textStyleRobotoRegular(
                                12,
                                AppColors.secondaryTextColor,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    DataCell(
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.circle,
                            size: 10,
                            color: controller.activeOrderData[index].isReturnedOrder!
                                ? workStatusColor[controller.getWorkStatusColor(controller.activeOrderData[index].warrantyStatus!)]
                                : workStatusColor[controller.getWorkStatusColor(controller.activeOrderData[index].bookingStatus!)],
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                controller.activeOrderData[index].isReturnedOrder!
                                    ? controller.activeOrderData[index].warrantyStatus!
                                    : controller.activeOrderData[index].bookingStatus!,
                                style: BaseStyle.textStyleRobotoMediumBold(
                                  14,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                              controller.activeOrderData[index].isReturnedOrder!
                                  ? Text(
                                AppStringKey.return_order.tr,
                                style: BaseStyle.textStyleRobotoRegular(
                                  12,
                                  AppColors.errorTextColor,
                                ),
                              )
                                  : const SizedBox.shrink()
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}