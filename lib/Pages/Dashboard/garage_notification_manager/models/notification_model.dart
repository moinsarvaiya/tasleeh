class NotificationModel {
  String title;
  String text;

  NotificationModel({
    required this.title,
    required this.text,
  });
}
