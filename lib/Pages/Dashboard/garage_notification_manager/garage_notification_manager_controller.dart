import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../../Utils/utilities.dart';
import '../../../core/constant/app_color.dart';
import '../../../core/constant/ui_constants.dart';
import '../custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import 'models/notification_model.dart';

class GarageNotificationsManagerController extends GetxController {
  RxList<NotificationModel> notifications = RxList();
  RxInt newNotifications = 0.obs;

  void newNotification(RemoteMessage message) {
    notifications.add(
      NotificationModel(
        title: message.notification != null
            ? message.notification!.title
            : message.data['title'],
        text: message.notification != null
            ? message.notification!.body
            : message.data['body'],
      ),
    );
    newNotifications.value++;
    notifications.refresh();
    Get.find<CustomDashboardStepperController>().updateGarageProfile();
    Utilities.showToast(
      "${message.notification!.title}\n${message.notification!.body}",
      fontSize: 14,
      gravity: ToastGravity.TOP,
      webPosition: 'right',
      webBgColor: '#52C41A',
      webShowClose: false,
      timeInSecForIosWeb: toastVisibleTime,
      textColor: AppColors.colorWhite,
      toastLength: Toast.LENGTH_LONG,
    );
  }
}
