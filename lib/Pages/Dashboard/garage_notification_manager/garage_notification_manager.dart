import 'package:al_baida_garage_fe/l10n/app_string_key.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../core/constant/app_color.dart';
import '../../../core/constant/app_images.dart';
import '../../../core/constant/base_style.dart';
import 'garage_notification_manager_controller.dart';

class GarageNotificationsManager extends StatefulWidget {
  const GarageNotificationsManager({super.key});

  @override
  State<GarageNotificationsManager> createState() =>
      _GarageNotificationsManagerState();
}

class _GarageNotificationsManagerState
    extends State<GarageNotificationsManager> {
  final controller = Get.find<GarageNotificationsManagerController>();
  final OverlayPortalController _overlayPortalController =
      OverlayPortalController();
  LayerLink layerLink = LayerLink();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CompositedTransformTarget(
            link: layerLink,
            child: OverlayPortal(
              controller: _overlayPortalController,
              overlayChildBuilder: (BuildContext context) {
                return CompositedTransformFollower(
                  link: layerLink,
                  targetAnchor: Alignment.topLeft,
                  child: Align(
                    alignment: Directionality.of(context) == TextDirection.rtl
                        ? AlignmentDirectional.topEnd
                        : AlignmentDirectional.topStart,
                    child: Container(
                      width: 300,
                      constraints: const BoxConstraints(maxHeight: 250),
                      margin: const EdgeInsets.only(
                        top: 30,
                        right: 30,
                      ),
                      decoration: BoxDecoration(
                        color: AppColors.colorWhite,
                        borderRadius: const BorderRadius.all(
                          Radius.circular(4),
                        ),
                        border: Border.all(
                          color: AppColors.borderColor,
                          width: 1,
                        ),
                      ),
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: controller.notifications.length > 0
                              ? controller.notifications.reversed
                                  .map(
                                    (element) => Container(
                                      padding: const EdgeInsets.all(16),
                                      width: double.maxFinite,
                                      decoration: const BoxDecoration(
                                        border: Border(
                                          bottom: BorderSide(
                                            color: AppColors.borderColor,
                                          ),
                                        ),
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            element.title,
                                            style: BaseStyle
                                                .textStyleRobotoSemiBold(
                                              14,
                                              AppColors.primaryTextColor,
                                            ),
                                          ),
                                          Text(
                                            element.text,
                                            style: BaseStyle
                                                .textStyleRobotoRegular(
                                              12,
                                              AppColors.primaryTextColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                  .toList()
                              : [
                                  Container(
                                    padding: const EdgeInsets.all(16),
                                    child: Text(
                                      AppStringKey.no_new_notification.tr,
                                      style: BaseStyle.textStyleRobotoRegular(
                                        14,
                                        AppColors.primaryTextColor,
                                      ),
                                    ),
                                  ),
                                ],
                        ),
                      ),
                    ),
                  ),
                );
              },
              child: MouseRegion(
                cursor: SystemMouseCursors.click,
                child: GestureDetector(
                  onTap: () {
                    _overlayPortalController.toggle();
                    controller.newNotifications.value = 0;
                  },
                  child: SvgPicture.asset(
                    AppImages.dashboardStepperNotification,
                    width: 25,
                    height: 25,
                    color: AppColors.primaryTextColor,
                  ),
                ),
              ),
            ),
          ),
          controller.newNotifications > 0
              ? Text(
                  controller.newNotifications.toString(),
                  style: BaseStyle.textStyleRobotoBold(
                    12,
                    AppColors.primaryTextColor,
                  ),
                )
              : const SizedBox.shrink(),
        ],
      ),
    );
  }
}
