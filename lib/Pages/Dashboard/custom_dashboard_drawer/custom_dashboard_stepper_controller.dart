import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../api/api_end_point.dart';
import '../../../api/api_interface.dart';
import '../../../api/api_presenter.dart';
import '../../../core/app_preference/app_preferences.dart';
import '../../../core/app_preference/storage_keys.dart';
import '../../../core/constant/app_images.dart';
import '../../../core/constant/ui_constants.dart';
import '../../../core/permissions/employee_permission.dart';
import '../../../core/permissions/manager_permission.dart';
import '../../../l10n/app_string_key.dart';
import '../../services/models/response_level_list_model.dart';
import '../MyAccount/my_account_screen.dart';
import '../Payment/payment_screen.dart';
import '../Support/support.dart';
import '../multi_branch_handler/multi_branch_handler_screen.dart';
import '../order_list/active_orders/active_order_screen.dart';
import '../order_list/completed_order/completed_order_screen.dart';
import '../order_list/requested_order/models/work_status_model.dart';
import '../order_list/requested_order/requested_order_screen.dart';
import '../order_list/return_order_detail/return_order_detail_screen.dart';
import '../order_list/return_orders/return_order_screen.dart';
import 'models/custom_stepper_model.dart';

class CustomDashboardStepperController extends GetxController implements ApiCallBacks {
  RxInt totalRevenue = RxInt(0);
  RxInt ordersReceived = RxInt(0);
  RxInt ordersPending = RxInt(0);
  RxInt ordersCompleted = RxInt(0);
  bool isFromActiveOrder = true;
  bool isReturnOrder = false;

  RxList<CustomStepperModel> customStepperList = RxList();
  RxInt stepperSelectedIndex = 0.obs;
  RxInt orderListItemSelectedIndex = 0.obs;
  RxBool isFromDashboardMenu = true.obs;

  RxInt requestedOrderCount = 0.obs;
  RxInt activeOrderCount = 0.obs;
  RxInt completedOrderCount = 0.obs;
  RxInt returnOrderCount = 0.obs;

  RxString garageProfileImage = RxString('');
  RxString garageName = RxString('');

  RxInt expandableViewIndex = 1.obs;
  bool isOrderListSelected = true;
  bool isBranchManagementVisible = true;
  bool isOrderListVisible = true;
  bool isPaymentVisible = true;
  bool isHelpVisible = true;
  bool isMyAccountVisible = true;

  List<WorkStatusModel> workStatusList = [
    WorkStatusModel(statusKey: workStatusType[WorkStatusType.newWork]!, statusTitle: AppStringKey.newPhrase.tr),
    WorkStatusModel(statusKey: workStatusType[WorkStatusType.accepted]!, statusTitle: AppStringKey.accepted.tr),
    WorkStatusModel(statusKey: workStatusType[WorkStatusType.rejected]!, statusTitle: AppStringKey.rejected.tr),
    WorkStatusModel(statusKey: workStatusType[WorkStatusType.cancelled]!, statusTitle: AppStringKey.cancelled.tr),
    WorkStatusModel(statusKey: workStatusType[WorkStatusType.pickupScheduled]!, statusTitle: AppStringKey.pickup_scheduled.tr),
    WorkStatusModel(statusKey: workStatusType[WorkStatusType.vehicleInTransit]!, statusTitle: AppStringKey.vehicle_in_transit.tr),
    WorkStatusModel(statusKey: workStatusType[WorkStatusType.vehicleReached]!, statusTitle: AppStringKey.vehicle_reached.tr),
    WorkStatusModel(statusKey: workStatusType[WorkStatusType.serviceStarted]!, statusTitle: AppStringKey.service_started.tr),
    WorkStatusModel(statusKey: workStatusType[WorkStatusType.paymentPending]!, statusTitle: AppStringKey.payment_pending.tr),
    WorkStatusModel(statusKey: workStatusType[WorkStatusType.outForDelivery]!, statusTitle: AppStringKey.out_for_delivery.tr),
    WorkStatusModel(statusKey: workStatusType[WorkStatusType.delivered]!, statusTitle: AppStringKey.delivered.tr),
    WorkStatusModel(statusKey: workStatusType[WorkStatusType.warrantyClaimed]!, statusTitle: AppStringKey.warranty_claimed.tr),
  ];

  Map<OrderStatus, String> orderListType = {
    OrderStatus.requested: 'requested',
    OrderStatus.active: 'active',
    OrderStatus.completed: 'delivered',
    OrderStatus.returned: 'return',
  };

  RxString orderDate = ''.obs;
  RxString pickUpDate = ''.obs;
  RxString serviceLevel = ''.obs;
  RxString workStatus = ''.obs;
  RxList<String> selectedServiceLevel = RxList();
  TextEditingController orderDateFieldController = TextEditingController();
  TextEditingController pickupDateFieldController = TextEditingController();
  TextEditingController minPriceController = TextEditingController();
  TextEditingController manPriceController = TextEditingController();
  FocusNode minPriceFocusNode = FocusNode();
  FocusNode manPriceFocusNode = FocusNode();

  RxList<ResponseLevelListModel> serviceLevelList = RxList();
  var orderId = '';

  void getServiceLevelData() {
    ApiPresenter(this).getServiceLevelData(AppPreferences.sharedPrefRead(StorageKeys.garageId));
  }

  void getOrdersSummary() {
    ApiPresenter(this).getOrdersSummary(AppPreferences.sharedPrefRead(StorageKeys.garageId));
  }

  void getGarageProfile() {
    garageProfileAction = GarageProfileAction.get;
    ApiPresenter(this).getGarageProfile(AppPreferences.sharedPrefRead(StorageKeys.garageId));
  }

  void updateGarageProfile() {
    garageProfileAction = GarageProfileAction.update;
    ApiPresenter(this).getGarageProfile(AppPreferences.sharedPrefRead(StorageKeys.garageId));
  }

  void setStepperData() {
    final isManager = AppPreferences.sharedPrefRead(StorageKeys.userType) == userTypePlatform[UserType.manager];
    expandableViewIndex.value = isManager ? 1 : 0;
    isOrderListSelected = !isManager;

    final permissions = getUserPermissions(isManager);

    isBranchManagementVisible = permissions.isBranchManagementVisible;
    isOrderListVisible = permissions.isOrderListVisible;
    isPaymentVisible = permissions.isPaymentVisible;
    isHelpVisible = permissions.isHelpVisible;
    isMyAccountVisible = permissions.isMyAccountVisible;

    if (isBranchManagementVisible) {
      customStepperList.add(
        CustomStepperModel(
          title: AppStringKey.branch_management,
          selectedItemIcon: AppImages.dashboardStepperSelectedMyAccount,
          itemIcon: AppImages.dashboardStepperMyAccount,
          isItemSelected: true,
          isSubListExpanded: false,
          childWidget: const MultiBranchHandlerScreen(),
          subList: [],
        ),
      );
    }
    if (isOrderListVisible) {
      customStepperList.add(
        CustomStepperModel(
          title: AppStringKey.dashboard_order_list,
          selectedItemIcon: AppImages.dashboardStepperSelectedOrderList,
          itemIcon: AppImages.dashboardStepperOrderList,
          isItemSelected: isOrderListSelected,
          childWidget: const RequestedOrderScreen(),
          isSubListExpanded: false,
          subList: [
            SubStepperList(
                subStepperTitle: AppStringKey.dashboard_request_order,
                childWidget: [
                  const RequestedOrderScreen(),
                ],
                isSubStepperItemSelected: true,
                childActiveIndex: 0,
                count: requestedOrderCount.value),
            SubStepperList(
                subStepperTitle: AppStringKey.dashboard_active_order,
                childWidget: [
                  const ActiveOrderScreen(),
                  const ReturnOrderDetailScreen(),
                ],
                isSubStepperItemSelected: false,
                childActiveIndex: 0,
                count: activeOrderCount.value),
            SubStepperList(
                subStepperTitle: AppStringKey.dashboard_completed_order,
                childWidget: [
                  const CompletedOrderScreen(),
                ],
                isSubStepperItemSelected: false,
                childActiveIndex: 0,
                count: completedOrderCount.value),
            SubStepperList(
                subStepperTitle: AppStringKey.dashboard_return_order,
                childWidget: [
                  const ReturnOrderScreen(),
                  const ReturnOrderDetailScreen(),
                ],
                isSubStepperItemSelected: false,
                childActiveIndex: 0,
                count: returnOrderCount.value),
          ],
        ),
      );
    }
    if (isPaymentVisible) {
      customStepperList.add(
        CustomStepperModel(
          title: AppStringKey.dashboard_payments,
          selectedItemIcon: AppImages.dashboardStepperSelectedPayment,
          itemIcon: AppImages.dashboardStepperPayment,
          isItemSelected: false,
          isSubListExpanded: false,
          childWidget: const PaymentScreen(),
          subList: [],
        ),
      );
    }
    if (isHelpVisible) {
      customStepperList.add(
        CustomStepperModel(
          title: AppStringKey.dashboard_help,
          selectedItemIcon: AppImages.dashboardStepperSelectedHelp,
          itemIcon: AppImages.dashboardStepperHelp,
          isItemSelected: false,
          isSubListExpanded: false,
          childWidget: const SupportScreen(),
          subList: [],
        ),
      );
    }
    if (isMyAccountVisible) {
      customStepperList.add(
        CustomStepperModel(
          title: AppStringKey.dashboard_my_account,
          selectedItemIcon: AppImages.dashboardStepperSelectedMyAccount,
          itemIcon: AppImages.dashboardStepperMyAccount,
          isItemSelected: false,
          isSubListExpanded: false,
          childWidget: const MyAccountDashBoardScreen(),
          subList: [],
        ),
      );
    }
  }

  dynamic getUserPermissions(bool isManager) {
    return isManager ? ManagerPermission.instance : EmployeePermission.instance;
  }

  void updateLanguage() {
    ApiPresenter(this).updateLanguage(
      AppPreferences.sharedPrefRead(StorageKeys.lang),
    );
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {}

  @override
  void onError(String errorMsg, responseCode, String apiEndPoint) {}

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.getLevelData:
        serviceLevelList.clear();
        serviceLevelList.addAll((object['data'] as List).map((e) => ResponseLevelListModel.fromJson(e)));
        break;
      case ApiEndPoints.getOrdersSummary:
        totalRevenue.value = object['data']['totalRevenue'];
        ordersReceived.value = object['data']['totalOrders'];
        ordersPending.value = object['data']['totalPendingOrders'];
        ordersCompleted.value = object['data']['totalCompletedOrders'];
        break;
      case ApiEndPoints.getGarageProfile:
        requestedOrderCount.value = object['data']['totalRequestedOrders'];
        activeOrderCount.value = object['data']['totalActiveOrders'];
        completedOrderCount.value = object['data']['totalCompletedOrders'];
        returnOrderCount.value = object['data']['totalReturnedOrders'];
        garageName.value = object['data']['garageDetails']['garageName'];
        garageProfileImage.value = object['data']['garageDetails']['garageImage'];
        if (garageProfileAction == GarageProfileAction.get) {
          customStepperList.clear();
          setStepperData();
        }
        break;
    }
  }

  void clearData() {
    orderDate.value = '';
    pickUpDate.value = '';
    serviceLevel.value = '';
    workStatus.value = '';
    orderDateFieldController.clear();
    pickupDateFieldController.clear();
    minPriceController.clear();
    manPriceController.clear();
    selectedServiceLevel.clear();
    selectedServiceLevel.refresh();
  }
}
