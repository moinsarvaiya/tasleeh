import 'package:flutter/material.dart';

class CustomStepperModel {
  String title;
  String itemIcon;
  String selectedItemIcon;
  bool isItemSelected;
  bool? isSubListExpanded;
  Widget childWidget;
  List<SubStepperList> subList;

  CustomStepperModel({
    required this.title,
    required this.itemIcon,
    required this.selectedItemIcon,
    required this.childWidget,
    required this.isItemSelected,
    this.isSubListExpanded,
    required this.subList,
  });
}

class SubStepperList {
  String subStepperTitle;
  List<Widget> childWidget;
  int childActiveIndex;
  bool isSubStepperItemSelected;
  int count;

  SubStepperList({
    required this.subStepperTitle,
    required this.childWidget,
    required this.childActiveIndex,
    required this.isSubStepperItemSelected,
    required this.count,
  });
}
