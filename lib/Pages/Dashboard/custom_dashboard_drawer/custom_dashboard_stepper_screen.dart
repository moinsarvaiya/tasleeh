import 'package:al_baida_garage_fe/Pages/services/add_level_drawer_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:image_network/image_network.dart';

import '../../../common/common_modal_widget.dart';
import '../../../common/language_selector.dart';
import '../../../common/space_horizontal.dart';
import '../../../common/space_vertical.dart';
import '../../../core/app_preference/app_preferences.dart';
import '../../../core/app_preference/storage_keys.dart';
import '../../../core/auth/signout.dart';
import '../../../core/constant/app_color.dart';
import '../../../core/constant/app_images.dart';
import '../../../core/constant/base_style.dart';
import '../../../core/constant/ui_constants.dart';
import '../../../core/constant/widget_functions.dart';
import '../../../core/extensions/common_extension.dart';
import '../../../l10n/app_string_key.dart';
import '../Payment/add_filter_drawer.dart';
import '../drawers/active_order_detail_drawer.dart';
import '../drawers/dashboard_completed_orderlist_filter_drawer.dart';
import '../drawers/dashboard_menu_drawer.dart';
import '../drawers/dashboard_orderlist_filter_drawer.dart';
import '../garage_notification_manager/garage_notification_manager.dart';
import '../multi_branch_handler/multi_branch_handler_controller.dart';
import '../order_list/requested_order/order_controller.dart';
import 'custom_dashboard_stepper_controller.dart';

class CustomDashboardStepperScreen extends StatefulWidget {
  const CustomDashboardStepperScreen({super.key});

  @override
  State<CustomDashboardStepperScreen> createState() => _CustomDashboardStepperScreenState();
}

class _CustomDashboardStepperScreenState extends State<CustomDashboardStepperScreen> {
  final controller = Get.find<CustomDashboardStepperController>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.updateLanguage();
      if (AppPreferences.sharedPrefRead(StorageKeys.userType) == userTypePlatform[UserType.manager]) {
        Get.find<MultiBranchHandlerController>().getBranches();
      } else {
        var customDashboardStepperController = Get.find<CustomDashboardStepperController>();
        customDashboardStepperController.getGarageProfile();
        customDashboardStepperController.getOrdersSummary();
        customDashboardStepperController.getServiceLevelData();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: mobileViewDrawerKey,
      endDrawer: const DashboardMenuDrawer(),
      backgroundColor: AppColors.primaryBackgroundColor,
      body: Scaffold(
        backgroundColor: Colors.transparent,
        key: dashboardOrderListDrawerKey,
        endDrawer: const DashboardOrderListFilterDrawer(),
        body: Scaffold(
          backgroundColor: Colors.transparent,
          key: dashboardPaymentScreenDrawerKey,
          endDrawer: const AddFilterWidget(),
          body: Scaffold(
            backgroundColor: Colors.transparent,
            key: updateOrderDrawerKey,
            endDrawer: const ActiveOrderDetailDrawer(),
            body: Scaffold(
              backgroundColor: Colors.transparent,
              key: dashboardCompletedOrderListDrawerKey,
              endDrawer: const DashboardCompletedOrderListFilterDrawer(),
              body: context.isDesktop ? _WebAppView(controller: controller) : _MobileView(controller: controller),
            ),
          ),
        ),
      ),
    );
  }
}

class _MobileView extends StatelessWidget {
  final CustomDashboardStepperController controller;

  const _MobileView({required this.controller});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          height: context.screenHeight * 0.08,
          color: AppColors.colorWhite,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SvgPicture.asset(
                AppImages.appTopIcon,
                width: 50,
                color: AppColors.primaryTextColor,
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const GlobalLanguageSelector(),
                  const SizedBox(
                    width: 16,
                  ),
                  Material(
                    child: Ink(
                      child: InkWell(
                        onTap: () {
                          mobileViewDrawerKey.currentState!.openEndDrawer();
                        },
                        child: SvgPicture.asset(
                          AppImages.menu,
                          width: 25,
                          height: 25,
                          color: AppColors.colorBlack,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          height: context.screenHeight * 0.92,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 30, 20, 25),
            child: Obx(
              () => controller.customStepperList.isNotEmpty
                  ? controller.customStepperList[controller.stepperSelectedIndex.value].isSubListExpanded!
                      ? controller.customStepperList[controller.stepperSelectedIndex.value].subList[controller.orderListItemSelectedIndex.value]
                              .childWidget[
                          controller.customStepperList[controller.stepperSelectedIndex.value].subList[controller.orderListItemSelectedIndex.value]
                              .childActiveIndex]
                      : controller.stepperSelectedIndex.value != controller.expandableViewIndex.value
                          ? controller.customStepperList[controller.stepperSelectedIndex.value].childWidget
                          : controller.customStepperList[controller.stepperSelectedIndex.value].subList[controller.orderListItemSelectedIndex.value]
                                  .childWidget[
                              controller.customStepperList[controller.stepperSelectedIndex.value].subList[controller.orderListItemSelectedIndex.value]
                                  .childActiveIndex]
                  : const SizedBox.shrink(),
            ),
          ),
        ),
      ],
    );
  }
}

class _WebAppView extends StatelessWidget {
  final CustomDashboardStepperController controller;

  const _WebAppView({
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    Scaffold.of(context).closeEndDrawer();
    return Row(
      children: [
        Container(
          width: 250,
          padding: EdgeInsets.symmetric(
            horizontal: context.screenWidth * 0.015,
            vertical: context.screenHeight * 0.02,
          ),
          decoration: BoxDecoration(
            border: LRTB_Border(0, 0.5, 0, 0, AppColors.borderColor),
            color: AppColors.colorWhite,
          ),
          height: context.screenHeight,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(
                AppImages.appTopIcon,
                width: 80,
                color: AppColors.primaryTextColor,
              ),
              const SpaceVertical(0.08),
              Expanded(
                child: Column(
                  children: [
                    Obx(
                      () => ListView.builder(
                        itemCount: controller.customStepperList.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          return Column(
                            children: [
                              Container(
                                margin: const EdgeInsets.symmetric(vertical: 5),
                                color: controller.customStepperList[index].isItemSelected ? AppColors.colorBlack : AppColors.colorWhite,
                                child: Material(
                                  color: controller.customStepperList[index].isItemSelected ? AppColors.colorBlack : AppColors.colorWhite,
                                  child: Ink(
                                    child: InkWell(
                                      onTap: () {
                                        controller.stepperSelectedIndex.value = index;
                                        if (index != controller.expandableViewIndex.value) {
                                          controller.customStepperList[controller.expandableViewIndex.value].isSubListExpanded = false;
                                          for (var element in controller.customStepperList[controller.expandableViewIndex.value].subList) {
                                            element.isSubStepperItemSelected = false;
                                          }
                                          controller.customStepperList[controller.expandableViewIndex.value].subList[0].isSubStepperItemSelected =
                                              true;
                                          controller.orderListItemSelectedIndex.value = 0;
                                        } else {
                                          controller.customStepperList[index].isSubListExpanded =
                                              !controller.customStepperList[index].isSubListExpanded!;
                                        }

                                        for (var element in controller.customStepperList) {
                                          element.isItemSelected = false;
                                        }
                                        controller.customStepperList[index].isItemSelected = true;
                                        controller.customStepperList.refresh();
                                        Get.find<OrderController>().resetPagination();
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                                        child: Column(
                                          children: [
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                Row(
                                                  children: [
                                                    SvgPicture.asset(
                                                      controller.customStepperList[index].isItemSelected
                                                          ? controller.customStepperList[index].selectedItemIcon
                                                          : controller.customStepperList[index].itemIcon,
                                                      width: 22,
                                                      height: 22,
                                                    ),
                                                    const SpaceHorizontal(0.008),
                                                    Text(
                                                      controller.customStepperList[index].title.tr,
                                                      style: BaseStyle.textStyleRobotoMedium(
                                                        14,
                                                        controller.customStepperList[index].isItemSelected
                                                            ? AppColors.colorWhite
                                                            : AppColors.colorBlack,
                                                      ),
                                                    )
                                                  ],
                                                ),
                                                controller.customStepperList[index].subList.isNotEmpty
                                                    ? Icon(
                                                        controller.customStepperList[index].isSubListExpanded!
                                                            ? Icons.arrow_drop_up
                                                            : Icons.arrow_drop_down,
                                                        size: 20,
                                                        color: controller.customStepperList[index].isItemSelected
                                                            ? AppColors.colorWhite
                                                            : AppColors.colorBlack,
                                                      )
                                                    : const SizedBox.shrink(),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              AnimatedSize(
                                duration: const Duration(milliseconds: 300), // Set the duration of the animation
                                curve: Curves.easeInOut, // Choose an animation curve
                                child: controller.customStepperList[index].subList.isNotEmpty &&
                                        controller.customStepperList[index].isSubListExpanded!
                                    ? ListView.builder(
                                        itemCount: controller.customStepperList[index].subList.length,
                                        shrinkWrap: true,
                                        itemBuilder: (context, subListIndex) {
                                          print('value after chnage ${controller.customStepperList[index].subList[subListIndex].subStepperTitle.tr}');
                                          return Material(
                                            color: Colors.transparent,
                                            child: Ink(
                                              child: InkWell(
                                                onTap: () {
                                                  Get.find<OrderController>().resetPagination();
                                                  controller.clearData();
                                                  controller.orderListItemSelectedIndex.value = subListIndex;
                                                  for (var element in controller.customStepperList[index].subList) {
                                                    element.isSubStepperItemSelected = false;
                                                  }
                                                  controller.customStepperList[index].subList[subListIndex].isSubStepperItemSelected = true;
                                                  controller.customStepperList.refresh();
                                                },
                                                child: Padding(
                                                  padding: const EdgeInsets.only(left: 20),
                                                  child: Row(
                                                    children: [
                                                      Container(
                                                        width: 1,
                                                        height: 30,
                                                        color: controller.customStepperList[index].subList[subListIndex].isSubStepperItemSelected
                                                            ? AppColors.colorBlack
                                                            : AppColors.colorGrey,
                                                      ),
                                                      const SpaceHorizontal(0.01),
                                                      Padding(
                                                        padding: const EdgeInsets.all(6),
                                                        child: Text(
                                                          '${controller.customStepperList[index].subList[subListIndex].subStepperTitle.tr} (${controller.customStepperList[index].subList[subListIndex].count})',
                                                          style: controller.customStepperList[index].subList[subListIndex].isSubStepperItemSelected
                                                              ? BaseStyle.textStyleRobotoSemiBold(
                                                                  13,
                                                                  AppColors.colorBlack,
                                                                )
                                                              : BaseStyle.textStyleRobotoMedium(
                                                                  13,
                                                                  AppColors.colorGrey,
                                                                ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      )
                                    : const SizedBox.shrink(),
                              ),
                            ],
                          );
                        },
                      ),
                    ),
                    const SpaceVertical(0.1),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      height: 50,
                      color: AppColors.colorWhite,
                      child: Material(
                        color: AppColors.colorWhite,
                        child: Ink(
                          child: InkWell(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return CommonModalWidget(
                                      modalIcon: AppImages.warning,
                                      title: AppStringKey.logout_warning.tr,
                                      description: AppStringKey.logout_warning_description.tr,
                                      firstButtonContent: AppStringKey.cancel.tr,
                                      firstButtonOnTap: () {
                                        Navigator.pop(context);
                                      },
                                      firstButtonBackgroundColor: AppColors.secondaryCtaColor,
                                      firstButtonTextColor: AppColors.inputFieldHeading,
                                      secondButtonContent: AppStringKey.logoutButton.tr,
                                      secondButtonOnTap: () {
                                        CommonSignOut().signOut();
                                      },
                                    );
                                  });
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                              child: Row(
                                children: [
                                  SvgPicture.asset(
                                    AppImages.signOut,
                                    width: 25,
                                  ),
                                  const SpaceHorizontal(0.008),
                                  Text(
                                    AppStringKey.logout.tr,
                                    style: BaseStyle.textStyleRobotoMedium(
                                      14,
                                      AppColors.colorBlack,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Expanded(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: context.screenWidth * 0.02, vertical: context.screenHeight * 0.01),
                decoration: BoxDecoration(
                  border: LRTB_Border(0.5, 0.5, 0.5, 0.5, AppColors.borderColor),
                  color: AppColors.colorWhite,
                ),
                child: Obx(
                  () => Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      const GlobalLanguageSelector(),
                      const SizedBox(
                        width: 42,
                      ),
                      const GarageNotificationsManager(),
                      const SizedBox(
                        width: 32,
                      ),
                      controller.garageProfileImage.value != ''
                          ? ImageNetwork(
                              image: controller.garageProfileImage.value,
                              height: 44,
                              width: 44,
                              borderRadius: BorderRadius.circular(44),
                              fitWeb: BoxFitWeb.fill,
                              key: ValueKey(controller.garageProfileImage.value),
                            )
                          : const CircleAvatar(
                              radius: 25, // Image radius
                              backgroundImage: AssetImage(
                                AppImages.profileImage,
                              ),
                            ),
                      const SizedBox(
                        width: 32,
                      ),
                      Text(
                        controller.garageName.value,
                        style: BaseStyle.textStyleRobotoSemiBold(
                          16,
                          AppColors.colorBlack,
                        ),
                      ),
                      const SizedBox(
                        width: 32,
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: SizedBox(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 30,
                      horizontal: 40,
                    ),
                    child: Obx(
                      () => controller.customStepperList.isNotEmpty
                          ? controller.customStepperList[controller.stepperSelectedIndex.value].isSubListExpanded!
                              ? controller.customStepperList[controller.stepperSelectedIndex.value]
                                      .subList[controller.orderListItemSelectedIndex.value].childWidget[
                                  controller.customStepperList[controller.stepperSelectedIndex.value]
                                      .subList[controller.orderListItemSelectedIndex.value].childActiveIndex]
                              : controller.stepperSelectedIndex.value != controller.expandableViewIndex.value
                                  ? controller.customStepperList[controller.stepperSelectedIndex.value].childWidget
                                  : controller.customStepperList[controller.stepperSelectedIndex.value]
                                          .subList[controller.orderListItemSelectedIndex.value].childWidget[
                                      controller.customStepperList[controller.stepperSelectedIndex.value]
                                          .subList[controller.orderListItemSelectedIndex.value].childActiveIndex]
                          : const SizedBox.shrink(),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
