import 'package:al_baida_garage_fe/Pages/Dashboard/custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../../Utils/app_constants.dart';
import '../../../../common/status_card.dart';
import '../../../../core/constant/app_images.dart';
import '../../../../l10n/app_string_key.dart';

class SummaryWidget extends StatelessWidget {
  const SummaryWidget({super.key});

  @override
  Widget build(BuildContext context) {
    var controller = Get.find<CustomDashboardStepperController>();
    return Obx(
      () => Wrap(
        spacing: 20,
        runSpacing: 20,
        children: [
          StatusCard(
            title: "${AppStringKey.total_revenue.tr} ${AppConstants.currencyString}",
            description: controller.totalRevenue.value,
            cardIcon: SvgPicture.asset(
              AppImages.salesIcon,
              width: 32,
              height: 32,
            ),
            width: 262,
            height: 104,
          ),
          StatusCard(
            title: AppStringKey.orders_received.tr,
            description: controller.ordersReceived.value,
            cardIcon: SvgPicture.asset(
              AppImages.ordersReceived,
              width: 32,
              height: 32,
            ),
            width: 262,
            height: 104,
          ),
          StatusCard(
            title: AppStringKey.orders_completed.tr,
            description: controller.ordersCompleted.value,
            cardIcon: SvgPicture.asset(
              AppImages.ordersCompleted,
              width: 32,
              height: 32,
            ),
            width: 262,
            height: 104,
          ),
          StatusCard(
            title: AppStringKey.orders_pending.tr,
            description: controller.ordersPending.value,
            cardIcon: SvgPicture.asset(
              AppImages.ordersReceived,
              width: 32,
              height: 32,
            ),
            width: 262,
            height: 104,
          )
        ],
      ),
    );
  }
}
