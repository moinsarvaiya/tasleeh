import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../../api/api_end_point.dart';
import '../../api/api_interface.dart';
import '../../api/api_presenter.dart';
import '../../common/common_modal_widget.dart';
import '../../core/app_preference/app_preferences.dart';
import '../../core/app_preference/storage_keys.dart';
import '../../core/constant/app_color.dart';
import '../../core/constant/app_images.dart';
import '../../core/constant/base_extension.dart';
import '../../core/constant/ui_constants.dart';
import '../../core/routes/route_paths.dart';
import '../../l10n/app_string_key.dart';
import '../Dashboard/multi_branch_handler/multi_branch_handler_controller.dart';

class OnboardingSummaryController extends GetxController implements ApiCallBacks {
  void validateData() {
    ApiPresenter(this).validateOnboardingForm('summary');
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: 'Error');
  }

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {
    errorMsg.showErrorSnackBar(title: 'Error');
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.validateData:
        if (object['data'].length == 0) {
          Fluttertoast.cancel();

          showDialog(
              context: Get.context!,
              builder: (BuildContext context) {
                return CommonModalWidget(
                  modalIcon: AppImages.checkCircleIcon,
                  title: AppStringKey.submit_success.tr,
                  description: AppStringKey.submit_success_description.tr,
                  firstButtonContent: AppStringKey.explore_izhal.tr,
                  firstButtonOnTap: () {},
                  secondButtonContent: AppPreferences.sharedPrefRead(StorageKeys.isAddBranchesFromAuth)
                      ? AppStringKey.back_to_login.tr
                      : AppStringKey.back_to_dashboard.tr,
                  secondButtonOnTap: () {
                    if (AppPreferences.sharedPrefRead(StorageKeys.addBranchesFrom) == addBranch) {
                      if (AppPreferences.sharedPrefRead(StorageKeys.isAddBranchesFromAuth)) {
                        Get.offAllNamed(RoutePaths.LOGIN);
                      } else {
                        Get.find<MultiBranchHandlerController>().getBranches();
                        Get.until((route) => Get.currentRoute == RoutePaths.DASHBOARD_STEPPER);
                      }
                    } else {
                      Get.find<MultiBranchHandlerController>().getBranches();
                      Get.until((route) => Get.currentRoute == RoutePaths.DASHBOARD_STEPPER);
                    }
                  },
                  secondButtonBackgroundColor: AppColors.secondaryCtaColor,
                  secondButtonTextColor: AppColors.colorBlack,
                );
              });
        } else {
          String incompleteBranches = object['data'].map((entry) => entry).join(', ');
          Fluttertoast.showToast(
            msg: "${AppStringKey.please_complete_this_step_to_continue.tr} $incompleteBranches",
            gravity: ToastGravity.BOTTOM,
            webPosition: 'center',
            webBgColor: '#FF4D4F',
            webShowClose: true,
            fontSize: 16,
            timeInSecForIosWeb: toastVisibleTime,
            textColor: AppColors.colorWhite,
          );
        }
        break;
    }
  }
}
