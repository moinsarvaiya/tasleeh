import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../common/language_selector.dart';
import '../../core/constant/app_color.dart';
import '../../core/constant/base_style.dart';
import '../../core/constant/ui_constants.dart';
import '../../core/extensions/common_extension.dart';
import '../../l10n/app_string_key.dart';
import '../account_detail/account_detail_screen.dart';
import '../basic_details/basic_details_screen.dart';
import '../custom_stepper/widgets/branch_handler/branch_handler_screen.dart';
import '../marketing/marketing_screen.dart';
import '../services/service_screen.dart';
import '../staff/add_staff_screen.dart';

class OnBoardingSummary extends StatefulWidget {
  const OnBoardingSummary({super.key});

  @override
  State<OnBoardingSummary> createState() => _OnBoardingSummaryState();
}

class _OnBoardingSummaryState extends State<OnBoardingSummary> {
  @override
  void initState() {
    super.initState();
    stepperPosition = StepperPosition.summary;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.colorWhite,
        border: Border.all(color: AppColors.strokeColor, width: 1),
      ),
      padding: context.isDesktop ? const EdgeInsets.fromLTRB(68, 40, 68, 32) : const EdgeInsets.fromLTRB(12, 16, 12, 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                AppStringKey.summary.tr,
                style: BaseStyle.textStyleRobotoSemiBold(
                  30,
                  AppColors.primaryTextColor,
                ),
              ),
              const GlobalLanguageSelector(),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          BranchHandler(
            titleName: AppStringKey.basic_details.tr,
            isReadOnly: true,
            formPage: const BasicDetailsScreen(
              isReadOnly: true,
            ),
            position: 0,
          ),
          const SizedBox(
            height: 45,
          ),
          BranchHandler(
            titleName: AppStringKey.account_detail.tr,
            isReadOnly: true,
            formPage: const AccountDetailScreen(
              isReadOnly: true,
            ),
            position: 1,
          ),
          context.isDesktop
              ? const SizedBox.shrink()
              : const SizedBox(
                  height: 30,
                ),
          BranchHandler(
            titleName: AppStringKey.marketing.tr,
            isReadOnly: true,
            formPage: const MarketingScreen(
              isReadOnly: true,
            ),
            position: 2,
          ),
          const SizedBox(
            height: 30,
          ),
          BranchHandler(
            titleName: AppStringKey.service_info.tr,
            isReadOnly: true,
            formPage: const ServiceScreen(
              isReadOnly: true,
              serviceRedirectionFrom: ServiceRedirectionFrom.summary,
            ),
            position: 3,
          ),
          const SizedBox(
            height: 30,
          ),
          BranchHandler(
            titleName: AppStringKey.staff_details.tr,
            isReadOnly: true,
            formPage: const AddStaffScreen(
              isReadOnly: true,
            ),
            position: 4,
          ),
        ],
      ),
    );
  }
}
