import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_network/image_network.dart';
import 'package:path/path.dart' as p;

import '../../../Utils/dimensions.dart';
import '../../../common/common_function.dart';
import '../../../common/hover_icon.dart';
import '../../../common/space_horizontal.dart';
import '../../../core/constant/app_color.dart';
import '../../../core/constant/app_images.dart';
import '../../../core/constant/base_style.dart';
import '../../../core/constant/ui_constants.dart';
import '../../../core/constant/widget_functions.dart';
import '../../../core/extensions/common_extension.dart';
import '../../../l10n/app_string_key.dart';

class FileUpload extends StatelessWidget {
  final String title;
  final String acceptedFileDescription;
  final String sizeDescription;
  final String buttonContent;
  final List<String>? imageUrlsList;
  final bool isReadOnly;
  final Function(int index)? onRemoveImage;
  final Function(Map<String, Uint8List> image)? onImagePick;

  const FileUpload({
    Key? key,
    required this.title,
    required this.acceptedFileDescription,
    required this.sizeDescription,
    required this.buttonContent,
    this.isReadOnly = false,
    this.imageUrlsList,
    this.onRemoveImage,
    this.onImagePick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    double screenWidth = mediaQuery.size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          title,
          overflow: TextOverflow.ellipsis,
          style:
              BaseStyle.textStyleRobotoRegular(14, AppColors.inputFieldHeading),
        ),
        const SizedBox(
          height: 8,
        ),
        TextButton(
          onPressed: isReadOnly
              ? null
              : () {
                  if (imageUrlsList!.length < maxImageUploadSize) {
                    CommonFunction.pickImages(imageUrlsList!.length)
                        .then((value) {
                      onImagePick!(value);
                    });
                  } else {
                    CommonFunction.showCustomSnackBar(
                      context: context,
                      message: AppStringKey.max_limit_reached_message.tr,
                      duration: const Duration(seconds: 3),
                      actionLabel: AppStringKey.close.tr,
                    );
                  }
                },
          style: TextButton.styleFrom(
            maximumSize:
                (context.isDesktop ? const Size(190, 40) : Size.infinite),
            textStyle: isReadOnly
                ? BaseStyle.textStyleRobotoRegular(
                    14,
                    AppColors.tertiaryTextColor,
                  )
                : BaseStyle.textStyleRobotoRegular(
                    14,
                    AppColors.inputFieldHeading,
                  ),
            splashFactory: NoSplash.splashFactory,
            shape: const BeveledRectangleBorder(
              side: BorderSide(
                color: AppColors.strokeColor,
                width: 0,
              ),
            ),
            backgroundColor: isReadOnly
                ? AppColors.tertiaryBackgroundColor
                : AppColors.colorWhite,
            foregroundColor: isReadOnly
                ? AppColors.strokeColor
                : AppColors.inputFieldHeading,
          ),
          child: Container(
            padding: screenWidth > Dimensions.lg
                ? const EdgeInsets.fromLTRB(16, 9, 16, 9)
                : const EdgeInsets.fromLTRB(8, 6, 8, 6),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.file_upload,
                  color: isReadOnly
                      ? AppColors.tertiaryTextColor
                      : AppColors.strokeColor,
                ),
                const SizedBox(
                  width: 8,
                ),
                Text(
                  buttonContent,
                )
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 8,
        ),
        ListView.builder(
          shrinkWrap: true,
          itemCount: imageUrlsList!.length,
          itemBuilder: (context, index) {
            return Container(
              width: 48,
              height: 48,
              padding: const EdgeInsets.symmetric(horizontal: 9, vertical: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                color: isReadOnly
                    ? AppColors.tertiaryBackgroundColor
                    : AppColors.colorWhite,
                border: LRTB_Border(1, 1, 1, 1, AppColors.strokeColor),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        ImageNetwork(
                          image: imageUrlsList![index],
                          fitWeb: BoxFitWeb.cover,
                          height: 48,
                          width: 48,
                        ),
                        const SpaceHorizontal(0.005),
                        Expanded(
                          child: Text(
                            p.basename(imageUrlsList![index]),
                            overflow: TextOverflow.ellipsis,
                            style: BaseStyle.textStyleRobotoRegular(
                              14,
                              AppColors.imageTextColor,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  HoverIcon(
                    icon: AppImages.deleteIcon,
                    defaultColor: AppColors.secondaryTextColor,
                    hoverColor: isReadOnly
                        ? AppColors.secondaryTextColor
                        : AppColors.errorTextColor,
                    iconSize: 14,
                    backgroundColor: isReadOnly
                        ? AppColors.tertiaryBackgroundColor
                        : AppColors.colorWhite,
                    onTap: isReadOnly
                        ? null
                        : () {
                            onRemoveImage!(index);
                          },
                  )
                ],
              ),
            );
          },
        ),
        const SizedBox(
          height: 6,
        ),
        Text(
          acceptedFileDescription,
          style: BaseStyle.textStyleRobotoRegular(
            14,
            AppColors.secondaryTextColor,
          ),
        ),
        Text(
          sizeDescription,
          style: BaseStyle.textStyleRobotoRegular(
            14,
            AppColors.secondaryTextColor,
          ),
        ),
        const SizedBox(height: 20),
      ],
    );
  }
}
