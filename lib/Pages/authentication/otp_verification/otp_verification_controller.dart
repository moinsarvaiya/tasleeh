import 'dart:async';
import 'dart:html' as html;

import 'package:al_baida_garage_fe/api/api_end_point.dart';
import 'package:al_baida_garage_fe/api/api_interface.dart';
import 'package:al_baida_garage_fe/api/api_presenter.dart';
import 'package:al_baida_garage_fe/core/constant/base_extension.dart';
import 'package:al_baida_garage_fe/core/routes/route_paths.dart';
import 'package:al_baida_garage_fe/l10n/app_string_key.dart';
import 'package:get/get.dart';

import '../../../Utils/app_constants.dart';
import '../../../core/constant/app_argument_key.dart';
import '../../../core/constant/ui_constants.dart';

class OTPVerificationController extends GetxController implements ApiCallBacks {
  RxString otp = ''.obs;
  RxInt resendTime = AppConstants.otpTimer.obs;
  RxBool isResendEnabled = false.obs;
  RxString errorText = "".obs;
  String phoneNumber = '';
  late Timer resendTimer;

  @override
  void onInit() {
    super.onInit();
    if (userComesFrom == UserComesFrom.signUp) {
      html.window.history.pushState(null, '', 'signup');
    } else if (userComesFrom == UserComesFrom.forgotPassword) {
      html.window.history.pushState(null, '', 'forgetPassword');
    }
    phoneNumber = Get.arguments[AppArgumentKey.phoneNumber];
    startResendTimer();
  }

  void startResendTimer() {
    resendTimer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (resendTime > 0) {
        resendTime--;
      } else {
        isResendEnabled.value = true;
        resendTimer.cancel();
      }
    });
  }

  Future<void> handleResendOtp() async {
    if (isResendEnabled.value) {
      resendOTP();
    }
  }

  void resendOTP() {
    ApiPresenter(this).resendOTP(phoneNumber);
  }

  void otpVerify() {
    ApiPresenter(this).otpVerify(phoneNumber, otp.value);
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: 'Error');
  }

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {
    errorText.value = AppStringKey.invalid_otp_error.tr;
    // errorMsg.showErrorSnackBar(title: 'Error');
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.otpSend:
        isResendEnabled.value = false;
        resendTime.value = AppConstants.otpTimer;
        startResendTimer();
        break;
      case ApiEndPoints.otpVerify:
        Get.toNamed(RoutePaths.CREATE_RESET_PASSWORD, arguments: {
          AppArgumentKey.phoneNumber: phoneNumber,
        });
        break;
    }
  }
}
