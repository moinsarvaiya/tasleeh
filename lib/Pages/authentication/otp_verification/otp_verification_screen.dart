import 'package:al_baida_garage_fe/Pages/authentication/otp_verification/otp_verification_controller.dart';
import 'package:al_baida_garage_fe/Pages/authentication/widgets/authentication_back_widget.dart';
import 'package:al_baida_garage_fe/core/constant/base_style.dart';
import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:al_baida_garage_fe/core/routes/route_paths.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:get/get.dart';

import '../../../Utils/app_constants.dart';
import '../../../Utils/dimensions.dart';
import '../../../core/constant/app_color.dart';
import '../../../l10n/app_string_key.dart';

class OTPVerificationScreen extends GetView<OTPVerificationController> {
  const OTPVerificationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return AuthenticationBackWidget(
      backIconText: '',
      bodyWidget: Container(
        padding: const EdgeInsetsDirectional.all(7),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(AppStringKey.mobile_verification.tr,
                  style: BaseStyle.textStyleRobotoSemiBold(
                    context.isDesktop ? 30 : 24,
                    AppColors.primaryTextColor,
                  )),
              Container(
                margin: const EdgeInsets.only(top: 12.0, bottom: 48),
                child: Center(
                  child: RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text: AppStringKey.enter_six_digit_code.tr,
                          style: BaseStyle.textStyleRobotoRegular(
                            context.isDesktop ? 14 : 12,
                            AppColors.primaryTextColor,
                          ),
                        ),
                        TextSpan(
                          text:
                              " +${AppConstants.countryCode}-${controller.phoneNumber}. ",
                          style: BaseStyle.textStyleRobotoBold(
                            context.isDesktop ? 14 : 12,
                            AppColors.primaryTextColor,
                          ),
                          recognizer: TapGestureRecognizer()..onTap = () {},
                        ),
                        TextSpan(
                          text: AppStringKey.change.tr,
                          style:
                              Theme.of(context).textTheme.bodySmall!.copyWith(
                                    fontWeight: FontWeight.w600,
                                    decoration: TextDecoration.underline,
                                  ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Get.offNamed(RoutePaths.SIGNUP);
                            },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Obx(
                () => OtpTextField(
                  numberOfFields: AppConstants.otpLength,
                  fieldWidth: context.screenWidth > Dimensions.sm ? 50.0 : 40.0,
                  margin: context.screenWidth > Dimensions.sm
                      ? const EdgeInsets.only(left: 11, right: 11)
                      : context.screenWidth > 370
                          ? const EdgeInsets.only(left: 8, right: 8)
                          : const EdgeInsets.only(left: 4, right: 4),
                  showFieldAsBox: true,
                  inputFormatters: [
                    FilteringTextInputFormatter.digitsOnly,
                  ],
                  enabledBorderColor: controller.errorText == ''
                      ? AppColors.strokeColor
                      : AppColors.errorTextColor,
                  focusedBorderColor: AppColors.primaryTextColor,
                  autoFocus: true,
                  cursorColor: AppColors.primaryTextColor,
                  onSubmit: (String verificationCode) {
                    controller.otp.value = verificationCode;
                  },
                ),
              ),
              Obx(
                () => Container(
                  width: double.infinity,
                  constraints: const BoxConstraints(maxWidth: 410),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        controller.errorText.value,
                        style: BaseStyle.textStyleRobotoRegular(
                          14,
                          AppColors.errorTextColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Obx(
                () => Container(
                  margin: const EdgeInsets.only(top: 20.0, bottom: 48),
                  child: controller.isResendEnabled.value
                      ? TextButton(
                          onPressed: controller.handleResendOtp,
                          child: Text(
                            AppStringKey.resend_otp.tr,
                            style: BaseStyle.textStyleRobotoBold(
                              16,
                              AppColors.primaryTextColor,
                            ),
                          ),
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              AppStringKey.resend_otp_in.tr,
                              style: BaseStyle.textStyleRobotoRegular(
                                16,
                                AppColors.primaryTextColor,
                              ),
                            ),
                            const SizedBox(
                              width: 4,
                            ),
                            Obx(
                              () => Text(
                                "${controller.resendTime} ${AppStringKey.seconds.tr}",
                                style: BaseStyle.textStyleRobotoBold(
                                  16,
                                  AppColors.primaryTextColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                ),
              ),
              Obx(
                () => Container(
                  padding: const EdgeInsets.all(10),
                  child: TextButton(
                    onPressed:
                        controller.otp.value.length == AppConstants.otpLength
                            ? () async {
                                controller.otpVerify();
                              }
                            : null,
                    style: controller.otp.value.length == AppConstants.otpLength
                        ? TextButton.styleFrom(
                            textStyle: BaseStyle.textStyleRobotoRegular(
                              16,
                              AppColors.primaryCtaColor,
                            ),
                            backgroundColor: AppColors.primaryCtaColor,
                            foregroundColor: Colors.white,
                            fixedSize: const Size(370, 50),
                            shape: const BeveledRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(2),
                              ),
                            ),
                          )
                        : TextButton.styleFrom(
                            textStyle: BaseStyle.textStyleRobotoRegular(
                              16,
                              AppColors.primaryCtaColor,
                            ),
                            backgroundColor: AppColors.inActiveCtaColor,
                            foregroundColor: AppColors.strokeColor,
                            side: const BorderSide(
                              color: AppColors.strokeColor,
                              width: 1,
                            ),
                            fixedSize: const Size(370, 50),
                            shape: const BeveledRectangleBorder(
                              borderRadius: BorderRadius.all(
                                Radius.circular(2),
                              ),
                            ),
                          ),
                    child: Text(AppStringKey.continue_ahead.tr),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
