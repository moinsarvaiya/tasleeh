import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../Utils/app_constants.dart';
import '../../../common/space_vertical.dart';
import '../../../core/constant/app_color.dart';
import '../../../core/constant/app_images.dart';
import '../../../core/constant/base_style.dart';
import '../../../core/extensions/common_extension.dart';
import '../../../l10n/app_string_key.dart';
import '../widgets/authentication_back_widget.dart';
import 'forget_password_controller.dart';

class ForgetPasswordScreen extends GetView<ForgetPasswordController> {
  const ForgetPasswordScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return AuthenticationBackWidget(
      backIconText: '',
      bodyWidget: Container(
        margin: EdgeInsets.symmetric(vertical: context.screenHeight * 0.15),
        child: Column(
          mainAxisAlignment: context.isMobile
              ? MainAxisAlignment.center
              : MainAxisAlignment.start,
          children: [
            Text(
              AppStringKey.forget_password.tr,
              style: BaseStyle.textStyleRobotoSemiBold(
                30,
                AppColors.primaryTextColor,
              ),
            ),
            const SpaceVertical(0.06),
            Container(
              width: double.infinity,
              constraints: const BoxConstraints(maxWidth: 355),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    AppStringKey.phone_no.tr,
                    style: BaseStyle.textStyleRobotoRegular(
                      14,
                      AppColors.primaryTextColor,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      Directionality(
                        textDirection: TextDirection.ltr,
                        child: Container(
                          height: 48,
                          padding: const EdgeInsets.symmetric(
                              vertical: 8, horizontal: 11),
                          color: const Color(0xFFEDEDED),
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                AppImages.qatarFlag,
                                width: 20,
                                height: 15,
                              ),
                              const SizedBox(
                                width: 6,
                              ),
                              Text(
                                "+${AppConstants.countryCode}",
                                style: BaseStyle.textStyleRobotoRegular(
                                  14,
                                  AppColors.primaryTextColor,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: TextField(
                          controller: controller.phoneNumberController,
                          keyboardType: TextInputType.phone,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          maxLength: AppConstants.maxMobileNumberLength,
                          onChanged: (value) {
                            controller.checkPhoneNumber(value);
                          },
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            counterText: "",
                            hintText: AppStringKey.enter_phone_no.tr,
                            hintStyle: BaseStyle.textStyleRobotoRegular(
                              16,
                              AppColors.inputFieldHintTextColor,
                            ),
                            enabledBorder: const OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                Radius.elliptical(2, 2),
                              ),
                              borderSide: BorderSide(
                                color: AppColors.strokeColor,
                              ),
                            ),
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            focusedBorder: const OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.elliptical(2, 2)),
                              borderSide: BorderSide(
                                color: AppColors.strokeColor,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            const SpaceVertical(0.14),
            Obx(
              () => Container(
                width: double.infinity,
                height: 40,
                constraints: const BoxConstraints(maxWidth: 356),
                child: TextButton(
                  onPressed: controller.isPhoneNumberValid.value
                      ? () {
                          controller.forgetPassword();
                        }
                      : null,
                  style: controller.isPhoneNumberValid.value
                      ? TextButton.styleFrom(
                          textStyle: BaseStyle.textStyleRobotoRegular(
                            16,
                            AppColors.primaryCtaColor,
                          ),
                          backgroundColor: AppColors.primaryCtaColor,
                          foregroundColor: Colors.white,
                          fixedSize: const Size(370, 50),
                          shape: const BeveledRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(2),
                            ),
                          ),
                        )
                      : TextButton.styleFrom(
                          textStyle: BaseStyle.textStyleRobotoRegular(
                            16,
                            AppColors.primaryCtaColor,
                          ),
                          backgroundColor: AppColors.inActiveCtaColor,
                          foregroundColor: AppColors.strokeColor,
                          side: const BorderSide(
                            color: AppColors.strokeColor,
                            width: 1,
                          ),
                          fixedSize: const Size(370, 50),
                          shape: const BeveledRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(2),
                            ),
                          ),
                        ),
                  child: Text(
                    AppStringKey.get_otp.tr,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
