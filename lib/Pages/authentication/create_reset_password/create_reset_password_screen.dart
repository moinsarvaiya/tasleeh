import 'package:al_baida_garage_fe/Pages/authentication/create_reset_password/create_reset_password_controller.dart';
import 'package:al_baida_garage_fe/Pages/authentication/widgets/authentication_back_widget.dart';
import 'package:al_baida_garage_fe/core/constant/base_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../Utils/password_helper.dart';
import '../../../common/password_strength_indicator.dart';
import '../../../core/constant/app_color.dart';
import '../../../core/constant/ui_constants.dart';
import '../../../l10n/app_string_key.dart';

class CreateResetPasswordScreen extends GetView<CreateResetPasswordController> {
  const CreateResetPasswordScreen({super.key});

  @override
  Widget build(BuildContext context) {
    const border = OutlineInputBorder(
      borderSide: BorderSide(
        width: 1,
        color: AppColors.strokeColor,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(4),
      ),
    );

    const errorBorder = OutlineInputBorder(
      borderSide: BorderSide(
        width: 1,
        color: AppColors.errorTextColor,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(4),
      ),
    );

    return AuthenticationBackWidget(
      backIconText: '',
      bodyWidget: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            userComesFrom == UserComesFrom.signUp
                ? AppStringKey.create_password.tr
                : AppStringKey.reset_password.tr,
            style: BaseStyle.textStyleRobotoSemiBold(
              30,
              AppColors.primaryTextColor,
            ),
          ),
          const SizedBox(
            height: 48,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStringKey.enter_password.tr,
                    style: BaseStyle.textStyleRobotoRegular(
                      14,
                      AppColors.primaryTextColor,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Obx(
                    () => SizedBox(
                      height: 40,
                      width: 356,
                      child: TextField(
                        controller: controller.passwordController.value,
                        onChanged: (password) {
                          controller.passwordText.value = password;
                        },
                        textInputAction: TextInputAction.next,
                        textAlign: TextAlign.start,
                        textAlignVertical: TextAlignVertical.bottom,
                        style: const TextStyle(
                          color: AppColors.primaryTextColor,
                        ),
                        decoration: InputDecoration(
                          hintText: '',
                          hintStyle: const TextStyle(
                            color: AppColors.primaryTextColor,
                          ),
                          filled: true,
                          fillColor: Colors.white,
                          focusedBorder:
                              controller.errorText == "" ? border : errorBorder,
                          enabledBorder:
                              controller.errorText == "" ? border : errorBorder,
                          suffixIcon: GestureDetector(
                            onTap: () {
                              // setState(() {
                              controller.obscurePassword.value =
                                  !controller.obscurePassword.value;
                              // });
                            },
                            child: Icon(
                              controller.obscurePassword.value
                                  ? Icons.visibility_off_outlined
                                  : Icons.visibility_outlined,
                              color: AppColors.inputFieldIconColor,
                              size: 20,
                            ),
                          ),
                        ),
                        obscureText: controller.obscurePassword.value,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 24,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStringKey.re_enter_password.tr,
                    style: BaseStyle.textStyleRobotoRegular(
                      14,
                      AppColors.primaryTextColor,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Obx(
                    () => SizedBox(
                      height: 40,
                      width: 356,
                      child: TextField(
                        controller: controller.reEnterPasswordController.value,
                        textAlign: TextAlign.start,
                        textAlignVertical: TextAlignVertical.bottom,
                        onChanged: (password) {
                          controller.confirmPasswordText.value = password;
                          // setState(() {}); // Trigger a rebuild to update the PasswordStrengthIndicator
                        },
                        textInputAction: TextInputAction.next,
                        style: const TextStyle(
                          color: AppColors.primaryTextColor,
                        ),
                        decoration: InputDecoration(
                          hintText: '',
                          hintStyle: const TextStyle(
                            color: AppColors.primaryTextColor,
                          ),
                          filled: true,
                          fillColor: Colors.white,
                          focusedBorder:
                              controller.errorText == "" ? border : errorBorder,
                          enabledBorder:
                              controller.errorText == "" ? border : errorBorder,
                          suffixIcon: GestureDetector(
                            onTap: () {
                              controller.obscureReEnterPassword.value =
                                  !controller.obscureReEnterPassword.value;
                            },
                            child: Icon(
                              controller.obscureReEnterPassword.value
                                  ? Icons.visibility_off_outlined
                                  : Icons.visibility_outlined,
                              color: AppColors.inputFieldIconColor,
                              size: 20,
                            ),
                          ),
                        ),
                        obscureText: controller.obscureReEnterPassword.value,
                      ),
                    ),
                  ),
                  Obx(
                    () => Text(
                      controller.errorText.value,
                      style: BaseStyle.textStyleRobotoRegular(
                        14,
                        AppColors.errorTextColor,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Obx(
                    () => IntrinsicWidth(
                      child: PasswordStrengthIndicator(
                        password: controller.passwordText.value,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          SizedBox(
            height: controller.errorText.isNotEmpty ? 12 : null,
          ),
          const SizedBox(
            height: 50,
          ),
          Obx(
            () => TextButton(
              onPressed: controller.confirmPasswordText.value.isNotEmpty &&
                      PasswordStrengthChecks.isPasswordValid(
                          controller.passwordText.value)
                  ? () async {
                      if (controller.passwordController.value.text.isEmpty) {
                        controller.errorText.value =
                            AppStringKey.please_enter_password.tr;
                      } else if (controller.passwordController.value.text !=
                          controller.reEnterPasswordController.value.text) {
                        controller.errorText.value =
                            AppStringKey.password_do_not_match.tr;
                      } else if (PasswordStrengthChecks.isPasswordValid(
                          controller.passwordController.value.text)) {
                        controller.errorText.value = "";
                        controller.setPassword();
                      }
                    }
                  : null,
              style: controller.confirmPasswordText.value.isNotEmpty &&
                      PasswordStrengthChecks.isPasswordValid(
                          controller.passwordText.value)
                  ? TextButton.styleFrom(
                      textStyle: BaseStyle.textStyleRobotoRegular(
                        16,
                        AppColors.primaryTextColor,
                      ),
                      backgroundColor: AppColors.primaryCtaColor,
                      foregroundColor: Colors.white,
                      fixedSize: const Size(356, 40),
                      shape: const BeveledRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(2),
                        ),
                      ),
                    )
                  : TextButton.styleFrom(
                      textStyle: BaseStyle.textStyleRobotoRegular(
                        16,
                        AppColors.strokeColor,
                      ),
                      backgroundColor: AppColors.inActiveCtaColor,
                      foregroundColor: AppColors.strokeColor,
                      side: const BorderSide(
                        color: AppColors.strokeColor,
                        width: 1,
                      ),
                      fixedSize: const Size(356, 40),
                      shape: const BeveledRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(2),
                        ),
                      ),
                    ),
              child: controller.confirmPasswordText.value.isNotEmpty &&
                      PasswordStrengthChecks.isPasswordValid(
                          controller.passwordText.value) &&
                      controller.passwordText.value ==
                          controller.confirmPasswordText.value
                  ? Text(
                      userComesFrom == UserComesFrom.signUp
                          ? AppStringKey.continue_ahead.tr
                          : AppStringKey.reset_password.tr,
                    )
                  : Text(
                      userComesFrom == UserComesFrom.signUp
                          ? AppStringKey.continue_ahead.tr
                          : AppStringKey.reset_password.tr,
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
