import 'dart:html' as html;

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../../api/api_end_point.dart';
import '../../../api/api_interface.dart';
import '../../../api/api_presenter.dart';
import '../../../api/garage_status.dart';
import '../../../common/custom_dialog.dart';
import '../../../core/constant/app_argument_key.dart';
import '../../../core/constant/app_color.dart';
import '../../../core/constant/app_images.dart';
import '../../../core/constant/base_extension.dart';
import '../../../core/constant/base_style.dart';
import '../../../core/constant/ui_constants.dart';
import '../../../core/extensions/common_extension.dart';
import '../../../core/routes/route_paths.dart';
import '../../../l10n/app_string_key.dart';

class CreateResetPasswordController extends GetxController implements ApiCallBacks {
  String phoneNumber = '';

  final Rx<TextEditingController> passwordController = TextEditingController().obs;
  final Rx<TextEditingController> reEnterPasswordController = TextEditingController().obs;

  RxBool obscurePassword = true.obs;
  RxBool obscureReEnterPassword = true.obs;
  RxString errorText = ''.obs;
  RxString passwordText = ''.obs;
  RxString confirmPasswordText = ''.obs;

  @override
  void onInit() {
    super.onInit();
    if (userComesFrom == UserComesFrom.signUp) {
      html.window.history.pushState(null, '', 'signup');
    } else if (userComesFrom == UserComesFrom.forgotPassword) {
      html.window.history.pushState(null, '', 'forgetPassword');
    }
    phoneNumber = Get.arguments[AppArgumentKey.phoneNumber];
  }

  void setPassword() {
    ApiPresenter(this).setPassword(
      passwordText.value,
      confirmPasswordText.value,
      phoneNumber,
      userComesFrom == UserComesFrom.signUp ? "signIn" : "forgotPassword",
    );
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: 'Error');
  }

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {
    errorMsg.showErrorSnackBar(title: 'Error');
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.createPassword:
        showDialog(
          context: Get.context!,
          builder: (BuildContext context) {
            return CustomDialog(
              padding: EdgeInsets.symmetric(
                vertical: context.isDesktop ? 72 : 56,
                horizontal: context.isDesktop ? 150 : 30,
              ),
              maxWidth: (context.isDesktop ? 750 : double.infinity),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SvgPicture.asset(
                    AppImages.checkCircleIcon,
                    width: 80,
                    height: 80,
                  ),
                  const SizedBox(
                    height: 48,
                  ),
                  Text(
                    userComesFrom == UserComesFrom.signUp ? AppStringKey.create_password_title.tr : AppStringKey.password_successful_title.tr,
                    style: BaseStyle.textStyleRobotoSemiBold(
                      context.isDesktop ? 24 : 18,
                      AppColors.primaryTextColor,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Container(
                    constraints: const BoxConstraints(maxWidth: 356),
                    child: Text(
                      userComesFrom == UserComesFrom.signUp ? AppStringKey.create_password_message.tr : AppStringKey.password_successful_message.tr,
                      style: BaseStyle.textStyleRobotoRegular(
                        14,
                        AppColors.primaryTextColor,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  const SizedBox(
                    height: 48,
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                      if (userComesFrom == UserComesFrom.signUp) {
                        GarageStatus().getGarageStatus(object['data']['accessToken'], object['data']['userId']);
                      } else {
                        Get.toNamed(RoutePaths.LOGIN);
                      }
                    },
                    style: TextButton.styleFrom(
                      textStyle: BaseStyle.textStyleRobotoRegular(
                        16,
                        AppColors.primaryCtaColor,
                      ),
                      padding: const EdgeInsets.symmetric(
                        vertical: 10,
                        horizontal: 46,
                      ),
                      foregroundColor: AppColors.primaryCtaTextColor,
                      backgroundColor: AppColors.primaryCtaColor,
                      shape: const BeveledRectangleBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(2),
                        ),
                      ),
                    ),
                    child: Text(
                      userComesFrom == UserComesFrom.signUp
                          ? AppStringKey.create_password_button_content.tr
                          : AppStringKey.password_successful_button_content.tr,
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            );
          },
        );
        break;
    }
  }
}
