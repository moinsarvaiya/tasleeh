import 'package:al_baida_garage_fe/Utils/utilities.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../Utils/app_constants.dart';
import '../../../api/api_end_point.dart';
import '../../../api/api_interface.dart';
import '../../../api/api_presenter.dart';
import '../../../api/garage_status.dart';
import '../../../core/constant/base_extension.dart';

class LoginController extends GetxController implements ApiCallBacks {
  final TextEditingController phoneNumberController = TextEditingController();
  final Rx<TextEditingController> passwordController = TextEditingController().obs;

  RxBool isPhoneNumberValid = false.obs;
  RxBool obscurePassword = true.obs;
  RxString phoneNumberFieldErrorText = ''.obs;
  RxString passwordFieldErrorText = ''.obs;
  RxString passwordText = ''.obs;

  void checkPhoneNumber(String input) {
    isPhoneNumberValid.value = (input.length <= AppConstants.maxMobileNumberLength && input.length >= AppConstants.minMobileNumberLength);
  }

  void login() async {
    String fcmToken = await getFCMToken();
    ApiPresenter(this).login(
      AppConstants.countryCode,
      phoneNumberController.text.toString(),
      passwordText.toString(),
      fcmToken,
    );
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: 'Error');
  }

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {
    // errorMsg.showErrorSnackBar(title: 'Error');
    if (statusCode == 404) {
      phoneNumberFieldErrorText.value = errorMsg;
    } else if (statusCode == 400) {
      passwordFieldErrorText.value = errorMsg;
    }
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) async {
    switch (apiEndPoint) {
      case ApiEndPoints.signIn:
        GarageStatus().getGarageStatus(
          object['data']['accessToken'],
          object['data']['userId'],
          userType: object['data']['userType'],
          employeeType: object['data']['employeeType'],
          garageId: object['data']['garageId'],
        );
        break;
    }
  }
}
