import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../Utils/app_constants.dart';
import '../../../common/language_selector.dart';
import '../../../common/space_vertical.dart';
import '../../../core/constant/app_color.dart';
import '../../../core/constant/app_images.dart';
import '../../../core/constant/base_style.dart';
import '../../../core/extensions/common_extension.dart';
import '../../../core/routes/route_paths.dart';
import '../../../core/auth/social_login_handler.dart';
import '../../../l10n/app_string_key.dart';
import '../widgets/authentication_back_widget.dart';
import 'login_controllers.dart';

class LoginScreen extends GetView<LoginController> {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    const border = OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.elliptical(2, 2)),
      borderSide: BorderSide(
        color: AppColors.strokeColor,
      ),
    );
    const errorBorder = OutlineInputBorder(
      borderSide: BorderSide(
        width: 1,
        color: AppColors.errorTextColor,
      ),
      borderRadius: BorderRadius.all(
        Radius.circular(4),
      ),
    );

    return AuthenticationBackWidget(
      backIconText: ''.tr,
      bodyWidget: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            AppStringKey.login.tr,
            style: BaseStyle.textStyleRobotoSemiBold(
              30,
              AppColors.primaryTextColor,
            ),
          ),
          const SpaceVertical(0.05),
          Container(
            width: double.infinity,
            constraints: const BoxConstraints(maxWidth: 355),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  AppStringKey.phone_no.tr,
                  style: BaseStyle.textStyleRobotoRegular(
                    14,
                    AppColors.primaryTextColor,
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Row(
                  children: [
                    Directionality(
                      textDirection: TextDirection.ltr,
                      child: Container(
                        height: 48,
                        padding: const EdgeInsets.symmetric(
                          vertical: 8,
                          horizontal: 11,
                        ),
                        color: AppColors.tertiaryBackgroundColor,
                        child: Row(
                          children: [
                            SvgPicture.asset(
                              AppImages.qatarFlag,
                              width: 20,
                              height: 15,
                            ),
                            const SizedBox(
                              width: 6,
                            ),
                            Text(
                              "+${AppConstants.countryCode}",
                              style: BaseStyle.textStyleRobotoRegular(
                                14,
                                AppColors.primaryTextColor,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Obx(
                        () => TextField(
                          controller: controller.phoneNumberController,
                          keyboardType: TextInputType.phone,
                          textInputAction: TextInputAction.next,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          maxLength: AppConstants.maxMobileNumberLength,
                          onChanged: (value) {
                            controller.checkPhoneNumber(value);
                          },
                          decoration: InputDecoration(
                            hintText: AppStringKey.enter_phone_no.tr,
                            hintStyle: BaseStyle.textStyleRobotoRegular(
                              16,
                              AppColors.inputFieldHintTextColor,
                            ),
                            counterText: "",
                            contentPadding: const EdgeInsets.symmetric(
                              vertical: 8,
                              horizontal: 12,
                            ),
                            focusedBorder:
                                controller.phoneNumberFieldErrorText ==
                                        RxString("")
                                    ? border
                                    : errorBorder,
                            enabledBorder:
                                controller.phoneNumberFieldErrorText ==
                                        RxString("")
                                    ? border
                                    : errorBorder,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
                Obx(
                  () => controller.phoneNumberFieldErrorText.value != ''
                      ? Text(
                          controller.phoneNumberFieldErrorText.value,
                          style: BaseStyle.textStyleRobotoRegular(
                            14,
                            AppColors.errorTextColor,
                          ),
                        )
                      : const SizedBox.shrink(),
                ),
                const SizedBox(
                  height: 24,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppStringKey.enter_password.tr,
                          style: BaseStyle.textStyleRobotoRegular(
                            14,
                            AppColors.primaryTextColor,
                          ),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Obx(
                          () => SizedBox(
                            height: 40,
                            width: 356,
                            child: TextField(
                              controller: controller.passwordController.value,
                              onChanged: (password) {
                                controller.passwordText.value = password;
                              },
                              textInputAction: TextInputAction.done,
                              textAlign: TextAlign.start,
                              textAlignVertical: TextAlignVertical.bottom,
                              style: const TextStyle(
                                color: AppColors.primaryTextColor,
                              ),
                              decoration: InputDecoration(
                                hintText: '',
                                hintStyle: const TextStyle(
                                  color: AppColors.primaryTextColor,
                                ),
                                filled: true,
                                fillColor: Colors.white,
                                focusedBorder:
                                    controller.passwordFieldErrorText ==
                                            RxString("")
                                        ? border
                                        : errorBorder,
                                enabledBorder:
                                    controller.passwordFieldErrorText ==
                                            RxString("")
                                        ? border
                                        : errorBorder,
                                suffixIcon: GestureDetector(
                                  onTap: () {
                                    // setState(() {
                                    controller.obscurePassword.value =
                                        !controller.obscurePassword.value;
                                    // });
                                  },
                                  child: Icon(
                                    controller.obscurePassword.value
                                        ? Icons.visibility_off_outlined
                                        : Icons.visibility_outlined,
                                    color: AppColors.inputFieldIconColor,
                                    size: 20,
                                  ),
                                ),
                              ),
                              obscureText: controller.obscurePassword.value,
                            ),
                          ),
                        ),
                        Obx(
                          () => controller.passwordFieldErrorText.value != ''
                              ? Text(
                                  controller.passwordFieldErrorText.value,
                                  style: BaseStyle.textStyleRobotoRegular(
                                    14,
                                    AppColors.errorTextColor,
                                  ),
                                )
                              : const SizedBox.shrink(),
                        ),
                      ],
                    ),
                    const SpaceVertical(0.03),
                    Row(
                      children: [
                        TextButton(
                          onPressed: () {
                            Get.toNamed(RoutePaths.FORGET_PASSWORD);
                          },
                          style: TextButton.styleFrom(
                            minimumSize: Size.zero,
                            padding: EdgeInsets.zero,
                            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                            foregroundColor: AppColors.primaryTextColor,
                            textStyle: BaseStyle.underLineTextStyle(
                              14,
                              AppColors.primaryTextColor,
                            ),
                          ),
                          child: Text(
                            AppStringKey.forget_password.tr,
                          ),
                        ),
                      ],
                    ),
                    const SpaceVertical(0.04),
                    Obx(
                      () => Container(
                        width: double.infinity,
                        height: 40,
                        constraints: const BoxConstraints(maxWidth: 356),
                        child: TextButton(
                          onPressed: controller.isPhoneNumberValid.value
                              ? () {
                                  controller.login();
                                }
                              : null,
                          style: controller.isPhoneNumberValid.value &&
                                  controller.passwordText != RxString('')
                              ? TextButton.styleFrom(
                                  textStyle: BaseStyle.textStyleRobotoRegular(
                                    16,
                                    AppColors.primaryTextColor,
                                  ),
                                  backgroundColor: AppColors.primaryCtaColor,
                                  foregroundColor: Colors.white,
                                  fixedSize: const Size(370, 50),
                                  shape: const BeveledRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(2),
                                    ),
                                  ),
                                )
                              : TextButton.styleFrom(
                                  textStyle: BaseStyle.textStyleRobotoRegular(
                                    16,
                                    AppColors.primaryTextColor,
                                  ),
                                  backgroundColor: AppColors.inActiveCtaColor,
                                  foregroundColor: AppColors.strokeColor,
                                  side: const BorderSide(
                                    color: AppColors.strokeColor,
                                    width: 1,
                                  ),
                                  fixedSize: const Size(370, 50),
                                  shape: const BeveledRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(2),
                                    ),
                                  ),
                                ),
                          child: Text(
                            AppStringKey.submit.tr,
                          ),
                        ),
                      ),
                    ),
                    const SpaceVertical(0.03),
                    Text(
                      AppStringKey.continue_with.tr,
                      style: BaseStyle.textStyleRobotoSemiBold(
                        context.isDesktop ? 16 : 12,
                        AppColors.primaryTextColor,
                      ),
                    ),
                    const SpaceVertical(0.01),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        IconButton(
                          onPressed: () {
                            SocialAuthHandler().googleLogin();
                          },
                          splashRadius: 10,
                          icon: SvgPicture.asset(
                            AppImages.googleLogo,
                            width: 28,
                            height: 28,
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        IconButton(
                          onPressed: () {
                            SocialAuthHandler().facebookLogin();
                          },
                          splashRadius: 15,
                          icon: SvgPicture.asset(
                            AppImages.facebookLogo,
                            width: 28,
                            height: 28,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
          const SpaceVertical(0.01),
          RichText(
            textAlign: TextAlign.center,
            textDirection: Directionality.of(context),
            text: TextSpan(
              children: [
                TextSpan(
                  text: AppStringKey.already_have_an_account.tr,
                  style: BaseStyle.textStyleRobotoRegular(
                    16,
                    AppColors.primaryTextColor,
                  ),
                ),
                TextSpan(
                  text: " ${AppStringKey.sign_up_now.tr}",
                  style: BaseStyle.underLineTextStyle(
                    16,
                    AppColors.primaryTextColor,
                  ),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      Get.offNamed(RoutePaths.SIGNUP);
                    },
                ),
              ],
            ),
          ),
          const SpaceVertical(0.02),
          const GlobalLanguageSelector(),
        ],
      ),
    );
  }
}
