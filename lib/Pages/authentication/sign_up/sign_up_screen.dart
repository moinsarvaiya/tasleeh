import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../../Utils/app_constants.dart';
import '../../../common/language_selector.dart';
import '../../../common/space_vertical.dart';
import '../../../core/constant/app_color.dart';
import '../../../core/constant/app_images.dart';
import '../../../core/constant/base_style.dart';
import '../../../core/extensions/common_extension.dart';
import '../../../core/routes/route_paths.dart';
import '../../../core/auth/social_login_handler.dart';
import '../../../l10n/app_string_key.dart';
import '../widgets/authentication_back_widget.dart';
import 'sign_up_controller.dart';

class SignupScreen extends GetView<SignupController> {
  const SignupScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return AuthenticationBackWidget(
      backIconText: AppStringKey.homepage.tr,
      bodyWidget: Container(
        margin: EdgeInsets.symmetric(vertical: context.screenHeight * 0.01),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              AppStringKey.signup.tr,
              style:
                  BaseStyle.textStyleRobotoSemiBold(30, AppColors.colorBlack),
            ),
            const SpaceVertical(0.06),
            Container(
              width: double.infinity,
              constraints: const BoxConstraints(maxWidth: 355),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    AppStringKey.phone_no.tr,
                    style: BaseStyle.textStyleRobotoRegular(
                      14,
                      AppColors.primaryTextColor,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    children: [
                      Directionality(
                        textDirection: TextDirection.ltr,
                        child: Container(
                          height: 48,
                          padding: const EdgeInsets.symmetric(
                            vertical: 8,
                            horizontal: 11,
                          ),
                          color: const Color(0xFFEDEDED),
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                AppImages.qatarFlag,
                                width: 20,
                                height: 15,
                              ),
                              const SizedBox(
                                width: 6,
                              ),
                              Text(
                                "+${AppConstants.countryCode}",
                                style: BaseStyle.textStyleRobotoRegular(
                                  14,
                                  AppColors.primaryTextColor,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: TextField(
                          controller: controller.phoneNumberController,
                          keyboardType: TextInputType.phone,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          maxLength: AppConstants.maxMobileNumberLength,
                          onChanged: (value) {
                            controller.checkPhoneNumber(value);
                          },
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            hintText: AppStringKey.enter_phone_no.tr,
                            counterText: "",
                            hintStyle: BaseStyle.textStyleRobotoRegular(
                              16,
                              AppColors.inputFieldHintTextColor,
                            ),
                            enabledBorder: const OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.elliptical(2, 2)),
                              borderSide: BorderSide(
                                color: AppColors.strokeColor,
                              ),
                            ),
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 8, horizontal: 12),
                            focusedBorder: const OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.elliptical(2, 2)),
                              borderSide: BorderSide(
                                color: AppColors.strokeColor,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            const SpaceVertical(0.04),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                children: [
                  TextSpan(
                    text: "${AppStringKey.sigin_up_agree.tr} ",
                    style: BaseStyle.textStyleRobotoRegular(
                      12,
                      AppColors.primaryTextColor,
                    ).copyWith(height: 1.6),
                  ),
                  TextSpan(
                    text: AppStringKey.term_of_use.tr,
                    style: BaseStyle.underLineTextStyle(
                      12,
                      AppColors.primaryTextColor,
                    ).copyWith(height: 1.6),
                    recognizer: TapGestureRecognizer()..onTap = () {},
                  ),
                  TextSpan(
                    text: " & ",
                    style: BaseStyle.textStyleRobotoRegular(
                      12,
                      AppColors.primaryTextColor,
                    ).copyWith(height: 1.6),
                  ),
                  TextSpan(
                    text: AppStringKey.privacy_policy.tr,
                    style: BaseStyle.underLineTextStyle(
                      12,
                      AppColors.primaryTextColor,
                    ).copyWith(height: 1.6),
                    recognizer: TapGestureRecognizer()..onTap = () {},
                  ),
                ],
              ),
            ),
            const SpaceVertical(0.02),
            Obx(
              () => Container(
                width: double.infinity,
                height: 40,
                constraints: const BoxConstraints(maxWidth: 356),
                child: TextButton(
                  onPressed: controller.isPhoneNumberValid.value
                      ? () {
                          controller.signUp();
                        }
                      : null,
                  style: controller.isPhoneNumberValid.value
                      ? TextButton.styleFrom(
                          textStyle: BaseStyle.textStyleRobotoRegular(
                            16,
                            AppColors.primaryTextColor,
                          ),
                          backgroundColor: AppColors.primaryCtaColor,
                          foregroundColor: Colors.white,
                          fixedSize: const Size(370, 50),
                          shape: const BeveledRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(2),
                            ),
                          ),
                        )
                      : TextButton.styleFrom(
                          textStyle: BaseStyle.textStyleRobotoRegular(
                            16,
                            AppColors.primaryTextColor,
                          ),
                          backgroundColor: AppColors.inActiveCtaColor,
                          foregroundColor: AppColors.strokeColor,
                          side: const BorderSide(
                            color: AppColors.strokeColor,
                            width: 1,
                          ),
                          fixedSize: const Size(370, 50),
                          shape: const BeveledRectangleBorder(
                            borderRadius: BorderRadius.all(
                              Radius.circular(2),
                            ),
                          ),
                        ),
                  child: Text(
                    AppStringKey.submit.tr,
                  ),
                ),
              ),
            ),
            const SpaceVertical(0.03),
            Text(
              AppStringKey.continue_with.tr,
              style: BaseStyle.textStyleRobotoSemiBold(
                context.isDesktop ? 16 : 12,
                AppColors.primaryTextColor,
              ),
            ),
            const SpaceVertical(0.01),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  onPressed: () {
                    SocialAuthHandler().googleLogin();
                  },
                  splashRadius: 10,
                  icon: SvgPicture.asset(
                    AppImages.googleLogo,
                    width: 28,
                    height: 28,
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                IconButton(
                  onPressed: () {
                    SocialAuthHandler().facebookLogin();
                  },
                  splashRadius: 15,
                  icon: SvgPicture.asset(
                    AppImages.facebookLogo,
                    width: 28,
                    height: 28,
                  ),
                ),
              ],
            ),
            const SpaceVertical(0.01),
            RichText(
              textAlign: TextAlign.center,
              textDirection: Directionality.of(context),
              text: TextSpan(
                children: [
                  TextSpan(
                    text: AppStringKey.already_have_an_account.tr,
                    style: BaseStyle.textStyleRobotoRegular(
                      16,
                      AppColors.primaryTextColor,
                    ),
                  ),
                  TextSpan(
                    text: " ${AppStringKey.sigin_in_now.tr}",
                    style: BaseStyle.underLineTextStyle(
                      16,
                      AppColors.primaryTextColor,
                    ),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        Get.toNamed(RoutePaths.LOGIN);
                      },
                  ),
                ],
              ),
            ),
            const SpaceVertical(0.02),
            const GlobalLanguageSelector(),
          ],
        ),
      ),
    );
  }
}
