import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../Utils/app_constants.dart';
import '../../../api/api_end_point.dart';
import '../../../api/api_interface.dart';
import '../../../api/api_presenter.dart';
import '../../../core/constant/app_argument_key.dart';
import '../../../core/constant/base_extension.dart';
import '../../../core/constant/ui_constants.dart';
import '../../../core/routes/route_paths.dart';

class SignupController extends GetxController implements ApiCallBacks {
  final TextEditingController phoneNumberController = TextEditingController();
  RxBool isPhoneNumberValid = false.obs;

  void checkPhoneNumber(String input) {
    isPhoneNumberValid.value =
        (input.length <= AppConstants.maxMobileNumberLength &&
            input.length >= AppConstants.minMobileNumberLength);
  }

  void signUp() {
    ApiPresenter(this).signUp(
        AppConstants.countryCode, phoneNumberController.text.toString());
  }

  @override
  void onConnectionError(String error, String apiEndPoint) {
    error.showErrorSnackBar(title: 'Error');
  }

  @override
  void onError(String errorMsg, dynamic statusCode, String apiEndPoint) {
    errorMsg.showErrorSnackBar(title: 'Error');
  }

  @override
  void onLoading(bool isLoading, String apiEndPoint) {}

  @override
  void onSuccess(object, String apiEndPoint) {
    switch (apiEndPoint) {
      case ApiEndPoints.otpSend:
        userComesFrom = UserComesFrom.signUp;
        Get.toNamed(RoutePaths.OTP_VERIFICATION, arguments: {
          AppArgumentKey.phoneNumber: phoneNumberController.text.toString(),
        });
        break;
    }
  }
}
