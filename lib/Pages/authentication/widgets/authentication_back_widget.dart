import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';

import '../../../common/page_back_button.dart';
import '../../../core/constant/app_color.dart';
import '../../../core/constant/widget_functions.dart';

class AuthenticationBackWidget extends StatelessWidget {
  final String backIconText;
  final Widget bodyWidget;

  const AuthenticationBackWidget({
    super.key,
    required this.backIconText,
    required this.bodyWidget,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          context.isDesktop ? AppColors.colorBackground : AppColors.colorWhite,
      body: Stack(
        children: [
          Positioned(
            left: context.isDesktop ? context.screenWidth * 0.19 : 0,
            right: context.isDesktop ? context.screenWidth * 0.19 : 0,
            bottom: context.isDesktop ? context.screenWidth * 0.055 : 0,
            top: context.isDesktop ? context.screenWidth * 0.055 : 0,
            child: Container(
              decoration: BoxDecoration(
                color: AppColors.colorWhite,
                border: LRTB_Border(0.5, 0.5, 0.5, 0.5, AppColors.borderColor),
              ),
              child: Center(
                child: bodyWidget,
              ),
            ),
          ),
          PageBackButton(
            buttonTitle: backIconText,
          ),
        ],
      ),
    );
  }
}
