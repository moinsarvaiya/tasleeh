import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PaginationHelper {
  final ScrollController scrollController = ScrollController();
  late Function(bool isOngoing, int page, int limit, bool refresh) loadData;
  bool isLastPage = false;
  RxBool isLoading = false.obs;
  int pageNumber = 1;
  int limit;

  PaginationHelper({
    required this.loadData,
    required this.limit,
  }) {
    init();
  }

  void init() {
    scrollController.addListener(() {
      if (scrollController.position.pixels == scrollController.position.maxScrollExtent && !isLastPage && !isLoading.value) {
        isLoading.value = true;
        loadData(false, pageNumber, limit, false);
      }
    });
  }

  void refresh() {
    if (!isLoading.value) {
      pageNumber = 1;
      isLastPage = false;
      loadData(false, pageNumber, limit, true);
    }
  }

  void dispose() {
    scrollController.dispose();
  }
}
