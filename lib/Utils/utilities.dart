import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';

import '../core/constant/app_color.dart';
import '../core/constant/app_images.dart';
import '../core/constant/ui_constants.dart';
import '../l10n/app_string_key.dart';

// Useful utilies of the app
class Utilities {
  static Future<bool> isConnectedNetwork() async {
    try {
      final result = await http.get(Uri.parse('www.google.com'));
      if (result.statusCode == 200) {
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      return false;
    }
  }

  static Color fromHex(String hexString) {
    final StringBuffer buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  static TimeOfDay stringToTimeOfDay(String tod) {
    final cleanedTimeString =
        tod.replaceAll('\u202F', ''); // Remove non-breaking space character

    int hour = int.parse(cleanedTimeString.split(':')[0]);
    final minute = cleanedTimeString.split(':').length > 1
        ? int.parse(cleanedTimeString.split(':')[1].substring(0, 2))
        : 0;
    final isAM = cleanedTimeString
            .substring(cleanedTimeString.length - 2)
            .toLowerCase() ==
        'am';

    if (!isAM && hour != 12) {
      hour += 12;
    } else if (isAM && hour == 12) {
      hour = 0;
    }

    return TimeOfDay(hour: hour, minute: minute);
  }

  static TimeOfDay stringToTimeOfDayFor24Hours(String tod) {
    final cleanedTimeString =
        tod.replaceAll('\u202F', ''); // Remove non-breaking space character

    int hour = int.parse(cleanedTimeString.split(':')[0]);
    final minute = cleanedTimeString.split(':').length > 1
        ? int.parse(cleanedTimeString.split(':')[1].substring(0, 2))
        : 0;
    return TimeOfDay(hour: hour, minute: minute);
  }

  static String timeOfDayToString(TimeOfDay timeOfDay) {
    return "${timeOfDay.hour.toString().padLeft(2, '0')}:${timeOfDay.minute.toString().padLeft(2, '0')}";
  }

  static String formatTimeOfDay(TimeOfDay time, String format) {
    DateTime now = DateTime.now();
    DateTime dateTime =
        DateTime(now.year, now.month, now.day, time.hour, time.minute);
    DateFormat formatter = DateFormat(format);
    return formatter.format(dateTime);
  }

  static showErrorMessage(String message, [String title = "Error"]) {
    Get.rawSnackbar(
      title: title,
      message: message,
      backgroundColor: AppColors.dividerColor,
    );
  }

  static showMessage(String message, [String title = "Success"]) {
    Get.rawSnackbar(title: title.isEmpty ? null : title, message: message);
  }

  /*static Future<File?> pickImage(ImageSource imageSource) async {
    final XFile? image = await ImagePicker().pickImage(source: imageSource);
    if (image != null) {
      return File(image.path);
    }
    return null;
  }*/

  static String greeting() {
    var hour = DateTime.now().hour;
    if (hour < 12) {
      return 'Morning';
    }
    if (hour < 17) {
      return 'Afternoon';
    }
    return 'Evening';
  }

  static String getDay() {
    return DateFormat('EEEE').format(DateTime.now());
  }

  static String getTodayDate() {
    return DateFormat('dd MMM, yyyy').format(DateTime.now());
  }

  // A helper function to get the correct suffix for the day
  static String getSuffix(int day) {
    if (day >= 11 && day <= 13) {
      return 'th';
    }
    switch (day % 10) {
      case 1:
        return 'st';
      case 2:
        return 'nd';
      case 3:
        return 'rd';
      default:
        return 'th';
    }
  }

  // static String getDateFormat(String format) => switch (format) {
  //       "DD MMMM YYYY" => 'dd MMMM y',
  //       "DD MMM YYYY" => 'dd MMM y',
  //       "DD-MM-YY" => 'dd-MM-yy',
  //       "YYYY-MM-DD" => 'yyyy-MM-dd',
  //       "YYYY MMMM DD" => 'yyyy MMMM dd',
  //       "YYYY MMM DD" => 'yyyy MMM dd',
  //       "MMMM DD YYYY" => 'MMMM d y',
  //       _ => 'MMMM d y',
  //     };

  static bool is24HourFormat(String time) {
    return !time.contains('a');
  }

  static String formatHHMMSSTimer(int seconds) {
    int hours = (seconds / 3600).truncate();
    seconds = (seconds % 3600).truncate();
    int minutes = (seconds / 60).truncate();

    String hoursStr = (hours).toString().padLeft(2, '0');
    String minutesStr = (minutes).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');

    if (hours == 0) {
      return "$minutesStr:$secondsStr";
    }

    return "$hoursStr:$minutesStr:$secondsStr";
  }

  static addTextToClipBoard(BuildContext context, String text) {
    Clipboard.setData(ClipboardData(text: text)).then(
      (_) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(AppStringKey.copied_to_clipboard_success_message.tr),
          ),
        );
      },
    );
  }

  static Future<String> getAppVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.version;
  }

  static void removeFocus() {
    FocusManager.instance.primaryFocus?.unfocus();
  }

  static Widget getPlaceHolder({double? height, double? width}) {
    return Container(
      color: Colors.white,
      child: Image.asset(
        AppImages.appTopIcon,
        width: width,
        height: height,
        fit: BoxFit.cover,
      ),
    );
  }

  static String availableDayFormat_yyyyMMdd(String inputDate) {
    final inputDateFormat = DateFormat("E, yyyy-MM-dd");
    final parsedDate = inputDateFormat.parse(inputDate);
    final outputDateFormat = DateFormat("yyyy-MM-dd");
    return outputDateFormat.format(parsedDate);
  }

  static String formatDuration(Duration duration) {
    final hours = duration.inHours;
    final minutes = duration.inMinutes.remainder(60);
    return '$hours:${minutes.toString().padLeft(2, '0')}';
  }

  static String getDifferenceTime(String timeRange) {
    final timeFormat = DateFormat("h:mm a");
    final times = timeRange.split(" - ");
    if (times.length != 2) {
      throw Exception("Invalid time range format");
    }
    final startTime = timeFormat.parse(times[0]);
    final endTime = timeFormat.parse(times[1]);
    final difference = endTime.difference(startTime);

    if (difference.inSeconds > 60) {
      return "${formatDuration(difference)} Min";
    } else {
      return "${difference.inSeconds} Second";
    }
  }

  static String reviewScreenDateFormat_ddMMMyyyy(String inputDate) {
    final inputDateFormat = DateFormat("E, yyyy-MM-dd");
    final parsedDate = inputDateFormat.parse(inputDate);
    final outputDateFormat = DateFormat("dd MMM yyyy");
    return outputDateFormat.format(parsedDate);
  }

  static Debouncer? _debouncer;

  static void customDebounce(int seconds, Function callBack) {
    if (_debouncer != null) {
      // There's an existing debouncer running, cancel it
      _debouncer!.cancel();
    } else {
      // No existing debouncer, create a new one
      _debouncer = Debouncer(delay: Duration(seconds: seconds));
    }

    _debouncer!.call(() {
      callBack();
      _debouncer = null; // Reset debouncer after callback execution
    });
  }

  static void openNewUrl(String url) {
    launchUrl(
      Uri.parse(url),
    );
  }

  static void showToast(
    String message, {
    ToastGravity gravity = ToastGravity.BOTTOM,
    String webPosition = 'center',
    String webBgColor = '#FF4D4F',
    bool webShowClose = true,
    double fontSize = 16,
    int timeInSecForIosWeb = toastVisibleTime,
    Color textColor = AppColors.colorWhite,
    Toast toastLength = Toast.LENGTH_SHORT,
  }) {
    Fluttertoast.showToast(
      msg: message,
      gravity: gravity,
      webPosition: webPosition,
      webBgColor: webBgColor,
      webShowClose: webShowClose,
      fontSize: fontSize,
      timeInSecForIosWeb: timeInSecForIosWeb,
      textColor: textColor,
      toastLength: toastLength,
    );
  }

  static void showNoInternetDialog(BuildContext context, Function onRetry) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            title: Text(AppStringKey.no_internet_connection.tr),
            content: Text(AppStringKey.no_internet_connection_desc.tr),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(AppStringKey.cancel.tr),
              ),
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  onRetry();
                },
                child: Text(AppStringKey.retry.tr),
              ),
            ],
          ),
        );
      },
    );
  }
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildOverscrollIndicator(
      BuildContext context, Widget child, ScrollableDetails details) {
    return child;
  }
}

class Debouncer {
  final Duration delay;
  Timer? _timer;

  Debouncer({required this.delay});

  void call(VoidCallback action) {
    _timer?.cancel();
    _timer = Timer(delay, action);
  }

  void cancel() {
    _timer?.cancel();
  }
}

Future<String> getFCMToken() async {
  String fcmToken = '';
  if (notificationsEnabled) {
    fcmToken = await FirebaseMessaging.instance.getToken() ?? '';
  }
  return fcmToken;
}

Future<String> getFromEnv(String key) async {
  await dotenv.load(fileName: 'config.env'); // Load the dotenv file
  String? value = dotenv.env[key];
  return value!;
}
