class AppConstants {
  static const String appName = 'Tasleeh';
  static const String appVersion = '1.0';

  static const int mobileNumberLength = 8;
  static const int maxMobileNumberLength = 8;
  static const int minMobileNumberLength = 3;
  static const int otpTimer = 60;
  static const int otpLength = 6;
  static const String countryCode = "974";
  static const String userType = "manager";
  static const String currencyString = "QAR";
  static const String callSupportNumber = "+973-1122341231";
  static const String emailSupport = "dummyid12@gmail.com";

  // Max file upload size
  static const double fileMaxSize = 10 * 1024 * 1024;
  static const List<String> supportedFileType = ['.png', '.jpg', '.mp4'];
  static const int maxNumberOfGarageImages = 10;
}
