import 'package:flutter/material.dart';

class LanguageProvider extends ChangeNotifier {
  Locale _appLocale = const Locale('en');

  Locale get appLocale => _appLocale;

  void changeLanguage(Locale locale) {
    if (_appLocale != locale) {
      _appLocale = locale;
      notifyListeners();
    }
  }
}
