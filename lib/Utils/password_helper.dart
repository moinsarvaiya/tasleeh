class PasswordStrengthChecks {
  static bool hasMinimumLength(String password) {
    return password.length >= 8;
  }

  static bool hasUppercase(String password) {
    return password.contains(RegExp(r'[A-Z]'));
  }

  static bool hasLowercase(String password) {
    return password.contains(RegExp(r'[a-z]'));
  }

  static bool hasSpecialCharacter(String password) {
    return password.contains(RegExp(r'[!@#$%^&*()]'));
  }

  static bool hasNumber(String password) {
    return password.contains(RegExp(r'[0-9]'));
  }

  static bool isPasswordValid(String password) {
    return hasMinimumLength(password) &&
        hasUppercase(password) &&
        hasLowercase(password) &&
        hasSpecialCharacter(password) &&
        hasNumber(password);
  }
}
