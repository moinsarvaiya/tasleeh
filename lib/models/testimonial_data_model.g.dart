// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'testimonial_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TestimonialDataModel _$TestimonialDataModelFromJson(
        Map<String, dynamic> json) =>
    TestimonialDataModel(
      garage: json['garage'] == null
          ? null
          : Garage.fromJson(json['garage'] as Map<String, dynamic>),
      name: json['name'] as String?,
      comment: json['comment'] as String?,
      clientImageUrl: json['clientImageUrl'] as String?,
    );

Map<String, dynamic> _$TestimonialDataModelToJson(
        TestimonialDataModel instance) =>
    <String, dynamic>{
      'garage': instance.garage,
      'name': instance.name,
      'comment': instance.comment,
      'clientImageUrl': instance.clientImageUrl,
    };

TestimonialDataProvider _$TestimonialDataProviderFromJson(
        Map<String, dynamic> json) =>
    TestimonialDataProvider(
      id: json['id'] as String?,
    );

Map<String, dynamic> _$TestimonialDataProviderToJson(
        TestimonialDataProvider instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
