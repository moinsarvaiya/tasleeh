// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'active_order_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActiveOrderDataModel _$ActiveOrderDataModelFromJson(
        Map<String, dynamic> json) =>
    ActiveOrderDataModel(
      name: json['name'] as String?,
      bookingId: json['bookingId'] as String?,
      bookingDate: json['bookingDate'] as String?,
      orderDate: json['orderDate'] as String?,
      number: json['number'] as String?,
      make: json['make'] as String?,
      model: json['model'] as String?,
      services: json['services'] as List<dynamic>?,
      distance: (json['distance'] as num?)?.toDouble(),
      pickupAddress: json['pickupAddress'] as String?,
      timeSlot: json['timeSlot'] as String?,
      pickupDate: json['pickupDate'] as String?,
      bookingStatus: json['bookingStatus'] as String?,
      warrantyStatus: json['warrantyStatus'] as String?,
      isReturnedOrder: json['isReturnedOrder'] as bool?,
    );

Map<String, dynamic> _$ActiveOrderDataModelToJson(
        ActiveOrderDataModel instance) =>
    <String, dynamic>{
      'name': instance.name,
      'bookingId': instance.bookingId,
      'bookingDate': instance.bookingDate,
      'orderDate': instance.orderDate,
      'number': instance.number,
      'make': instance.make,
      'model': instance.model,
      'services': instance.services,
      'distance': instance.distance,
      'pickupAddress': instance.pickupAddress,
      'timeSlot': instance.timeSlot,
      'pickupDate': instance.pickupDate,
      'bookingStatus': instance.bookingStatus,
      'warrantyStatus': instance.warrantyStatus,
      'isReturnedOrder': instance.isReturnedOrder,
    };

ActiveOrderDataProvider _$ActiveOrderDataProviderFromJson(
        Map<String, dynamic> json) =>
    ActiveOrderDataProvider(
      id: json['id'] as String?,
    );

Map<String, dynamic> _$ActiveOrderDataProviderToJson(
        ActiveOrderDataProvider instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
