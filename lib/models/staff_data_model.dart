import 'package:json_annotation/json_annotation.dart';

import 'garage_id_model.dart';

part 'staff_data_model.g.dart';

@JsonSerializable()
class StaffDataModel {
  Garage? garage;
  String? firstName;
  String? lastName;
  String? phone;
  String? countryCode;
  String? email;
  String? password;
  String? userType;
  String? employeeType;

  StaffDataModel({
    this.garage,
    this.firstName,
    this.lastName,
    this.phone,
    this.countryCode,
    this.email,
    this.password,
    this.userType,
    this.employeeType,
  });

  factory StaffDataModel.fromJson(Map<String, dynamic> json) =>
      _$StaffDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$StaffDataModelToJson(this);
}

@JsonSerializable()
class StaffDataProvider {
  String? id;

  StaffDataProvider({this.id});

  factory StaffDataProvider.fromJson(Map<String, dynamic> json) =>
      _$StaffDataProviderFromJson(json);

  Map<String, dynamic> toJson() => _$StaffDataProviderToJson(this);
}
