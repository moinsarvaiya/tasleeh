// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'garage_social_links_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SocialLinksModel _$SocialLinksModelFromJson(Map<String, dynamic> json) =>
    SocialLinksModel(
      garage: json['garage'] == null
          ? null
          : Garage.fromJson(json['garage'] as Map<String, dynamic>),
      type: json['type'] as String?,
      link: json['link'] as String?,
    );

Map<String, dynamic> _$SocialLinksModelToJson(SocialLinksModel instance) =>
    <String, dynamic>{
      'garage': instance.garage,
      'type': instance.type,
      'link': instance.link,
    };

SocialLinksDataProvider _$SocialLinksDataProviderFromJson(
        Map<String, dynamic> json) =>
    SocialLinksDataProvider(
      id: json['id'] as String?,
    );

Map<String, dynamic> _$SocialLinksDataProviderToJson(
        SocialLinksDataProvider instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
