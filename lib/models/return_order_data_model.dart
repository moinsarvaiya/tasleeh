import 'package:json_annotation/json_annotation.dart';

part 'return_order_data_model.g.dart';

@JsonSerializable()
class ReturnOrderDataModel {
  String? customerName;
  String? orderId;
  String? orderTime;
  String? orderDate;
  String? vehicleMake;
  String? vehicleModel;
  String? serviceSelected;
  String? pickUpDropAddress;
  String? timeSlot;
  String? pickUpDropDate;
  String? remarks;
  String? servicePrice;

  ReturnOrderDataModel({
    this.customerName,
    this.orderId,
    this.orderTime,
    this.orderDate,
    this.vehicleMake,
    this.vehicleModel,
    this.serviceSelected,
    this.pickUpDropAddress,
    this.timeSlot,
    this.pickUpDropDate,
    this.remarks,
    this.servicePrice,
  });

  factory ReturnOrderDataModel.fromJson(Map<String, dynamic> json) =>
      _$ReturnOrderDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$ReturnOrderDataModelToJson(this);
}

@JsonSerializable()
class ReturnOrderDataProvider {
  String? id;

  ReturnOrderDataProvider({this.id});

  factory ReturnOrderDataProvider.fromJson(Map<String, dynamic> json) =>
      _$ReturnOrderDataProviderFromJson(json);

  Map<String, dynamic> toJson() => _$ReturnOrderDataProviderToJson(this);
}
