import 'package:json_annotation/json_annotation.dart';

part 'submit_branch_data_model.g.dart';

@JsonSerializable()
class SubmitBranchDataModel {
  String? garageName;
  String? branchName;
  String? email;
  String? countryCode1;
  String? phone1;
  String? countryCode2;
  String? phone2;
  String? lat;
  String? lng;
  String? logo;
  String? commercialRegistration;
  String? tradeLicence;
  String? computerCard;
  String? openingHour;
  String? closingHour;
  String? dateOfEstablishment;
  String? addressLine1;
  String? addressLine2;
  String? buildingNumber;
  String? pickUp;
  String? zip;
  String? mapLocation;
  String? contactPersonFirstName;
  String? contactPersonLastName;
  String? contactPersonEmail;
  String? contactPersonCountryCode;
  String? contactPersonPhone;
  DataProvider? provider;
  DataProvider? city;

  /* String? deletedAt;
  String? id;
  String? createdAt;
  String? updatedAt;*/

  SubmitBranchDataModel({
    this.garageName,
    this.branchName,
    this.email,
    this.countryCode1,
    this.phone1,
    this.countryCode2,
    this.phone2,
    this.lat,
    this.lng,
    this.logo,
    this.commercialRegistration,
    this.tradeLicence,
    this.computerCard,
    this.openingHour,
    this.closingHour,
    this.dateOfEstablishment,
    this.zip,
    this.addressLine1,
    this.addressLine2,
    this.buildingNumber,
    this.pickUp,
    this.mapLocation,
    this.contactPersonFirstName,
    this.contactPersonLastName,
    this.contactPersonEmail,
    this.contactPersonCountryCode,
    this.contactPersonPhone,
    this.provider,
    this.city,
  });

  factory SubmitBranchDataModel.fromJson(Map<String, dynamic> json) =>
      _$SubmitBranchDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$SubmitBranchDataModelToJson(this);
}

@JsonSerializable()
class DataProvider {
  String? id;

  DataProvider({this.id});

  factory DataProvider.fromJson(Map<String, dynamic> json) =>
      _$DataProviderFromJson(json);

  Map<String, dynamic> toJson() => _$DataProviderToJson(this);
}
