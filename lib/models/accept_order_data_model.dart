import 'package:json_annotation/json_annotation.dart';

import 'garage_id_model.dart';

part 'accept_order_data_model.g.dart';

@JsonSerializable()
class AcceptOrderDataModel {
  String? status;
  String? remarks;
  bool? isThirdPartyAssigned;

  AcceptOrderDataModel({
    this.status,
    this.remarks,
    this.isThirdPartyAssigned,
  });
  factory AcceptOrderDataModel.fromJson(Map<String, dynamic> json) =>
      _$AcceptOrderDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$AcceptOrderDataModelToJson(this);
}

@JsonSerializable()
class AcceptOrderDataProvider {
  String? id;

  AcceptOrderDataProvider({this.id});

  factory AcceptOrderDataProvider.fromJson(Map<String, dynamic> json) =>
      _$AcceptOrderDataProviderFromJson(json);

  Map<String, dynamic> toJson() => _$AcceptOrderDataProviderToJson(this);
}
