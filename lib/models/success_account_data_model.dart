import 'package:al_baida_garage_fe/models/garage_id_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'success_account_data_model.g.dart';

@JsonSerializable()
class SuccessAccountModel {
  Garage? garage;
  String? accountHolderName;
  String? accountNumber;
  String? IBAN;
  String? bankName;
  String? swift;
  String? address;
  String? deletedAt;
  String? id;
  String? createdAt;
  String? updatedAt;

  SuccessAccountModel({
    this.garage,
    this.IBAN,
    this.accountHolderName,
    this.bankName,
    this.accountNumber,
    this.address,
    this.swift,
    this.deletedAt,
    this.id,
    this.createdAt,
    this.updatedAt,
  });

  factory SuccessAccountModel.fromJson(Map<String, dynamic> json) =>
      _$SuccessAccountModelFromJson(json);

  Map<String, dynamic> toJson() => _$SuccessAccountModelToJson(this);
}
