import 'package:json_annotation/json_annotation.dart';

part 'comments_model.g.dart';

@JsonSerializable()
class CommentModel {
  String? section;
  String? comments;
  String? createdAt;
  String? updatedAt;
  String? deletedAt;

  CommentModel({
    this.section,
    this.comments,
    this.createdAt,
    this.updatedAt,
    this.deletedAt,
  });

  factory CommentModel.fromJson(Map<String, dynamic> json) =>
      _$CommentModelFromJson(json);

  Map<String, dynamic> toJson() => _$CommentModelToJson(this);
}

