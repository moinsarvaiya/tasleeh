import 'package:al_baida_garage_fe/models/garage_id_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'garage_social_links_model.g.dart';

@JsonSerializable()
class SocialLinksModel {
  Garage? garage;
  String? type;
  String? link;

  SocialLinksModel({
    this.garage,
    this.type,
    this.link,
  });
  factory SocialLinksModel.fromJson(Map<String, dynamic> json) =>
      _$SocialLinksModelFromJson(json);

  Map<String, dynamic> toJson() => _$SocialLinksModelToJson(this);
}

@JsonSerializable()
class SocialLinksDataProvider {
  String? id;

  SocialLinksDataProvider({this.id});

  factory SocialLinksDataProvider.fromJson(Map<String, dynamic> json) =>
      _$SocialLinksDataProviderFromJson(json);

  Map<String, dynamic> toJson() => _$SocialLinksDataProviderToJson(this);
}
