import 'package:al_baida_garage_fe/Pages/basic_details/city_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'success_branch_data_model.g.dart';

@JsonSerializable()
class SuccessBranchDataModel {
  String? garageName;
  String? branchName;
  String? email;
  String? countryCode1;
  String? phone1;
  String? countryCode2;
  String? phone2;
  double? lat;
  double? lng;
  String? logo;
  String? commercialRegistration;
  String? tradeLicence;
  String? computerCard;
  String? openingHour;
  String? closingHour;
  String? dateOfEstablishment;
  String? addressLine1;
  String? addressLine2;
  String? buildingNumber;
  String? pickUp;
  String? zip;
  String? mapLocation;
  String? contactPersonFirstName;
  String? contactPersonLastName;
  String? contactPersonEmail;
  String? contactPersonCountryCode;
  String? contactPersonPhone;
  ResponseDataProvider? provider;
  CityModel? city;
  String? deletedAt;
  String? id;
  String? createdAt;
  String? updatedAt;

  SuccessBranchDataModel(
      {this.garageName,
      this.branchName,
      this.email,
      this.countryCode1,
      this.phone1,
      this.countryCode2,
      this.phone2,
      this.lat,
      this.lng,
      this.logo,
      this.commercialRegistration,
      this.tradeLicence,
      this.computerCard,
      this.openingHour,
      this.closingHour,
      this.dateOfEstablishment,
      this.addressLine1,
      this.addressLine2,
      this.buildingNumber,
      this.pickUp,
      this.zip,
      this.mapLocation,
      this.contactPersonFirstName,
      this.contactPersonLastName,
      this.contactPersonEmail,
      this.contactPersonCountryCode,
      this.contactPersonPhone,
      this.provider,
      this.city,
      this.deletedAt,
      this.id,
      this.createdAt,
      this.updatedAt});

  factory SuccessBranchDataModel.fromJson(Map<String, dynamic> json) =>
      _$SuccessBranchDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$SuccessBranchDataModelToJson(this);
}

@JsonSerializable()
class ResponseDataProvider {
  String? id;

  ResponseDataProvider({this.id});

  factory ResponseDataProvider.fromJson(Map<String, dynamic> json) =>
      _$ResponseDataProviderFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseDataProviderToJson(this);
}
