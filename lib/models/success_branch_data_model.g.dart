// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'success_branch_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SuccessBranchDataModel _$SuccessBranchDataModelFromJson(
        Map<String, dynamic> json) =>
    SuccessBranchDataModel(
      garageName: json['garageName'] as String?,
      branchName: json['branchName'] as String?,
      email: json['email'] as String?,
      countryCode1: json['countryCode1'] as String?,
      phone1: json['phone1'] as String?,
      countryCode2: json['countryCode2'] as String?,
      phone2: json['phone2'] as String?,
      lat: (json['lat'] as num?)?.toDouble(),
      lng: (json['lng'] as num?)?.toDouble(),
      logo: json['logo'] as String?,
      commercialRegistration: json['commercialRegistration'] as String?,
      tradeLicence: json['tradeLicence'] as String?,
      computerCard: json['computerCard'] as String?,
      openingHour: json['openingHour'] as String?,
      closingHour: json['closingHour'] as String?,
      dateOfEstablishment: json['dateOfEstablishment'] as String?,
      addressLine1: json['addressLine1'] as String?,
      addressLine2: json['addressLine2'] as String?,
      buildingNumber: json['buildingNumber'] as String?,
      pickUp: json['pickUp'] as String?,
      zip: json['zip'] as String?,
      mapLocation: json['mapLocation'] as String?,
      contactPersonFirstName: json['contactPersonFirstName'] as String?,
      contactPersonLastName: json['contactPersonLastName'] as String?,
      contactPersonEmail: json['contactPersonEmail'] as String?,
      contactPersonCountryCode: json['contactPersonCountryCode'] as String?,
      contactPersonPhone: json['contactPersonPhone'] as String?,
      provider: json['provider'] == null
          ? null
          : ResponseDataProvider.fromJson(
              json['provider'] as Map<String, dynamic>),
      city: json['city'] == null
          ? null
          : CityModel.fromJson(json['city'] as Map<String, dynamic>),
      deletedAt: json['deletedAt'] as String?,
      id: json['id'] as String?,
      createdAt: json['createdAt'] as String?,
      updatedAt: json['updatedAt'] as String?,
    );

Map<String, dynamic> _$SuccessBranchDataModelToJson(
        SuccessBranchDataModel instance) =>
    <String, dynamic>{
      'garageName': instance.garageName,
      'branchName': instance.branchName,
      'email': instance.email,
      'countryCode1': instance.countryCode1,
      'phone1': instance.phone1,
      'countryCode2': instance.countryCode2,
      'phone2': instance.phone2,
      'lat': instance.lat,
      'lng': instance.lng,
      'logo': instance.logo,
      'commercialRegistration': instance.commercialRegistration,
      'tradeLicence': instance.tradeLicence,
      'computerCard': instance.computerCard,
      'openingHour': instance.openingHour,
      'closingHour': instance.closingHour,
      'dateOfEstablishment': instance.dateOfEstablishment,
      'addressLine1': instance.addressLine1,
      'addressLine2': instance.addressLine2,
      'buildingNumber': instance.buildingNumber,
      'pickUp': instance.pickUp,
      'zip': instance.zip,
      'mapLocation': instance.mapLocation,
      'contactPersonFirstName': instance.contactPersonFirstName,
      'contactPersonLastName': instance.contactPersonLastName,
      'contactPersonEmail': instance.contactPersonEmail,
      'contactPersonCountryCode': instance.contactPersonCountryCode,
      'contactPersonPhone': instance.contactPersonPhone,
      'provider': instance.provider,
      'city': instance.city,
      'deletedAt': instance.deletedAt,
      'id': instance.id,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };

ResponseDataProvider _$ResponseDataProviderFromJson(
        Map<String, dynamic> json) =>
    ResponseDataProvider(
      id: json['id'] as String?,
    );

Map<String, dynamic> _$ResponseDataProviderToJson(
        ResponseDataProvider instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
