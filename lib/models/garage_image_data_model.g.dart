// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'garage_image_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GarageImageModel _$GarageImageModelFromJson(Map<String, dynamic> json) =>
    GarageImageModel(
      garage: json['garage'] == null
          ? null
          : Garage.fromJson(json['garage'] as Map<String, dynamic>),
      imageUrl: json['imageUrl'] as String?,
      isCoverImage: json['isCoverImage'] as bool?,
    );

Map<String, dynamic> _$GarageImageModelToJson(GarageImageModel instance) =>
    <String, dynamic>{
      'garage': instance.garage,
      'imageUrl': instance.imageUrl,
      'isCoverImage': instance.isCoverImage,
    };

GarageImageDataProvider _$GarageImageDataProviderFromJson(
        Map<String, dynamic> json) =>
    GarageImageDataProvider(
      id: json['id'] as String?,
    );

Map<String, dynamic> _$GarageImageDataProviderToJson(
        GarageImageDataProvider instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
