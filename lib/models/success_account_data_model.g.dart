// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'success_account_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SuccessAccountModel _$SuccessAccountModelFromJson(Map<String, dynamic> json) =>
    SuccessAccountModel(
      garage: json['garage'] == null
          ? null
          : Garage.fromJson(json['garage'] as Map<String, dynamic>),
      IBAN: json['IBAN'] as String?,
      accountHolderName: json['accountHolderName'] as String?,
      bankName: json['bankName'] as String?,
      accountNumber: json['accountNumber'] as String?,
      address: json['address'] as String?,
      swift: json['swift'] as String?,
      deletedAt: json['deletedAt'] as String?,
      id: json['id'] as String?,
      createdAt: json['createdAt'] as String?,
      updatedAt: json['updatedAt'] as String?,
    );

Map<String, dynamic> _$SuccessAccountModelToJson(
        SuccessAccountModel instance) =>
    <String, dynamic>{
      'garage': instance.garage,
      'accountHolderName': instance.accountHolderName,
      'accountNumber': instance.accountNumber,
      'IBAN': instance.IBAN,
      'bankName': instance.bankName,
      'swift': instance.swift,
      'address': instance.address,
      'deletedAt': instance.deletedAt,
      'id': instance.id,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };
