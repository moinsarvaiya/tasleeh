import 'package:json_annotation/json_annotation.dart';

part 'garage_id_model.g.dart';

@JsonSerializable()
class Garage {
  String? id;

  Garage({this.id});

  factory Garage.fromJson(Map<String, dynamic> json) => _$GarageFromJson(json);

  Map<String, dynamic> toJson() => _$GarageToJson(this);
}
