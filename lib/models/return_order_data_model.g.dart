// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'return_order_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReturnOrderDataModel _$ReturnOrderDataModelFromJson(
        Map<String, dynamic> json) =>
    ReturnOrderDataModel(
      customerName: json['customerName'] as String?,
      orderId: json['orderId'] as String?,
      orderTime: json['orderTime'] as String?,
      orderDate: json['orderDate'] as String?,
      vehicleMake: json['vehicleMake'] as String?,
      vehicleModel: json['vehicleModel'] as String?,
      serviceSelected: json['serviceSelected'] as String?,
      pickUpDropAddress: json['pickUpDropAddress'] as String?,
      timeSlot: json['timeSlot'] as String?,
      pickUpDropDate: json['pickUpDropDate'] as String?,
      remarks: json['remarks'] as String?,
      servicePrice: json['servicePrice'] as String?,
    );

Map<String, dynamic> _$ReturnOrderDataModelToJson(
        ReturnOrderDataModel instance) =>
    <String, dynamic>{
      'customerName': instance.customerName,
      'orderId': instance.orderId,
      'orderTime': instance.orderTime,
      'orderDate': instance.orderDate,
      'vehicleMake': instance.vehicleMake,
      'vehicleModel': instance.vehicleModel,
      'serviceSelected': instance.serviceSelected,
      'pickUpDropAddress': instance.pickUpDropAddress,
      'timeSlot': instance.timeSlot,
      'pickUpDropDate': instance.pickUpDropDate,
      'remarks': instance.remarks,
      'servicePrice': instance.servicePrice,
    };

ReturnOrderDataProvider _$ReturnOrderDataProviderFromJson(
        Map<String, dynamic> json) =>
    ReturnOrderDataProvider(
      id: json['id'] as String?,
    );

Map<String, dynamic> _$ReturnOrderDataProviderToJson(
        ReturnOrderDataProvider instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
