// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'garage_id_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Garage _$GarageFromJson(Map<String, dynamic> json) => Garage(
      id: json['id'] as String?,
    );

Map<String, dynamic> _$GarageToJson(Garage instance) => <String, dynamic>{
      'id': instance.id,
    };
