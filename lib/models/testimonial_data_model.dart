import 'package:json_annotation/json_annotation.dart';

import 'garage_id_model.dart';

part 'testimonial_data_model.g.dart';

@JsonSerializable()
class TestimonialDataModel {
  Garage? garage;
  String? name;
  String? comment;
  String? clientImageUrl;

  TestimonialDataModel(
      {this.garage, this.name, this.comment, this.clientImageUrl});
  factory TestimonialDataModel.fromJson(Map<String, dynamic> json) =>
      _$TestimonialDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$TestimonialDataModelToJson(this);
}

@JsonSerializable()
class TestimonialDataProvider {
  String? id;

  TestimonialDataProvider({this.id});

  factory TestimonialDataProvider.fromJson(Map<String, dynamic> json) =>
      _$TestimonialDataProviderFromJson(json);

  Map<String, dynamic> toJson() => _$TestimonialDataProviderToJson(this);
}
