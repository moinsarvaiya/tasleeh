import 'package:json_annotation/json_annotation.dart';

part 'active_order_data_model.g.dart';

@JsonSerializable()
class ActiveOrderDataModel {
  String? name;
  String? bookingId;
  String? bookingDate;
  String? orderDate;
  String? number;
  String? make;
  String? model;
  List? services;
  double? distance;
  String? pickupAddress;
  String? timeSlot;
  String? pickupDate;
  String? bookingStatus;
  String? warrantyStatus;
  bool? isReturnedOrder;

  ActiveOrderDataModel({
    this.name,
    this.bookingId,
    this.bookingDate,
    this.orderDate,
    this.number,
    this.make,
    this.model,
    this.services,
    this.distance,
    this.pickupAddress,
    this.timeSlot,
    this.pickupDate,
    this.bookingStatus,
    this.warrantyStatus,
    this.isReturnedOrder,
  });

  factory ActiveOrderDataModel.fromJson(Map<String, dynamic> json) =>
      _$ActiveOrderDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$ActiveOrderDataModelToJson(this);
}

@JsonSerializable()
class ActiveOrderDataProvider {
  String? id;

  ActiveOrderDataProvider({this.id});

  factory ActiveOrderDataProvider.fromJson(Map<String, dynamic> json) =>
      _$ActiveOrderDataProviderFromJson(json);

  Map<String, dynamic> toJson() => _$ActiveOrderDataProviderToJson(this);
}
