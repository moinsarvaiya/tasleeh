import 'package:json_annotation/json_annotation.dart';

import 'garage_id_model.dart';

part 'garage_image_data_model.g.dart';

@JsonSerializable()
class GarageImageModel {
  Garage? garage;
  String? imageUrl;
  bool? isCoverImage;

  GarageImageModel({
    this.garage,
    this.imageUrl,
    this.isCoverImage,
  });
  factory GarageImageModel.fromJson(Map<String, dynamic> json) =>
      _$GarageImageModelFromJson(json);

  Map<String, dynamic> toJson() => _$GarageImageModelToJson(this);
}

@JsonSerializable()
class GarageImageDataProvider {
  String? id;

  GarageImageDataProvider({this.id});

  factory GarageImageDataProvider.fromJson(Map<String, dynamic> json) =>
      _$GarageImageDataProviderFromJson(json);

  Map<String, dynamic> toJson() => _$GarageImageDataProviderToJson(this);
}
