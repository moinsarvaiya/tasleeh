// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'staff_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StaffDataModel _$StaffDataModelFromJson(Map<String, dynamic> json) =>
    StaffDataModel(
      garage: json['garage'] == null
          ? null
          : Garage.fromJson(json['garage'] as Map<String, dynamic>),
      firstName: json['firstName'] as String?,
      lastName: json['lastName'] as String?,
      phone: json['phone'] as String?,
      countryCode: json['countryCode'] as String?,
      email: json['email'] as String?,
      password: json['password'] as String?,
      userType: json['userType'] as String?,
      employeeType: json['employeeType'] as String?,
    );

Map<String, dynamic> _$StaffDataModelToJson(StaffDataModel instance) =>
    <String, dynamic>{
      'garage': instance.garage,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'phone': instance.phone,
      'countryCode': instance.countryCode,
      'email': instance.email,
      'password': instance.password,
      'userType': instance.userType,
      'employeeType': instance.employeeType,
    };

StaffDataProvider _$StaffDataProviderFromJson(Map<String, dynamic> json) =>
    StaffDataProvider(
      id: json['id'] as String?,
    );

Map<String, dynamic> _$StaffDataProviderToJson(StaffDataProvider instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
