// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_detail_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountDetailDataModel _$AccountDetailDataModelFromJson(
        Map<String, dynamic> json) =>
    AccountDetailDataModel(
      garage: json['garage'] == null
          ? null
          : Garage.fromJson(json['garage'] as Map<String, dynamic>),
      accountHolderName: json['accountHolderName'] as String?,
      accountNumber: json['accountNumber'] as String?,
      confirmAccountNumber: json['confirmAccountNumber'] as String?,
      IBAN: json['IBAN'] as String?,
      bankName: json['bankName'] as String?,
      address: json['address'] as String?,
      swift: json['swift'] as String?,
    );

Map<String, dynamic> _$AccountDetailDataModelToJson(
        AccountDetailDataModel instance) =>
    <String, dynamic>{
      'garage': instance.garage,
      'accountHolderName': instance.accountHolderName,
      'accountNumber': instance.accountNumber,
      'confirmAccountNumber': instance.confirmAccountNumber,
      'IBAN': instance.IBAN,
      'bankName': instance.bankName,
      'swift': instance.swift,
      'address': instance.address,
    };

AccountDetailDataProvider _$AccountDetailDataProviderFromJson(
        Map<String, dynamic> json) =>
    AccountDetailDataProvider(
      id: json['id'] as String?,
    );

Map<String, dynamic> _$AccountDetailDataProviderToJson(
        AccountDetailDataProvider instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
