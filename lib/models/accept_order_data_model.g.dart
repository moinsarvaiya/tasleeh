// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'accept_order_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AcceptOrderDataModel _$AcceptOrderDataModelFromJson(
        Map<String, dynamic> json) =>
    AcceptOrderDataModel(
      status: json['status'] as String?,
      remarks: json['remarks'] as String?,
      isThirdPartyAssigned: json['isThirdPartyAssigned'] as bool?,
    );

Map<String, dynamic> _$AcceptOrderDataModelToJson(
        AcceptOrderDataModel instance) =>
    <String, dynamic>{
      'status': instance.status,
      'remarks': instance.remarks,
      'isThirdPartyAssigned': instance.isThirdPartyAssigned,
    };

AcceptOrderDataProvider _$AcceptOrderDataProviderFromJson(
        Map<String, dynamic> json) =>
    AcceptOrderDataProvider(
      id: json['id'] as String?,
    );

Map<String, dynamic> _$AcceptOrderDataProviderToJson(
        AcceptOrderDataProvider instance) =>
    <String, dynamic>{
      'id': instance.id,
    };
