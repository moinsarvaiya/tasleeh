class BranchModel {
  String? branchId;
  String? branchName;

  BranchModel({this.branchId, this.branchName});
}
