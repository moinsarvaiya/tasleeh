import 'package:json_annotation/json_annotation.dart';

import 'garage_id_model.dart';

part 'account_detail_data_model.g.dart';

@JsonSerializable()
class AccountDetailDataModel {
  Garage? garage;
  String? accountHolderName;
  String? accountNumber;
  String? confirmAccountNumber;
  String? IBAN;
  String? bankName;
  String? swift;
  String? address;

  AccountDetailDataModel({
    this.garage,
    this.accountHolderName,
    this.accountNumber,
    this.confirmAccountNumber,
    this.IBAN,
    this.bankName,
    this.address,
    this.swift,
  });

  factory AccountDetailDataModel.fromJson(Map<String, dynamic> json) =>
      _$AccountDetailDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$AccountDetailDataModelToJson(this);
}

@JsonSerializable()
class AccountDetailDataProvider {
  String? id;

  AccountDetailDataProvider({this.id});

  factory AccountDetailDataProvider.fromJson(Map<String, dynamic> json) =>
      _$AccountDetailDataProviderFromJson(json);

  Map<String, dynamic> toJson() => _$AccountDetailDataProviderToJson(this);
}
