import 'package:flutter/material.dart';

class SupportedLanguages {
  static List<Locale> supportedLanguages = [
    const Locale('en'),
    const Locale('ar'),
  ];
}
