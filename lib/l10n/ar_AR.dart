import 'app_string_key.dart';

Map<String, String> ar_AR = {
  AppStringKey.above_data_from_the_past:
      'البيانات المذكورة أعلاه هي من آخر 30 يومًا.',
  AppStringKey.accept: 'يقبل',
  AppStringKey.accepted: 'قبلت',
  AppStringKey.account_detail: 'تفاصيل الحساب',
  AppStringKey.account_holder_name: 'اسم صاحب الحساب',
  AppStringKey.account_no: 'رقم الحساب',
  AppStringKey.accounts: 'الحسابات',
  AppStringKey.actions: 'إجراءات',
  AppStringKey.active_order_detail: 'تفاصيل الطلب النشط',
  AppStringKey.active_order_list: 'قائمة الطلبات النشطة',
  AppStringKey.add: 'إضافة',
  AppStringKey.add_another_bank_account: 'إضافة حساب مصرفي آخر',
  AppStringKey.add_another_service: 'أضف خدمة أخرى',
  AppStringKey.add_another_staff: 'إضافة موظف آخر',
  AppStringKey.add_another_testimonial: 'أضف شهادة أخرى',
  AppStringKey.add_bank_account: 'إضافة حساب بنكي',
  AppStringKey.add_branch: 'إضافة فرع',
  AppStringKey.add_category: 'إضافة فئة',
  AppStringKey.add_customer_testimonials: 'إضافة شهادات العملاء',
  AppStringKey.add_images: 'إضافة الصور',
  AppStringKey.add_levels: 'أضف المستوى',
  AppStringKey.add_levels_desc:
      'إضافة مستويات إلى فئة الخدمات الفرعية للحزم المجدولة',
  AppStringKey.add_service: 'أضف الخدمات',
  AppStringKey.add_staff: 'أضف فريق العمل',
  AppStringKey.add_sub_category: 'أضف فئة فرعية',
  AppStringKey.add_video_photos: 'إضافة الصور ومقاطع الفيديو',
  AppStringKey.add_video_photos_desc:
      'أضف الصور ومقاطع الفيديو لإظهار شكل مركز الخدمة الخاص بك. يمكنك تحميل ما يصل إلى 10 صور / مقاطع فيديو.',
  AppStringKey.added_dialog_bottom_desc:
      'ملحوظة - تفرض البدع رسومًا قدرها 5% على كل خدمة',
  AppStringKey.added_levels: 'المستويات المضافة',
  AppStringKey.address_line: 'سطر العنوان',
  AppStringKey.already_have_an_account: 'هل لديك حساب بالفعل ؟',
  AppStringKey.amount_received: 'المبلغ المُستلم',
  AppStringKey.api_fail_error: 'حدث خطأ. الرجاء معاودة المحاولة في وقت لاحق.',
  AppStringKey.application_review: 'طلبك لا يزال قيد المراجعة!',
  AppStringKey.application_review_description:
      'سنقوم بإخطارك بمجرد الانتهاء من ذلك عبر رقم هاتفك المسجل ومعرف البريد الإلكتروني.',
  AppStringKey.apply_filter: 'تطبيق المرشحات',
  AppStringKey.are_you_sure_cancel_return_order:
      'هل أنت متأكد أنك تريد إلغاء استلام أمر التقديم؟ سيكلف ذلك 140 ريال قطري مقابل خدمة في غير محلها من طرف ثالث.',
  AppStringKey.are_you_sure_to_delete: 'هل أنت متأكد؟ تريد إجراء عملية الحذف',
  AppStringKey.at: 'في',
  AppStringKey.back: 'رجوع',
  AppStringKey.back_to_dashboard: 'العودة إلى لوحة التحكم',
  AppStringKey.back_to_login: 'العودة لتسجيل الدخول',
  AppStringKey.bank_address: 'عنوان البنك',
  AppStringKey.bank_name: 'اسم البنك',
  AppStringKey.basic: 'الحسابات الأساسية',
  AppStringKey.basic_details: 'تفاصيل أساسية',
  AppStringKey.below_contains_customer_detail_info:
      'يحتوي أدناه على معلومات مفصلة للعميل. قم بمراجعة ذلك على لوحة القيادة الرئيسية.',
  AppStringKey.below_contains_service_detail_info:
      'يحتوي أدناه على معلومات مفصلة عن الخدمة. قم بمراجعة ذلك على لوحة القيادة الرئيسية.',
  AppStringKey.below_contains_vehicle_detail_info:
      'يحتوي أدناه معلومات مفصلة عن السيارة. قم بمراجعة ذلك على لوحة القيادة الرئيسية.',
  AppStringKey.between: 'بين',
  AppStringKey.booking_id: 'معرف الحجز',
  AppStringKey.branch_handler: 'معالج الفروع',
  AppStringKey.branch_handler_desc:
      'من خلال النقر على أي عنصر أدناه يمكنك تغيير الفرع',
  AppStringKey.branch_management: 'إدارة الفروع',
  AppStringKey.branch_staff: 'فريق عمل الفرع',
  AppStringKey.branch_text: 'الفرع',
  AppStringKey.building_number: 'رقم المبنى',
  AppStringKey.call_support: 'اتصل بالدعم',
  AppStringKey.cancel: 'إلغاء',
  AppStringKey.cancel_pick_for_requested_order: 'إلغاء الاستلام للطلب',
  AppStringKey.cancel_pickup_for_return_order: 'إلغاء الاستلام لطلب التقديم',
  AppStringKey.cancelled: 'ألغيت',
  AppStringKey.change: 'تغيير',
  AppStringKey.chat_support: 'شارك استفساراتك مع دعم الدردشة لدينا',
  AppStringKey.chat_support_description:
      'إذا كنت تشعر أن مشكلتك تتطلب مزيدًا من الاهتمام، فيمكنك طلب التصعيد إلى مستوى دعم أعلى. لمساعدتك بشكل أفضل، يرجى تقديم المزيد من التفاصيل حول مشكلتك',
  AppStringKey.chat_with_us: 'دردش معنا',
  AppStringKey.city: 'المدينة',
  AppStringKey.click_to_drag_file_title:
      'انقر أو اسحب الملفات إلى هذه المنطقة للتحميل',
  AppStringKey.click_to_drag_formats_desc:
      'التنسيقات المقبولة هي .png, .jpg و.mp4. الحد الأقصى لحجم التحميل هو 10 ميغابايت.',
  AppStringKey.click_to_upload: 'انقر للرفع',
  AppStringKey.client_name: 'اسم العميل',
  AppStringKey.client_photo_or_video: 'صورة العميل أو الفيديو',
  AppStringKey.close: 'إغلاق',
  AppStringKey.commercial_registration: 'السجل التجاري',
  AppStringKey.completed_order_filter_desc:
      'قم بتصفية قائمة الطلبات المكتملة عن طريق تحديد مرشح واحد أو عدة مرشحات',
  AppStringKey.completed_order_list_title: 'قائمة الطلبات المكتملة',
  AppStringKey.computer_card: 'بطاقة حاسوب',
  AppStringKey.confirm: 'يتأكد',
  AppStringKey.confirm_location: 'تأكيد الموقع',
  AppStringKey.contact_number: 'رقم الاتصال',
  AppStringKey.contact_person_description:
      'تفاصيل للعميل للاتصال بالجراج الخاص بك',
  AppStringKey.contact_person_details: 'تفاصيل جهة الاتصال',
  AppStringKey.continue_ahead: 'أستمر',
  AppStringKey.continue_with: 'أو تابع بـ',
  AppStringKey.copied_to_clipboard_success_message:
      'نسخ إلى الحافظة الخاصة بك!',
  AppStringKey.cover_image: 'صورة الغلاف',
  AppStringKey.create_pass: 'إنشاء كلمة سر',
  AppStringKey.create_password: 'إنشاء كلمة مرور',
  AppStringKey.create_password_button_content: 'لنبدأ',
  AppStringKey.create_password_message:
      'الآن لنبدأ في إعداد ملفك الشخصي. لن يستغرق هذا الأمر 10 دقائق، وليست جميع الخطوات إلزامية.',
  AppStringKey.create_password_title: 'تم إنشاء حسابك بنجاح!',
  AppStringKey.currently_no_data_found: 'حاليًا لم يتم العثور على بيانات',
  AppStringKey.customer_info: 'معلومات العملاء',
  AppStringKey.customer_testimonials: 'شهادات العملاء',
  AppStringKey.dashboard_active_order: 'الطلبات النشطة',
  AppStringKey.dashboard_completed_order: 'الطلبات المكتملة',
  AppStringKey.dashboard_help: 'المساعدة',
  AppStringKey.dashboard_my_account: 'الحسابات الخاصة بي',
  AppStringKey.dashboard_order_list: 'لائحة الطلبات',
  AppStringKey.dashboard_payments: 'المدفوعات',
  AppStringKey.dashboard_request_order: 'الطلب المطلوب',
  AppStringKey.dashboard_return_order: 'أمر التقديم',
  AppStringKey.date_of_establishment: 'تاريخ التأسيس:',
  AppStringKey.decline_return_service_for_order: 'رفض خدمة التقديم للطلب',
  AppStringKey.decline_service_order: 'رفض الخدمة للطلب',
  AppStringKey.delete: 'حذف',
  AppStringKey.delivered: 'تم التوصيل',
  AppStringKey.do_not_have_account: 'ليس لديك حساب ؟',
  AppStringKey.driver: 'السائق',
  AppStringKey.edit: 'تعديل',
  AppStringKey.edit_bank_details: 'تحرير تفاصيل البنك',
  AppStringKey.edit_details: 'تعديل التفاصيل',
  AppStringKey.edit_order: 'تعديل التفاصيل',
  AppStringKey.edit_services_details: 'تعديل تفاصيل الخدمة',
  AppStringKey.email: 'معرف البريد الإلكتروني',
  AppStringKey.email_support: 'دعم البريد الإلكتروني',
  AppStringKey.email_support_description:
      'إذا كنت تشعر أن مشكلتك تتطلب مزيدًا من الاهتمام، فاطلب التصعيد إلى مستوى دعم أعلى. تواصل معنا على',
  AppStringKey.end_date: 'حدد وقت الانتهاء',
  AppStringKey.end_operation_time: 'حدد وقت الانتهاء',
  AppStringKey.english: 'اللغة الإنجليزية',
  AppStringKey.enter_account_holder_name: 'أدخل اسم صاحب الحساب',
  AppStringKey.enter_account_no: 'أدخل رقم الحساب',
  AppStringKey.enter_address_line: 'أدخل سطر العنوان',
  AppStringKey.enter_amount: 'أدخل السعر (ريال قطري)',
  AppStringKey.enter_bank_address: 'أدخل عنوان البنك',
  AppStringKey.enter_bank_name: 'أدخل اسم البنك',
  AppStringKey.enter_building_number: 'أدخل رقم المبنى',
  AppStringKey.enter_client_name: 'أدخل اسم العميل',
  AppStringKey.enter_discount_amount: 'أدخل سعر الخصم (ريال قطري)',
  AppStringKey.enter_email: 'أدخل معرف البريد الإلكتروني',
  AppStringKey.enter_facebook_handle: 'أدخل رابط استخدام الفيسبوك الخاص بك',
  AppStringKey.enter_feedback: 'أدخل التعليقات',
  AppStringKey.enter_first_name: 'أدخل الاسم الأول',
  AppStringKey.enter_garage_name: 'أدخل اسم الجراج',
  AppStringKey.enter_IBAN_code: 'أدخل رمز الحساب المصرفي الدولي',
  AppStringKey.enter_instagram_handle: 'أدخل رابط استخدام انستجرام الخاص بك',
  AppStringKey.enter_last_name: 'أدخل اسم العائلة',
  AppStringKey.enter_level_desc: 'وصف المستوى',
  AppStringKey.enter_level_name: 'الرجاء إدخال اسم المستوى',
  AppStringKey.enter_manually: 'الإدخال يدويًا',
  AppStringKey.enter_max_number_of_pickups:
      'أدخل الحد الأقصى لعدد الاستلامات خلال 30 دقيقة',
  AppStringKey.enter_mobile_number: 'أدخل رقم الهاتف',
  AppStringKey.enter_password: 'أدخل كلمة المرور',
  AppStringKey.enter_phone_no: 'أدخل رقم الهاتف',
  AppStringKey.enter_reason: 'أدخل السبب',
  AppStringKey.enter_six_digit_code:
      'يرجى إدخال الرمز المكون من 6 أرقام المرسل إلى+',
  AppStringKey.enter_staff_type: 'أدخل نوع فريق العمل',
  AppStringKey.enter_swift_code: 'أدخل رمز سويفت',
  AppStringKey.enter_tik_tok_handle: 'أدخل رابط استخدام تيك توك الخاص بك',
  AppStringKey.enter_twitter_handle: 'أدخل رابط استخدام تويتر الخاص بك',
  AppStringKey.enter_valid_discount_price: 'الرجاء إدخال سعر مخفض صالح',
  AppStringKey.enter_website_handle: 'أدخل رابط استخدام موقع الويب الخاص بك',
  AppStringKey.enter_zip_code: 'أدخل الرمز البريدي للمنطقة',
  AppStringKey.error_404: 'خطأ 404',
  AppStringKey.error_found_in_application:
      'تم العثور على الأخطاء التالية في تطبيقك',
  AppStringKey.explore_izhal: 'اكتشف إيزال',
  AppStringKey.facebook_link: 'رابط الفيسبوك',
  AppStringKey.feedback: 'تعليق',
  AppStringKey.file_size_exceeds: 'حجم الملف يتجاوز 10 ميغابايت',
  AppStringKey.file_upload_format: 'التنسيق المقبول هو .png و .jpg و .pdf.',
  AppStringKey.filter: 'التصفية',
  AppStringKey.filter_out_payment_list:
      'قم بتصفية قائمة المدفوعات عن طريق تحديد مرشح واحد أو عدة عوامل تصفية',
  AppStringKey.find_location: 'البحث عن الموقع',
  AppStringKey.finish_setup: 'إنهاء الإعداد',
  AppStringKey.first_name: 'الاسم الأول',
  AppStringKey.forget_password: 'هل نسيت كلمة المرور ؟',
  AppStringKey.garage: 'جراج',
  AppStringKey.garage_blacklist: 'الجراج المدرجة في القائمة السوداء',
  AppStringKey.garage_blacklist_description:
      'نأسف لإبلاغك بأنه قد تم رفض طلبك، وأن الجراج الخاص بك موجود الآن في قائمتنا السوداء. تواصل مع دعم العملاء لدينا لأية استفسارات بخصوص هذا القرار',
  AppStringKey.garage_details: 'تفاصيل المرآب',
  AppStringKey.garage_images: 'صور الجراج',
  AppStringKey.garage_name: 'اسم الجراج',
  AppStringKey.get_otp: 'الحصول على كلمة مرور لمرة واحدة',
  AppStringKey.go_back_to_dashboard: 'ارجع إلى لوحة التحكم',
  AppStringKey.hint_max_price: 'الحد الأقصى للسعر',
  AppStringKey.hint_min_price: 'الحد الأدنى للسعر',
  AppStringKey.homepage: 'الصفحة الرئيسية',
  AppStringKey.IBAN_code: 'رمز الحساب المصرفي الدولي',
  AppStringKey.instagram_link: 'رابط الانستجرام',
  AppStringKey.invalid_otp_error: 'الرجاء إدخال كلمة المرور لمرة واحدة صالحة.',
  AppStringKey.invalid_password_error: 'كلمة المرور التي أدخلتها غير صحيحة.',
  AppStringKey.invalid_phone_number: 'يرجى إدخال رقم هاتف صالح',
  AppStringKey.item_already_added: 'تمت إضافة العنصر بالفعل',
  AppStringKey.label_price_range: 'نطاق السعر',
  AppStringKey.label_select_service_levels: 'مستويات الخدمة',
  AppStringKey.last_name: 'الاسم الأخير',
  AppStringKey.level_already_added: 'تمت إضافة هذا المستوى بالفعل ...',
  AppStringKey.levels: 'المستويات',
  AppStringKey.list_of_sub_category_added:
      'تمت إضافة قائمة الفئات الفرعية في هذه القائمة',
  AppStringKey.login: 'تسجيل الدخول',
  AppStringKey.logo: 'الشعار',
  AppStringKey.logout: 'تسجيل الخروج',
  AppStringKey.logout_warning: 'هل أنت متأكد أنك تريد تسجيل الخروج؟',
  AppStringKey.logout_warning_description:
      'لا تقلق! يتم حفظ البيانات الخاصة بك في الوقت الحقيقي. يمكنك المتابعة من حيث توقفت في أي وقت.',
  AppStringKey.logoutButton: 'تسجيل خروج',
  AppStringKey.make_cover_photo: 'إنشاء صورة الغلاف',
  AppStringKey.map_location: 'موقع الخريطة',
  AppStringKey.marketing: 'تسويق',
  AppStringKey.max_limit_reached_message:
      'لقد وصلت إلى الحد الأقصى لتحميل الصورة',
  AppStringKey.max_upload_limit_reaches: 'تم الوصول إلى الحد الأقصى للتحميل',
  AppStringKey.mb: 'ميجابايت',
  AppStringKey.missing_garage_id: 'معرف الجراج مفقود للفئة',
  AppStringKey.mobile_number: 'رقم الهاتف المحمول',
  AppStringKey.mobile_verification: 'التحقق من الهاتف المحمول',
  AppStringKey.my_account: 'حساباتي',
  AppStringKey.name: 'الاسم',
  AppStringKey.newPhrase: 'جديد',
  AppStringKey.next: 'التالي',
  AppStringKey.no: 'لا',
  AppStringKey.no_data_found: 'البيانات غير متوفرة',
  AppStringKey.no_internet_connection: 'لا يوجد اتصال بالإنترنت!',
  AppStringKey.no_internet_connection_desc:
      'تحقق من اتصالك بالإنترنت وحاول مرة أخرى',
  AppStringKey.no_new_notification: 'لا يوجد إشعارات جديدة',
  AppStringKey.not_exist_phone_no_error:
      'هذا الرقم غير موجود. حاول التسجيل بدلا من ذلك.',
  AppStringKey.operation_hours: 'ساعات العمل',
  AppStringKey.optional: 'اختياري',
  AppStringKey.order_accepted: 'تم قبول الطلب',
  AppStringKey.order_date: 'تاريخ الطلب',
  AppStringKey.order_details: 'تفاصيل الطلب',
  AppStringKey.order_id: 'رقم التعريف الخاص بالطلب',
  AppStringKey.order_list: 'لائحة الطلبات',
  AppStringKey.order_list_desc:
      'قائمة الطلبات بناءً على القائمة المنسدلة المحددة',
  AppStringKey.order_list_filter:
      'قم بتصفية قائمة الطلبات النشطة عن طريق تحديد مرشح واحد أو عدة عوامل تصفية',
  AppStringKey.orders_completed: 'الطلبات مكتملة',
  AppStringKey.orders_pending: 'الطلبات المعلقة',
  AppStringKey.orders_received: 'الطلبات الواردة',
  AppStringKey.out_for_delivery: 'خارج للتوصيل',
  AppStringKey.page_not_found: 'الصفحة غير موجودة',
  AppStringKey.pagination_loading: 'تحميل...',
  AppStringKey.paid_by_customer: 'مدفوعات العميل',
  AppStringKey.paid_to_garage: 'الدفع إلى المرآب',
  AppStringKey.password_do_not_match: 'كلمة المرور غير مطابقة',
  AppStringKey.password_lower_case_error:
      'يجب أن تحتوي كلمة المرور على حرف صغير واحد على الأقل.',
  AppStringKey.password_lowercase_req_label: 'حالة واحدة صغيرة على الأقل',
  AppStringKey.password_min_length_label: 'على الأقل 8 أحرف',
  AppStringKey.password_minimum_length_error:
      'يجب أن تتكون كلمة المرور من 8 أحرف على الأقل.',
  AppStringKey.password_numerical_error:
      'يجب ان تحتوي كلمة المرور على الاقل رقما واحدا.',
  AppStringKey.password_special_char_req_label: 'حرف خاص واحد على الأقل',
  AppStringKey.password_special_character_error:
      'يجب أن تحتوي كلمة المرور على حرف خاص واحد على الأقل.',
  AppStringKey.password_successful_button_content: 'تسجيل الدخول',
  AppStringKey.password_successful_message:
      'تم تحديث كلمة المرور بنجاح. يمكنك تسجيل الدخول الآن باستخدام كلمة المرور الجديدة.',
  AppStringKey.password_successful_title: 'تم تغيير كلمة المرور بنجاح!',
  AppStringKey.password_upper_case_error:
      'يجب أن تحتوي كلمة المرور على حرف كبير واحد على الأقل.',
  AppStringKey.password_uppercase_req_label: 'حرف كبير واحد على الأقل',
  AppStringKey.payment: 'المدفوعات',
  AppStringKey.payment_details: 'بيانات الدفع',
  AppStringKey.payment_pending: 'انتظار الدفع',
  AppStringKey.paymentDesc: 'هل تلقيت الدفع!',
  AppStringKey.phone_no: 'رقم الهاتف',
  AppStringKey.pick_for_requested_order: 'استلام للطلب',
  AppStringKey.pick_up_date: 'تاريخ الاستلام',
  AppStringKey.pickup: 'استلام',
  AppStringKey.pickup_address: 'عنوان الاستلام',
  AppStringKey.pickup_drop_details: 'تفاصيل الاستلام',
  AppStringKey.pickup_for_return_order: 'استلام قائمة الطلبات',
  AppStringKey.pickup_scheduled: 'تم تحديد موعد الاستلام',
  AppStringKey.pickup_slot_limit: 'حد فتحة الاستلام',
  AppStringKey.pin_map_location: 'تثبيت موقع الخريطة',
  AppStringKey.please_complete_required_fields:
      'يرجى إكمال هذه الفروع للمتابعة',
  AppStringKey.please_complete_this_step_to_continue:
      'يرجى إكمال هذه الخطوات للمتابعة',
  AppStringKey.please_delete_category_first: 'يرجى حذف هذه الفئة أولا',
  AppStringKey.please_enter_password: 'الرجاء إدخال كلمة المرور',
  AppStringKey.previous: 'السابق',
  AppStringKey.privacy_policy: 'سياسة الخصوصية',
  AppStringKey.re_enter_account_no: 'أعد إدخال رقم الحساب',
  AppStringKey.re_enter_password: 'إعادة إدخال كلمة المرور',
  AppStringKey.reject: 'يرفض',
  AppStringKey.rejected: 'مرفوض',
  AppStringKey.rejected_application: 'تم رفض الطلب',
  AppStringKey.rejected_application_description:
      'شكرا لطلبك. بعد مراجعة متأنية، نأسف لإبلاغك بأن طلبك لم ينجح في الوقت الحالي. لمزيد من التفاصيل، يرجى التواصل مع فريق دعم العملاء لدينا.',
  AppStringKey.remarks: 'ملاحظات',
  AppStringKey.rename: 'إعادة تسمية',
  AppStringKey.requested_order_list_title: 'قائمة الطلبات المطلوبة',
  AppStringKey.requested_title_amount: 'مبلغ',
  AppStringKey.requested_title_pickup_detail: 'تفاصيل الاستلام',
  AppStringKey.requested_title_service_selected: 'الخدمة مختارة',
  AppStringKey.requested_vehicle_detail_title: 'تفاصيل السيارة',
  AppStringKey.resend_otp: 'إعادة إرسال كلمة المرور لمرة واحدة',
  AppStringKey.resend_otp_in: 'إعادة إرسال كلمة المرور لمرة واحدة في',
  AppStringKey.reset: 'إعادة تعيين',
  AppStringKey.reset_password: 'إعادة تعيين كلمة المرور',
  AppStringKey.retry: 'أعد المحاولة',
  AppStringKey.return_order: 'أمر التقديم',
  AppStringKey.return_order_detail: 'تفاصيل طلب التقديم',
  AppStringKey.return_order_list: 'إعادة قائمة طلبات',
  AppStringKey.return_request: 'طلب ارجاع',
  AppStringKey.save: 'حفظ',
  AppStringKey.save_changes: 'حفظ التغييرات',
  AppStringKey.search_car: 'بحث السيارة',
  AppStringKey.seconds: 'ثانية',
  AppStringKey.select_categories: 'حدد الفئات',
  AppStringKey.select_category: 'الرجاء تحديد الفئة',
  AppStringKey.select_city: 'اختر مدينة',
  AppStringKey.select_date: 'اختر التاريخ',
  AppStringKey.select_one_level:
      'الرجاء إضافة مستوى واحد على الأقل للفئة الفرعية',
  AppStringKey.select_one_model:
      'الرجاء إضافة نموذج واحد على الأقل للفئة الفرعية',
  AppStringKey.select_service: 'اختر الخدمة',
  AppStringKey.select_service_category: 'اختر فئة الخدمة',
  AppStringKey.select_service_levels: 'حدد مستويات الخدمة',
  AppStringKey.select_sub_category: 'الرجاء تحديد الفئة الفرعية',
  AppStringKey.select_sub_category_image: 'الرجاء إضافة صورة الفئة الفرعية',
  AppStringKey.select_sub_service_category: 'حدد الفئة الفرعية للخدمة',
  AppStringKey.select_work_status: 'حدد حالة العمل',
  AppStringKey.select_your_level: 'حدد / أضف المستويات الخاصة بك',
  AppStringKey.selected_car: 'سيارات مختارة',
  AppStringKey.selected_car_desc:
      'تعرف على السيارات التي قمت بإضافتها إلى الخدمات الدورية - الباقات المجدولة.',
  AppStringKey.selected_car_not_available: 'السيارة المختارة غير متوفرة',
  AppStringKey.self_drop: 'تقديم الذات',
  AppStringKey.serial_number: 'رقم المسلسل',
  AppStringKey.service_category: 'فئة الخدمة',
  AppStringKey.service_completed: 'اكتملت الخدمة',
  AppStringKey.service_completed_date: 'تاريخ انتهاء الخدمة',
  AppStringKey.service_info: 'معلومات الخدمة',
  AppStringKey.service_level: 'مستويات الخدمة',
  AppStringKey.service_no: 'تعديل أو إضافة خدمات جديدة للطلب رقم',
  AppStringKey.service_request: 'طلب خدمة',
  AppStringKey.service_selected: 'الخدمة مختارة',
  AppStringKey.service_service_type: 'حدد نوع الخدمة',
  AppStringKey.service_started: 'بدأت الخدمة',
  AppStringKey.service_status: 'حالة الخدمة',
  AppStringKey.service_sub_category: 'الفئة الفرعية للخدمة',
  AppStringKey.services: 'خدمات',
  AppStringKey.showcase_what_customers_have_to_say: 'اعرض ما يقوله عملاؤك عنك.',
  AppStringKey.sigin_in_now: 'سجّل الدخول الآن',
  AppStringKey.sigin_up_agree: 'من خلال الاشتراك، فإنك توافق على',
  AppStringKey.sign_out: 'تسجيل الخروج',
  AppStringKey.sign_up_now: ' سجّل الدخول الآن',
  AppStringKey.signup: 'التسجيل',
  AppStringKey.skip: 'تخطى',
  AppStringKey.skip_for_now: 'تخطي هذه الخطوة في الوقت الحالي',
  AppStringKey.slot: 'ثغرة',
  AppStringKey.social_links: 'الروابط الاجتماعية',
  AppStringKey.social_media_links: 'روابط وسائل التواصل الاجتماعي',
  AppStringKey.specify_reason_for_order_decline:
      'يرجى تقديم السبب المحدد لرفض الخدمة عند الطلب',
  AppStringKey.staff: 'فريق العمل',
  AppStringKey.staff_create_password_valid: 'يجب ألا تكون كلمة السر فارغة',
  AppStringKey.staff_details: 'تفاصيل فريق العمل',
  AppStringKey.staff_email_not_valid: 'يُرجى إدخال عنوان بريد إلكتروني صالح',
  AppStringKey.staff_email_required:
      'يجب ألا يكون معرف البريد الإلكتروني فارغًا',
  AppStringKey.staff_first_name_required: 'يجب ألا يكون الاسم الأول فارغًا',
  AppStringKey.staff_last_name_required: 'يجب ألا يكون الاسم الأخير فارغًا',
  AppStringKey.staff_password_required: 'يجب أن لا تكون كلمة السر فارغة',
  AppStringKey.staff_phone_length_required: 'يجب أن يكون طول الرقم بين 3 إلى 8',
  AppStringKey.staff_phone_required: 'رقم الهاتف لا ينبغي أن يكون فارغًا',
  AppStringKey.staff_staff_type_required: 'الرجاء تحديد نوع فريق العمل',
  AppStringKey.staff_type: 'نوع فريق العمل',
  AppStringKey.start_date: 'حدد وقت البدء',
  AppStringKey.start_operation_time: 'حدد وقت البدء',
  AppStringKey.status: 'حالة',
  AppStringKey.stepper_account_details: 'تفاصيل الحساب',
  AppStringKey.stepper_basic_detail: 'التفاصيل الأساسية',
  AppStringKey.stepper_marketing: 'التسويق',
  AppStringKey.stepper_services: 'الخدمات',
  AppStringKey.stepper_staff: 'الموظفون',
  AppStringKey.sub_categories: 'الفئات الفرعية',
  AppStringKey.submission_failed: 'فشل التقديم',
  AppStringKey.submission_failed_description:
      'يرجى التحقق من المعلومات التالية وتعديلها قبل إعادة الإرسال.',
  AppStringKey.submit: 'إرسال',
  AppStringKey.submit_success: 'تم تقديم طلبك بنجاح!',
  AppStringKey.submit_success_description:
      'سنراجع طلبك ونرد عليك خلال 7 أيام عمل. وسيتم إعلامك عبر رقم هاتفك المسجل ومعرف البريد الإلكتروني.',
  AppStringKey.submitted_category_desc:
      'عرض تفاصيل الخدمات التي تندرج ضمن الفئة الشاملة',
  AppStringKey.summary: 'الملخص',
  AppStringKey.support: 'الدعم',
  AppStringKey.support_description:
      'هذه هي صفحة الدعم الخاصة بنا. لا تتردد في التواصل معنا للاستفسارات الخاصة بك.',
  AppStringKey.swift_code: 'رمز السويفت',
  AppStringKey.switch_to_main_branch: 'التبديل إلى الفرع الرئيسي',
  AppStringKey.term_of_use: 'شروط الاستخدام',
  AppStringKey.third_party: 'طرف ثالث',
  AppStringKey.tik_tok_link: 'رابط التيك توك',
  AppStringKey.today: 'اليوم',
  AppStringKey.total_amount: 'المبلغ الإجمالي',
  AppStringKey.total_bill: 'إجمالي الفاتورة',
  AppStringKey.total_pending: 'إجمالي الطلبات المعلقة',
  AppStringKey.total_received: 'المبلغ المُستلم',
  AppStringKey.total_revenue: 'إجمالي الإيرادات (ربع السنة)',
  AppStringKey.total_sales: 'إجمالي المبيعات',
  AppStringKey.trade_license: 'الرخصة التجارية',
  AppStringKey.twitter_link: 'رابط تويتر',
  AppStringKey.unsupported_file_extension: 'امتداد الملف غير معتمد',
  AppStringKey.update_now: 'تحديث الان',
  AppStringKey.update_order: 'تحديث الطلب',
  AppStringKey.update_order_detail: 'تحديث تفاصيل الطلب',
  AppStringKey.upload_max_size_mb: 'الحد الأقصى لحجم الصورة المرفوعة هو',
  AppStringKey.upload_photo: 'حمل الصورة',
  AppStringKey.upload_photo_desc: '.png .jpg مدعومة\nحجم يصل إلى 10 ميجابايت',
  AppStringKey.user_more_then_4_images_upload_desc:
      '85% من المستخدمين يقومون بالحجز من مراكز الخدمة التي تمت إضافة أكثر من 4 صور لأعمالهم.',
  AppStringKey.validation_account_number_not_match: 'رقم الحساب غير متطابق',
  AppStringKey.validation_empty_field: 'لا ينبغي أن يكون الحقل فارغًا',
  AppStringKey.validation_invalid_email: 'أدخل معرف بريد إلكتروني صالحًا',
  AppStringKey.validation_invalid_input: 'إدخال غير صالح',
  AppStringKey.validation_invalid_number: 'أدخل رقم هاتف صالحًا',
  AppStringKey.vehicle_images: 'صور المركبات',
  AppStringKey.vehicle_in_transit: 'مركبة في العبور',
  AppStringKey.vehicle_info: 'معلومات المركبة',
  AppStringKey.vehicle_model: 'طراز السيارة',
  AppStringKey.vehicle_reached: 'وصلت المركبة',
  AppStringKey.vehicle_received: 'تم استلام المركبة',
  AppStringKey.view: 'عرض',
  AppStringKey.view_added_card: 'عرض السيارات المضافة',
  AppStringKey.view_less: 'عرض أقل',
  AppStringKey.view_more: 'عرض المزيد',
  AppStringKey.warranty_claimed: 'مطالبة الضمان',
  AppStringKey.website_link: 'رابط موقع الويب',
  AppStringKey.will_be_able_to_pick_order:
      'هل ستتمكن من استلام السيارة لمعرف الطلب أعلاه؟',
  AppStringKey.work_status: 'حالة العمل',
  AppStringKey.yes: 'نعم',
  AppStringKey.you_need_to_pickup_order:
      'تحتاج إلى استلام السيارة مع معرف الطلب',
  AppStringKey.zip_code: 'الرمز البريدي',
};
