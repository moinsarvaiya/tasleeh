import 'package:get/get_navigation/src/root/internacionalization.dart';

import 'ar_AR.dart';
import 'en_EN.dart';

class Languages extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_EN': en_EN,
        'ar_AR': ar_AR,
      };
}
