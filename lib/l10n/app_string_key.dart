// ignore_for_file: constant_identifier_names
class AppStringKey {
  static const String english = 'language';
  static const String signup = 'signup';
  static const String login = 'login';
  static const String delete = 'delete';
  static const String are_you_sure_to_delete = 'are_you_sure_to_delete';
  static const String close = 'close';
  static const String create_password = "create_password";
  static const String re_enter_password = "re_enter_password";
  static const String continue_ahead = "continue_ahead";
  static const String password_successful_message =
      "password_successful_message";
  static const String password_successful_title = "password_successful_title";
  static const String password_successful_button_content =
      "password_successful_button_content";
  static const String create_password_title = "create_password_title";
  static const String create_password_message = "create_password_message";
  static const String create_password_button_content =
      "create_password_button_content";
  static const String password_min_length_label = "password_min_length_label";
  static const String password_uppercase_req_label =
      "password_uppercase_req_label";
  static const String password_lowercase_req_label =
      "password_lowercase_req_label";
  static const String password_special_char_req_label =
      "password_special_char_req_label";
  static const String enter_password = "enter_password";
  static const String please_enter_password = "please_enter_password";
  static const String password_do_not_match = "password_do_not_match";
  static const String forget_password = "forget_password";
  static const String phone_no = "phone_no";
  static const String enter_phone_no = "enter_phone_no";
  static const String reset_password = "reset_password";
  static const String get_otp = "get_otp";
  static const String invalid_phone_number = "invalid_phone_number";
  static const String do_not_have_account = "do_not_have_account";
  static const String sign_up_now = "sigin_up_now";
  static const String continue_with = "continue_with";
  static const String mobile_verification = "mobile_verification";
  static const String enter_six_digit_code = "enter_six_digit_code";
  static const String change = "change";
  static const String resend_otp_in = "resend_otp_in";
  static const String resend_otp = "resend_otp";
  static const String already_have_an_account = "already_have_an_account";
  static const String sigin_in_now = "sigin_in_now";
  static const String submit = "submit";
  static const String privacy_policy = "privacy_policy";
  static const String term_of_use = "term_of_use";
  static const String sigin_up_agree = "sigin_up_agree";
  static const String homepage = "homepage";
  static const String invalid_password_error = "invalid_password_error";
  static const String not_exist_phone_no_error = "not_exist_phone_no_error";
  static const String invalid_otp_error = "invalid_otp_error";
  static const String api_fail_error = "api_fail_error";
  static const String stepper_basic_detail = 'stepper_basic_detail';
  static const String stepper_account_details = 'stepper_account_details';
  static const String stepper_marketing = 'stepper_marketing';
  static const String stepper_services = 'stepper_services';
  static const String stepper_staff = 'stepper_staff';
  static const String skip_for_now = 'skip_for_now';
  static const String previous = 'previous';
  static const String next = 'next';
  static const String account_detail = 'Account Details';
  static const String optional = 'optional';
  static const String add_branch = 'add_branch';
  static const String branch_text = 'branch_text';
  static const String bank_name = 'bank_name';
  static const String IBAN_code = 'IBAN_code';
  static const String enter_IBAN_code = 'enter_IBAN_code';
  static const String enter_bank_name = 'enter_bank_name';
  static const String account_holder_name = 'account_holder_name';
  static const String account_no = 'account_no';
  static const String enter_account_no = 'enter_account_no';
  static const String re_enter_account_no = 're_enter_account_no';
  static const String enter_account_holder_name = 'enter_account_holder_name';
  static const String garage_details = "garage_details";
  static const String logo = "logo";
  static const String click_to_upload = "click_to_upload";
  static const String file_upload_format = "file_upload_format";
  static const String upload_max_size_mb = "upload_max_size_mb";
  static const String mb = "mb";
  static const String commercial_registration = "commercial_registration";
  static const String trade_license = "trade_license";
  static const String computer_card = "computer_card";
  static const String garage_name = "garage_name";
  static const String enter_garage_name = "enter_garage_name";
  static const String email = "email";
  static const String enter_email = "enter_email";
  static const String operation_hours = "operation_hours";
  static const String start_date = "start_date";
  static const String end_date = "end_date";
  static const String date_of_establishment = "date_of_establishment";
  static const String address_line = "address_line";
  static const String enter_address_line = "enter_address_line";
  static const String city = "city";
  static const String zip_code = "zip_code";
  static const String select_city = "select_city";
  static const String enter_zip_code = "enter_zip_code";
  static const String map_location = "map_location";
  static const String pin_map_location = "pin_map_location";
  static const String contact_person_details = "contact_person_deatils";
  static const String first_name = "first_name";
  static const String last_name = "last_name";
  static const String enter_first_name = "enter_first_name";
  static const String enter_last_name = "enter_last_name";
  static const String select_date = "select_date";
  static const String basic_details = "basic_details";
  static const String confirm_location = "confirm_location";
  static const String cancel = "cancel";
  static const String find_location = "find_location";
  static const String serial_number = "serial_number";
  static const String create_pass = "create_pass";
  static const String actions = "actions";
  static const String save = "save";
  static const String add_another_staff = "add_another_staff";
  static const String add_another_service = "add_another_service";
  static const String add_staff = "add_staff";
  static const String staff = "staff";
  static const String mobile_number = "mobile_number";
  static const String currently_no_data_found = "current_no_data_found";
  static const String enter_mobile_number = "enter_mobile_number";
  static const String staff_details = "staff_details";
  static const String edit_details = "edit_details";
  static const String save_changes = "save_changes";
  static const String sign_out = "sign_out";
  static const String marketing = "marketing";
  static const String add_video_photos = "add_video_photos";
  static const String add_video_photos_desc = "add_video_photos_desc";
  static const String user_more_then_4_images_upload_desc =
      "user_more_then_4_images_upload_desc";
  static const String click_to_drag_file_title = "click_to_drag_file_title";
  static const String click_to_drag_formats_desc = "click_to_drag_formats_desc";
  static const String rename = "rename";

  static const String add_customer_testimonials = "add_customer_testimonials";
  static const String add_another_testimonial = "add_another_testimonial";
  static const String showcase_what_customers_have_to_say =
      "showcase_what_customers_have_to_say";
  static const String social_media_links = "social_media_links";
  static const String enter_instagram_handle = "enter_instagram_handle";
  static const String instagram_link = "instagram_link";
  static const String enter_twitter_handle = "enter_twitter_handle";
  static const String twitter_link = "twitter_link";
  static const String enter_facebook_handle = "enter_facebook_handle";
  static const String facebook_link = "facebook_link";
  static const String enter_website_handle = "enter_website_handle";
  static const String website_link = "website_link";
  static const String enter_tik_tok_handle = "enter_tik_tok_handle";
  static const String tik_tok_link = "tik_tok_link";
  static const String client_photo_or_video = "client_photo_or_video";
  static const String client_name = "client_name";
  static const String enter_client_name = "enter_client_name";
  static const String feedback = "feedback";
  static const String enter_feedback = "enter_feedback";
  static const String make_cover_photo = "make_cover_photo";
  static const String cover_image = "cover_image";
  static const String max_limit_reached_message = "max_limit_reached_message";
  static const String add_service = "add_service";
  static const String add_category = "add_category";
  static const String select_service = "select_service";
  static const String staff_type = "staff_type";
  static const String enter_staff_type = "enter_staff_type";
  static const String select_categories = "select_categories";
  static const String add_sub_category = "add_sub_category";
  static const String sub_categories = "sub_categories";
  static const String levels = "levels";
  static const String view_added_card = "view_added_card";
  static const String upload_photo = "upload_photo";
  static const String upload_photo_desc = "upload_photo_desc";
  static const String added_levels = "added_levels";
  static const String add_levels = "add_levels";
  static const String add_levels_desc = "add_levels_desc";
  static const String edit = "edit";
  static const String selected_car = "selected_car";
  static const String selected_car_desc = "selected_car_desc";
  static const String select_your_level = "select_your_level";
  static const String enter_amount = "enter_amount";
  static const String enter_discount_amount = "enter_discount_amount";
  static const String enter_level_desc = "enter_level_desc";
  static const String enter_manually = "enter_manually";
  static const String item_already_added = "item_already_added";
  static const String added_dialog_bottom_desc = "added_dialog_bottom_desc";
  static const String reset = "reset";
  static const String add = "add";
  static const String name = "name";
  static const String contact_person_description = 'contact_person_description';
  static const String edit_bank_details = 'edit_bank_details';
  static const String accounts = 'accounts';
  static const String basic = 'basic';
  static const String my_account = 'my_account';
  static const String add_bank_account = 'add_bank_account';
  static const String social_links = 'social_links';
  static const String garage_images = 'garage_images';
  static const String customer_testimonials = 'customer_testimonials';
  static const String add_images = 'add-images';
  static const String edit_services_details = 'edit_services_details';
  static const String validation_empty_field = 'validation_empty_field';
  static const String validation_invalid_number = 'validation_invalid_number';
  static const String validation_invalid_email = 'validation_invalid_email';
  static const String validation_invalid_input = 'validation_invalid_input';
  static const String validation_account_number_not_match =
      'validation_two_value_not_match';
  static const String driver = 'driver';
  static const String branch_staff = 'branch_staff';
  static const String start_operation_time = 'start_operation_time';
  static const String end_operation_time = 'end_operation_time';

  static const String staff_first_name_required = 'staff_first_name_required';
  static const String staff_last_name_required = 'staff_last_name_required';
  static const String staff_phone_required = 'staff_phone_required';
  static const String staff_phone_length_required =
      'staff_phone_length_required';
  static const String staff_email_required = 'staff_email_required';
  static const String staff_email_not_valid = 'staff_email_not_valid';
  static const String staff_create_password_valid =
      'staff_create_password_valid';
  static const String staff_staff_type_required = 'staff_staff_type_required';
  static const String view_more = 'view_more';
  static const String view_less = 'view_less';
  static const String summary = 'summary';
  static const String payment_details = 'payment_details';
  static const String vehicle_model = 'vehicle_model';
  static const String service_selected = 'service_selected';
  static const String order_details = 'order_details';
  static const String paid_by_customer = 'paid_by_customer';
  static const String paid_to_garage = 'paid_to_garage';
  static const String amount_received = 'amount_received';
  static const String filter = 'filter';
  static const String total_sales = 'total_sales';
  static const String total_pending = 'total_pending';
  static const String total_received = 'total_received';
  static const String above_data_from_the_past = 'above_data_from_the_past';
  static const String order_date = 'order_date';
  static const String pick_up_date = 'pick_up_date';
  static const String filter_out_payment_list = 'filter_out_payment_list';
  static const String please_complete_required_fields =
      'please_complete_required_fields';
  static const String search_car = 'search_car';

  static const String total_revenue = 'total_revenue';
  static const String orders_received = 'orders_received';
  static const String orders_completed = 'orders_completed';
  static const String orders_pending = 'orders_pending';
  static const String return_order_list = 'return_order_list';
  static const String pickup_drop_details = 'pickup_drop_details';
  static const String remarks = 'remarks';
  static const String status = 'status';
  static const String pickup_for_return_order = 'pickup_for_return_order';
  static const String will_be_able_to_pick_order = 'will_be_able_to_pick_order';
  static const String pickup = 'pickup';
  static const String slot = 'slot';
  static const String yes = 'yes';
  static const String no = 'no';
  static const String cancel_pickup_for_return_order =
      'cancel_pickup_for_return_order';
  static const String are_you_sure_cancel_return_order =
      'are_you_sure_cancel_return_order';
  static const String confirm = 'confirm';
  static const String decline_return_service_for_order =
      'decline_return_service_for_order';
  static const String specify_reason_for_order_decline =
      'specify_reason_for_order_decline';
  static const String enter_reason = 'enter_reason';
  static const String go_back_to_dashboard = 'go_back_to_dashboard';
  static const String return_order_detail = 'return_order_detail';
  static const String work_status = 'work_status';
  static const String service_status = 'service_status';
  static const String vehicle_received = 'vehicle_received';
  static const String service_started = 'service_started';
  static const String vehicle_in_transit = 'vehicle_in_transit';
  static const String service_completed = 'service_completed';
  static const String customer_info = 'customer_info';
  static const String below_contains_customer_detail_info =
      'below_contains_customer_detail_info';
  static const String below_contains_service_detail_info =
      'below_contains_service_detail_info';
  static const String below_contains_vehicle_detail_info =
      'below_contains_vehicle_detail_info';
  static const String contact_number = 'contact_number';
  static const String pickup_address = 'pickup_address';
  static const String service_info = 'service_info';
  static const String return_request = 'return_request';
  static const String service_request = 'service_request';
  static const String vehicle_info = 'vehicle_info';
  static const String vehicle_images = 'vehicle_images';
  static const String booking_id = 'booking_id';
  static const String reject = 'reject';
  static const String accept = 'accept';
  static const String list_of_sub_category_added = "list_of_sub_category_added";
  static const String total_bill = 'total_bill';
  static const String total_amount = 'total_amount';

  static const String selected_car_not_available = 'selected_car_not_available';
  static const String select_category = 'select_category';
  static const String select_sub_category = 'select_sub_category';
  static const String select_sub_category_image = 'select_sub_category_image';
  static const String missing_garage_id = 'missing_garage_id';
  static const String select_one_model = 'select_one_model';
  static const String select_one_level = 'select_one_level';
  static const String submitted_category_desc = 'submitted_category_desc';
  static const String view = 'view';
  static const String level_already_added = 'level_already_added';
  static const String enter_level_name = 'enter_level_name';
  static const String enter_valid_discount_price = 'enter_valid_discount_price';
  static const String please_delete_category_first =
      'please_delete_category_first';
  static const String bank_address = 'bank_address';
  static const String swift_code = 'swift_code';
  static const String enter_bank_address = 'enter_bank_address';
  static const String enter_swift_code = 'enter_swift_code';
  static const String add_another_bank_account = 'add_another_bank_account';
  static const String pickup_slot_limit = 'pickup_slot_limit';
  static const String enter_max_number_of_pickups =
      'enter_max_number_of_pickups';
  static const String building_number = 'building_number';
  static const String enter_building_number = 'enter_building_number';
  static const String support = 'support';
  static const String email_support = 'email_support';
  static const String call_support = 'call_support';
  static const String chat_support = 'chat_support';
  static const String chat_support_description = 'chat_support_description';
  static const String chat_with_us = 'chat_with_us';
  static const String support_description = 'support_description';
  static const String email_support_description = 'email_support_description';
  static const String back = 'back';
  static const String seconds = 'seconds';
  static const String dashboard_order_list = 'dashboard_order_list';
  static const String dashboard_request_order = 'dashboard_request_order';
  static const String dashboard_active_order = 'dashboard_active_order';
  static const String dashboard_completed_order = 'dashboard_completed_order';
  static const String dashboard_return_order = 'dashboard_return_order';
  static const String dashboard_payments = 'dashboard_payments';
  static const String dashboard_help = 'dashboard_help';
  static const String dashboard_my_account = 'dashboard_my_account';
  static const String garage = 'garage';
  static const String logout = 'logout';

  static const String order_list_filter = 'order_list_filter';
  static const String select_service_levels = 'select_service_levels';
  static const String label_select_service_levels =
      'label_select_service_levels';
  static const String hint_min_price = 'hint_min_price';
  static const String hint_max_price = 'hint_max_price';
  static const String label_price_range = 'label_price_range';
  static const String select_work_status = 'select_work_status';
  static const String order_list = 'order_list';
  static const String order_list_desc = 'order_list_desc';
  static const String services = 'services';
  static const String requested_order_list_title = 'requested_order_list_title';
  static const String requested_vehicle_detail_title =
      'requested_vehicle_detail_title';
  static const String requested_title_service_selected =
      'requested_title_service_selected';
  static const String requested_title_pickup_detail =
      'requested_title_pickup_detail';
  static const String requested_title_amount = 'requested_title_amount';
  static const String decline_service_order = 'decline_service_order';
  static const String you_need_to_pickup_order = 'you_need_to_pickup_order';
  static const String at = 'at';
  static const String between = 'between';
  static const String third_party = 'third_party';
  static const String order_id = 'order_id';
  static const String apply_filter = 'apply_filter';
  static const String return_order = 'return_order';
  static const String active_order_list = 'active_order_list';
  static const String skip = 'skip';
  static const String please_complete_this_step_to_continue =
      'please_complete_this_step_to_continue';
  static const String update_order_detail = 'update_order_detail';
  static const String service_no = 'service_no';
  static const String select_service_category = 'select_service_category';
  static const String select_sub_service_category =
      'select_sub_service_category';
  static const String service_category = 'service_category';
  static const String service_sub_category = 'service_sub_category';
  static const String service_service_type = 'service_service_type';
  static const String service_level = 'service_level';
  static const String update_order = 'update_order';
  static const String edit_order = 'edit_order';
  static const String no_data_found = 'no_data_found';
  static const String completed_order_filter_desc =
      'completed_order_filter_desc';
  static const String service_completed_date = 'service_completed_date';
  static const String pick_for_requested_order = 'pick_for_requested_order';
  static const String cancel_pick_for_requested_order =
      'cancel_pick_for_requested_order';
  static const String order_accepted = 'order_accepted';
  static const String completed_order_list_title = 'completed_order_list_title';
  static const String active_order_detail = 'active_order_detail';
  static const String file_size_exceeds = 'file_size_exceeds';
  static const String unsupported_file_extension = 'unsupported_file_extension';
  static const String page_not_found = 'page_not_found';
  static const String error_404 = 'error_404';
  static const String logout_warning = 'logout_warning';
  static const String logout_warning_description = 'logout_warning_description';
  static const String logoutButton = 'logOutButton';
  static const String pagination_loading = 'pagination_loading';
  static const String self_drop = 'self_drop';
  static const String newPhrase = 'new';
  static const String accepted = 'accepted';
  static const String rejected = 'rejected';
  static const String cancelled = 'cancelled';
  static const String pickup_scheduled = 'pickup_scheduled';
  static const String vehicle_reached = 'vehicle_reached';
  static const String payment_pending = 'payment_pending';
  static const String out_for_delivery = 'out_for_delivery';
  static const String delivered = 'delivered';
  static const String warranty_claimed = 'warranty_claimed';
  static const String application_review = 'application_review';
  static const String application_review_description =
      'application_review_description';
  static const String explore_izhal = 'explore_izhal';
  static const String update_now = 'update_now';
  static const String submission_failed = 'submission_failed';
  static const String submission_failed_description =
      'submission_failed_description';
  static const String error_found_in_application = 'error_found_in_application';
  static const String rejected_application = 'rejected_application';
  static const String rejected_application_description =
      'rejected_application_description';
  static const String garage_blacklist = 'garage_blacklist';
  static const String garage_blacklist_description =
      'garage_blacklist_description';
  static const String no_new_notification = 'no_new_notification';
  static const String submit_success = 'submit_success';
  static const String submit_success_description = 'submit_success_description';
  static const String finish_setup = 'finish_setup';
  static const String staff_password_required = 'staff_password_required';
  static const String password_minimum_length_error =
      'password_minimum_length_error';
  static const String password_special_character_error =
      'password_special_character_error';
  static const String password_lower_case_error = 'password_lower_case_error';
  static const String password_upper_case_error = 'password_upper_case_error';
  static const String password_numerical_error = 'password_numerical_error';
  static const String back_to_login = 'back_to_login';
  static const String back_to_dashboard = 'back_to_dashboard';
  static const String branch_handler = 'branch_handler';
  static const String branch_handler_desc = 'branch_handler_desc';
  static const String switch_to_main_branch = 'switch_to_main_branch';
  static const String branch_management = 'branch_management';
  static const String max_upload_limit_reaches = 'max_upload_limit_reaches';
  static const String no_internet_connection = 'no_internet_connection';
  static const String no_internet_connection_desc =
      'no_internet_connection_desc';
  static const String retry = 'retry';
  static const String today = 'today';
  static const String payment = 'payment';
  static const String paymentDesc = 'paymentDesc';
  static const String copied_to_clipboard_success_message =
      'copied_to_clipboard_success_message';
}
