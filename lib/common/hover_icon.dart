import 'package:al_baida_garage_fe/core/constant/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HoverIcon extends StatefulWidget {
  final String icon;
  final Color defaultColor;
  final Color hoverColor;
  final VoidCallback? onTap;
  final double iconSize;
  final Color backgroundColor;

  const HoverIcon({
    super.key,
    required this.icon,
    required this.defaultColor,
    required this.hoverColor,
    required this.onTap,
    this.iconSize = 20,
    this.backgroundColor = AppColors.colorWhite,
  });

  @override
  _HoverIconState createState() => _HoverIconState();
}

class _HoverIconState extends State<HoverIcon> {
  bool isHovered = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (_) => setState(() => isHovered = true),
      onExit: (_) => setState(() => isHovered = false),
      child: Material(
        child: Ink(
          color: widget.backgroundColor,
          child: InkWell(
            onTap: widget.onTap,
            child: SvgPicture.asset(
              widget.icon,
              color: isHovered ? widget.hoverColor : widget.defaultColor,
              height: widget.iconSize,
              width: widget.iconSize,
            ),
          ),
        ),
      ),
    );
  }
}
