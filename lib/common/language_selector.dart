import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../Pages/Dashboard/custom_dashboard_drawer/custom_dashboard_stepper_controller.dart';
import '../Pages/custom_stepper/custom_stepper_controller.dart';
import '../core/app_preference/app_preferences.dart';
import '../core/app_preference/storage_keys.dart';
import '../core/constant/app_color.dart';
import '../core/constant/app_images.dart';
import '../core/constant/base_style.dart';
import '../core/constant/ui_constants.dart';
import '../core/extensions/common_extension.dart';
import '../core/routes/route_paths.dart';

class GlobalLanguageSelector extends StatefulWidget {
  const GlobalLanguageSelector({super.key});

  @override
  State<GlobalLanguageSelector> createState() => _GlobalLanguageSelectorState();
}

class _GlobalLanguageSelectorState extends State<GlobalLanguageSelector> {
  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField2(
      items: languageSelectorItems,
      onChanged: (val) {
        if (val == englishLocaleCode) {
          Get.updateLocale(
            const Locale('en', 'EN'),
          );
        } else if (val == arabicLocaleCode) {
          Get.updateLocale(
            const Locale('ar', 'AR'),
          );
        }

        if (Get.currentRoute == RoutePaths.CUSTOM_STEPPER) {
          Get.find<CustomStepperController>().updateLanguage();
        } else if (Get.currentRoute == RoutePaths.DASHBOARD_STEPPER) {
          Get.find<CustomDashboardStepperController>().updateLanguage();
        }

        AppPreferences.sharedPrefWrite(StorageKeys.lang, val);
      },
      value: AppPreferences.sharedPrefRead(StorageKeys.lang).toString(),
      decoration: InputDecoration(
        constraints: BoxConstraints(maxWidth: context.isDesktop ? 125 : 100),
        contentPadding: EdgeInsets.zero,
        prefixIcon: SvgPicture.asset(
          AppImages.multiLanguageIcon,
          color: AppColors.primaryTextColor,
        ),
        prefixIconColor: AppColors.primaryTextColor,
        prefixIconConstraints: context.isDesktop
            ? const BoxConstraints(maxHeight: 24, maxWidth: 24)
            : const BoxConstraints(maxHeight: 16, maxWidth: 16),
        iconColor: AppColors.primaryTextColor,
        border: InputBorder.none,
      ),
      style: context.isDesktop
          ? BaseStyle.textStyleRobotoSemiBold(
              16,
              AppColors.primaryTextColor,
            )
          : BaseStyle.textStyleRobotoSemiBold(
              12,
              AppColors.primaryTextColor,
            ),
      isDense: false,
      isExpanded: false,
      alignment: Alignment.center,
      iconStyleData: IconStyleData(
        icon: SvgPicture.asset(
          AppImages.arrowHeadDown,
        ),
        iconSize: 12,
        iconEnabledColor: AppColors.primaryTextColor,
      ),
    );
  }
}
