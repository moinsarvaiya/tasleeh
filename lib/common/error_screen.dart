import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../core/constant/app_color.dart';
import '../core/constant/base_style.dart';
import '../core/routes/route_paths.dart';
import '../l10n/app_string_key.dart';

class ErrorScreen extends StatelessWidget {
  const ErrorScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              AppStringKey.error_404.tr,
              style: BaseStyle.textStyleRobotoSemiBold(
                24,
                AppColors.primaryTextColor,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              AppStringKey.page_not_found.tr,
              style: BaseStyle.textStyleRobotoRegular(
                20,
                AppColors.primaryTextColor,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            TextButton(
              onPressed: () {
                Get.toNamed(RoutePaths.LOGIN);
              },
              style: TextButton.styleFrom(
                padding: const EdgeInsets.symmetric(
                  horizontal: 30,
                  vertical: 16,
                ),
                textStyle: BaseStyle.textStyleRobotoMedium(
                  16,
                  AppColors.primaryTextColor,
                ),
                backgroundColor: AppColors.primaryTextColor,
                foregroundColor: AppColors.colorWhite,
                shape: const BeveledRectangleBorder(),
              ),
              child: Text(
                AppStringKey.go_back_to_dashboard.tr,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
