import 'package:al_baida_garage_fe/core/constant/app_color.dart';
import 'package:al_baida_garage_fe/core/constant/base_style.dart';
import 'package:al_baida_garage_fe/l10n/app_string_key.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../core/extensions/common_extension.dart';

class PageBackButton extends StatelessWidget {
  final String? buttonTitle;

  const PageBackButton({super.key, this.buttonTitle});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: context.isDesktop ? 48 : 36,
      // Use Directionality widget to determine text direction
      left: Directionality.of(context) == TextDirection.ltr
          ? (context.isDesktop ? 48 : 16)
          : null,
      right: Directionality.of(context) == TextDirection.rtl
          ? (context.isDesktop ? 48 : 16)
          : null,
      child: TextButton(
        onPressed: () {
          if (Navigator.of(context).canPop()) {
            Navigator.of(context).pop();
          }
        },
        child: Row(
          children: [
            const Icon(
              Icons.arrow_back,
              color: AppColors.primaryTextColor,
            ),
            const SizedBox(
              width: 8,
            ),
            Text(
              (buttonTitle != null && buttonTitle != ''
                  ? buttonTitle!
                  : AppStringKey.back.tr),
              style: BaseStyle.textStyleRobotoSemiBold(
                16,
                AppColors.primaryTextColor,
              ),
            )
          ],
        ),
      ),
    );
  }
}
