import 'package:al_baida_garage_fe/Pages/Dashboard/order_list/requested_order/order_controller.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../core/constant/app_color.dart';
import '../Utils/dimensions.dart';
import '../core/constant/base_style.dart';
import '../l10n/app_string_key.dart';

class CommonOrderTable extends StatelessWidget {
  final List<DataColumn2> columns;
  final List<DataRow2> data;
  final double columnSpacing;
  final double headingRowHeight;
  final double dividerThickness;
  final EdgeInsets padding;
  final ScrollController? scrollController;

  const CommonOrderTable({
    super.key,
    this.columnSpacing = 36,
    this.headingRowHeight = 60,
    this.dividerThickness = 0.35,
    this.padding = const EdgeInsets.all(16),
    required this.columns,
    required this.data,
    this.scrollController,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: padding,
      child: Stack(
        children: [
          DataTable2(
            scrollController: scrollController,
            columnSpacing: columnSpacing,
            dataRowHeight: context.width > Dimensions.lg ? 80 : 110,
            empty: Center(
              child: Text(
                AppStringKey.currently_no_data_found.tr,
                style: BaseStyle.textStyleRobotoSemiBold(
                  16,
                  AppColors.primaryTextColor,
                ),
              ),
            ),
            headingRowHeight: headingRowHeight,
            dividerThickness: dividerThickness,
            headingTextStyle: BaseStyle.textStyleRobotoSemiBold(
              14,
              AppColors.primaryTextColor,
            ),
            columns: columns,
            rows: data,
          ),
          Obx(
            () => Get.find<OrderController>().paginationHelper.isLoading.value
                ? Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const SizedBox(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator(
                            color: AppColors.primaryCtaColor,
                            strokeWidth: 3,
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        Text(
                          AppStringKey.pagination_loading.tr,
                          style: BaseStyle.textStyleRobotoMedium(
                            16,
                            AppColors.primaryCtaColor,
                          ),
                        )
                      ],
                    ),
                  )
                : const SizedBox.shrink(),
          ),
        ],
      ),
    );
  }
}
