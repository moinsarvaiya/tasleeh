import 'package:flutter/material.dart';

import '../core/constant/base_style.dart';
import '../custom_classes/custom_text.dart';

class HoverText extends StatefulWidget {
  final Color defaultColor;
  final Color hoverColor;
  final VoidCallback onTap;
  final String text;
  final TextAlign textAlign;

  const HoverText({
    required this.text,
    required this.defaultColor,
    required this.hoverColor,
    required this.onTap,
    required this.textAlign,
  });

  @override
  _HoverIconState createState() => _HoverIconState();
}

class _HoverIconState extends State<HoverText> {
  bool isHovered = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (_) => setState(() => isHovered = true),
      onExit: (_) => setState(() => isHovered = false),
      child: Material(
        child: Ink(
          child: InkWell(
            onTap: widget.onTap,
            child: CustomText(
              textAlign: widget.textAlign,
              text: widget.text,
              textStyle: BaseStyle.textStyleRobotoBold(
                8,
                isHovered ? widget.hoverColor : widget.defaultColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
