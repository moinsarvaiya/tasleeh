import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../core/app_preference/app_preferences.dart';
import '../core/app_preference/storage_keys.dart';
import '../core/constant/app_color.dart';
import '../core/constant/app_images.dart';
import '../core/constant/base_style.dart';
import '../core/constant/ui_constants.dart';
import '../core/extensions/common_extension.dart';
import '../core/routes/route_paths.dart';
import '../l10n/app_string_key.dart';
import '../models/comments_model.dart';
import 'custom_dialog.dart';

class SubmissionFailedModal extends StatelessWidget {
  List<CommentModel> reviews = [];

  SubmissionFailedModal({required this.reviews, super.key});

  @override
  Widget build(BuildContext context) {
    return context.isDesktop
        ? SubmissionFailedModalWebView(
            reviews: reviews,
          )
        : SubmissionFailedModalMobileView(
            reviews: reviews,
          );
  }
}

class SubmissionFailedModalWebView extends StatelessWidget {
  List<CommentModel> reviews = [];

  SubmissionFailedModalWebView({required this.reviews, super.key});

  @override
  Widget build(BuildContext context) {
    return CustomDialog(
      padding: const EdgeInsets.symmetric(vertical: 48, horizontal: 56),
      maxWidth: 720,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SvgPicture.asset(
            AppImages.failed,
            height: 80,
            width: 80,
          ),
          const SizedBox(
            height: 24,
          ),
          Text(
            AppStringKey.submission_failed.tr,
            textAlign: TextAlign.center,
            style: BaseStyle.textStyleRobotoMediumBold(24, AppColors.primaryTextColor),
          ),
          Text(
            textAlign: TextAlign.center,
            AppStringKey.submission_failed_description.tr,
            style: BaseStyle.textStyleRobotoMediumBold(14, AppColors.secondaryTextColor),
          ),
          const SizedBox(
            height: 24,
          ),
          (reviews.isNotEmpty)
              ? Container(
                  padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 40),
                  decoration: const BoxDecoration(
                    color: AppColors.secondaryBackgroundColor,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppStringKey.error_found_in_application.tr,
                        style: BaseStyle.textStyleRobotoExtraBold(
                          16,
                          AppColors.primaryTextColor,
                        ),
                      ),
                      const SizedBox(
                        height: 14,
                      ),
                      ...reviews.map(
                        (item) => Column(
                          children: [
                            Row(
                              children: [
                                SvgPicture.asset(
                                  AppImages.reject,
                                ),
                                const SizedBox(
                                  width: 8,
                                ),
                                Text(
                                  "${item.section!}: ",
                                  style: BaseStyle.textStyleRobotoMediumBold(14, AppColors.primaryTextColor),
                                ),
                                Text(
                                  item.comments!,
                                  style: BaseStyle.textStyleRobotoMediumBold(14, AppColors.primaryTextColor),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 14,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              : const SizedBox.shrink(),
          const SizedBox(
            height: 24,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Ink(
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    constraints: const BoxConstraints(minWidth: 150),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                      color: AppColors.secondaryCtaColor,
                    ),
                    padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 4),
                    child: Text(
                      AppStringKey.explore_izhal.tr,
                      style: BaseStyle.textStyleRobotoMediumBold(16, AppColors.inputFieldHeading),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              Ink(
                child: InkWell(
                  onTap: () {
                    AppPreferences.sharedPrefWrite(StorageKeys.addBranchesFrom, editBranch);
                    AppPreferences.sharedPrefWrite(StorageKeys.isAddBranchesFromAuth, false);
                    Get.toNamed(RoutePaths.CUSTOM_STEPPER);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    constraints: const BoxConstraints(minWidth: 150),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                      color: Colors.black,
                    ),
                    padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 4),
                    child: Text(
                      AppStringKey.update_now.tr,
                      style: BaseStyle.textStyleRobotoMediumBold(16, Colors.white),
                    ),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class SubmissionFailedModalMobileView extends StatelessWidget {
  List<CommentModel> reviews = [];

  SubmissionFailedModalMobileView({required this.reviews, super.key});

  @override
  Widget build(BuildContext context) {
    return CustomDialog(
      padding: const EdgeInsets.all(16),
      content: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          SvgPicture.asset(
            AppImages.failed,
          ),
          const SizedBox(
            height: 18,
          ),
          Text(
            AppStringKey.submission_failed.tr,
            textAlign: TextAlign.center,
            style: BaseStyle.textStyleRobotoMediumBold(20, AppColors.primaryTextColor),
          ),
          Text(
            textAlign: TextAlign.center,
            AppStringKey.submission_failed_description.tr,
            style: BaseStyle.textStyleRobotoMediumBold(12, AppColors.secondaryTextColor),
          ),
          const SizedBox(
            height: 18,
          ),
          (reviews.isNotEmpty)
              ? Container(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppStringKey.error_found_in_application.tr,
                        style: BaseStyle.textStyleRobotoExtraBold(14, AppColors.primaryTextColor),
                      ),
                      const SizedBox(
                        height: 14,
                      ),
                      ...reviews.map(
                        (item) => Column(children: [
                          Row(
                            children: [
                              SvgPicture.asset(
                                AppImages.reject,
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              Text(
                                "${item.section!}: ",
                                style: BaseStyle.textStyleRobotoMediumBold(12, AppColors.primaryTextColor),
                              ),
                              Flexible(
                                child: Text(
                                  item.comments!,
                                  style: BaseStyle.textStyleRobotoMediumBold(12, AppColors.primaryTextColor),
                                ),
                              )
                            ],
                          ),
                          const SizedBox(
                            height: 12,
                          ),
                        ]),
                      ),
                    ],
                  ),
                )
              : const SizedBox.shrink(),
          const SizedBox(
            height: 18,
          ),
          Column(
            children: [
              Ink(
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                      color: AppColors.secondaryCtaColor,
                    ),
                    padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 4),
                    child: Text(
                      AppStringKey.explore_izhal.tr,
                      style: BaseStyle.textStyleRobotoMediumBold(16, AppColors.inputFieldHeading),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 14,
              ),
              Ink(
                child: InkWell(
                  onTap: () {
                    AppPreferences.sharedPrefWrite(StorageKeys.addBranchesFrom, editBranch);
                    AppPreferences.sharedPrefWrite(StorageKeys.isAddBranchesFromAuth, false);
                    Get.toNamed(RoutePaths.CUSTOM_STEPPER);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                      color: Colors.black,
                    ),
                    padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 4),
                    child: Text(
                      AppStringKey.update_now.tr,
                      style: BaseStyle.textStyleRobotoMediumBold(16, Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
