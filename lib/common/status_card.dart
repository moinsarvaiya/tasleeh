import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';

import '../core/constant/app_color.dart';
import '../core/constant/base_style.dart';

class StatusCard extends StatelessWidget {
  final String title;
  final int description;
  final Widget cardIcon;
  final String? percentageChange;
  final bool? isPositive;
  final double? height;
  final double? width;
  final Color? cardBackgroundColor;

  const StatusCard(
      {required this.title,
      required this.description,
      required this.cardIcon,
      this.isPositive = true,
      this.percentageChange = '',
      this.height,
      this.width,
      this.cardBackgroundColor,
      super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? 360,
      padding: const EdgeInsets.all(20),
      constraints: BoxConstraints(minHeight: height ?? 112),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4.0),
        color: cardBackgroundColor ?? Colors.white,
      ),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  cardIcon,
                  const SizedBox(
                    width: 16,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        style: BaseStyle.textStyleRobotoMediumBold(
                          context.isDesktop ? 16 : 14,
                          AppColors.secondaryTextColor,
                        ),
                      ),
                      SizedBox(
                        height: context.isDesktop ? 9 : 4,
                      ),
                      Text(
                        description.toString(),
                        style: BaseStyle.textStyleRobotoMediumBold(
                          context.isDesktop ? 28 : 20,
                          AppColors.primaryTextColor,
                        ),
                      )
                    ],
                  )
                ],
              ),
              percentageChange != ''
                  ? Container(
                      padding: EdgeInsets.all(context.isDesktop ? 4 : 2),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4.0),
                          color: isPositive!
                              ? AppColors.greenBackgroundColor
                              : AppColors.redBackgroundColor),
                      child: Row(children: [
                        Icon(
                          isPositive!
                              ? Icons.arrow_drop_up_sharp
                              : Icons.arrow_drop_down_sharp,
                          size: context.isDesktop ? 24 : 18,
                          color: isPositive!
                              ? AppColors.incrementColor
                              : AppColors.errorTextColor,
                        ),
                        const SizedBox(
                          width: 4,
                        ),
                        Text(
                          "${isPositive! ? '+' : '-'}$percentageChange%",
                          style: BaseStyle.textStyleRobotoMediumBold(
                            context.isDesktop ? 16 : 12,
                            isPositive!
                                ? AppColors.incrementColor
                                : AppColors.errorTextColor,
                          ),
                        )
                      ]),
                    )
                  : const SizedBox()
            ],
          )
        ],
      ),
    );
  }
}
