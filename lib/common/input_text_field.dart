// ignore_for_file: must_be_immutable

import 'package:al_baida_garage_fe/core/constant/base_style.dart';
import 'package:al_baida_garage_fe/l10n/app_string_key.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../Utils/app_constants.dart';
import '../core/constant/app_color.dart';
import '../core/constant/app_images.dart';

class InputTextField extends StatelessWidget {
  String hintText;
  String label;
  bool isRequireField;
  bool? isLabelVisible;
  Widget? icon;
  Widget? prefixIcon;
  int? maxLength = 1000;
  TextEditingController textEditingController;
  bool? isPasswordField;
  String? errorText;
  TextInputAction? keyBoardAction;
  FocusNode? focusScope;
  TextInputType? textInputType;
  Function(String?)? onChange;
  Function()? onTap;
  bool isReadOnly;
  Color? readOnlyBackgroundColor;
  bool? validationRequired;
  bool isAbscureText = false;
  bool? isPhoneNo = false;
  int? maxLines = 1;
  int? minLines = 1;
  bool expands = false;
  TextStyle? hintStyle;
  Color? backgroundColor;
  List<TextInputFormatter>? inputFormatter = [];
  final String? Function(String?)? validationMessage;
  final bool Function(String?)? validationFunction;
  final Function(String?, bool)? onChangeWithValidationStatus;

  InputTextField({
    super.key,
    required this.hintText,
    required this.label,
    required this.isRequireField,
    this.isLabelVisible = true,
    this.icon,
    this.prefixIcon,
    required this.textEditingController,
    this.isPasswordField,
    this.errorText,
    this.keyBoardAction,
    this.focusScope,
    this.textInputType,
    this.onTap,
    this.onChange,
    this.maxLength,
    this.maxLines,
    this.minLines,
    this.isReadOnly = false,
    this.readOnlyBackgroundColor,
    this.validationRequired = false,
    this.isPhoneNo,
    this.inputFormatter,
    this.expands = false,
    this.backgroundColor = AppColors.colorWhite,
    this.hintStyle,
    this.validationMessage,
    this.validationFunction,
    this.onChangeWithValidationStatus,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
          visible: isLabelVisible ?? true,
          child: Wrap(
            children: [
              Text(
                label,
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                      color: AppColors.inputFieldHeading,
                    ),
              ),
              // Text(
              //   isRequireField ? "*" : "",
              //   style: const TextStyle(
              //     color: AppColors.errorTextColor,
              //     fontFamily: BaseFonts.roboto,
              //     fontSize: 14.0,
              //   ),
              // )
            ],
          ),
        ),
        const SizedBox(
          height: 6,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            isPhoneNo == true
                ? Directionality(
                    textDirection: TextDirection.ltr,
                    child: Container(
                      height: 48,
                      padding: const EdgeInsets.symmetric(
                          vertical: 8, horizontal: 11),
                      decoration: BoxDecoration(
                        color: const Color(0xFFEDEDED),
                        border: Border.all(
                          color:
                              AppColors.strokeColor, // Customize border color
                        ),
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(2),
                          bottomLeft: Radius.circular(2),
                        ),
                      ),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            AppImages.qatarFlag,
                            width: 20,
                            height: 15,
                          ),
                          const SizedBox(
                            width: 6,
                          ),
                          Text(
                            '+${AppConstants.countryCode}',
                            style: Theme.of(context).textTheme.bodyMedium,
                          )
                        ],
                      ),
                    ),
                  )
                : Container(),
            Expanded(
              child: TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                maxLines: maxLines,
                minLines: minLines,
                readOnly: isReadOnly,
                style: BaseStyle.textStyleRobotoRegular(
                  16,
                  isReadOnly
                      ? AppColors.tertiaryTextColor
                      : AppColors.primaryTextColor,
                ),
                decoration: InputDecoration(
                  filled: true,
                  fillColor:
                      isReadOnly ? readOnlyBackgroundColor : backgroundColor,
                  contentPadding: EdgeInsets.symmetric(
                    vertical: icon != null ? 15 : 8,
                    horizontal: 15,
                  ),
                  border: InputBorder.none,
                  counterText: "",
                  hintText: hintText,
                  hintStyle: hintStyle ??
                      BaseStyle.textStyleRobotoRegular(
                        16,
                        AppColors.inputFieldHintTextColor,
                      ),
                  // Add decoration to change the border color on error
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(2),
                    borderSide: const BorderSide(
                      color: AppColors.borderColor, // Default border color
                      width: 0,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(2),
                    borderSide: const BorderSide(
                      color: AppColors.borderColor, // Border color when focused
                      width: 0,
                    ),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(2),
                    borderSide: const BorderSide(
                      color: AppColors.errorTextColor, // Border color on error
                      width: 0,
                    ),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(2),
                    borderSide: const BorderSide(
                      color: AppColors
                          .errorTextColor, // Border color on error when focused
                      width: 0,
                    ),
                  ),
                  errorStyle: const TextStyle(
                    color: AppColors.errorTextColor, // Text color on error
                  ),
                  suffixIcon: icon,
                  prefixIcon: prefixIcon,
                ),
                keyboardType: textInputType,
                obscureText: isPasswordField ?? false,
                obscuringCharacter: "*",
                controller: textEditingController,
                textInputAction: keyBoardAction,
                focusNode: focusScope,
                maxLength: maxLength,
                onChanged: (value) {
                  String filteredValue = '';
                  bool isValid = false;

                  if (textInputType == TextInputType.text && value.isNotEmpty) {
                    filteredValue =
                        value.replaceAll(RegExp(r'[^a-zA-Z\s]'), '');
                    // if (filteredValue != value) {
                    textEditingController.value = TextEditingValue(
                      text: filteredValue,
                      selection:
                          TextSelection.collapsed(offset: filteredValue.length),
                    );
                    isValid = _validateInput(filteredValue);
                    // }
                  } else {
                    filteredValue = value;
                    isValid = _validateInput(value);
                  }

                  if (onChangeWithValidationStatus != null) {
                    onChangeWithValidationStatus!(filteredValue, isValid);
                  }

                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    (context as Element).markNeedsBuild();
                  });
                },
                onTap: onTap,
                inputFormatters: inputFormatter,
                expands: expands,
                validator: (value) {
                  if (isRequireField && focusScope!.hasFocus) {
                    if (value != '' && focusScope != null) {
                      if (validationFunction != null && focusScope!.hasFocus) {
                        return validationFunction!(value)
                            ? null
                            : (validationMessage != null
                                ? validationMessage!(value)
                                : AppStringKey.validation_invalid_input.tr);
                      }
                    } else {
                      return AppStringKey.validation_empty_field.tr;
                    }
                  } else if (validationRequired! && focusScope!.hasFocus) {
                    return validationFunction!(value)
                        ? null
                        : (validationMessage != null
                            ? validationMessage!(value)
                            : AppStringKey.validation_invalid_input.tr);
                  }
                  return null;
                },
              ),
            ),
          ],
        ),
      ],
    );
  }

  bool _validateInput(String? value) {
    if (validationFunction != null) {
      return validationFunction!(value);
    }
    return true;
  }
}
