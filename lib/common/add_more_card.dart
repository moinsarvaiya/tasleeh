import 'package:flutter/material.dart';

import '../core/constant/app_color.dart';
import '../core/constant/base_style.dart';

class AddCard extends StatelessWidget {
  final double? width;
  final double? height;
  final String content;
  final Color borderColor;
  final onTap;

  const AddCard({
    this.height,
    this.width,
    required this.content,
    this.borderColor = AppColors.borderColor,
    this.onTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Ink(
      child: InkWell(
        onTap: onTap,
        child: Container(
          width: width ?? 432,
          height: height ?? 252,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4.0),
            border: Border.all(
              width: 1,
              color: AppColors.borderColor,
            ),
            color: Colors.white,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.all(16),
 
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.circular(100),
                ),
                child: const Icon(
                  Icons.add,
                  color: Colors.white,
                  size: 32,
                ),
              ),
              const SizedBox(
                height: 16,
              ),
              Text(
                content,
                style: BaseStyle.textStyleRobotoSemiBold(
                  18,
                  AppColors.primaryTextColor,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
