import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../Pages/services/add_level_drawer_widget.dart';
import '../core/constant/app_color.dart';
import '../core/constant/ui_constants.dart';

class CustomDialog extends StatelessWidget {
  final Widget content;
  final EdgeInsets padding;
  final double? maxHeight;
  final double? maxWidth;

  const CustomDialog({
    Key? key,
    required this.content,
    required this.padding,
    this.maxWidth,
    this.maxHeight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: addLevelInService,
      endDrawer: const AddLevelDrawerWidget(),
      backgroundColor: Colors.transparent,
      body: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: Stack(
          children: [
            Container(
              padding: const EdgeInsets.all(16),
              constraints: BoxConstraints(
                maxWidth: maxWidth ?? double.maxFinite,
                maxHeight: maxHeight ?? double.maxFinite,
              ),
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.white,
                borderRadius: BorderRadius.circular(4),
              ),
              child: Padding(
                padding: padding,
                child: content,
              ),
            ),
            Positioned(
              right: 20,
              top: 20,
              child: Material(
                child: Ink(
                  child: InkWell(
                    onTap: () {
                      Get.back();
                    },
                    child: const Icon(
                      Icons.clear,
                      size: 22,
                      color: AppColors.colorBlack,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
