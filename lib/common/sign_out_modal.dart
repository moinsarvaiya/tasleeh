import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../core/auth/signout.dart';
import '../core/constant/app_color.dart';
import '../core/constant/app_images.dart';
import '../core/constant/base_style.dart';
import '../core/extensions/common_extension.dart';
import '../l10n/app_string_key.dart';

class SignOutModal extends StatelessWidget {
  const SignOutModal({super.key});

  @override
  Widget build(BuildContext context) {
    return context.isDesktop
        ? const SignOutModalWebView()
        : const SignOutModalMobileView();
  }
}

class SignOutModalWebView extends StatelessWidget {
  const SignOutModalWebView({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(
        minHeight: 450,
        maxWidth: 720,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          SvgPicture.asset(
            AppImages.warning,
          ),
          const SizedBox(
            height: 24,
          ),
          Text(
            AppStringKey.logout_warning.tr,
            textAlign: TextAlign.center,
            style: BaseStyle.textStyleRobotoMediumBold(
                24, AppColors.primaryTextColor),
          ),
          Text(
            textAlign: TextAlign.center,
            AppStringKey.logout_warning_description.tr,
            style: BaseStyle.textStyleRobotoMediumBold(
                14, AppColors.secondaryTextColor),
          ),
          const SizedBox(
            height: 24,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Ink(
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    constraints: const BoxConstraints(minWidth: 150),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                      color: AppColors.secondaryCtaColor,
                    ),
                    padding:
                        const EdgeInsets.symmetric(vertical: 15, horizontal: 4),
                    child: Text(
                      AppStringKey.cancel.tr,
                      style: BaseStyle.textStyleRobotoMediumBold(
                          16, AppColors.inputFieldHeading),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                width: 20,
              ),
              Ink(
                child: InkWell(
                  onTap: () {
                    CommonSignOut().signOut();
                  },
                  child: Container(
                    alignment: Alignment.center,
                    constraints: const BoxConstraints(minWidth: 150),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(2)),
                      color: Colors.black,
                    ),
                    padding:
                        const EdgeInsets.symmetric(vertical: 15, horizontal: 4),
                    child: Text(
                      AppStringKey.logoutButton.tr,
                      style:
                          BaseStyle.textStyleRobotoMediumBold(16, Colors.white),
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
    // );
  }
}

class SignOutModalMobileView extends StatelessWidget {
  const SignOutModalMobileView({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minHeight: 390),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            SvgPicture.asset(
              AppImages.warning,
            ),
            const SizedBox(
              height: 18,
            ),
            Text(
              AppStringKey.logout_warning.tr,
              textAlign: TextAlign.center,
              style: BaseStyle.textStyleRobotoMediumBold(
                  20, AppColors.primaryTextColor),
            ),
            Text(
              textAlign: TextAlign.center,
              AppStringKey.logout_warning_description.tr,
              style: BaseStyle.textStyleRobotoMediumBold(
                  12, AppColors.secondaryTextColor),
            ),
            const SizedBox(
              height: 18,
            ),
            Column(
              children: [
                Ink(
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(2)),
                        color: AppColors.secondaryCtaColor,
                      ),
                      padding: const EdgeInsets.symmetric(
                          vertical: 15, horizontal: 4),
                      child: Text(
                        AppStringKey.cancel.tr,
                        style: BaseStyle.textStyleRobotoMediumBold(
                            16, AppColors.inputFieldHeading),
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 14,
                ),
                Ink(
                  child: InkWell(
                    onTap: () {
                      CommonSignOut().signOut();
                    },
                    child: Container(
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(2)),
                        color: Colors.black,
                      ),
                      padding: const EdgeInsets.symmetric(
                          vertical: 15, horizontal: 4),
                      child: Text(
                        AppStringKey.logoutButton.tr,
                        style: BaseStyle.textStyleRobotoMediumBold(
                            16, Colors.white),
                      ),
                    ),
                  ),
                )
              ],
            )
          ]),
    );
  }
}
