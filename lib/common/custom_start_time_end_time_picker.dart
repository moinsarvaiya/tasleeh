// ignore_for_file: must_be_immutable

import 'package:al_baida_garage_fe/Pages/basic_details/basic_details_controller.dart';
import 'package:al_baida_garage_fe/core/constant/base_style.dart';
import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../Pages/custom_stepper/widgets/custom_square_corner_button.dart';
import '../core/constant/app_color.dart';
import '../core/constant/app_images.dart';
import '../l10n/app_string_key.dart';

class CustomStartTimeEndTimePicker extends StatelessWidget {
  String hintText;
  String label;
  bool isRequireField;
  bool? isLabelVisible;
  TextEditingController textEditingController;
  Function(String?)? onChange;
  bool isReadOnly;

  CustomStartTimeEndTimePicker({
    super.key,
    required this.hintText,
    required this.label,
    required this.isRequireField,
    this.isLabelVisible = true,
    required this.textEditingController,
    this.onChange,
    this.isReadOnly = false,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
          visible: isLabelVisible ?? true,
          child: Wrap(
            children: [
              Text(
                label,
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                      color: AppColors.inputFieldHeading,
                    ),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 6,
        ),
        Container(
          decoration: BoxDecoration(
            color: isReadOnly ? AppColors.tertiaryBackgroundColor : AppColors.colorWhite,
          ),
          child: Row(
            children: [
              Expanded(
                child: Container(
                  height: 48,
                  padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    border: Border.all(color: AppColors.borderColor, width: 0),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Material(
                          child: Ink(
                            color: isReadOnly ? AppColors.tertiaryBackgroundColor : AppColors.colorWhite,
                            child: InkWell(
                              onTap: isReadOnly
                                  ? null
                                  : () {
                                      showDialog(
                                        context: context,
                                        builder: (_) => const CustomTimePickerDialog(fromWhere: 'start'),
                                      );
                                    },
                              child: Obx(
                                () => Text(
                                  Get.find<BasicDetailsController>().startTime.value,
                                  style: BaseStyle.textStyleRobotoRegular(
                                    16,
                                    Get.find<BasicDetailsController>().startTime.value != AppStringKey.start_operation_time.tr
                                        ? isReadOnly
                                            ? AppColors.tertiaryTextColor
                                            : AppColors.primaryCtaColor
                                        : AppColors.inputFieldHintTextColor,
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SvgPicture.asset(
                        AppImages.forwardArrow,
                        width: 8,
                        height: 8,
                      ),
                      Expanded(
                        child: Material(
                          child: Ink(
                            color: isReadOnly ? AppColors.tertiaryBackgroundColor : AppColors.colorWhite,
                            child: InkWell(
                              onTap: isReadOnly
                                  ? null
                                  : () {
                                      showDialog(
                                        context: context,
                                        builder: (_) => const CustomTimePickerDialog(fromWhere: 'end'),
                                      );
                                    },
                              child: Obx(
                                () => Text(
                                  Get.find<BasicDetailsController>().endTime.value,
                                  style: BaseStyle.textStyleRobotoRegular(
                                    16,
                                    Get.find<BasicDetailsController>().startTime.value != AppStringKey.start_operation_time.tr
                                        ? isReadOnly
                                            ? AppColors.tertiaryTextColor
                                            : AppColors.primaryCtaColor
                                        : AppColors.inputFieldHintTextColor,
                                  ),
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SvgPicture.asset(
                        AppImages.clock,
                        width: 15,
                        height: 15,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class CustomTimePickerDialog extends StatelessWidget {
  final String fromWhere;

  const CustomTimePickerDialog({super.key, required this.fromWhere});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.05,
        height: MediaQuery.of(context).size.height * 0.6,
        color: Colors.white,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Row(
                children: [
                  // Hours picker
                  Expanded(
                    child: Stack(
                      children: [
                        Positioned(
                          child: Center(
                            child: Container(
                              color: Colors.grey.shade300,
                              width: double.maxFinite,
                              height: 35,
                            ),
                          ),
                        ),
                        ListWheelScrollView(
                          physics: const FixedExtentScrollPhysics(),
                          itemExtent: 50,
                          children: List.generate(
                            24,
                            (index) => Obx(
                              () => Center(
                                child: Text(
                                  index.toString().padLeft(2, '0'),
                                  style: TextStyle(
                                    fontSize: Get.find<BasicDetailsController>().hoursCenterTime.value == index ? 27 : 20,
                                    fontWeight:
                                        Get.find<BasicDetailsController>().hoursCenterTime.value == index ? FontWeight.bold : FontWeight.normal,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          onSelectedItemChanged: (value) {
                            Get.find<BasicDetailsController>().hoursCenterTime.value = value;
                          },
                        ),
                      ],
                    ),
                  ),
                  // Center stick indicator
                  Container(
                    width: 1,
                    color: Colors.grey.shade500,
                  ),
                  // Minutes picker
                  Expanded(
                    child: Stack(
                      children: [
                        Positioned(
                          child: Center(
                            child: Container(
                              color: Colors.grey.shade300,
                              width: double.maxFinite,
                              height: 35,
                            ),
                          ),
                        ),
                        ListWheelScrollView(
                          physics: const FixedExtentScrollPhysics(),
                          itemExtent: 50,
                          children: List.generate(
                            60,
                            (index) => Obx(
                              () => Center(
                                child: Text(
                                  index.toString().padLeft(2, '0'),
                                  style: TextStyle(
                                    fontSize: Get.find<BasicDetailsController>().minuteCenterTime.value == index ? 27 : 20,
                                    fontWeight:
                                        Get.find<BasicDetailsController>().minuteCenterTime.value == index ? FontWeight.bold : FontWeight.normal,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          onSelectedItemChanged: (value) {
                            Get.find<BasicDetailsController>().minuteCenterTime.value = value;
                          },
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                CustomSquareCornerButton(
                  buttonText: 'Set',
                  buttonTextColor: AppColors.colorWhite,
                  backgroundColor: AppColors.colorBlack,
                  height: context.isDesktop ? 0.025 : 0.06,
                  width: context.isDesktop ? 0.07 : 0.3,
                  textSize: context.isDesktop ? 8 : 10,
                  isResponsive: false,
                  callBack: () {
                    if (fromWhere == 'start') {
                      Get.find<BasicDetailsController>().startTime.value =
                          '${Get.find<BasicDetailsController>().hoursCenterTime.value.toString().padLeft(2, '0')}:${Get.find<BasicDetailsController>().minuteCenterTime.value.toString().padLeft(2, '0')}';
                    } else {
                      Get.find<BasicDetailsController>().endTime.value =
                          '${Get.find<BasicDetailsController>().hoursCenterTime.value.toString().padLeft(2, '0')}:${Get.find<BasicDetailsController>().minuteCenterTime.value.toString().padLeft(2, '0')}';
                    }
                    Get.back();
                    Get.find<BasicDetailsController>().submitFieldData();
                  },
                )
              ],
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
