import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../core/constant/app_color.dart';

class LoadingDialog {
  static bool isLoading = false;

  static void fullScreenLoader() {
    if (!isLoading) {
      isLoading = true;
      Get.dialog(
        const Center(
          child: CircularProgressIndicator(), // Customize this based on your requirements
        ),
        barrierDismissible: false,
        barrierColor: Colors.white.withOpacity(0.6),
      );
    }
  }

  static void closeFullScreenDialog() {
    if (isLoading) {
      isLoading = false;
      Get.back();
    }
  }

  static Widget indicator() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: AppColors.errorTextColor.withOpacity(0.3),
      child: const Center(
        child: CircularProgressIndicator(
          color: AppColors.errorTextColor,
        ),
      ),
    );
  }
}
