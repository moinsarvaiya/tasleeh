import 'package:al_baida_garage_fe/Utils/password_helper.dart';
import 'package:al_baida_garage_fe/l10n/app_string_key.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../core/constant/app_color.dart';

class PasswordStrengthIndicator extends StatefulWidget {
  final String password;

  const PasswordStrengthIndicator({Key? key, required this.password})
      : super(key: key);

  @override
  _PasswordStrengthIndicatorState createState() =>
      _PasswordStrengthIndicatorState();
}

class _PasswordStrengthIndicatorState extends State<PasswordStrengthIndicator> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        PasswordStrengthCheck(
          isChecked: PasswordStrengthChecks.hasMinimumLength(widget.password),
          label: AppStringKey.password_min_length_label.tr,
        ),
        PasswordStrengthCheck(
          isChecked: PasswordStrengthChecks.hasUppercase(widget.password),
          label: AppStringKey.password_uppercase_req_label.tr,
        ),
        PasswordStrengthCheck(
          isChecked: PasswordStrengthChecks.hasLowercase(widget.password),
          label: AppStringKey.password_lowercase_req_label.tr,
        ),
        PasswordStrengthCheck(
          isChecked:
              PasswordStrengthChecks.hasSpecialCharacter(widget.password),
          label: AppStringKey.password_special_char_req_label.tr,
        ),
      ],
    );
  }
}

class PasswordStrengthCheck extends StatelessWidget {
  final bool isChecked;
  final String label;

  const PasswordStrengthCheck(
      {super.key, required this.isChecked, required this.label});

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        isChecked
            ? const Icon(
                Icons.check,
                size: 20,
                color: AppColors.successTextGreenColor,
              )
            : const Icon(
                Icons.circle_rounded,
                size: 5,
              ),
        Text(
          "\t$label",
          style: Theme.of(context).textTheme.bodyMedium,
        ),
      ],
    );
  }
}
