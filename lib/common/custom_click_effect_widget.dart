import 'package:flutter/material.dart';

class CustomClickEffectWidget extends StatelessWidget {
  final Widget widget;
  final double borderRadius;
  final Function() onTap;

  const CustomClickEffectWidget({super.key, required this.widget, required this.borderRadius, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Ink(
        child: InkWell(
          onTap: onTap,
          customBorder: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius),
          ),
          child: widget,
        ),
      ),
    );
  }
}
