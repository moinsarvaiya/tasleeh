import 'dart:async';
import 'dart:typed_data';
import 'package:get/get.dart';
import 'package:al_baida_garage_fe/Utils/app_constants.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

import '../l10n/app_string_key.dart';

class CommonFunction {
  static Future<Map<String, Uint8List>> pickImages(int imageLength) async {
    Map<String, Uint8List> fileData = {};
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf', 'jpg', 'jpeg', 'png'],
      allowMultiple: true,
    );
    if (result != null) {
      List<PlatformFile> files = result.files;
      for (var file in files) {
        String fileName = file.name.toLowerCase();
        Uint8List fileBytes = file.bytes!;
        int fileSize = fileBytes.length;
        if (fileName.endsWith('.pdf') ||
            fileName.endsWith('.jpg') ||
            fileName.endsWith('.jpeg') ||
            fileName.endsWith('.png')) {
          if (fileSize <= AppConstants.fileMaxSize) {
            fileData[fileName] = fileBytes;
          }else{
            showCustomSnackBar(context: Get.context!, message: AppStringKey.file_size_exceeds.tr);
          }
        } else {}
      }
    } else {}
    return fileData;
  }

 static void showCustomSnackBar({
    required BuildContext context,
    required String message,
    Duration duration = const Duration(seconds: 3),
    String? actionLabel,
    VoidCallback? onPressed,
    Color? backgroundColor,
    Color? textColor,
    Color? disabledTextColor,
  }) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: duration,
        action: actionLabel != null
            ? SnackBarAction(
          label: actionLabel,
          onPressed: onPressed ??
                  () {
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
              },
          textColor: textColor ?? Colors.black,
          disabledTextColor: disabledTextColor ?? Colors.grey,
          backgroundColor: backgroundColor ?? Colors.white,
        )
            : null,
      ),
    );
  }
}
