import 'package:al_baida_garage_fe/common/space_horizontal.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../Pages/custom_stepper/widgets/custom_square_corner_button.dart';
import '../core/constant/app_color.dart';
import '../core/constant/base_style.dart';
import '../l10n/app_string_key.dart';

class CommonOrderCard extends StatelessWidget {
  final String? topLeftHeading;
  final String? topLeftSubHeading;
  final String? topRightHeading;
  final String? topRightSubHeading;
  final String? centerLeftHeading;
  final String? centerLeftContent;
  final Icon? centerLeftIcon;
  final String? centerRightHeading;
  final String? centerRightContent;
  final Icon? centerRightIcon;
  final bool showBottomContent;
  final String? bottomHeading;
  final String? bottomContent;
  final SvgPicture? bottomIcon;
  final double? bottomPadding;

  const CommonOrderCard({
    super.key,
    this.topLeftHeading,
    this.topLeftSubHeading,
    this.topRightHeading,
    this.topRightSubHeading,
    this.centerLeftHeading,
    this.centerLeftIcon,
    this.centerLeftContent,
    this.centerRightHeading,
    this.centerRightIcon,
    this.centerRightContent,
    this.showBottomContent = false,
    this.bottomHeading,
    this.bottomContent,
    this.bottomIcon,
    this.bottomPadding = 8,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin:   EdgeInsets.only(bottom: bottomPadding ?? 8),
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: AppColors.strokeColor,
        ),
        borderRadius: BorderRadius.circular(4),
        color: AppColors.colorWhite,
      ),
      child: Column(
        children: [
          Container(
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(color: AppColors.strokeColor),
              ),
            ),
            padding: const EdgeInsets.fromLTRB(16, 10, 16, 12),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      topLeftHeading!,
                      style: BaseStyle.textStyleRobotoBold(
                        14,
                        AppColors.primaryTextColor,
                      ),
                    ),
                    Text(
                      topLeftSubHeading!,
                      style: BaseStyle.textStyleRobotoBold(
                        10,
                        AppColors.secondaryTextColor,
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      topRightHeading!,
                      style: BaseStyle.textStyleRobotoBold(
                        14,
                        AppColors.primaryTextColor,
                      ),
                    ),
                    Text(
                      topRightSubHeading!,
                      style: BaseStyle.textStyleRobotoBold(
                        10,
                        AppColors.secondaryTextColor,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  decoration: const BoxDecoration(
                    border: Border(
                      right: BorderSide(color: AppColors.strokeColor),
                    ),
                  ),
                  padding: const EdgeInsets.symmetric(
                    vertical: 12,
                    horizontal: 16,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          centerLeftIcon!,
                          const SizedBox(
                            width: 4,
                          ),
                          Text(
                            centerLeftHeading!,
                            style: BaseStyle.textStyleRobotoRegular(
                              12,
                              AppColors.secondaryTextColor,
                            ),
                          ),
                        ],
                      ),
                      Text(
                        centerLeftContent!,
                        style: BaseStyle.textStyleRobotoRegular(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(
                  padding: const EdgeInsets.symmetric(
                    vertical: 12,
                    horizontal: 16,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          centerLeftIcon!,
                          const SizedBox(
                            width: 4,
                          ),
                          Text(
                            centerRightHeading!,
                            style: BaseStyle.textStyleRobotoRegular(
                              12,
                              AppColors.secondaryTextColor,
                            ),
                          ),
                        ],
                      ),
                      Text(
                        centerRightContent!,
                        style: BaseStyle.textStyleRobotoRegular(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
          showBottomContent
              ? Container(
                  decoration: const BoxDecoration(
                    border: Border(
                      top: BorderSide(color: AppColors.strokeColor),
                    ),
                  ),
                  padding: const EdgeInsets.symmetric(
                    vertical: 12,
                    horizontal: 16,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          bottomIcon!,
                          const SizedBox(
                            width: 4,
                          ),
                          Text(
                            bottomHeading!,
                            style: BaseStyle.textStyleRobotoRegular(
                              12,
                              AppColors.secondaryTextColor,
                            ),
                          ),
                        ],
                      ),
                      Text(
                        bottomContent!,
                        style: BaseStyle.textStyleRobotoRegular(
                          14,
                          AppColors.primaryTextColor,
                        ),
                      ),
                    ],
                  ),
                )
              : const SizedBox.shrink()
        ],
      ),
    );
  }
}
