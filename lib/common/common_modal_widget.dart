import 'package:al_baida_garage_fe/common/custom_dialog.dart';
import 'package:al_baida_garage_fe/core/constant/app_color.dart';
import 'package:al_baida_garage_fe/core/constant/base_style.dart';
import 'package:al_baida_garage_fe/core/extensions/common_extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CommonModalWidget extends StatelessWidget {
  final dynamic modalIcon;
  final String title;
  final String description;
  final String firstButtonContent;
  final VoidCallback firstButtonOnTap;
  final Color? firstButtonTextColor;
  final Color? firstButtonBackgroundColor;
  final String? secondButtonContent;
  final VoidCallback? secondButtonOnTap;
  final Color? secondButtonBackgroundColor;
  final Color? secondButtonTextColor;

  const CommonModalWidget(
      {required this.modalIcon,
      required this.title,
      required this.description,
      required this.firstButtonContent,
      required this.firstButtonOnTap,
      this.secondButtonContent,
      this.secondButtonOnTap,
      this.firstButtonBackgroundColor,
      this.firstButtonTextColor,
      this.secondButtonBackgroundColor,
      this.secondButtonTextColor,
      super.key});

  @override
  Widget build(BuildContext context) {
    return context.isDesktop
        ? CommonModalWidgetWebView(
            modalIcon: modalIcon,
            title: title,
            description: description,
            firstButtonContent: firstButtonContent,
            firstButtonOnTap: firstButtonOnTap,
            secondButtonContent: secondButtonContent,
            secondButtonOnTap: secondButtonOnTap,
            firstButtonTextColor: firstButtonTextColor,
            secondButtonBackgroundColor: secondButtonBackgroundColor,
            firstButtonBackgroundColor: firstButtonBackgroundColor,
            secondButtonTextColor: secondButtonTextColor,
          )
        : CommonModalWidgetMobileView(
            modalIcon: modalIcon,
            title: title,
            description: description,
            firstButtonContent: firstButtonContent,
            firstButtonOnTap: firstButtonOnTap,
            secondButtonContent: secondButtonContent,
            secondButtonOnTap: secondButtonOnTap,
            firstButtonTextColor: firstButtonTextColor,
            secondButtonBackgroundColor: secondButtonBackgroundColor,
            firstButtonBackgroundColor: firstButtonBackgroundColor,
            secondButtonTextColor: secondButtonTextColor,
          );
  }
}

class CommonModalWidgetWebView extends StatelessWidget {
  final dynamic modalIcon;
  final String title;
  final String description;
  final String firstButtonContent;
  final VoidCallback firstButtonOnTap;
  final Color? firstButtonTextColor;
  final Color? firstButtonBackgroundColor;
  final String? secondButtonContent;
  final VoidCallback? secondButtonOnTap;
  final Color? secondButtonBackgroundColor;
  final Color? secondButtonTextColor;

  const CommonModalWidgetWebView(
      {required this.modalIcon,
      required this.title,
      required this.description,
      required this.firstButtonContent,
      required this.firstButtonOnTap,
      this.secondButtonContent,
      this.secondButtonOnTap,
      this.firstButtonBackgroundColor,
      this.firstButtonTextColor,
      this.secondButtonBackgroundColor,
      this.secondButtonTextColor,
      super.key});

  @override
  Widget build(BuildContext context) {
    return CustomDialog(
      padding: const EdgeInsets.all(16),
      content: Container(
        constraints: const BoxConstraints(minHeight: 444, minWidth: 720),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            modalIcon.toString().contains('.png')
                ? Image.asset(
                    modalIcon,
                    height: 100,
                    width: 100,
                  )
                : SvgPicture.asset(
                    modalIcon,
                  ),
            const SizedBox(
              height: 48,
            ),
            Text(
              title,
              textAlign: TextAlign.center,
              style: BaseStyle.textStyleRobotoMediumBold(24, AppColors.primaryTextColor),
            ),
            const SizedBox(
              height: 8,
            ),
            Container(
              constraints: const BoxConstraints(maxWidth: 550),
              child: Text(
                textAlign: TextAlign.center,
                description,
                style: BaseStyle.textStyleRobotoMediumBold(14, AppColors.secondaryTextColor),
              ),
            ),
            const SizedBox(
              height: 48,
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Ink(
                  child: InkWell(
                    onTap: firstButtonOnTap,
                    child: Container(
                      constraints: const BoxConstraints(minWidth: 150, minHeight: 40),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(Radius.circular(2)),
                        color: firstButtonBackgroundColor ?? Colors.black,
                      ),
                      padding: const EdgeInsets.symmetric(vertical: 7, horizontal: 15),
                      child: Text(
                        firstButtonContent,
                        style: BaseStyle.textStyleRobotoMediumBold(16, firstButtonTextColor ?? Colors.white),
                      ),
                    ),
                  ),
                ),
                Visibility(
                  visible: secondButtonContent != null,
                  child: Ink(
                    child: InkWell(
                      onTap: secondButtonOnTap,
                      child: Container(
                        margin: const EdgeInsets.only(left: 20),
                        constraints: const BoxConstraints(minWidth: 150, minHeight: 40),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          borderRadius: const BorderRadius.all(Radius.circular(2)),
                          color: secondButtonBackgroundColor ?? Colors.black,
                        ),
                        padding: const EdgeInsets.symmetric(vertical: 7, horizontal: 15),
                        child: Text(
                          secondButtonContent ?? '',
                          style: BaseStyle.textStyleRobotoMediumBold(16, secondButtonTextColor ?? Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class CommonModalWidgetMobileView extends StatelessWidget {
  final dynamic modalIcon;
  final String title;
  final String description;
  final String firstButtonContent;
  final VoidCallback firstButtonOnTap;
  final Color? firstButtonTextColor;
  final Color? firstButtonBackgroundColor;
  final String? secondButtonContent;
  final VoidCallback? secondButtonOnTap;
  final Color? secondButtonBackgroundColor;
  final Color? secondButtonTextColor;

  const CommonModalWidgetMobileView(
      {required this.modalIcon,
      required this.title,
      required this.description,
      required this.firstButtonContent,
      required this.firstButtonOnTap,
      this.secondButtonContent,
      this.secondButtonOnTap,
      this.firstButtonBackgroundColor,
      this.firstButtonTextColor,
      this.secondButtonBackgroundColor,
      this.secondButtonTextColor,
      super.key});

  @override
  Widget build(BuildContext context) {
    return CustomDialog(
      padding: const EdgeInsets.all(16),
      content: Container(
        constraints: const BoxConstraints(minHeight: 400),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            modalIcon.toString().contains('.png')
                ? Image.asset(
                    modalIcon,
                    height: 70,
                    width: 70,
                  )
                : SvgPicture.asset(
                    modalIcon,
                  ),
            const SizedBox(
              height: 24,
            ),
            Text(
              title,
              textAlign: TextAlign.center,
              style: BaseStyle.textStyleRobotoMediumBold(20, AppColors.primaryTextColor),
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              textAlign: TextAlign.center,
              description,
              style: BaseStyle.textStyleRobotoMediumBold(12, AppColors.secondaryTextColor),
            ),
            const SizedBox(
              height: 24,
            ),
            Ink(
              child: InkWell(
                onTap: firstButtonOnTap,
                child: Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(Radius.circular(2)),
                    color: firstButtonBackgroundColor ?? Colors.black,
                  ),
                  padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 4),
                  child: Text(
                    firstButtonContent,
                    style: BaseStyle.textStyleRobotoMediumBold(16, firstButtonTextColor ?? Colors.white),
                  ),
                ),
              ),
            ),
            Visibility(
              visible: secondButtonContent != null,
              child: Ink(
                child: InkWell(
                  onTap: secondButtonOnTap,
                  child: Container(
                    margin: const EdgeInsets.only(top: 14),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(2)),
                      color: secondButtonBackgroundColor ?? Colors.black,
                    ),
                    padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 4),
                    child: Text(
                      secondButtonContent ?? '',
                      style: BaseStyle.textStyleRobotoMediumBold(16, secondButtonTextColor ?? Colors.white),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
