import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

import '../core/constant/app_color.dart';
import '../core/constant/base_style.dart';
import '../core/extensions/common_extension.dart';
import '../l10n/app_string_key.dart';

class DatePicker extends StatefulWidget {
  final Function(String date) dateSetter;
  final bool isReadOnly;
  final TextEditingController? textEditingController;

  const DatePicker({
    super.key,
    this.textEditingController,
    required this.dateSetter,
    this.isReadOnly = false,
  });

  @override
  State<DatePicker> createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  DateTime selectedDate = DateTime.now();
  bool isCalenderVisible = false;

  DateRangePickerController selectedDateController =
      DateRangePickerController();

  void _onSelectionChanged(DateRangePickerSelectionChangedArgs args) {
    setState(
      () {
        widget.textEditingController?.text =
            selectedDateController.selectedDate.toString().split(' ')[0];
        isCalenderVisible = false;
        widget.dateSetter(
          selectedDateController.selectedDate.toString().split(' ')[0],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          onTap: widget.isReadOnly
              ? null
              : () {
                  setState(() {
                    isCalenderVisible = !isCalenderVisible;
                  });
                },
          controller: widget.textEditingController,
          readOnly: true,
          decoration: InputDecoration(
            filled: true,
            fillColor: widget.isReadOnly
                ? AppColors.tertiaryBackgroundColor
                : AppColors.colorWhite,
            suffixIcon: const Icon(
              Icons.calendar_today_outlined,
              size: 12.5,
              color: AppColors.tertiaryTextColor,
            ),
            contentPadding: const EdgeInsets.symmetric(
              vertical: 16,
              horizontal: 12,
            ),
            hintText: AppStringKey.select_date.tr,
            hintStyle: BaseStyle.textStyleRobotoRegular(
              16,
              AppColors.inputFieldHintTextColor,
            ),
            focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.elliptical(2, 2),
              ),
              borderSide: BorderSide(
                color: AppColors.strokeColor,
              ),
            ),
            enabledBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.elliptical(2, 2),
              ),
              borderSide: BorderSide(
                color: AppColors.strokeColor,
              ),
            ),
            isDense: true,
          ),
        ),
        Visibility(
          visible: isCalenderVisible,
          child: Container(
            margin: const EdgeInsets.only(top: 8),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(2),
              border: Border.all(
                color: AppColors.calenderBorderColor,
              ),
            ),
            child: Column(
              children: [
                SfDateRangePicker(
                  controller: selectedDateController,
                  onSelectionChanged: _onSelectionChanged,
                  todayHighlightColor: AppColors.primaryTextColor,
                  showNavigationArrow: true,
                  initialDisplayDate: selectedDate,
                  view: DateRangePickerView.month,
                  initialSelectedDate: selectedDate,
                  maxDate: DateTime.now(),
                  selectionShape: DateRangePickerSelectionShape.rectangle,                  
                  monthViewSettings: DateRangePickerMonthViewSettings(
                    firstDayOfWeek: 1,
                    dayFormat: context.isTabletView ? 'EE' : 'EEE',
                    showTrailingAndLeadingDates: true,
                  ),
                  yearCellStyle: DateRangePickerYearCellStyle(
                    todayTextStyle: BaseStyle.textStyleRobotoRegular(
                      14,
                      AppColors.primaryTextColor,
                    ),
                  ),
                  monthFormat: 'MMM',
                  selectionColor: AppColors.primaryTextColor,
                  headerHeight: 40,
                  headerStyle: const DateRangePickerHeaderStyle(
                    textAlign: TextAlign.center,
                    textStyle: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: AppColors.primaryTextColor,
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: AppColors.tertiaryBackgroundColor,
                    ),
                  ),
                  child: TextButton(
                    onPressed: () {
                      setState(() {
                        selectedDateController.selectedDate = DateTime.now();
                        widget.textEditingController?.text =
                            selectedDateController.selectedDate
                                .toString()
                                .split(' ')[0];
                        isCalenderVisible = false;
                        widget.dateSetter(
                          selectedDateController.selectedDate
                              .toString()
                              .split(' ')[0],
                        );
                      });
                    },
                    style: TextButton.styleFrom(
                      shape: const BeveledRectangleBorder(),
                      backgroundColor: Colors.white,
                      foregroundColor: AppColors.primaryTextColor,
                      textStyle: BaseStyle.textStyleRobotoSemiBold(
                        14,
                        AppColors.primaryTextColor,
                      ),
                    ),
                    child: Text(AppStringKey.today.tr),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
